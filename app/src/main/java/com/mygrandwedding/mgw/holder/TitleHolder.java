package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;

/**
 * Created by gladwinbobby on 18/09/17
 */

public class TitleHolder extends RecyclerView.ViewHolder {

    public TextView textViewTitle;

    public TitleHolder(View itemView) {
        super(itemView);
        textViewTitle = itemView.findViewById(R.id.txt_title);
    }
}
