package com.mygrandwedding.mgw;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mygrandwedding.mgw.adapter.CityAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.CityCallback;
import com.mygrandwedding.mgw.model.Location;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.task.AddLocationListTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.apptik.widget.MultiSlider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

public class VendorFilterActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, CityCallback, MultiSlider.OnThumbValueChangeListener, View.OnClickListener {

    private Context context;
    private Toolbar toolbar;
    private RelativeLayout layoutFilter;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewLocation;
    private MultiSlider slider;
    private TextView textViewPrice;
    private RatingBar ratingBar;
    private Button buttonApplyFilter;
    private List<Location> locationList;
    private CityAdapter cityAdapter;
    private MyPreference preference;
    private int filterPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_filter);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
        loadLocations();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reset, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_reset:
                preference.resetFilter();
                setResult(RESULT_OK);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getLocations();
    }

    @Override
    public void onCityClick(int position) {
        Location location = locationList.get(position);
        for (Location loc : locationList) {
            loc.setSelected(loc.getId() == location.getId());
        }
        cityAdapter.notifyDataSetChanged();
    }

    @Override
    public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
        setSliderText();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonApplyFilter) {
            for (Location location : locationList) {
                if (location.isSelected()) preference.setCityId(location.getId());
            }
            preference.setMinPrice(slider.getThumb(0).getValue());
            preference.setMaxPrice(slider.getThumb(1).getValue());
            preference.setRating(ratingBar.getRating());
            setResult(RESULT_OK);
            finish();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutFilter = findViewById(R.id.filter);
        refreshLayout = findViewById(R.id.refresh);
        viewLocation = findViewById(R.id.location);
        slider = findViewById(R.id.slider);
        textViewPrice = findViewById(R.id.txt_price);
        ratingBar = findViewById(R.id.rating);
        buttonApplyFilter = findViewById(R.id.btn_apply_filter);

        context = this;
        locationList = new ArrayList<>();
        cityAdapter = new CityAdapter(locationList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        slider.setOnThumbValueChangeListener(this);
        buttonApplyFilter.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewLocation.setLayoutManager(new GridLayoutManager(context, 2));
        viewLocation.setAdapter(cityAdapter);
        viewLocation.setNestedScrollingEnabled(false);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
    }

    private void loadLocations() {
        LiveData<List<Location>> data = getAppDatabase(context).generalDao().getLocations();
        data.observe(this, new Observer<List<Location>>() {
            @Override
            public void onChanged(@Nullable List<Location> locations) {
                locationList.clear();
                if (locations != null && !locations.isEmpty()) {
                    layoutFilter.setVisibility(View.VISIBLE);
                    locationList.add(new Location(0, true, "All Cities"));
                    locationList.addAll(locations);
                    cityAdapter.notifyDataSetChanged();
                    setFilterData();
                } else {
                    refreshLayout.setRefreshing(true);
                    getLocations();
                }
                cityAdapter.notifyDataSetChanged();
            }
        });
    }

    private void setFilterData() {
        for (Location location : locationList) {
            location.setSelected(location.getId() == preference.getCityId());
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            VendorCategory category = bundle.getParcelable(EXTRA_VENDOR);
            if (category != null) filterPrice = category.getFilterPrice();
        }
        slider.getThumb(0).setValue(preference.getMinPrice());
        slider.getThumb(1).setValue(preference.getMaxPrice());
        if (preference.getRating() != -1) ratingBar.setRating(preference.getRating());
    }

    private void setSliderText() {
        int minPrice = slider.getThumb(0).getValue();
        int maxPrice = slider.getThumb(1).getValue();
        textViewPrice.setText(String.format(Locale.getDefault(), getString(R.string.format_price),
                String.valueOf(minPrice * filterPrice), String.valueOf(maxPrice * filterPrice)));
    }

    private void getLocations() {
        Call<List<Location>> call = getApiService().getLocations(preference.getUserToken());
        call.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(@NonNull Call<List<Location>> call,
                                   @NonNull Response<List<Location>> response) {
                refreshLayout.setRefreshing(false);
                List<Location> locationListResponse = response.body();
                if (response.isSuccessful() && locationListResponse != null) {
                    refreshLayout.setEnabled(false);
                    new AddLocationListTask(context, locationListResponse).execute();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Location>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
