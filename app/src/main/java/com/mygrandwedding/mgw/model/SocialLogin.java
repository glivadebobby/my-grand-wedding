package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 13/09/17
 */

public class SocialLogin {

    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("client")
    private String client;

    public SocialLogin(String accessToken, String client) {
        this.accessToken = accessToken;
        this.client = client;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
