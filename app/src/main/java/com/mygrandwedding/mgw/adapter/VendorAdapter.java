package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.VendorCallback;
import com.mygrandwedding.mgw.holder.VendorHolder;
import com.mygrandwedding.mgw.model.Vendor;

import java.util.List;
import java.util.Locale;

/**
 * Created by gladwinbobby on 06/09/17
 */

public class VendorAdapter extends RecyclerView.Adapter<VendorHolder> {

    private Context context;
    private List<Vendor> vendorList;
    private VendorCallback callback;

    public VendorAdapter(Context context, List<Vendor> vendorList, VendorCallback callback) {
        this.context = context;
        this.vendorList = vendorList;
        this.callback = callback;
    }

    @Override
    public VendorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VendorHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vendor, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(VendorHolder holder, int position) {
        Vendor vendor = vendorList.get(position);
        GlideApp.with(context)
                .load(vendor.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewVendor);
        holder.textViewVendor.setText(vendor.getBusinessName());
        holder.textViewLocation.setText(vendor.getLocation().getLocation());
        if (vendor.getMinPrice() == 0)
            holder.textViewCost.setText(context.getString(R.string.txt_price_on_request));
        else
            holder.textViewCost.setText(String.format(Locale.getDefault(), context.getString(R.string.format_cost_onwards), vendor.getMinPrice()));
        holder.textViewRating.setText(vendor.getRating() == null ? context.getString(R.string.error_na) : String.valueOf(vendor.getRating()));
        holder.textViewReview.setText(context.getResources().getQuantityString(R.plurals.review, vendor.getReview(), vendor.getReview()));
        if (vendor.getVendorType() == 1) {
            holder.diagonalView.setVisibility(View.VISIBLE);
            holder.imageViewType.setVisibility(View.VISIBLE);
            holder.imageViewType.setImageResource(R.drawable.ic_featured);
        } else if (vendor.getVendorType() == 2) {
            holder.diagonalView.setVisibility(View.INVISIBLE);
            holder.imageViewType.setVisibility(View.INVISIBLE);
        } else if (vendor.getVendorType() == 3) {
            holder.diagonalView.setVisibility(View.VISIBLE);
            holder.imageViewType.setVisibility(View.VISIBLE);
            holder.imageViewType.setImageResource(R.drawable.ic_recommended);
        }
    }

    @Override
    public int getItemCount() {
        return vendorList.size();
    }
}
