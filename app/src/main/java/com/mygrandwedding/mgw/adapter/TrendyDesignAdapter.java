package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.TrendyDesignCallback;
import com.mygrandwedding.mgw.holder.TrendyDesignCategoryHolder;
import com.mygrandwedding.mgw.model.TrendyDesign;

import java.util.List;

/**
 * Created by gladwinbobby on 10/09/17
 */

public class TrendyDesignAdapter extends RecyclerView.Adapter<TrendyDesignCategoryHolder> {

    private Context context;
    private List<TrendyDesign> trendyDesignList;
    private TrendyDesignCallback callback;

    public TrendyDesignAdapter(Context context, List<TrendyDesign> trendyDesignList, TrendyDesignCallback callback) {
        this.context = context;
        this.trendyDesignList = trendyDesignList;
        this.callback = callback;
    }

    @Override
    public TrendyDesignCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TrendyDesignCategoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trendy_design_category, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(TrendyDesignCategoryHolder holder, int position) {
        TrendyDesign trendyDesign = trendyDesignList.get(position);
        GlideApp.with(context)
                .load(trendyDesign.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewTrendyDesign);
        holder.textViewCategory.setText(trendyDesign.getCategory().getName());
    }

    @Override
    public int getItemCount() {
        return trendyDesignList.size();
    }
}
