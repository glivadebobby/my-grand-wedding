package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by gladwinbobby on 24/09/17
 */

public class BusinessDetail {

    @SerializedName("id")
    private int id;
    @SerializedName("vendor_type")
    private int vendorType;
    @SerializedName("min_price")
    private double minPrice;
    @SerializedName("max_price")
    private double maxPrice;
    @SerializedName("is_verified")
    private boolean isVerified;
    @SerializedName("is_paid")
    private boolean isPaid;
    @SerializedName("business_name")
    private String businessName;
    @SerializedName("registered_address")
    private String registeredAddress;
    @SerializedName("running_address")
    private String runningAddress;
    @SerializedName("description")
    private String description;
    @SerializedName("location")
    private Location location;
    @SerializedName("category")
    private VendorCategory category;
    @SerializedName("images")
    private ArrayList<Image> imageList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVendorType() {
        return vendorType;
    }

    public void setVendorType(int vendorType) {
        this.vendorType = vendorType;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public String getRunningAddress() {
        return runningAddress;
    }

    public void setRunningAddress(String runningAddress) {
        this.runningAddress = runningAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public VendorCategory getCategory() {
        return category;
    }

    public void setCategory(VendorCategory category) {
        this.category = category;
    }

    public ArrayList<Image> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<Image> imageList) {
        this.imageList = imageList;
    }
}
