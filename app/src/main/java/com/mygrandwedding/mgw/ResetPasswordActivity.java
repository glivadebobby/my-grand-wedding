package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.ForgotPassword;
import com.mygrandwedding.mgw.model.ResetPassword;
import com.mygrandwedding.mgw.model.User;

import java.util.Locale;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.ROLE_USER;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutOtp, layoutNewPassword;
    private TextInputEditText editTextOtp, editTextNewPassword;
    private Button buttonSubmit;
    private TextView textViewSendOtp;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initObjects();
        initToolbar();
        initCallbacks();
        setPasswordFont();
        setSendOtpText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (view == editTextOtp) isValidOtp();
        if (view == editTextNewPassword) isValidNewPassword();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            processResetPassword();
        } else if (view == textViewSendOtp) {
            setLoading(true);
            sendOtp();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutOtp = findViewById(R.id.otp);
        layoutNewPassword = findViewById(R.id.new_password);
        editTextOtp = findViewById(R.id.input_otp);
        editTextNewPassword = findViewById(R.id.input_new_password);
        buttonSubmit = findViewById(R.id.btn_submit);
        textViewSendOtp = findViewById(R.id.txt_send_otp);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        editTextOtp.setOnFocusChangeListener(this);
        editTextNewPassword.setOnFocusChangeListener(this);
        buttonSubmit.setOnClickListener(this);
        textViewSendOtp.setOnClickListener(this);
    }

    private void setPasswordFont() {
        layoutOtp.setTypeface(textViewSendOtp.getTypeface());
        layoutNewPassword.setTypeface(textViewSendOtp.getTypeface());
        editTextOtp.setTypeface(textViewSendOtp.getTypeface());
        editTextNewPassword.setTypeface(textViewSendOtp.getTypeface());
        editTextOtp.setTransformationMethod(new PasswordTransformationMethod());
        editTextNewPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void setSendOtpText() {
        SpannableString spannableString = new SpannableString(getString(R.string.txt_send_otp));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_txt)), 20, 30, 0);
        spannableString.setSpan(new RelativeSizeSpan(1.05f), 20, 30, 0);
        textViewSendOtp.setText(spannableString);
    }

    private void processResetPassword() {
        String otp = editTextOtp.getText().toString().trim();
        String newPassword = editTextNewPassword.getText().toString().trim();
        if (isValidOtp(otp) && isValidNewPassword(newPassword)) {
            setLoading(true);
            resetPassword(new ResetPassword(preference.getRole(), preference.getPhone(), otp, newPassword, UUID.randomUUID().toString()));
        }
    }

    private void isValidOtp() {
        isValidOtp(editTextOtp.getText().toString().trim());
    }

    private boolean isValidOtp(String otp) {
        if (otp.isEmpty()) {
            layoutOtp.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutOtp.getHint()));
            return false;
        } else if (otp.length() < 6) {
            layoutOtp.setError(String.format(Locale.getDefault(), getString(R.string.error_length), layoutOtp.getHint(), 6));
            return false;
        }
        layoutOtp.setErrorEnabled(false);
        return true;
    }

    private void isValidNewPassword() {
        isValidNewPassword(editTextNewPassword.getText().toString().trim());
    }

    private boolean isValidNewPassword(String newPassword) {
        if (newPassword.isEmpty()) {
            layoutNewPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutNewPassword.getHint()));
            return false;
        } else if (newPassword.length() < 6) {
            layoutNewPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        layoutNewPassword.setErrorEnabled(false);
        return true;
    }

    private void resetPassword(ResetPassword resetPassword) {
        Call<User> call = getApiService().resetPassword(resetPassword);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    preference.setId(userResponse.getId());
                    preference.setName(userResponse.getFirstName());
                    preference.setPhone(userResponse.getUsername());
                    preference.setEmail(userResponse.getEmail());
                    preference.setToken(userResponse.getToken());
                    launchClearStack(context, preference.getRole() == ROLE_USER ? MainActivity.class : VendorMainActivity.class);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void sendOtp() {
        Call<Void> call = getApiService().sendOtp(new ForgotPassword(preference.getRole(), preference.getPhone()));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "OTP code sent again to " + preference.getPhone(), Toast.LENGTH_SHORT).show();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
