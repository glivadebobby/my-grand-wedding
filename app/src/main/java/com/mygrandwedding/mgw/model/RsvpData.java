package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gladwinbobby on 03/12/17
 */

public class RsvpData {

    @SerializedName("count")
    private int count;
    @SerializedName("total_attenders")
    private int totalAttenders;
    @SerializedName("prev_url")
    private String prevUrl;
    @SerializedName("next_url")
    private String nextUrl;
    @SerializedName("results")
    private List<Rsvp> rsvpList;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalAttenders() {
        return totalAttenders;
    }

    public void setTotalAttenders(int totalAttenders) {
        this.totalAttenders = totalAttenders;
    }

    public String getPrevUrl() {
        return prevUrl;
    }

    public void setPrevUrl(String prevUrl) {
        this.prevUrl = prevUrl;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public List<Rsvp> getRsvpList() {
        return rsvpList;
    }

    public void setRsvpList(List<Rsvp> rsvpList) {
        this.rsvpList = rsvpList;
    }
}
