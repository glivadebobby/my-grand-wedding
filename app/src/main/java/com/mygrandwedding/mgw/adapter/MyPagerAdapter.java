package com.mygrandwedding.mgw.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by gladwinbobby on 03/09/17
 */

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mFragmentList;

    public MyPagerAdapter(FragmentManager manager, List<Fragment> fragmentList) {
        super(manager);
        mFragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

}
