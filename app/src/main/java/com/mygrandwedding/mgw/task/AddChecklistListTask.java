package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.Checklist;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 18/10/17
 */

public class AddChecklistListTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private List<Checklist> checklistList;

    public AddChecklistListTask(Context context, List<Checklist> checklistList) {
        this.context = context;
        this.checklistList = checklistList;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getAppDatabase(context).deleteDao().deleteChecklist();
        getAppDatabase(context).weddingDao().insertChecklistList(checklistList);
        return null;
    }
}
