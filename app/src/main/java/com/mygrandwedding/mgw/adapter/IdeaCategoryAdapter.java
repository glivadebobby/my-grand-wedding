package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.CategoryCallback;
import com.mygrandwedding.mgw.holder.IdeaCategoryHolder;
import com.mygrandwedding.mgw.model.IdeaCategory;

import java.util.List;

/**
 * Created by gladwinbobby on 05/09/17
 */

public class IdeaCategoryAdapter extends RecyclerView.Adapter<IdeaCategoryHolder> {

    private Context context;
    private List<IdeaCategory> ideaCategoryList;
    private CategoryCallback callback;

    public IdeaCategoryAdapter(Context context, List<IdeaCategory> ideaCategoryList, CategoryCallback callback) {
        this.context = context;
        this.ideaCategoryList = ideaCategoryList;
        this.callback = callback;
    }

    @Override
    public IdeaCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new IdeaCategoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_idea_category, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(IdeaCategoryHolder holder, int position) {
        IdeaCategory vendorCategory = ideaCategoryList.get(position);
        GlideApp.with(context)
                .load(vendorCategory.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewCategory);
        holder.textViewCategory.setText(vendorCategory.getCategory());
    }

    @Override
    public int getItemCount() {
        return ideaCategoryList.size();
    }
}
