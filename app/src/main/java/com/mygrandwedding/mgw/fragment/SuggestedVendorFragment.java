package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.VendorDetailActivity;
import com.mygrandwedding.mgw.adapter.VendorAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.VendorCallback;
import com.mygrandwedding.mgw.model.Vendor;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.model.VendorData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_IMAGE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;
import static com.mygrandwedding.mgw.util.MyData.BRIDAL_WEAR;
import static com.mygrandwedding.mgw.util.MyData.DJ_MUSIC;
import static com.mygrandwedding.mgw.util.MyData.FASHION_JEWELRY;
import static com.mygrandwedding.mgw.util.MyData.GROOM_WEAR;
import static com.mygrandwedding.mgw.util.MyData.INVITATION_CARDS;
import static com.mygrandwedding.mgw.util.MyData.MAKEUP_ARTISTS;
import static com.mygrandwedding.mgw.util.MyData.OTHERS;
import static com.mygrandwedding.mgw.util.MyData.PANDIT_PUROHIT;
import static com.mygrandwedding.mgw.util.MyData.REAL_JEWELRY;
import static com.mygrandwedding.mgw.util.MyData.TRANSPORTATIONS;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_DECORATORS;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_PHOTO_VIDEO;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_VENUES;

/**
 * A simple {@link Fragment} subclass.
 */
public class SuggestedVendorFragment extends Fragment implements VendorCallback,
        SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemSelectedListener {

    private Context context;
    private View rootView;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewVendors;
    private Spinner spinnerCategory;
    private LinearLayout layoutEmpty;
    private TextView textViewEmpty;
    private List<VendorCategory> vendorCategoryList;
    private List<Vendor> vendorList;
    private LinearLayoutManager layoutManager;
    private ArrayAdapter<VendorCategory> categoryArrayAdapter;
    private VendorAdapter vendorAdapter;
    private MyPreference preference;
    private int id;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    public SuggestedVendorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_suggested_vendor, container, false);
        initObjects();
        initCallbacks();
        initSpinner();
        initRecyclerView();
        initRefresh();
        initEmpty();
        loadVendorCategories();
        return rootView;
    }

    @Override
    public void onVendorClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, vendorList.get(position).getUser());
        bundle.putString(EXTRA_IMAGE, vendorList.get(position).getImage());
        launchWithBundle(context, VendorDetailActivity.class, bundle);
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView == spinnerCategory) {
            VendorCategory category = vendorCategoryList.get(i);
            if (category.getId() > 0) {
                id = category.getId();
                vendorList.clear();
                vendorAdapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(true);
                resetList();
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void initObjects() {
        refreshLayout = rootView.findViewById(R.id.refresh);
        viewVendors = rootView.findViewById(R.id.vendors);
        spinnerCategory = rootView.findViewById(R.id.spin_category);
        layoutEmpty = rootView.findViewById(R.id.empty);
        textViewEmpty = rootView.findViewById(R.id.txt_empty);

        context = getActivity();
        vendorCategoryList = new ArrayList<>();
        vendorList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        categoryArrayAdapter = new ArrayAdapter<>(context, R.layout.item_spinner, vendorCategoryList);
        vendorAdapter = new VendorAdapter(context, vendorList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        spinnerCategory.setOnItemSelectedListener(this);
    }

    private void initSpinner() {
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(categoryArrayAdapter);
    }

    private void initRecyclerView() {
        viewVendors.setLayoutManager(layoutManager);
        viewVendors.setAdapter(vendorAdapter);
        viewVendors.setNestedScrollingEnabled(false);
        viewVendors.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getVendors();
                }
            }
        });
    }

    private void initEmpty() {
        textViewEmpty.setText(getString(R.string.error_empty_vendor));
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
    }

    private void loadVendorCategories() {
        vendorCategoryList.add(new VendorCategory("Choose a category"));
        vendorCategoryList.add(new VendorCategory(WEDDING_VENUES, "Wedding Venues"));
        vendorCategoryList.add(new VendorCategory(INVITATION_CARDS, "Invitation Cards"));
        vendorCategoryList.add(new VendorCategory(WEDDING_DECORATORS, "Wedding Decorators"));
        vendorCategoryList.add(new VendorCategory(WEDDING_PHOTO_VIDEO, "Wedding Photo/Video"));
        vendorCategoryList.add(new VendorCategory(PANDIT_PUROHIT, "Pandit/Purohit"));
        vendorCategoryList.add(new VendorCategory(MAKEUP_ARTISTS, "Makeup Artists"));
        vendorCategoryList.add(new VendorCategory(REAL_JEWELRY, "Real Jewelry"));
        vendorCategoryList.add(new VendorCategory(FASHION_JEWELRY, "Fashion Jewelry"));
        vendorCategoryList.add(new VendorCategory(BRIDAL_WEAR, "Bridal Wear"));
        vendorCategoryList.add(new VendorCategory(GROOM_WEAR, "Groom Wear"));
        vendorCategoryList.add(new VendorCategory(DJ_MUSIC, "DJ & Music"));
        vendorCategoryList.add(new VendorCategory(TRANSPORTATIONS, "Transportations"));
        vendorCategoryList.add(new VendorCategory(OTHERS, "Others"));
        categoryArrayAdapter.notifyDataSetChanged();
    }

    private void resetList() {
        count = 0;
        setUrl();
        getVendors();
    }

    private void setUrl() {
        url = MGW_URL + "vendor/app/list/" + id + "/";
        url += "?wedding=" + preference.getWeddingId();
    }

    private void clearList() {
        if (count == 0) {
            vendorList.clear();
            vendorAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        layoutEmpty.setVisibility(vendorList.isEmpty() ? View.VISIBLE : View.GONE);
    }

    private void getVendors() {
        setLoading(true);
        Call<VendorData> call = getApiService().getSuggestedVendors(preference.getUserToken(), url);
        call.enqueue(new Callback<VendorData>() {
            @Override
            public void onResponse(@NonNull Call<VendorData> call,
                                   @NonNull Response<VendorData> response) {
                refreshLayout.setRefreshing(false);
                VendorData vendorDataResponse = response.body();
                if (response.isSuccessful() && vendorDataResponse != null) {
                    setLoading(false);
                    clearList();
                    count = vendorDataResponse.getCount();
                    url = vendorDataResponse.getNextUrl();
                    vendorList.addAll(vendorDataResponse.getVendorList());
                    vendorAdapter.notifyDataSetChanged();
                } else processError(context, response.code(), response.errorBody());
                isEmpty();
            }

            @Override
            public void onFailure(@NonNull Call<VendorData> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
