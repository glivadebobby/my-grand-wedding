package com.mygrandwedding.mgw.util;

import android.text.format.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by gladwinbobby on 15/09/17
 */

public class DateParser {
    public static int getDayOfMonth(String myDate) {
        Calendar calendar = Calendar.getInstance();
        if (myDate == null) return calendar.get(Calendar.DAY_OF_MONTH);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = dateFormat.parse(myDate);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd", Locale.getDefault());
            return Integer.parseInt(simpleDateFormat.format(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public static int getMonth(String myDate) {
        Calendar calendar = Calendar.getInstance();
        if (myDate == null) return calendar.get(Calendar.MONTH);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = dateFormat.parse(myDate);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM", Locale.getDefault());
            return Integer.parseInt(simpleDateFormat.format(date)) - 1;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar.get(Calendar.MONTH);
    }

    public static int getYear(String myDate) {
        Calendar calendar = Calendar.getInstance();
        if (myDate == null) return calendar.get(Calendar.YEAR) - 20;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = dateFormat.parse(myDate);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
            return Integer.parseInt(simpleDateFormat.format(date));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return calendar.get(Calendar.YEAR) - 20;
    }

    public static String getDate(int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public static String getFormattedDate(int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
        return dateFormat.format(calendar.getTime());
    }

    public static String getFormattedDate(String myDate) {
        if (myDate == null) return null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = dateFormat.parse(myDate);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return myDate;
    }

    public static long getWeddingDuration(String myDate) {
        if (myDate == null) return -1;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = dateFormat.parse(myDate);
            if (DateUtils.isToday(date.getTime())) return 0;
            return date.getTime() - System.currentTimeMillis();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static long getWeddingTime(String myDate) {
        if (myDate == null) return System.currentTimeMillis();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = dateFormat.parse(myDate);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return System.currentTimeMillis();
    }

    public static String getRelativeTime(String createdOn) {
        if (createdOn == null) return null;
        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault()).parse(createdOn);
            return DateUtils.getRelativeTimeSpanString(date.getTime(), System.currentTimeMillis(),
                    DateUtils.SECOND_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return createdOn;
    }

    public static long getWeddingDays(int duration, long weddingDuration) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(weddingDuration);
        calendar.add(Calendar.DAY_OF_MONTH, -duration);
        long diff = calendar.getTimeInMillis() - System.currentTimeMillis();
        long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        long months = days / 30;
        if (months == 0) return days;
        else return days -= months * 30;
    }

    public static String getDuration(int duration, long weddingDuration) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(weddingDuration);
        calendar.add(Calendar.DAY_OF_MONTH, -duration);
        long diff = calendar.getTimeInMillis() - System.currentTimeMillis();
        long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        long months = days / 30;
        if (months == 0) return days + " " + (days == 1 ? "day" : "days") + " " + "to go";
        else {
            days -= months * 30;
            return months + " " + (months == 1 ? "month" : "months") + " " + "and" + " " + days + " " + (days == 1 ? "day" : "days") + " " + "to go";
        }
    }

    public static String getCreatedDate(String createdOn) {
        if (createdOn == null) return null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault());
        try {
            Date date = dateFormat.parse(createdOn);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return createdOn;
    }

    public static String getDob(String dob) {
        if (dob == null) return null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date date = dateFormat.parse(dob);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dob;
    }
}
