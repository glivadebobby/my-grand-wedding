package com.mygrandwedding.mgw.model;

import java.util.List;

/**
 * Created by gladwinbobby on 18/10/17
 */

public class ChecklistDuration {

    public static final int TYPE_INCOMPLETE = -1;
    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_COMPLETED = 1;
    private int type;
    private boolean selected;
    private String duration;
    private List<Checklist> checklistList;

    public ChecklistDuration(String duration, List<Checklist> checklistList) {
        type = TYPE_NORMAL;
        this.duration = duration;
        this.checklistList = checklistList;
    }

    public ChecklistDuration(int type, String duration, List<Checklist> checklistList) {
        this.type = type;
        this.duration = duration;
        this.checklistList = checklistList;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<Checklist> getChecklistList() {
        return checklistList;
    }

    public void setChecklistList(List<Checklist> checklistList) {
        this.checklistList = checklistList;
    }
}
