package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.mygrandwedding.mgw.adapter.AboutUsAdapter;
import com.mygrandwedding.mgw.model.AboutUs;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AboutActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private RecyclerView viewAboutUs;
    private List<AboutUs> aboutUsList;
    private AboutUsAdapter aboutUsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initObjects();
        initToolbar();
        initRecyclerView();
        loadAboutUs();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        viewAboutUs = findViewById(R.id.about_us);

        context = this;
        aboutUsList = new ArrayList<>();
        aboutUsAdapter = new AboutUsAdapter(aboutUsList);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewAboutUs.setLayoutManager(new LinearLayoutManager(context));
        viewAboutUs.setAdapter(aboutUsAdapter);
    }

    private void loadAboutUs() {
        aboutUsList.clear();
        aboutUsList.add(new AboutUs("Who We Are?", "\"MY GRAND WEDDING\" is a one stop solution for all your wedding needs, well known for delivering Luxurious Wedding Experiences integrated with trendy services. We aim to walk the extra mile to ensure that you cherish your wedding bliss and make it an everlasting memory so that it remains imprinted in your heart .Mr. R.Sharath, Founder & CEO, of My Grand Wedding Pvt. Ltd., Started his career at Taj group 20 Years ago, he has been an Ex-Hotelier and a successful Event Manager for the past 10 Years.  Mrs. Jane Catherine, Managing Director of My Grand Wedding Pvt. Ltd., is an MBA graduate, with an eye for colors, designs, perfection and detail, started her career as an Event Planner in an IT Company. At present, the duo aims to create wonderful moments for the couple and their family."));
        aboutUsList.add(new AboutUs("What we do?", "My Grand Wedding is an online wedding planning website and app which provide end to end wedding support for more than 20 categories of vendors in India. View vendor profiles and shortlist by photos, ratings & reviews. Experience the exclusive features like wedding calculator, trendy designs, ideas and more only at My Grand Wedding.!"));
        aboutUsList.add(new AboutUs("Vendors", "We have listed more than 20 wedding categories in more than 20 top cities in India. View vendor’s profiles with photos, price range, ratings & reviews. Login to the site or app to shortlist top vendors from your city. Experience the service of the vendors, rate them and provide your valuable reviews."));
        aboutUsList.add(new AboutUs("Experience the exclusive features of My Grand Wedding", "Login/ Register to My Grand Wedding app or site to experience the following features\n• Create your wedding with couple\'s name and wedding date\n• Invite your friends & families to join your wedding\n• Post your wedding activities\n• Shortlist vendors\n• Like and refer vendors\n• Calculate your wedding budget\n• Get vendors according to your budget"));
        aboutUsList.add(new AboutUs("Trendy designs", "View the trendy collections in all the wedding categories like Lehenga, jewelry, mehandi designs etc…"));
        aboutUsList.add(new AboutUs("Ideas & Blogs", "Get updated with trendy ideas and stories of weddings."));
        aboutUsAdapter.notifyDataSetChanged();
    }
}
