package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 06/09/17
 */

public interface VendorCallback {
    void onVendorClick(int position);
}
