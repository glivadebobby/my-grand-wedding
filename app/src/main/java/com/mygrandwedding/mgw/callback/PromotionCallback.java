package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 10/09/17
 */

public interface PromotionCallback {
    void onPromotionClick(int position);
}
