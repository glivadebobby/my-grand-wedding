package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 16/10/17
 */

public class Password {

    @SerializedName("password")
    private String password;

    public Password(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
