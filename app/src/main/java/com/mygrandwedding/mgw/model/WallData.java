package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gladwinbobby on 23/10/17
 */

public class WallData {

    @SerializedName("count")
    private int count;
    @SerializedName("prev_url")
    private String prevUrl;
    @SerializedName("next_url")
    private String nextUrl;
    @SerializedName("results")
    private List<Wall> wallList;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPrevUrl() {
        return prevUrl;
    }

    public void setPrevUrl(String prevUrl) {
        this.prevUrl = prevUrl;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public List<Wall> getWallList() {
        return wallList;
    }

    public void setWallList(List<Wall> wallList) {
        this.wallList = wallList;
    }
}
