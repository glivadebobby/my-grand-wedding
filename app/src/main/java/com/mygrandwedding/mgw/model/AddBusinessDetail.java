package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 27/09/17
 */

public class AddBusinessDetail {

    @SerializedName("category")
    private int mCategory;
    @SerializedName("location")
    private int location;
    @SerializedName("min_price")
    private double minPrice;
    @SerializedName("max_price")
    private double maxPrice;
    @SerializedName("business_name")
    private String businessName;
    @SerializedName("registered_address")
    private String registeredAddress;
    @SerializedName("running_address")
    private String runningAddress;
    @SerializedName("description")
    private String description;

    public AddBusinessDetail(int mCategory, int location, double minPrice, double maxPrice,
                             String businessName, String registeredAddress, String runningAddress,
                             String description) {
        this.mCategory = mCategory;
        this.location = location;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.businessName = businessName;
        this.registeredAddress = registeredAddress;
        this.runningAddress = runningAddress;
        this.description = description;
    }

    public int getmCategory() {
        return mCategory;
    }

    public void setmCategory(int mCategory) {
        this.mCategory = mCategory;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public String getRunningAddress() {
        return runningAddress;
    }

    public void setRunningAddress(String runningAddress) {
        this.runningAddress = runningAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
