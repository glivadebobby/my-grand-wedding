package com.mygrandwedding.mgw;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Register;
import com.mygrandwedding.mgw.model.SocialLogin;
import com.mygrandwedding.mgw.model.User;

import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener, FacebookCallback<LoginResult> {

    private Context context;
    private Toolbar toolbar;
    private TextView textViewFacebook, textViewLogin;
    private TextInputLayout layoutName, layoutEmail, layoutPhone, layoutPassword;
    private TextInputEditText editTextName, editTextEmail, editTextPhone, editTextPassword;
    private Button buttonContinue;
    private FrameLayout layoutLoading;
    private CallbackManager callbackManager;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initObjects();
        initToolbar();
        initCallbacks();
        setPasswordFont();
        setLoginText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextName) isValidName();
            if (view == editTextEmail) isValidEmail();
            if (view == editTextPhone) isValidPhone();
            if (view == editTextPassword) isValidPassword();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        setLoading(true);
        fbLogin(new SocialLogin(loginResult.getAccessToken().getToken(), UUID.randomUUID().toString()));
    }

    @Override
    public void onCancel() {
        Toast.makeText(context, getString(R.string.error_fb_cancelled), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(FacebookException error) {
        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view == textViewFacebook) startFbLogin();
        else if (view == buttonContinue) processSignUp();
        else if (view == textViewLogin) launch(context, LoginActivity.class);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        textViewFacebook = findViewById(R.id.txt_facebook);
        textViewLogin = findViewById(R.id.txt_login);
        layoutName = findViewById(R.id.name);
        layoutEmail = findViewById(R.id.email);
        layoutPhone = findViewById(R.id.phone);
        layoutPassword = findViewById(R.id.password);
        editTextName = findViewById(R.id.input_name);
        editTextEmail = findViewById(R.id.input_email);
        editTextPhone = findViewById(R.id.input_phone);
        editTextPassword = findViewById(R.id.input_password);
        buttonContinue = findViewById(R.id.btn_continue);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        callbackManager = CallbackManager.Factory.create();
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        textViewFacebook.setOnClickListener(this);
        textViewLogin.setOnClickListener(this);
        editTextName.setOnFocusChangeListener(this);
        editTextEmail.setOnFocusChangeListener(this);
        editTextPhone.setOnFocusChangeListener(this);
        editTextPassword.setOnFocusChangeListener(this);
        buttonContinue.setOnClickListener(this);
        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    private void setPasswordFont() {
        layoutPassword.setTypeface(editTextName.getTypeface());
        editTextPassword.setTypeface(editTextName.getTypeface());
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void setLoginText() {
        SpannableString spannableString = new SpannableString(getString(R.string.txt_already_login));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_txt)), 25, 30, 0);
        spannableString.setSpan(new RelativeSizeSpan(1.05f), 25, 30, 0);
        textViewLogin.setText(spannableString);
    }

    private void startFbLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday"));
    }

    private void processSignUp() {
        String name = editTextName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String phone = editTextPhone.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        if (isValidName(name) && isValidEmail(email) && isValidPhone(phone) && isValidPassword(password)) {
            setLoading(true);
            register(new Register(name, phone, email, password));
        }
    }

    private void isValidName() {
        isValidName(editTextName.getText().toString().trim());
    }

    private boolean isValidName(String name) {
        if (name.isEmpty()) {
            layoutName.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutName.getHint()));
            return false;
        }
        layoutName.setErrorEnabled(false);
        return true;
    }

    private void isValidEmail() {
        isValidEmail(editTextEmail.getText().toString().trim());
    }

    private boolean isValidEmail(String email) {
        if (email.isEmpty()) {
            layoutEmail.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutEmail.getHint()));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            layoutEmail.setError(String.format(Locale.getDefault(), getString(R.string.error_invalid), layoutEmail.getHint()));
            return false;
        }
        layoutEmail.setErrorEnabled(false);
        return true;
    }

    private void isValidPhone() {
        isValidPhone(editTextPhone.getText().toString().trim());
    }

    private boolean isValidPhone(String phone) {
        if (phone.isEmpty()) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPhone.getHint()));
            return false;
        } else if (phone.length() < 10) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_length), layoutPhone.getHint(), 10));
            return false;
        }
        layoutPhone.setErrorEnabled(false);
        return true;
    }

    private void isValidPassword() {
        isValidPassword(editTextPassword.getText().toString().trim());
    }

    private boolean isValidPassword(String password) {
        if (password.isEmpty()) {
            layoutPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPassword.getHint()));
            return false;
        } else if (password.length() < 6) {
            layoutPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        layoutPassword.setErrorEnabled(false);
        return true;
    }

    private void register(Register register) {
        Call<User> call = getApiService().register(register);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    preference.setPhone(userResponse.getUsername());
                    Toast.makeText(context, "OTP code sent to " + userResponse.getUsername(), Toast.LENGTH_SHORT).show();
                    launch(context, OtpActivity.class);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void fbLogin(SocialLogin socialLogin) {
        Call<User> call = getApiService().fbLogin(socialLogin);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    handleLoginResponse(userResponse);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void handleLoginResponse(User userResponse) {
        preference.setId(userResponse.getId());
        preference.setName(userResponse.getFirstName());
        if (userResponse.getUsername() == null || userResponse.getUsername().isEmpty()
                || !TextUtils.isDigitsOnly(userResponse.getUsername()))
            preference.setVerified(false);
        else
            preference.setPhone(userResponse.getUsername());
        if (userResponse.getEmail() == null || userResponse.getEmail().isEmpty())
            preference.setVerified(false);
        else
            preference.setEmail(userResponse.getEmail());
        preference.setToken(userResponse.getToken());
        if (!preference.isVerified())
            launch(context, AdditionalDetailActivity.class);
        else
            launchClearStack(context, MainActivity.class);
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
