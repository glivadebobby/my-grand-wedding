package com.mygrandwedding.mgw.fragment;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.IdeaActivity;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.adapter.IdeaCategoryAdapter;
import com.mygrandwedding.mgw.callback.CategoryCallback;
import com.mygrandwedding.mgw.model.IdeaCategory;
import com.mygrandwedding.mgw.task.AddIdeaCategoryListTask;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class IdeaCategoryFragment extends Fragment implements CategoryCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private View rootView;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewCategories;
    private List<IdeaCategory> ideaCategoryList;
    private IdeaCategoryAdapter categoryAdapter;

    public IdeaCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_idea_category, container, false);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        loadIdeaCategories();
        return rootView;
    }

    @Override
    public void onCategoryClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, ideaCategoryList.get(position).getId());
        bundle.putString(EXTRA_TITLE, ideaCategoryList.get(position).getCategory());
        launchWithBundle(context, IdeaActivity.class, bundle);
    }

    @Override
    public void onRefresh() {
        getCategories();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getCategories();
    }

    private void initObjects() {
        refreshLayout = rootView.findViewById(R.id.refresh);
        viewCategories = rootView.findViewById(R.id.categories);

        context = getActivity();
        ideaCategoryList = new ArrayList<>();
        categoryAdapter = new IdeaCategoryAdapter(context, ideaCategoryList, this);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecyclerView() {
        viewCategories.setLayoutManager(new GridLayoutManager(context, 2));
        viewCategories.setAdapter(categoryAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void loadIdeaCategories() {
        LiveData<List<IdeaCategory>> data = getAppDatabase(context).generalDao().getIdeaCategories();
        data.observe(this, new Observer<List<IdeaCategory>>() {
            @Override
            public void onChanged(@Nullable List<IdeaCategory> ideaCategories) {
                ideaCategoryList.clear();
                if (ideaCategories != null && !ideaCategories.isEmpty()) {
                    ideaCategoryList.addAll(ideaCategories);
                }
                categoryAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getCategories() {
        Call<List<IdeaCategory>> call = getApiService().getIdeaCategory();
        call.enqueue(new Callback<List<IdeaCategory>>() {
            @Override
            public void onResponse(@NonNull Call<List<IdeaCategory>> call,
                                   @NonNull Response<List<IdeaCategory>> response) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    List<IdeaCategory> categoryListResponse = response.body();
                    if (response.isSuccessful() && categoryListResponse != null) {
                        new AddIdeaCategoryListTask(context, categoryListResponse).execute();
                    } else processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<IdeaCategory>> call, @NonNull Throwable t) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    processFailure(context, t);
                }
            }
        });
    }
}
