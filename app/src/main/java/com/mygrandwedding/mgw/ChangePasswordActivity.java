package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.ChangePassword;
import com.mygrandwedding.mgw.model.Data;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutCurrentPassword, layoutNewPassword;
    private TextInputEditText editTextCurrentPassword, editTextNewPassword;
    private Button buttonSubmit;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initObjects();
        initToolbar();
        initCallbacks();
        setPasswordFont();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (view == editTextCurrentPassword) isValidCurrentPassword();
        if (view == editTextNewPassword) isValidNewPassword();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            processChangePassword();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutCurrentPassword = findViewById(R.id.current_password);
        layoutNewPassword = findViewById(R.id.new_password);
        editTextCurrentPassword = findViewById(R.id.input_current_password);
        editTextNewPassword = findViewById(R.id.input_new_password);
        buttonSubmit = findViewById(R.id.btn_submit);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        editTextCurrentPassword.setOnFocusChangeListener(this);
        editTextNewPassword.setOnFocusChangeListener(this);
        buttonSubmit.setOnClickListener(this);
    }

    private void setPasswordFont() {
        layoutCurrentPassword.setTypeface(TypefaceUtils.load(getAssets(), "fonts/SourceSansPro-Regular.ttf"));
        layoutNewPassword.setTypeface(TypefaceUtils.load(getAssets(), "fonts/SourceSansPro-Regular.ttf"));
        editTextCurrentPassword.setTypeface(TypefaceUtils.load(getAssets(), "fonts/SourceSansPro-Regular.ttf"));
        editTextNewPassword.setTypeface(TypefaceUtils.load(getAssets(), "fonts/SourceSansPro-Regular.ttf"));
        editTextCurrentPassword.setTransformationMethod(new PasswordTransformationMethod());
        editTextNewPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void processChangePassword() {
        String currentPassword = editTextCurrentPassword.getText().toString().trim();
        String newPassword = editTextNewPassword.getText().toString().trim();
        if (isValidCurrentPassword(currentPassword) && isValidNewPassword(newPassword)) {
            setLoading(true);
            changePassword(new ChangePassword(currentPassword, newPassword));
        }
    }

    private void isValidCurrentPassword() {
        isValidCurrentPassword(editTextCurrentPassword.getText().toString().trim());
    }

    private boolean isValidCurrentPassword(String changePassword) {
        if (changePassword.isEmpty()) {
            layoutCurrentPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutCurrentPassword.getHint()));
            return false;
        } else if (changePassword.length() < 6) {
            layoutCurrentPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        layoutCurrentPassword.setErrorEnabled(false);
        return true;
    }

    private void isValidNewPassword() {
        isValidNewPassword(editTextNewPassword.getText().toString().trim());
    }

    private boolean isValidNewPassword(String newPassword) {
        if (newPassword.isEmpty()) {
            layoutNewPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutNewPassword.getHint()));
            return false;
        } else if (newPassword.length() < 6) {
            layoutNewPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        layoutNewPassword.setErrorEnabled(false);
        return true;
    }

    private void changePassword(ChangePassword changePassword) {
        Call<Data> call = getApiService().changePassword(preference.getUserToken(), changePassword);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    Toast.makeText(context, dataResponse.getData(), Toast.LENGTH_SHORT).show();
                    finish();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
