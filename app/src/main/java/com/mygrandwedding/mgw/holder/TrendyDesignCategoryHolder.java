package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.TrendyDesignCallback;

/**
 * Created by gladwinbobby on 10/09/17
 */

public class TrendyDesignCategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewTrendyDesign;
    public TextView textViewCategory;
    private TrendyDesignCallback callback;

    public TrendyDesignCategoryHolder(View itemView, TrendyDesignCallback callback) {
        super(itemView);
        imageViewTrendyDesign = itemView.findViewById(R.id.img_trendy_design);
        textViewCategory = itemView.findViewById(R.id.txt_category);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onTrendyDesignClick(getLayoutPosition());
        }
    }
}
