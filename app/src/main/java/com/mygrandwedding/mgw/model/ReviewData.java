package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gladwinbobby on 25/09/17
 */

public class ReviewData {

    @SerializedName("count")
    private int count;
    @SerializedName("prev_url")
    private String prevUrl;
    @SerializedName("next_url")
    private String nextUrl;
    @SerializedName("results")
    private List<Review> reviewList;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getPrevUrl() {
        return prevUrl;
    }

    public void setPrevUrl(String prevUrl) {
        this.prevUrl = prevUrl;
    }

    public String getNextUrl() {
        return nextUrl;
    }

    public void setNextUrl(String nextUrl) {
        this.nextUrl = nextUrl;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }
}
