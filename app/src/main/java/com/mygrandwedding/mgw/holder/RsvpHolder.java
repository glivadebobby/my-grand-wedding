package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.RsvpCallback;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by gladwinbobby on 03/12/17
 */

public class RsvpHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView textViewName, textViewMessage, textViewCount;
    public CircleImageView imageViewProfile;
    private RsvpCallback callback;

    public RsvpHolder(View itemView, RsvpCallback callback) {
        super(itemView);
        textViewName = itemView.findViewById(R.id.txt_name);
        textViewMessage = itemView.findViewById(R.id.txt_message);
        textViewCount = itemView.findViewById(R.id.txt_count);
        imageViewProfile = itemView.findViewById(R.id.img_profile);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onRsvpClick(getLayoutPosition());
        }
    }
}
