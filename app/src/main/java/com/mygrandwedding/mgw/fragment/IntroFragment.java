package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroFragment extends Fragment {

    private static final String ARGUMENT_IMAGE = "image";
    private static final String ARGUMENT_INTRO = "intro";
    private Context context;
    private View rootView;
    private ImageView imageView;
    private TextView textViewIntro;

    public IntroFragment() {
        // Required empty public constructor
    }

    public static IntroFragment newInstance(int image, String intro) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARGUMENT_IMAGE, image);
        bundle.putString(ARGUMENT_INTRO, intro);
        IntroFragment introFragment = new IntroFragment();
        introFragment.setArguments(bundle);
        return introFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_intro, container, false);
        initObjects();
        processBundle();
        return rootView;
    }

    private void initObjects() {
        imageView = rootView.findViewById(R.id.img_intro);
        textViewIntro = rootView.findViewById(R.id.txt_intro);

        context = getActivity();
    }

    private void processBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            GlideApp.with(context)
                    .load(bundle.getInt(ARGUMENT_IMAGE))
                    .into(imageView);
            textViewIntro.setText(bundle.getString(ARGUMENT_INTRO));
        }
    }
}
