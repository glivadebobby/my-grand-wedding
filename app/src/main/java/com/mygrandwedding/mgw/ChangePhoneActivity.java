package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.MobileNumber;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.R.id.phone;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.MyActivity.launch;

public class ChangePhoneActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutPhone;
    private TextInputEditText editTextPhone;
    private Button buttonSubmit;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_phone);
        initObjects();
        initToolbar();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            processChangePhone();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutPhone = findViewById(phone);
        editTextPhone = findViewById(R.id.input_phone);
        buttonSubmit = findViewById(R.id.btn_submit);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        buttonSubmit.setOnClickListener(this);
    }

    private void processChangePhone() {
        String phone = editTextPhone.getText().toString().trim();
        if (isValidPhone(phone)) {
            setLoading(true);
            changePhone(new MobileNumber(phone));
        }
    }

    private boolean isValidPhone(String phone) {
        if (phone.isEmpty()) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPhone.getHint()));
            return false;
        } else if (phone.length() < 10) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_length), layoutPhone.getHint(), 10));
            return false;
        }
        layoutPhone.setErrorEnabled(false);
        return true;
    }

    private void changePhone(final MobileNumber mobileNumber) {
        Call<Data> call = getApiService().changeMobile(preference.getUserToken(), mobileNumber);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    Toast.makeText(context, "OTP code sent to " + mobileNumber.getMobileNumber(), Toast.LENGTH_SHORT).show();
                    preference.setPhone(mobileNumber.getMobileNumber());
                    launch(context, ChangePhoneOtpActivity.class);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonSubmit.setVisibility(loading ? View.GONE : View.VISIBLE);
    }
}
