package com.mygrandwedding.mgw.model;

/**
 * Created by gladwinbobby on 07/09/17
 */

public class Photo {

    private String photo;

    public Photo(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
