package com.mygrandwedding.mgw;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.ChecklistAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.CheckCallback;
import com.mygrandwedding.mgw.callback.ChecklistCallback;
import com.mygrandwedding.mgw.model.Checklist;
import com.mygrandwedding.mgw.model.ChecklistDuration;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.UpdateChecklist;
import com.mygrandwedding.mgw.model.Wedding;
import com.mygrandwedding.mgw.task.AddChecklistListTask;
import com.mygrandwedding.mgw.viewmodel.ChecklistViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.R.id.checklist;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;
import static com.mygrandwedding.mgw.util.DateParser.getDuration;
import static com.mygrandwedding.mgw.util.DateParser.getWeddingDays;
import static com.mygrandwedding.mgw.util.DateParser.getWeddingTime;

public class ChecklistActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ChecklistCallback, CheckCallback, View.OnClickListener {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewChecklist;
    private Button buttonSave;
    private FrameLayout layoutLoading;
    private List<ChecklistDuration> checklistDurationList;
    private List<Integer> idList;
    private ChecklistAdapter checklistAdapter;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checklist);
        initObjects();
        initToolbar();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        loadSelectedWedding();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getChecklist();
    }

    @Override
    public void onChecklistClick(int position) {
        ChecklistDuration checklistDuration = checklistDurationList.get(position);
        checklistDuration.setSelected(!checklistDuration.isSelected());
        checklistAdapter.notifyItemChanged(position);
    }

    @Override
    public void onCheckClick(int position, int checkPosition) {
        if (preference.isCollaborator()) return;
        buttonSave.setVisibility(View.VISIBLE);
        Checklist checklist = checklistDurationList.get(position).getChecklistList().get(checkPosition);
        checklist.setChecked(!checklist.isChecked());
        int positionToRemove = -1;
        for (int i = 0; i < idList.size(); i++) {
            Integer id = idList.get(i);
            if (id == checklist.getId()) {
                positionToRemove = i;
                break;
            }
        }
        if (positionToRemove == -1) idList.add(checklist.getId());
        else idList.remove(positionToRemove);
        checklistAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCheckedTextClick(int position, int checkPosition) {
        Checklist checklist = checklistDurationList.get(position).getChecklistList().get(checkPosition);
        if (checklist.getCategory() != null) {
            new LaunchVendorActivityTask(checklist.getCategory()).execute();
        }
        if (preference.isCollaborator()) return;
        buttonSave.setVisibility(View.VISIBLE);
        checklist.setChecked(!checklist.isChecked());
        int positionToRemove = -1;
        for (int i = 0; i < idList.size(); i++) {
            Integer id = idList.get(i);
            if (id == checklist.getId()) {
                positionToRemove = i;
                break;
            }
        }
        if (positionToRemove == -1) idList.add(checklist.getId());
        else idList.remove(positionToRemove);
        checklistAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSave) {
            updateChecklist();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewChecklist = findViewById(checklist);
        buttonSave = findViewById(R.id.btn_save);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
        checklistDurationList = new ArrayList<>();
        idList = new ArrayList<>();
        checklistAdapter = new ChecklistAdapter(context, checklistDurationList, this, this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        buttonSave.setOnClickListener(this);
    }

    private void initRecyclerView() {
        viewChecklist.setLayoutManager(new LinearLayoutManager(context));
        viewChecklist.setAdapter(checklistAdapter);
        viewChecklist.setNestedScrollingEnabled(false);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(new Runnable() { //// TODO: 19/10/17 remove this and update the content on update
            @Override
            public void run() {
                getChecklist();
            }
        });
    }

    private void loadSelectedWedding() {
        LiveData<Wedding> data = getAppDatabase(context).weddingDao().getSelectedWedding(preference.getWeddingId());
        data.observe(this, new Observer<Wedding>() {
            @Override
            public void onChanged(@Nullable Wedding wedding) {
                if (wedding != null) {
                    loadChecklist(getWeddingTime(wedding.getDate()));
                }
            }
        });
    }

    private void loadChecklist(final long weddingDuration) {
        ChecklistViewModel viewModel = ViewModelProviders.of(this).get(ChecklistViewModel.class);
        viewModel.getData(context).observe(this, new Observer<List<Checklist>>() {
            @Override
            public void onChanged(@Nullable List<Checklist> checklists) {
                checklistDurationList.clear();
                if (checklists != null && !checklists.isEmpty()) {
                    int duration = -1;
                    for (Checklist checklist : checklists) {
                        long weddingDays = getWeddingDays(checklist.getDuration(), weddingDuration);
                        if (checklist.isChecked()) {
                            setCompleted(checklist);
                        } else if (weddingDays < 0) {
                            setIncomplete(checklist);
                        } else {
                            setNormal(duration, weddingDuration, checklist);
                        }
                        duration = checklist.getDuration();
                    }
                } else getChecklist();
                checklistAdapter.notifyDataSetChanged();
            }
        });
    }

    private void setIncomplete(Checklist checklist) {
        if (checklistDurationList.isEmpty() || checklistDurationList.get(0).getType() != ChecklistDuration.TYPE_INCOMPLETE) {
            List<Checklist> checklistList = new ArrayList<>();
            checklistList.add(checklist);
            checklistDurationList.add(0, new ChecklistDuration(ChecklistDuration.TYPE_INCOMPLETE, getString(R.string.txt_incomplete), checklistList));
        } else {
            ChecklistDuration checklistDuration = checklistDurationList.get(0);
            List<Checklist> checklistList = checklistDuration.getChecklistList();
            checklistList.add(checklist);
        }
    }

    private void setCompleted(Checklist checklist) {
        if (checklistDurationList.isEmpty() || checklistDurationList.get(checklistDurationList.size() - 1).getType() != ChecklistDuration.TYPE_COMPLETED) {
            List<Checklist> checklistList = new ArrayList<>();
            checklistList.add(checklist);
            checklistDurationList.add(new ChecklistDuration(ChecklistDuration.TYPE_COMPLETED, getString(R.string.txt_completed), checklistList));
        } else {
            ChecklistDuration checklistDuration = checklistDurationList.get(checklistDurationList.size() - 1);
            List<Checklist> checklistList = checklistDuration.getChecklistList();
            checklistList.add(checklist);
        }
    }

    private void setNormal(int duration, long weddingDuration, Checklist checklist) {
        int checklistDurationPosition;
        if (!checklistDurationList.isEmpty() &&
                checklistDurationList.get(checklistDurationList.size() - 1).getType() == ChecklistDuration.TYPE_COMPLETED)
            checklistDurationPosition = checklistDurationList.size() - 1;
        else
            checklistDurationPosition = checklistDurationList.size();
        if (duration == checklist.getDuration()) {
            ChecklistDuration checklistDuration = checklistDurationList.get(checklistDurationPosition - 1);
            List<Checklist> checklistList = checklistDuration.getChecklistList();
            checklistList.add(checklist);
        } else {
            List<Checklist> checklistList = new ArrayList<>();
            checklistList.add(checklist);
            checklistDurationList.add(checklistDurationPosition, new ChecklistDuration(getDuration(checklist.getDuration(), weddingDuration), checklistList));
        }
    }

    private void getChecklist() {
        Call<List<Checklist>> call = getApiService().getChecklist(preference.getUserToken(), preference.getWeddingId());
        call.enqueue(new Callback<List<Checklist>>() {
            @Override
            public void onResponse(@NonNull Call<List<Checklist>> call, @NonNull Response<List<Checklist>> response) {
                refreshLayout.setRefreshing(false);
                List<Checklist> checklistListResponse = response.body();
                if (response.isSuccessful() && checklistListResponse != null) {
                    new AddChecklistListTask(context, checklistListResponse).execute();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Checklist>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void updateChecklist() {
        setLoading(true);
        Call<Data> call = getApiService().updateChecklist(preference.getUserToken(), preference.getWeddingId(), new UpdateChecklist(idList));
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    Toast.makeText(context, dataResponse.getData(), Toast.LENGTH_SHORT).show();
                    List<Checklist> checklistList = new ArrayList<>();
                    for (ChecklistDuration checklistDuration : checklistDurationList) {
                        checklistList.addAll(checklistDuration.getChecklistList());
                    }
                    new AddChecklistListTask(context, checklistList).execute();
                    buttonSave.setVisibility(View.GONE);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonSave.setVisibility(loading ? View.GONE : View.VISIBLE);
    }

    private class LaunchVendorActivityTask extends AsyncTask<Void, Void, Void> {

        private int category;

        LaunchVendorActivityTask(int category) {
            this.category = category;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(EXTRA_VENDOR, getAppDatabase(context).generalDao().getVendorCategory(category));
            launchWithBundle(context, VendorActivity.class, bundle);
            return null;
        }
    }
}
