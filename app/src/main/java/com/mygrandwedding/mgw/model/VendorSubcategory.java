package com.mygrandwedding.mgw.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 05/09/17
 */

@Entity(primaryKeys = {"id", "category"})
public class VendorSubcategory implements Parcelable {

    public static final Creator<VendorSubcategory> CREATOR = new Creator<VendorSubcategory>() {
        @Override
        public VendorSubcategory createFromParcel(Parcel in) {
            return new VendorSubcategory(in);
        }

        @Override
        public VendorSubcategory[] newArray(int size) {
            return new VendorSubcategory[size];
        }
    };

    @SerializedName("id")
    private int id;
    @SerializedName("category")
    private int category;
    @SerializedName("name")
    private String name;

    public VendorSubcategory(int id, int category, String name) {
        this.id = id;
        this.category = category;
        this.name = name;
    }

    @Ignore
    protected VendorSubcategory(Parcel in) {
        id = in.readInt();
        category = in.readInt();
        name = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(category);
        dest.writeString(name);
    }
}
