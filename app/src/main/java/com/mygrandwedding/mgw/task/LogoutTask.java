package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.SplashActivity;
import com.mygrandwedding.mgw.app.MyPreference;

import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class LogoutTask extends AsyncTask<Void, Void, Void> {

    private Context context;

    public LogoutTask(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getAppDatabase(context).deleteDao().deleteUser();
        getAppDatabase(context).deleteDao().deleteWedding();
        getAppDatabase(context).deleteDao().deleteLocation();
        getAppDatabase(context).deleteDao().deleteVendorCategory();
        getAppDatabase(context).deleteDao().deleteBlogCategory();
        getAppDatabase(context).deleteDao().deleteIdeaCategory();
        getAppDatabase(context).deleteDao().deleteChecklist();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        new MyPreference(context).clearUser();
        launchClearStack(context, SplashActivity.class);
    }
}
