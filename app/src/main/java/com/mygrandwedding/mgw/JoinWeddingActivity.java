package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Code;
import com.mygrandwedding.mgw.model.NotifyRefresh;
import com.mygrandwedding.mgw.model.Wedding;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_CODE;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

public class JoinWeddingActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutInvitationCode;
    private TextInputEditText editTextInvitationCode;
    private Button buttonJoinNow;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_wedding);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextInvitationCode) isValidInvitationCode();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonJoinNow) {
            processJoinWedding();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutInvitationCode = findViewById(R.id.invitation_code);
        editTextInvitationCode = findViewById(R.id.input_invitation_code);
        buttonJoinNow = findViewById(R.id.btn_join_now);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        editTextInvitationCode.setOnFocusChangeListener(this);
        buttonJoinNow.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            editTextInvitationCode.setText(bundle.getString(EXTRA_CODE));
            editTextInvitationCode.setSelection(editTextInvitationCode.getText().toString().length());
        }
    }

    private void processJoinWedding() {
        String code = editTextInvitationCode.getText().toString().trim();
        if (isValidInvitationCode(code)) {
            setLoading(true);
            joinWedding(new Code(code));
        }
    }

    private void isValidInvitationCode() {
        isValidInvitationCode(editTextInvitationCode.getText().toString().trim());
    }

    private boolean isValidInvitationCode(String name) {
        if (name.isEmpty()) {
            layoutInvitationCode.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), getString(R.string.txt_invitation_code)));
            return false;
        }
        layoutInvitationCode.setErrorEnabled(false);
        return true;
    }

    private void joinWedding(Code code) {
        Call<Wedding> call = getApiService().joinWedding(preference.getUserToken(), code);
        call.enqueue(new Callback<Wedding>() {
            @Override
            public void onResponse(@NonNull Call<Wedding> call, @NonNull Response<Wedding> response) {
                setLoading(false);
                Wedding weddingResponse = response.body();
                if (response.isSuccessful()) {
                    new AddWeddingTask(weddingResponse).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Wedding> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonJoinNow.setVisibility(loading ? View.GONE : View.VISIBLE);
    }

    private class AddWeddingTask extends AsyncTask<Void, Void, Void> {

        private Wedding wedding;

        AddWeddingTask(Wedding wedding) {
            this.wedding = wedding;
        }

        @Override
        protected Void doInBackground(Void... params) {
            getAppDatabase(context).weddingDao().insertWedding(wedding);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            EventBus.getDefault().post(new NotifyRefresh());
            Toast.makeText(context, "Joined Wedding", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
