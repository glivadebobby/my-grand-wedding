package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.AssociatedWeddingCallback;

/**
 * Created by gladwinbobby on 02/10/17
 */

public class AssociatedWeddingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView textViewTitle, textViewDescription, textViewRole;
    public ImageView imageViewRemove;
    private AssociatedWeddingCallback callback;

    public AssociatedWeddingHolder(View itemView, AssociatedWeddingCallback callback) {
        super(itemView);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        textViewDescription = itemView.findViewById(R.id.txt_description);
        textViewRole = itemView.findViewById(R.id.txt_role);
        imageViewRemove = itemView.findViewById(R.id.img_remove);
        this.callback = callback;
        imageViewRemove.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewRemove) {
            callback.onRemoveClick(getLayoutPosition());
        }
    }
}
