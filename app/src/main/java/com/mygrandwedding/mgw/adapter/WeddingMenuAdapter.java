package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.WeddingMenuCallback;
import com.mygrandwedding.mgw.holder.WeddingMenuHolder;
import com.mygrandwedding.mgw.model.Wedding;

import java.util.List;

/**
 * Created by gladwinbobby on 02/10/17
 */

public class WeddingMenuAdapter extends RecyclerView.Adapter<WeddingMenuHolder> {

    private Context context;
    private List<Wedding> weddingList;
    private WeddingMenuCallback callback;

    public WeddingMenuAdapter(Context context, List<Wedding> weddingList, WeddingMenuCallback callback) {
        this.context = context;
        this.weddingList = weddingList;
        this.callback = callback;
    }

    @Override
    public WeddingMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WeddingMenuHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_wedding, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(WeddingMenuHolder holder, int position) {
        Wedding wedding = weddingList.get(position);
        holder.selected.setVisibility(wedding.isSelected() ? View.VISIBLE : View.INVISIBLE);
        holder.textViewTitle.setText(wedding.getTitle());
        holder.textViewRole.setText(wedding.getRole());
    }

    @Override
    public int getItemCount() {
        return weddingList.size();
    }
}
