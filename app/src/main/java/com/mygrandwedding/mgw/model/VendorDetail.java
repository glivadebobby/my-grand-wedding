package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gladwinbobby on 24/09/17
 */

public class VendorDetail {

    @SerializedName("id")
    private int id;
    @SerializedName("review_count")
    private int reviewCount;
    @SerializedName("total_likes")
    private int totalLoves;
    @SerializedName("total_shortlists")
    private int totalShortlists;
    @SerializedName("is_active")
    private boolean isActive;
    @SerializedName("is_liked")
    private boolean isLoved;
    @SerializedName("is_shortlisted")
    private boolean isShortlisted;
    @SerializedName("rating")
    private Float rating;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("business_details")
    private BusinessDetail businessDetail;
    @SerializedName("my_feedback")
    private Review review;
    @SerializedName("feedback")
    private List<Review> reviewList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public int getTotalLoves() {
        return totalLoves;
    }

    public void setTotalLoves(int totalLoves) {
        this.totalLoves = totalLoves;
    }

    public int getTotalShortlists() {
        return totalShortlists;
    }

    public void setTotalShortlists(int totalShortlists) {
        this.totalShortlists = totalShortlists;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isLoved() {
        return isLoved;
    }

    public void setLoved(boolean loved) {
        isLoved = loved;
    }

    public boolean isShortlisted() {
        return isShortlisted;
    }

    public void setShortlisted(boolean shortlisted) {
        isShortlisted = shortlisted;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BusinessDetail getBusinessDetail() {
        return businessDetail;
    }

    public void setBusinessDetail(BusinessDetail businessDetail) {
        this.businessDetail = businessDetail;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }
}
