package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.MyBudgetCallback;
import com.mygrandwedding.mgw.holder.MyBudgetCostHolder;
import com.mygrandwedding.mgw.holder.MyBudgetHolder;
import com.mygrandwedding.mgw.holder.VenueTypeHolder;
import com.mygrandwedding.mgw.model.MyBudget;

import java.util.List;
import java.util.Locale;

import static com.mygrandwedding.mgw.util.MyData.FOOD_COST;
import static com.mygrandwedding.mgw.util.MyData.HOTEL_RESORT;
import static com.mygrandwedding.mgw.util.MyData.MANDAPAM;
import static com.mygrandwedding.mgw.util.MyParser.parseNull;

/**
 * Created by gladwinbobby on 25/12/17
 */

public class MyBudgetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_VENUE_TYPE = 1;
    public static final int TYPE_MY_BUDGET_COST = 2;
    public static final int TYPE_MY_BUDGET = 3;
    private Context context;
    private List<MyBudget> myBudgetList;
    private MyBudgetCallback callback;

    public MyBudgetAdapter(Context context, List<MyBudget> myBudgetList, MyBudgetCallback callback) {
        this.context = context;
        this.myBudgetList = myBudgetList;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case TYPE_VENUE_TYPE:
                return new VenueTypeHolder(inflater.inflate(R.layout.item_venue_type, parent, false), callback);
            case TYPE_MY_BUDGET_COST:
                return new MyBudgetCostHolder(inflater.inflate(R.layout.item_my_budget_cost, parent, false), callback);
            case TYPE_MY_BUDGET:
                return new MyBudgetHolder(inflater.inflate(R.layout.item_my_budget, parent, false), callback);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_VENUE_TYPE:
                bindVenueTypeHolder((VenueTypeHolder) holder, position);
                break;
            case TYPE_MY_BUDGET_COST:
                bindMyBudgetCostHolder((MyBudgetCostHolder) holder, position);
                break;
            case TYPE_MY_BUDGET:
                bindMyBudgetHolder((MyBudgetHolder) holder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return myBudgetList.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return myBudgetList.size();
    }

    private void bindVenueTypeHolder(VenueTypeHolder holder, int position) {
        MyBudget myBudget = myBudgetList.get(position);
        holder.textViewVenueType.setText(myBudget.getTitle());
    }

    private void bindMyBudgetCostHolder(MyBudgetCostHolder holder, int position) {
        MyBudget myBudget = myBudgetList.get(position);
        holder.imageViewCategory.setImageResource(myBudget.getIcon());
        holder.textViewCategory.setText(myBudget.getTitle());
        holder.textViewNoOf.setText(String.format(Locale.getDefault(), context.getString(R.string.format_no_of), myBudget.getNoOf()));
        holder.textViewCount.setText(parseNull(myBudget.getCount()));
        holder.textViewCostPer.setText(String.format(Locale.getDefault(), context.getString(R.string.format_cost_per), myBudget.getCostPer()));
        holder.textViewCost.setText(parseNull(myBudget.getPerItemCost()));
        if (myBudget.getSubCategory() != null && myBudget.getSubCategory() == HOTEL_RESORT && myBudget.getCategory() == FOOD_COST)
            holder.textViewCategory.setText(getVenueText());
        if (myBudget.getSubCategory() != null && (myBudget.getSubCategory() == MANDAPAM || myBudget.getSubCategory() == HOTEL_RESORT))
            holder.textViewPrice.setText(parseNull(myBudget.getFoodCost()));
        else holder.textViewPrice.setText(parseNull(myBudget.getTotalCost()));
    }

    private void bindMyBudgetHolder(MyBudgetHolder holder, int position) {
        MyBudget myBudget = myBudgetList.get(position);
        holder.imageViewCategory.setImageResource(myBudget.getIcon());
        holder.textViewCategory.setText(myBudget.getTitle());
        holder.textViewPrice.setText(parseNull(myBudget.getTotalCost()));
    }

    private SpannableString getVenueText() {
        SpannableString spannableString = new SpannableString("Food Cost\n(Venue is complementary)");
        spannableString.setSpan(new RelativeSizeSpan(0.8f), 10, spannableString.length(), 0);
        return spannableString;
    }
}
