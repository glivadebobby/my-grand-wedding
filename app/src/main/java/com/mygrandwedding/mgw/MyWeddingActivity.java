package com.mygrandwedding.mgw;

import android.app.DatePickerDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.MyWedding;
import com.mygrandwedding.mgw.model.NotifyRefresh;
import com.mygrandwedding.mgw.model.Wedding;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;
import static com.mygrandwedding.mgw.util.DateParser.getDate;
import static com.mygrandwedding.mgw.util.DateParser.getDayOfMonth;
import static com.mygrandwedding.mgw.util.DateParser.getFormattedDate;
import static com.mygrandwedding.mgw.util.DateParser.getMonth;
import static com.mygrandwedding.mgw.util.DateParser.getYear;

public class MyWeddingActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextView textViewGroom, textViewBride;
    private TextInputLayout layoutWeddingTitle, layoutWeddingLocation, layoutWeddingDate;
    private TextInputEditText editTextWeddingTitle, editTextWeddingLocation, editTextWeddingDate;
    private Button buttonSave;
    private FrameLayout layoutLoading;
    private MyPreference preference;
    private Wedding wedding;
    private boolean bride;
    private String weddingDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wedding);
        initObjects();
        initToolbar();
        initCallbacks();
        setBride();
        loadMyWedding();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextWeddingTitle) isValidTitle();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == textViewGroom) {
            bride = false;
            setBride();
        } else if (view == textViewBride) {
            bride = true;
            setBride();
        } else if (view == editTextWeddingDate) {
            promptDatePickerDialog();
        } else if (view == buttonSave) {
            processMyWedding();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        textViewGroom = findViewById(R.id.txt_groom);
        textViewBride = findViewById(R.id.txt_bride);
        layoutWeddingTitle = findViewById(R.id.wedding_title);
        layoutWeddingLocation = findViewById(R.id.wedding_location);
        layoutWeddingDate = findViewById(R.id.wedding_date);
        editTextWeddingTitle = findViewById(R.id.input_wedding_title);
        editTextWeddingLocation = findViewById(R.id.input_wedding_location);
        editTextWeddingDate = findViewById(R.id.input_wedding_date);
        buttonSave = findViewById(R.id.btn_save);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        textViewGroom.setOnClickListener(this);
        textViewBride.setOnClickListener(this);
        editTextWeddingTitle.setOnFocusChangeListener(this);
        editTextWeddingLocation.setOnFocusChangeListener(this);
        editTextWeddingDate.setOnClickListener(this);
        buttonSave.setOnClickListener(this);
    }

    private void loadMyWedding() {
        LiveData<Wedding> data = getAppDatabase(context).weddingDao().getMyWedding(preference.getId());
        data.observe(this, new Observer<Wedding>() {
            @Override
            public void onChanged(@Nullable Wedding wedding) {
                MyWeddingActivity.this.wedding = wedding;
                if (wedding != null) {
                    bride = wedding.isBride(preference.getId());
                    setBride();
                    editTextWeddingTitle.setText(wedding.getTitle());
                    editTextWeddingTitle.setSelection(editTextWeddingTitle.getText().length());
                    editTextWeddingLocation.setText(wedding.getLocation());
                    weddingDate = wedding.getDate();
                    editTextWeddingDate.setText(getFormattedDate(weddingDate));
                }
            }
        });
    }

    private void setBride() {
        if (bride) {
            textViewGroom.setTextColor(ContextCompat.getColor(context, R.color.secondary_txt));
            textViewBride.setTextColor(ContextCompat.getColor(context, R.color.primary));
            textViewGroom.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_groom, 0, 0);
            textViewBride.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_bride_selected, 0, 0);
        } else {
            textViewGroom.setTextColor(ContextCompat.getColor(context, R.color.primary));
            textViewBride.setTextColor(ContextCompat.getColor(context, R.color.secondary_txt));
            textViewGroom.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_groom_selected, 0, 0);
            textViewBride.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_bride, 0, 0);
        }
    }

    private void promptDatePickerDialog() {
        int year = getYear(weddingDate);
        int month = getMonth(weddingDate);
        int dayOfMonth = getDayOfMonth(weddingDate);
        openDatePickerDialog(year, month, dayOfMonth);
    }

    private void openDatePickerDialog(int year, int month, int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                weddingDate = getDate(year, month, dayOfMonth);
                editTextWeddingDate.setText(getFormattedDate(year, month, dayOfMonth));
            }
        }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void processMyWedding() {
        String title = editTextWeddingTitle.getText().toString().trim();
        String location = editTextWeddingLocation.getText().toString().trim();
        if (isValidTitle(title) && isValidLocation() && isValidDate()) {
            setLoading(true);
            myWedding(new MyWedding(preference.getId(), bride, title, location, weddingDate));
        }
    }

    private void isValidTitle() {
        isValidTitle(editTextWeddingTitle.getText().toString().trim());
    }

    private boolean isValidTitle(String title) {
        if (title.isEmpty()) {
            layoutWeddingTitle.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), getString(R.string.txt_wedding_title)));
            return false;
        }
        layoutWeddingTitle.setErrorEnabled(false);
        return true;
    }

    private boolean isValidLocation() {
        return isValidLocation(editTextWeddingLocation.getText().toString().trim());
    }

    private boolean isValidLocation(String location) {
        if (location.isEmpty()) {
            Toast.makeText(context, String.format(Locale.getDefault(), getString(R.string.error_empty),
                    getString(R.string.hint_location)), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean isValidDate() {
        return isValidDate(editTextWeddingDate.getText().toString().trim());
    }

    private boolean isValidDate(String date) {
        if (date.isEmpty()) {
            layoutWeddingDate.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), getString(R.string.txt_wedding_date)));
            return false;
        }
        return true;
    }

    private void myWedding(MyWedding myWedding) {
        Call<Wedding> call;
        if (wedding == null)
            call = getApiService().createWedding(preference.getUserToken(), myWedding);
        else {
            MyWedding updateWedding = new MyWedding(myWedding.getTitle(), myWedding.getLocation(), myWedding.getDate());
            if (bride) updateWedding.setBride(preference.getId());
            else updateWedding.setGroom(preference.getId());
            call = getApiService().updateWedding(preference.getUserToken(), wedding.getId(), updateWedding);
        }
        call.enqueue(new Callback<Wedding>() {
            @Override
            public void onResponse(@NonNull Call<Wedding> call, @NonNull Response<Wedding> response) {
                setLoading(false);
                Wedding weddingResponse = response.body();
                if (response.isSuccessful() && weddingResponse != null) {
                    if (wedding == null) new AddWeddingTask(weddingResponse).execute();
                    else new UpdateWeddingTask(weddingResponse).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Wedding> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonSave.setVisibility(loading ? View.GONE : View.VISIBLE);
    }

    private class AddWeddingTask extends AsyncTask<Void, Void, Void> {

        private Wedding wedding;

        AddWeddingTask(Wedding wedding) {
            this.wedding = wedding;
        }

        @Override
        protected Void doInBackground(Void... params) {
            getAppDatabase(context).weddingDao().insertWedding(wedding);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            EventBus.getDefault().post(new NotifyRefresh());
            finish();
        }
    }

    private class UpdateWeddingTask extends AsyncTask<Void, Void, Void> {

        private Wedding wedding;

        UpdateWeddingTask(Wedding wedding) {
            this.wedding = wedding;
        }

        @Override
        protected Void doInBackground(Void... params) {
            getAppDatabase(context).weddingDao().updateWedding(wedding);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            EventBus.getDefault().post(new NotifyRefresh());
            finish();
        }
    }
}
