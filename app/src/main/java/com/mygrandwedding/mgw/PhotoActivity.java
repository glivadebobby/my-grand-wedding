package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.mygrandwedding.mgw.adapter.ImageAdapter;
import com.mygrandwedding.mgw.callback.PhotoCallback;
import com.mygrandwedding.mgw.model.Image;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_PHOTOS;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_POSITION;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class PhotoActivity extends AppCompatActivity implements PhotoCallback {

    private Context context;
    private Toolbar toolbar;
    private RecyclerView viewPhotos;
    private List<Image> imageList;
    private ImageAdapter imageAdapter;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        initObjects();
        initToolbar();
        initRecyclerView();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPhotoClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, id);
        bundle.putInt(EXTRA_POSITION, position);
        bundle.putString(EXTRA_TITLE, getIntent().getStringExtra(EXTRA_TITLE));
        bundle.putParcelableArrayList(EXTRA_PHOTOS, getIntent().getParcelableArrayListExtra(EXTRA_PHOTOS));
        launchWithBundle(context, PhotoDetailActivity.class, bundle);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        viewPhotos = findViewById(R.id.photos);

        context = this;
        imageList = new ArrayList<>();
        imageAdapter = new ImageAdapter(context, imageList, this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(String.format(Locale.getDefault(),
                        getString(R.string.format_images), bundle.getString(EXTRA_TITLE)));
            ArrayList<Image> images = bundle.getParcelableArrayList(EXTRA_PHOTOS);
            if (images != null) imageList.addAll(images);
            imageAdapter.notifyDataSetChanged();
        }
    }

    private void initRecyclerView() {
        viewPhotos.setLayoutManager(new GridLayoutManager(context, 2));
        viewPhotos.setAdapter(imageAdapter);
    }
}
