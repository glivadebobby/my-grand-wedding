package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;

/**
 * Created by gladwinbobby on 11/10/17
 */

public class FaqHolder extends RecyclerView.ViewHolder {

    public TextView textViewQuestion, textViewAnswer;

    public FaqHolder(View itemView) {
        super(itemView);
        textViewQuestion = itemView.findViewById(R.id.txt_question);
        textViewAnswer = itemView.findViewById(R.id.txt_answer);
    }
}
