package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 30/09/17
 */

public class UpdateProfile {

    @SerializedName("first_name")
    private String firstName;
    @SerializedName("gender")
    private String gender;
    @SerializedName("dob")
    private String dob;

    public UpdateProfile(String firstName, String gender, String dob) {
        this.firstName = firstName;
        this.gender = gender;
        this.dob = dob;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
