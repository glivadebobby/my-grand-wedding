package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 12/09/17
 */

public class ForgotPassword {

    @SerializedName("role")
    private int role;
    @SerializedName("mobile_number")
    private String mobileNumber;

    public ForgotPassword(int role, String mobileNumber) {
        this.role = role;
        this.mobileNumber = mobileNumber;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
