package com.mygrandwedding.mgw.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.mygrandwedding.mgw.model.Checklist;
import com.mygrandwedding.mgw.model.Wedding;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by gladwinbobby on 02/10/17
 */

@Dao
public interface WeddingDao {
    @Insert(onConflict = REPLACE)
    void insertWedding(Wedding wedding);

    @Insert(onConflict = REPLACE)
    void insertWeddingList(List<Wedding> weddingList);

    @Insert(onConflict = REPLACE)
    void insertChecklistList(List<Checklist> checklistList);

    @Query("SELECT * FROM Wedding")
    LiveData<List<Wedding>> getAssociatedWeddings();

    @Query("SELECT * FROM Wedding WHERE groom =:userId OR bride =:userId")
    LiveData<Wedding> getMyWedding(int userId);

    @Query("SELECT * FROM Wedding WHERE id =:weddingId")
    LiveData<Wedding> getSelectedWedding(int weddingId);

    @Query("SELECT * FROM Checklist ORDER BY duration")
    LiveData<List<Checklist>> getChecklist();

    @Update
    void updateWedding(Wedding wedding);

    @Delete
    void deleteWedding(Wedding wedding);
}
