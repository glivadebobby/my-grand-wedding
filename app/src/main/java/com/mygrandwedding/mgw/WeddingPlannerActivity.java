package com.mygrandwedding.mgw;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.mygrandwedding.mgw.adapter.VendorCategoryMgwAdapter;
import com.mygrandwedding.mgw.adapter.WpPhotoAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.CategoryCallback;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.task.AddVendorCategoryListTask;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.MGW_CONTACT;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchDialPad;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

public class WeddingPlannerActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, CategoryCallback {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout layoutWeddingPlanner;
    private RecyclerView viewPhotos, viewCategories;
    private FloatingActionButton buttonCall, buttonSpecialRate;
    private List<Integer> photoList;
    private List<VendorCategory> vendorCategoryList;
    private WpPhotoAdapter photoAdapter;
    private VendorCategoryMgwAdapter categoryAdapter;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wedding_planner);
        initObjects();
        initToolbar();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        loadPhotos();
        loadVendorCategories();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonCall) {
            launchDialPad(context, MGW_CONTACT);
        } else if (view == buttonSpecialRate) {
            launch(context, GetQuoteActivity.class);
        }
    }

    @Override
    public void onRefresh() {
        getCategories();
    }

    @Override
    public void onCategoryClick(int position) {

    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        layoutWeddingPlanner = findViewById(R.id.wedding_planner);
        viewPhotos = findViewById(R.id.photos);
        viewCategories = findViewById(R.id.categories);
        buttonCall = findViewById(R.id.fab_call);
        buttonSpecialRate = findViewById(R.id.fab_special_rate);

        context = this;
        photoList = new ArrayList<>();
        vendorCategoryList = new ArrayList<>();
        photoAdapter = new WpPhotoAdapter(context, photoList);
        categoryAdapter = new VendorCategoryMgwAdapter(context, vendorCategoryList, this);
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        buttonCall.setOnClickListener(this);
        buttonSpecialRate.setOnClickListener(this);
    }

    private void initRecyclerView() {
        viewPhotos.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        viewPhotos.setAdapter(photoAdapter);
        viewPhotos.setNestedScrollingEnabled(false);

        viewCategories.setLayoutManager(new GridLayoutManager(context, 2));
        viewCategories.setAdapter(categoryAdapter);
        viewCategories.setNestedScrollingEnabled(false);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
    }

    private void loadPhotos() {
        photoList.clear();
        photoList.add(R.drawable.wp1);
        photoList.add(R.drawable.wp2);
        photoList.add(R.drawable.wp3);
        photoList.add(R.drawable.wp4);
        photoList.add(R.drawable.wp5);
        photoList.add(R.drawable.wp6);
        photoList.add(R.drawable.wp7);
        photoList.add(R.drawable.wp8);
        photoList.add(R.drawable.wp9);
        photoList.add(R.drawable.wp10);
        photoList.add(R.drawable.wp11);
        photoAdapter.notifyDataSetChanged();
    }

    private void loadVendorCategories() {
        LiveData<List<VendorCategory>> data = getAppDatabase(context).generalDao().getVendorCategories();
        data.observe(this, new Observer<List<VendorCategory>>() {
            @Override
            public void onChanged(@Nullable List<VendorCategory> vendorCategories) {
                vendorCategoryList.clear();
                if (vendorCategories != null && !vendorCategories.isEmpty()) {
                    vendorCategoryList.addAll(vendorCategories);
                    refreshLayout.setEnabled(false);
                    layoutWeddingPlanner.setVisibility(View.VISIBLE);
                } else {
                    refreshLayout.setRefreshing(true);
                    getCategories();
                }
                categoryAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getCategories() {
        Call<List<VendorCategory>> call = getApiService()
                .getVendorCategory(preference.getCityId() > 0 ? preference.getCityId() : null);
        call.enqueue(new Callback<List<VendorCategory>>() {
            @Override
            public void onResponse(@NonNull Call<List<VendorCategory>> call,
                                   @NonNull Response<List<VendorCategory>> response) {
                refreshLayout.setRefreshing(false);
                List<VendorCategory> categoryListResponse = response.body();
                if (response.isSuccessful() && categoryListResponse != null) {
                    new AddVendorCategoryListTask(context, categoryListResponse).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<List<VendorCategory>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
