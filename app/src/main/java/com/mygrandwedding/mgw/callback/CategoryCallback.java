package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 05/09/17
 */

public interface CategoryCallback {
    void onCategoryClick(int position);
}
