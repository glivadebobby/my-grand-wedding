package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.ForgotPassword;
import com.mygrandwedding.mgw.model.UpdateUsername;
import com.mygrandwedding.mgw.model.User;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.R.id.phone;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

public class ChangePhoneOtpActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutOtp;
    private TextInputEditText editTextOtp;
    private Button buttonSubmit;
    private TextView textViewSendOtp;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_phone_otp);
        initObjects();
        initToolbar();
        initCallbacks();
        setPasswordFont();
        setSendOtpText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            processChangePhoneOtp();
        } else if (view == textViewSendOtp) {
            setLoading(true);
            sendOtp();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutOtp = findViewById(R.id.otp);
        editTextOtp = findViewById(R.id.input_otp);
        buttonSubmit = findViewById(R.id.btn_submit);
        textViewSendOtp = findViewById(R.id.txt_send_otp);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        buttonSubmit.setOnClickListener(this);
        textViewSendOtp.setOnClickListener(this);
    }

    private void setPasswordFont() {
        layoutOtp.setTypeface(textViewSendOtp.getTypeface());
        editTextOtp.setTypeface(textViewSendOtp.getTypeface());
        editTextOtp.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void setSendOtpText() {
        SpannableString spannableString = new SpannableString(getString(R.string.txt_send_otp));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_txt)), 20, 30, 0);
        spannableString.setSpan(new RelativeSizeSpan(1.05f), 20, 30, 0);
        textViewSendOtp.setText(spannableString);
    }

    private void processChangePhoneOtp() {
        String otp = editTextOtp.getText().toString().trim();
        if (isValidOtp(otp)) {
            setLoading(true);
            updateUsername(new UpdateUsername(preference.getPhone(), otp));
        }
    }

    private boolean isValidOtp(String otp) {
        if (otp.isEmpty()) {
            layoutOtp.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutOtp.getHint()));
            return false;
        } else if (otp.length() < 6) {
            layoutOtp.setError(String.format(Locale.getDefault(), getString(R.string.error_length), layoutOtp.getHint(), 6));
            return false;
        }
        layoutOtp.setErrorEnabled(false);
        return true;
    }

    private void updateUsername(UpdateUsername updateUsername) {
        Call<Data> call = getApiService().updateUsername(preference.getUserToken(), updateUsername);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    Toast.makeText(context, dataResponse.getData(), Toast.LENGTH_SHORT).show();
                    new UpdateUserTask().execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void sendOtp() {
        Call<Void> call = getApiService().sendOtp(new ForgotPassword(preference.getRole(), preference.getPhone()));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "OTP code sent again to " + phone, Toast.LENGTH_SHORT).show();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private class UpdateUserTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            User user = getAppDatabase(context).userDao().getUserData(preference.getId());
            user.setUsername(preference.getPhone());
            getAppDatabase(context).userDao().updateUser(user);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            launchClearStack(context, MainActivity.class);
        }
    }
}
