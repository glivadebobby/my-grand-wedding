package com.mygrandwedding.mgw;

import android.app.DatePickerDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.CreateQuote;
import com.mygrandwedding.mgw.model.Location;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.task.AddLocationListTask;
import com.mygrandwedding.mgw.task.AddVendorCategoryListTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.CHENNAI_CITY_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;
import static com.mygrandwedding.mgw.util.DateParser.getDate;
import static com.mygrandwedding.mgw.util.DateParser.getDayOfMonth;
import static com.mygrandwedding.mgw.util.DateParser.getFormattedDate;
import static com.mygrandwedding.mgw.util.DateParser.getMonth;
import static com.mygrandwedding.mgw.util.DateParser.getYear;

public class GetQuoteActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout layoutQuote;
    private TextInputLayout layoutName, layoutEmail, layoutPhone, layoutLocation, layoutFunctionDate, layoutMessage;
    private TextInputEditText editTextName, editTextEmail, editTextPhone, editTextFunctionDate, editTextMessage;
    private AutoCompleteTextView textViewLocation;
    private Spinner spinnerRequirement;
    private TextView textViewRequirement, textViewQuoteGuest;
    private Button buttonSubmit;
    private FrameLayout layoutLoading;
    private List<Location> locationList;
    private List<VendorCategory> vendorCategoryList;
    private ArrayAdapter<Location> locationArrayAdapter;
    private ArrayAdapter<VendorCategory> categoryArrayAdapter;
    private MyPreference preference;
    private Integer id;
    private String vendorName;
    private String weddingDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_quote);
        initObjects();
        initToolbar();
        initCallbacks();
        initSpinner();
        initAutocomplete();
        processBundle();
        initRefresh();
        loadLocations();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == editTextFunctionDate) {
            promptDatePickerDialog();
        } else if (view == textViewQuoteGuest) {
            displayGuestQuote();
        } else if (view == buttonSubmit) {
            processGetQuote();
        }
    }

    @Override
    public void onRefresh() {
        getLocations();
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextName) isValidName();
            if (view == editTextEmail) isValidEmail();
            if (view == editTextPhone) isValidPhone();
            if (view == textViewLocation) isValidLocation();
            if (view == editTextMessage) isValidMessage();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        layoutQuote = findViewById(R.id.quote);
        layoutName = findViewById(R.id.name);
        layoutEmail = findViewById(R.id.email);
        layoutPhone = findViewById(R.id.phone);
        layoutLocation = findViewById(R.id.location);
        layoutFunctionDate = findViewById(R.id.function_date);
        layoutMessage = findViewById(R.id.message);
        editTextName = findViewById(R.id.input_name);
        editTextEmail = findViewById(R.id.input_email);
        editTextPhone = findViewById(R.id.input_phone);
        editTextFunctionDate = findViewById(R.id.input_function_date);
        editTextMessage = findViewById(R.id.input_message);
        textViewLocation = findViewById(R.id.input_location);
        spinnerRequirement = findViewById(R.id.spin_requirement);
        textViewRequirement = findViewById(R.id.txt_requirement);
        textViewQuoteGuest = findViewById(R.id.txt_quote_guest);
        buttonSubmit = findViewById(R.id.btn_submit);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        locationList = new ArrayList<>();
        vendorCategoryList = new ArrayList<>();
        locationArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, locationList);
        categoryArrayAdapter = new ArrayAdapter<>(context, R.layout.item_spinner, vendorCategoryList);
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        editTextName.setOnFocusChangeListener(this);
        editTextEmail.setOnFocusChangeListener(this);
        editTextPhone.setOnFocusChangeListener(this);
        textViewLocation.setOnFocusChangeListener(this);
        editTextFunctionDate.setOnClickListener(this);
        editTextMessage.setOnFocusChangeListener(this);
        textViewQuoteGuest.setOnClickListener(this);
        buttonSubmit.setOnClickListener(this);
    }

    private void initSpinner() {
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerRequirement.setAdapter(categoryArrayAdapter);
    }

    private void initAutocomplete() {
        textViewLocation.setThreshold(1);
        textViewLocation.setAdapter(locationArrayAdapter);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(EXTRA_ID) && bundle.containsKey(EXTRA_VENDOR)) {
            id = bundle.getInt(EXTRA_ID);
            vendorName = bundle.getString(EXTRA_VENDOR);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(String.format(Locale.getDefault(),
                        getString(R.string.format_get_quote), vendorName));
            }
            textViewRequirement.setVisibility(View.GONE);
            spinnerRequirement.setVisibility(View.GONE);
        }
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
    }

    private void loadLocations() {
        LiveData<List<Location>> data = getAppDatabase(context).generalDao().getLocations();
        data.observe(this, new Observer<List<Location>>() {
            @Override
            public void onChanged(@Nullable List<Location> locations) {
                locationList.clear();
                if (locations != null && !locations.isEmpty()) {
                    locationList.addAll(locations);
                    locationArrayAdapter.notifyDataSetChanged();
                    for (int i = 0; i < locationList.size(); i++) {
                        Location location = locationList.get(i);
                        if (location.getId() == preference.getCityId()) {
                            textViewLocation.setText(location.getLocation());
                        }
                    }
                    loadVendorCategories();
                } else {
                    refreshLayout.setRefreshing(true);
                    getLocations();
                }
                locationArrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private void loadVendorCategories() {
        LiveData<List<VendorCategory>> data = getAppDatabase(context).generalDao().getVendorCategories();
        data.observe(this, new Observer<List<VendorCategory>>() {
            @Override
            public void onChanged(@Nullable List<VendorCategory> vendorCategories) {
                vendorCategoryList.clear();
                if (vendorCategories != null && !vendorCategories.isEmpty()) {
                    vendorCategoryList.addAll(vendorCategories);
                    if (preference.getCityId() == CHENNAI_CITY_ID) {
                        vendorCategoryList.add(0, new VendorCategory(0, 0, R.drawable.bg_wedding_planners, "Wedding Planners"));
                    }
                    categoryArrayAdapter.notifyDataSetChanged();
                    refreshLayout.setEnabled(false);
                    layoutQuote.setVisibility(View.VISIBLE);
                    buttonSubmit.setVisibility(View.VISIBLE);
                    if (preference.getUserToken() == null) displayGuestQuote();
                } else {
                    refreshLayout.setRefreshing(true);
                    getCategories();
                }
                categoryArrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private void processGetQuote() {
        String name = editTextName.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String phone = editTextPhone.getText().toString().trim();
        String location = textViewLocation.getText().toString().trim();
        String requirement = id == null ?
                vendorCategoryList.get(spinnerRequirement.getSelectedItemPosition()).getCategory()
                : "Quote Request - " + vendorName;
        String message = editTextMessage.getText().toString().trim();
        if (isValidName(name) && isValidEmail(email) && isValidPhone(phone) && isValidLocation(location) && isValidMessage(message) && isValidFunctionDate()) {
            setLoading(true);
            createQuote(new CreateQuote(id, name.isEmpty() ? null : name, email.isEmpty() ? null
                    : email, phone.isEmpty() ? null : phone, location, requirement,
                    message.isEmpty() ? null : message, weddingDate));
        }
    }

    private void isValidName() {
        isValidName(editTextName.getText().toString().trim());
    }

    private boolean isValidName(String name) {
        if (textViewQuoteGuest.getVisibility() == View.VISIBLE) return true;
        if (name.isEmpty()) {
            layoutName.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutName.getHint()));
            return false;
        }
        layoutName.setErrorEnabled(false);
        return true;
    }

    private void isValidEmail() {
        isValidEmail(editTextEmail.getText().toString().trim());
    }

    private boolean isValidEmail(String email) {
        if (textViewQuoteGuest.getVisibility() == View.VISIBLE) return true;
        if (!email.isEmpty() && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            layoutEmail.setError(String.format(Locale.getDefault(), getString(R.string.error_invalid), layoutEmail.getHint()));
            return false;
        }
        layoutEmail.setErrorEnabled(false);
        return true;
    }

    private void isValidPhone() {
        isValidPhone(editTextPhone.getText().toString().trim());
    }

    private boolean isValidPhone(String phone) {
        if (textViewQuoteGuest.getVisibility() == View.VISIBLE) return true;
        if (phone.isEmpty()) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPhone.getHint()));
            return false;
        } else if (phone.length() < 10) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_length), layoutPhone.getHint(), 10));
            return false;
        }
        layoutPhone.setErrorEnabled(false);
        return true;
    }

    private void isValidLocation() {
        isValidLocation(textViewLocation.getText().toString().trim());
    }

    private boolean isValidLocation(String location) {
        if (location.isEmpty()) {
            layoutLocation.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutLocation.getHint()));
            return false;
        }
        layoutLocation.setErrorEnabled(false);
        return true;
    }

    private boolean isValidFunctionDate() {
        return isValidFunctionDate(editTextFunctionDate.getText().toString().trim());
    }

    private boolean isValidFunctionDate(String functionDate) {
        if (functionDate.isEmpty()) {
            layoutFunctionDate.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), "Function Date"));
            return false;
        }
        layoutFunctionDate.setErrorEnabled(false);
        return true;
    }

    private void isValidMessage() {
        isValidMessage(editTextMessage.getText().toString().trim());
    }

    private boolean isValidMessage(String description) {
        if (description.isEmpty()) {
            layoutMessage.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutMessage.getHint()));
            return false;
        }
        layoutMessage.setErrorEnabled(false);
        return true;
    }

    private void promptDatePickerDialog() {
        int year = getYear(weddingDate);
        int month = getMonth(weddingDate);
        int dayOfMonth = getDayOfMonth(weddingDate);
        openDatePickerDialog(year, month, dayOfMonth);
    }

    private void openDatePickerDialog(int year, int month, int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                weddingDate = getDate(year, month, dayOfMonth);
                editTextFunctionDate.setText(getFormattedDate(year, month, dayOfMonth));
            }
        }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void getLocations() {
        Call<List<Location>> call = getApiService().getLocations(preference.getUserToken());
        call.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(@NonNull Call<List<Location>> call, @NonNull Response<List<Location>> response) {
                refreshLayout.setRefreshing(false);
                List<Location> locationListResponse = response.body();
                if (response.isSuccessful() && locationListResponse != null) {
                    new AddLocationListTask(context, locationListResponse).execute();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Location>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void getCategories() {
        Call<List<VendorCategory>> call = getApiService()
                .getVendorCategory(preference.getCityId() > 0 ? preference.getCityId() : null);
        call.enqueue(new Callback<List<VendorCategory>>() {
            @Override
            public void onResponse(@NonNull Call<List<VendorCategory>> call,
                                   @NonNull Response<List<VendorCategory>> response) {
                refreshLayout.setRefreshing(false);
                List<VendorCategory> categoryListResponse = response.body();
                if (response.isSuccessful() && categoryListResponse != null) {
                    new AddVendorCategoryListTask(context, categoryListResponse).execute();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<VendorCategory>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void createQuote(CreateQuote createQuote) {
        Call<CreateQuote> call = getApiService().createQuote(preference.getUserToken(), createQuote);
        call.enqueue(new Callback<CreateQuote>() {
            @Override
            public void onResponse(@NonNull Call<CreateQuote> call, @NonNull Response<CreateQuote> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "We received your Enquiry. We will get back to you shortly.", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<CreateQuote> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void displayGuestQuote() {
        layoutName.setVisibility(View.VISIBLE);
        layoutEmail.setVisibility(View.VISIBLE);
        layoutPhone.setVisibility(View.VISIBLE);
        textViewQuoteGuest.setVisibility(View.GONE);
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonSubmit.setVisibility(loading ? View.GONE : View.VISIBLE);
    }
}
