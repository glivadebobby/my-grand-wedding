package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.CheckCallback;
import com.mygrandwedding.mgw.callback.ChecklistCallback;
import com.mygrandwedding.mgw.holder.ChecklistHolder;
import com.mygrandwedding.mgw.model.ChecklistDuration;

import java.util.List;

/**
 * Created by gladwinbobby on 18/10/17
 */

public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistHolder> {

    private Context context;
    private List<ChecklistDuration> checklistDurationList;
    private ChecklistCallback callback;
    private CheckCallback checkCallback;

    public ChecklistAdapter(Context context, List<ChecklistDuration> checklistDurationList, ChecklistCallback callback, CheckCallback checkCallback) {
        this.context = context;
        this.checklistDurationList = checklistDurationList;
        this.callback = callback;
        this.checkCallback = checkCallback;
    }

    @Override
    public ChecklistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChecklistHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checklist, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(ChecklistHolder holder, int position) {
        ChecklistDuration checklistDuration = checklistDurationList.get(position);
        holder.textViewDuration.setText(checklistDuration.getDuration());
        holder.textViewDuration.setCompoundDrawablesWithIntrinsicBounds(0, 0,
                checklistDuration.isSelected() ? R.drawable.ic_collapse : R.drawable.ic_expand, 0);
        holder.viewCheck.setVisibility(checklistDuration.isSelected() ? View.VISIBLE : View.GONE);
        holder.viewCheck.setLayoutManager(new LinearLayoutManager(context));
        holder.viewCheck.setAdapter(new CheckAdapter(checklistDuration.getChecklistList(), checkCallback, holder.getLayoutPosition()));
        holder.viewCheck.setNestedScrollingEnabled(false);
    }

    @Override
    public int getItemCount() {
        return checklistDurationList.size();
    }
}
