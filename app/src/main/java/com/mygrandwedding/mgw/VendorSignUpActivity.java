package com.mygrandwedding.mgw;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mygrandwedding.mgw.adapter.AddImageAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.ImageCallback;
import com.mygrandwedding.mgw.model.AddBusinessDetail;
import com.mygrandwedding.mgw.model.Category;
import com.mygrandwedding.mgw.model.Location;
import com.mygrandwedding.mgw.model.RegisterDropdown;
import com.mygrandwedding.mgw.model.User;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.MyActivity.launch;

public class VendorSignUpActivity extends AppCompatActivity implements View.OnClickListener,
        View.OnFocusChangeListener, Runnable, SwipeRefreshLayout.OnRefreshListener, ImageCallback, CompoundButton.OnCheckedChangeListener {

    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 30;
    private static final int REQUEST_IMAGES = 4;
    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout layoutSignUp;
    private TextView textViewLogin;
    private TextInputLayout layoutName, layoutDescription, layoutMinPrice, layoutMaxPrice,
            layoutRegisteredAddress, layoutRunningAddress, layoutEmail, layoutPhone, layoutPassword;
    private TextInputEditText editTextName, editTextDescription, editTextMinPrice, editTextMaxPrice,
            editTextRegisteredAddress, editTextRunningAddress, editTextEmail, editTextPhone, editTextPassword;
    private Spinner spinnerCategory, spinnerLocation;
    private CheckBox checkBoxRunningAddress;
    private RecyclerView viewImages;
    private Button buttonContinue;
    private FrameLayout layoutLoading;
    private List<Category> categoryList;
    private List<Location> locationList;
    private List<String> imageList;
    private ArrayAdapter<Category> categoryArrayAdapter;
    private ArrayAdapter<Location> locationArrayAdapter;
    private AddImageAdapter imageAdapter;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_sign_up);
        initObjects();
        initToolbar();
        initCallbacks();
        initSpinner();
        initRecyclerView();
        initRefresh();
        setPasswordFont();
        setLoginText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onDestroy() {
        EasyImage.clearConfiguration(context);
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextName) isValidName();
            if (view == editTextDescription) isValidDescription();
            if (view == editTextMinPrice) isValidMinPrice();
            if (view == editTextMaxPrice) isValidMaxPrice();
            if (view == editTextRegisteredAddress) isValidRegisteredAddress();
            if (view == editTextRunningAddress) isValidRunningAddress();
            if (view == editTextEmail) isValidEmail();
            if (view == editTextPhone) isValidPhone();
            if (view == editTextPassword) isValidPassword();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (compoundButton == checkBoxRunningAddress) {
            if (b) {
                editTextRunningAddress.setText(editTextRegisteredAddress.getText().toString().trim());
                editTextRunningAddress.setEnabled(false);
            } else {
                editTextRunningAddress.setEnabled(true);
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonContinue) processSignUp();
        else if (view == textViewLogin) finish();
    }

    @Override
    public void onRefresh() {
        getRegisterDropdown();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getRegisterDropdown();
    }

    @Override
    public void onAddClick() {
        processPickImages();
    }

    @Override
    public void onPictureClick(int position) {

    }

    @Override
    public void onRemoveClick(int position) {
        imageList.remove(position);
        imageAdapter.notifyItemRemoved(position);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                for (File file : imageFiles) {
                    imageList.add(file.getPath());
                }
                imageAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED)
                    pickImages();
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        layoutSignUp = findViewById(R.id.sign_up);
        textViewLogin = findViewById(R.id.txt_login);
        layoutName = findViewById(R.id.name);
        layoutDescription = findViewById(R.id.description);
        layoutMinPrice = findViewById(R.id.min_price);
        layoutMaxPrice = findViewById(R.id.max_price);
        layoutRegisteredAddress = findViewById(R.id.registered_address);
        layoutRunningAddress = findViewById(R.id.running_address);
        layoutEmail = findViewById(R.id.email);
        layoutPhone = findViewById(R.id.phone);
        layoutPassword = findViewById(R.id.password);
        editTextName = findViewById(R.id.input_name);
        editTextDescription = findViewById(R.id.input_description);
        editTextMinPrice = findViewById(R.id.input_min_price);
        editTextMaxPrice = findViewById(R.id.input_max_price);
        editTextRegisteredAddress = findViewById(R.id.input_registered_address);
        editTextRunningAddress = findViewById(R.id.input_running_address);
        editTextEmail = findViewById(R.id.input_email);
        editTextPhone = findViewById(R.id.input_phone);
        editTextPassword = findViewById(R.id.input_password);
        spinnerCategory = findViewById(R.id.spin_category);
        spinnerLocation = findViewById(R.id.spin_location);
        checkBoxRunningAddress = findViewById(R.id.chk_running_address);
        viewImages = findViewById(R.id.images);
        buttonContinue = findViewById(R.id.btn_continue);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        categoryList = new ArrayList<>();
        locationList = new ArrayList<>();
        imageList = new ArrayList<>();
        categoryArrayAdapter = new ArrayAdapter<>(context, R.layout.item_spinner, categoryList);
        locationArrayAdapter = new ArrayAdapter<>(context, R.layout.item_spinner, locationList);
        imageAdapter = new AddImageAdapter(context, imageList, this);
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        textViewLogin.setOnClickListener(this);
        editTextName.setOnFocusChangeListener(this);
        editTextDescription.setOnFocusChangeListener(this);
        editTextMinPrice.setOnFocusChangeListener(this);
        editTextMaxPrice.setOnFocusChangeListener(this);
        editTextRegisteredAddress.setOnFocusChangeListener(this);
        editTextRunningAddress.setOnFocusChangeListener(this);
        editTextEmail.setOnFocusChangeListener(this);
        editTextPhone.setOnFocusChangeListener(this);
        editTextPassword.setOnFocusChangeListener(this);
        checkBoxRunningAddress.setOnCheckedChangeListener(this);
        buttonContinue.setOnClickListener(this);
    }

    private void initSpinner() {
        categoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(categoryArrayAdapter);
        locationArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocation.setAdapter(locationArrayAdapter);
    }

    private void initRecyclerView() {
        viewImages.setLayoutManager(new GridLayoutManager(context, 3));
        viewImages.setAdapter(imageAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void setPasswordFont() {
        layoutPassword.setTypeface(editTextName.getTypeface());
        editTextPassword.setTypeface(editTextName.getTypeface());
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void setLoginText() {
        SpannableString spannableString = new SpannableString(getString(R.string.txt_already_login));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_txt)), 25, 30, 0);
        spannableString.setSpan(new RelativeSizeSpan(1.05f), 25, 30, 0);
        textViewLogin.setText(spannableString);
    }

    private void processSignUp() {
        String name = editTextName.getText().toString().trim();
        String description = editTextDescription.getText().toString().trim();
        String minPrice = editTextMinPrice.getText().toString().trim();
        String maxPrice = editTextMaxPrice.getText().toString().trim();
        String registeredAddress = editTextRegisteredAddress.getText().toString().trim();
        String runningAddress = editTextRunningAddress.getText().toString().trim();
        String email = editTextEmail.getText().toString().trim();
        String phone = editTextPhone.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        if (isValidName(name) && isValidDescription(description) && isValidMinPrice(minPrice)
                && isValidMaxPrice(maxPrice) && isValidRegisteredAddress(registeredAddress)
                && isValidRunningAddress(runningAddress) && isValidEmail(email)
                && isValidPhone(phone) && isValidPassword(password) && isImagesAdded()) {
            setLoading(true);
            Map<String, RequestBody> map = new HashMap<>();
            map.put("username", RequestBody.create(MediaType.parse("text/plain"), phone));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), email));
            map.put("business_details", RequestBody.create(MediaType.parse("text/plain"),
                    new Gson().toJson(new AddBusinessDetail(
                            categoryList.get(spinnerCategory.getSelectedItemPosition()).getId(),
                            locationList.get(spinnerLocation.getSelectedItemPosition()).getId(),
                            Double.parseDouble(minPrice), Double.parseDouble(maxPrice),
                            name, registeredAddress, runningAddress, description))));
            MultipartBody.Part[] imageParts = new MultipartBody.Part[imageList.size()];
            for (int i = 0; i < imageList.size(); i++) {
                File file = new File(imageList.get(i));
                RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                imageParts[i] = MultipartBody.Part.createFormData("images", file.getName(),
                        requestBody);
            }
            register(map, imageParts);
        }
    }

    private void isValidName() {
        isValidName(editTextName.getText().toString().trim());
    }

    private boolean isValidName(String name) {
        if (name.isEmpty()) {
            layoutName.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutName.getHint()));
            return false;
        }
        layoutName.setErrorEnabled(false);
        return true;
    }

    private void isValidDescription() {
        isValidDescription(editTextDescription.getText().toString().trim());
    }

    private boolean isValidDescription(String description) {
        if (description.isEmpty()) {
            layoutDescription.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutDescription.getHint()));
            return false;
        }
        layoutDescription.setErrorEnabled(false);
        return true;
    }

    private void isValidMinPrice() {
        isValidMinPrice(editTextMinPrice.getText().toString().trim());
    }

    private boolean isValidMinPrice(String minPrice) {
        if (minPrice.isEmpty()) {
            layoutMinPrice.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutMinPrice.getHint()));
            return false;
        }
        layoutMinPrice.setErrorEnabled(false);
        return true;
    }

    private void isValidMaxPrice() {
        isValidMaxPrice(editTextMaxPrice.getText().toString().trim());
    }

    private boolean isValidMaxPrice(String maxPrice) {
        if (maxPrice.isEmpty()) {
            layoutMaxPrice.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutMaxPrice.getHint()));
            return false;
        }
        try {
            if (Double.parseDouble(editTextMinPrice.getText().toString().trim())
                    > Double.parseDouble(editTextMaxPrice.getText().toString().trim())) {
                layoutMaxPrice.setError(getString(R.string.error_max_price));
                return false;
            }
        } catch (Exception e) {
            layoutMaxPrice.setError(getString(R.string.error_invalid));
            return false;
        }
        layoutMaxPrice.setErrorEnabled(false);
        return true;
    }

    private void isValidRegisteredAddress() {
        if (checkBoxRunningAddress.isChecked()) {
            editTextRunningAddress.setText(editTextRegisteredAddress.getText().toString().trim());
            editTextRunningAddress.setEnabled(false);
        }
        isValidRegisteredAddress(editTextRegisteredAddress.getText().toString().trim());
    }

    private boolean isValidRegisteredAddress(String registeredAddress) {
        if (registeredAddress.isEmpty()) {
            layoutRegisteredAddress.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutRegisteredAddress.getHint()));
            return false;
        }
        layoutRegisteredAddress.setErrorEnabled(false);
        return true;
    }

    private void isValidRunningAddress() {
        isValidRunningAddress(editTextRunningAddress.getText().toString().trim());
    }

    private boolean isValidRunningAddress(String runningAddress) {
        if (runningAddress.isEmpty()) {
            layoutRunningAddress.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutRunningAddress.getHint()));
            return false;
        }
        layoutRunningAddress.setErrorEnabled(false);
        return true;
    }

    private void isValidEmail() {
        isValidEmail(editTextEmail.getText().toString().trim());
    }

    private boolean isValidEmail(String email) {
        if (email.isEmpty()) {
            layoutEmail.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutEmail.getHint()));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            layoutEmail.setError(String.format(Locale.getDefault(), getString(R.string.error_invalid), layoutEmail.getHint()));
            return false;
        }
        layoutEmail.setErrorEnabled(false);
        return true;
    }

    private void isValidPhone() {
        isValidPhone(editTextPhone.getText().toString().trim());
    }

    private boolean isValidPhone(String phone) {
        if (phone.isEmpty()) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPhone.getHint()));
            return false;
        } else if (phone.length() < 10) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_length), layoutPhone.getHint(), 10));
            return false;
        }
        layoutPhone.setErrorEnabled(false);
        return true;
    }

    private void isValidPassword() {
        isValidPassword(editTextPassword.getText().toString().trim());
    }

    private boolean isValidPassword(String password) {
        if (password.isEmpty()) {
            layoutPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPassword.getHint()));
            return false;
        } else if (password.length() < 6) {
            layoutPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        layoutPassword.setErrorEnabled(false);
        return true;
    }

    private boolean isImagesAdded() {
        if (imageList.isEmpty()) {
            Toast.makeText(context, getString(R.string.error_empty_image), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) == PERMISSION_GRANTED;
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    private void processPickImages() {
        if (hasStoragePermission()) {
            pickImages();
        } else {
            requestStoragePermission();
        }
    }

    private void pickImages() {
        EasyImage.configuration(context)
                .setImagesFolderName(getString(R.string.app_name))
                .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(this, "Select Image", REQUEST_IMAGES);
    }

    private void getRegisterDropdown() {
        Call<RegisterDropdown> call = getApiService().getRegisterDropdown();
        call.enqueue(new Callback<RegisterDropdown>() {
            @Override
            public void onResponse(@NonNull Call<RegisterDropdown> call, @NonNull Response<RegisterDropdown> response) {
                refreshLayout.setRefreshing(false);
                RegisterDropdown registerDropdownResponse = response.body();
                if (response.isSuccessful() && registerDropdownResponse != null) {
                    layoutSignUp.setVisibility(View.VISIBLE);
                    categoryList.clear();
                    locationList.clear();
                    categoryList.addAll(registerDropdownResponse.getCategoryList());
                    locationList.addAll(registerDropdownResponse.getLocationList());
                    categoryArrayAdapter.notifyDataSetChanged();
                    locationArrayAdapter.notifyDataSetChanged();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<RegisterDropdown> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void register(Map<String, RequestBody> map, MultipartBody.Part[] partList) {
        Call<User> call = getApiService().createVendor(map, partList);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    preference.setPhone(userResponse.getUsername());
                    Toast.makeText(context, "OTP code sent to " + userResponse.getUsername(), Toast.LENGTH_SHORT).show();
                    launch(context, OtpActivity.class);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
