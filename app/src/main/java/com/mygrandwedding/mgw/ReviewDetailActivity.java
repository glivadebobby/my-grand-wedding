package com.mygrandwedding.mgw;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.ReviewAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.ReviewCallback;
import com.mygrandwedding.mgw.model.Review;
import com.mygrandwedding.mgw.model.ReviewData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_RATING;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_REVIEW;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;

public class ReviewDetailActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Runnable, ReviewCallback {

    private static final int REQUEST_REVIEW = 4;
    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewReviews;
    private List<Review> reviewList;
    private LinearLayoutManager layoutManager;
    private ReviewAdapter reviewAdapter;
    private MyPreference preference;
    private int id;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String vendor, url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_detail);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_REVIEW) {
            if (resultCode == Activity.RESULT_OK) {
                run();
            }
        }
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    @Override
    public void onEditClick(int position) {
        Review review = reviewList.get(position);
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, id);
        bundle.putFloat(EXTRA_RATING, review.getRating());
        bundle.putString(EXTRA_VENDOR, vendor);
        bundle.putString(EXTRA_REVIEW, review.getReview());
        Intent intent = new Intent(this, ReviewActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_REVIEW);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewReviews = findViewById(R.id.reviews);

        context = this;
        preference = new MyPreference(context);
        reviewList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        reviewAdapter = new ReviewAdapter(context, reviewList, this, preference.getId());
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            vendor = bundle.getString(EXTRA_VENDOR);
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(String.format(Locale.getDefault(),
                        getString(R.string.format_reviews_on), vendor));
        }
    }

    private void initRecyclerView() {
        viewReviews.setLayoutManager(layoutManager);
        viewReviews.setAdapter(reviewAdapter);
        viewReviews.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getReviews();
                }
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void resetList() {
        count = 0;
        setUrl();
        getReviews();
    }

    private void setUrl() {
        url = MGW_URL + "vendor/app/list/feedback/" + id + "/";
    }

    private void clearList() {
        if (count == 0) {
            reviewList.clear();
            reviewAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        if (reviewList.isEmpty()) {
            Toast.makeText(context, "No reviews found", Toast.LENGTH_SHORT).show();
        }
    }

    private void getReviews() {
        setLoading(true);
        Call<ReviewData> call = getApiService().getReviews(url);
        call.enqueue(new Callback<ReviewData>() {
            @Override
            public void onResponse(@NonNull Call<ReviewData> call,
                                   @NonNull Response<ReviewData> response) {
                refreshLayout.setRefreshing(false);
                ReviewData reviewDataResponse = response.body();
                if (response.isSuccessful() && reviewDataResponse != null) {
                    setLoading(false);
                    clearList();
                    count = reviewDataResponse.getCount();
                    url = reviewDataResponse.getNextUrl();
                    reviewList.addAll(reviewDataResponse.getReviewList());
                    reviewAdapter.notifyDataSetChanged();
                    isEmpty();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ReviewData> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
