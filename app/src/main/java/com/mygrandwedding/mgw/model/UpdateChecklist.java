package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gladwinbobby on 18/10/17
 */

public class UpdateChecklist {

    @SerializedName("checklist")
    private List<Integer> idList;

    public UpdateChecklist(List<Integer> idList) {
        this.idList = idList;
    }

    public List<Integer> getIdList() {
        return idList;
    }

    public void setIdList(List<Integer> idList) {
        this.idList = idList;
    }
}
