package com.mygrandwedding.mgw;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Login;
import com.mygrandwedding.mgw.model.SocialLogin;
import com.mygrandwedding.mgw.model.User;

import java.util.Arrays;
import java.util.Locale;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener, FacebookCallback<LoginResult> {

    private Context context;
    private Toolbar toolbar;
    private TextView textViewFacebook, textViewForgotPassword, textViewSignUp;
    private TextInputLayout layoutUsername, layoutPassword;
    private TextInputEditText editTextUsername, editTextPassword;
    private Button buttonContinue;
    private FrameLayout layoutLoading;
    private CallbackManager callbackManager;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initObjects();
        initToolbar();
        initCallbacks();
        setPasswordFont();
        setSignUpText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextUsername) isValidUsername();
            if (view == editTextPassword) isValidPassword();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        setLoading(true);
        fbLogin(new SocialLogin(loginResult.getAccessToken().getToken(), UUID.randomUUID().toString()));
    }

    @Override
    public void onCancel() {
        Toast.makeText(context, getString(R.string.error_fb_cancelled), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(FacebookException error) {
        Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        if (view == textViewFacebook) startFbLogin();
        else if (view == textViewForgotPassword) launch(context, SendOtpActivity.class);
        else if (view == textViewSignUp) launch(context, SignUpActivity.class);
        else if (view == buttonContinue) processLogin();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        textViewFacebook = findViewById(R.id.txt_facebook);
        textViewForgotPassword = findViewById(R.id.txt_forgot_password);
        textViewSignUp = findViewById(R.id.txt_sign_up);
        layoutUsername = findViewById(R.id.username);
        layoutPassword = findViewById(R.id.password);
        editTextUsername = findViewById(R.id.input_username);
        editTextPassword = findViewById(R.id.input_password);
        buttonContinue = findViewById(R.id.btn_continue);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        callbackManager = CallbackManager.Factory.create();
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        textViewFacebook.setOnClickListener(this);
        textViewForgotPassword.setOnClickListener(this);
        textViewSignUp.setOnClickListener(this);
        editTextUsername.setOnFocusChangeListener(this);
        editTextPassword.setOnFocusChangeListener(this);
        buttonContinue.setOnClickListener(this);
        LoginManager.getInstance().registerCallback(callbackManager, this);
    }

    private void setPasswordFont() {
        layoutPassword.setTypeface(editTextUsername.getTypeface());
        editTextPassword.setTypeface(editTextUsername.getTypeface());
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void setSignUpText() {
        SpannableString spannableString = new SpannableString(getString(R.string.txt_new_user));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_txt)), 18, 25, 0);
        spannableString.setSpan(new RelativeSizeSpan(1.05f), 18, 25, 0);
        textViewSignUp.setText(spannableString);
    }

    private void startFbLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday"));
    }

    private void processLogin() {
        String username = editTextUsername.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        if (isValidUsername(username) && isValidPassword(password)) {
            setLoading(true);
            login(new Login(preference.getRole(), username, password, UUID.randomUUID().toString()));
        }
    }

    private void isValidUsername() {
        isValidUsername(editTextUsername.getText().toString().trim());
    }

    private boolean isValidUsername(String username) {
        if (username.isEmpty()) {
            layoutUsername.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutUsername.getHint()));
            return false;
        }
        layoutUsername.setErrorEnabled(false);
        return true;
    }

    private void isValidPassword() {
        isValidPassword(editTextPassword.getText().toString().trim());
    }

    private boolean isValidPassword(String password) {
        if (password.isEmpty()) {
            layoutPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPassword.getHint()));
            return false;
        } else if (password.length() < 6) {
            layoutPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        layoutPassword.setErrorEnabled(false);
        return true;
    }

    private void login(Login login) {
        Call<User> call = getApiService().login(login);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    handleLoginResponse(userResponse);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void fbLogin(SocialLogin socialLogin) {
        Call<User> call = getApiService().fbLogin(socialLogin);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    handleLoginResponse(userResponse);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void handleLoginResponse(User userResponse) {
        preference.setId(userResponse.getId());
        preference.setName(userResponse.getFirstName());
        if (userResponse.getUsername() == null || userResponse.getUsername().isEmpty()
                || !TextUtils.isDigitsOnly(userResponse.getUsername()))
            preference.setVerified(false);
        else
            preference.setPhone(userResponse.getUsername());
        if (userResponse.getEmail() == null || userResponse.getEmail().isEmpty())
            preference.setVerified(false);
        else
            preference.setEmail(userResponse.getEmail());
        preference.setToken(userResponse.getToken());
        if (!preference.isVerified())
            launch(context, AdditionalDetailActivity.class);
        else
            launchClearStack(context, MainActivity.class);
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
