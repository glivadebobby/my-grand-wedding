package com.mygrandwedding.mgw.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

/**
 * Created by gladwinbobby on 16/10/17
 */

@Dao
public interface DeleteDao {
    @Query("DELETE FROM User")
    void deleteUser();

    @Query("DELETE FROM Wedding")
    void deleteWedding();

    @Query("DELETE FROM Location")
    void deleteLocation();

    @Query("DELETE FROM VendorCategory")
    void deleteVendorCategory();

    @Query("DELETE FROM VendorSubcategory")
    void deleteVendorSubcategory();

    @Query("DELETE FROM BlogCategory")
    void deleteBlogCategory();

    @Query("DELETE FROM IdeaCategory")
    void deleteIdeaCategory();

    @Query("DELETE FROM Checklist")
    void deleteChecklist();
}
