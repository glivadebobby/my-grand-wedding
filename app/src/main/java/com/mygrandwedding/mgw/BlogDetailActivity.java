package com.mygrandwedding.mgw;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.app.MyActivity;
import com.mygrandwedding.mgw.model.Blog;

import im.delight.android.webview.AdvancedWebView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.util.DateParser.getCreatedDate;

public class BlogDetailActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private Toolbar toolbar;
    private ImageView imageViewBlog;
    private SwipeRefreshLayout refreshLayout;
    private TextView textViewTitle, textViewCategory, textViewDate;
    private AdvancedWebView webView;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        webView.onResume();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        webView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        webView.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_share:
                shareBlog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        webView.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onRefresh() {
        getBlog();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getBlog();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        imageViewBlog = findViewById(R.id.img_blog);
        refreshLayout = findViewById(R.id.refresh);
        textViewTitle = findViewById(R.id.txt_title);
        textViewCategory = findViewById(R.id.txt_category);
        textViewDate = findViewById(R.id.txt_date);
        webView = findViewById(R.id.web_view);

        context = this;
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
        }
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void getBlog() {
        Call<Blog> call = getApiService().getBlogDetail(id);
        call.enqueue(new Callback<Blog>() {
            @Override
            public void onResponse(@NonNull Call<Blog> call, @NonNull Response<Blog> response) {
                refreshLayout.setRefreshing(false);
                Blog blogResponse = response.body();
                if (response.isSuccessful() && blogResponse != null) {
                    GlideApp.with(context)
                            .load(blogResponse.getImage())
                            .placeholder(R.drawable.placeholder)
                            .into(imageViewBlog);
                    textViewTitle.setText(blogResponse.getTitle());
                    textViewCategory.setText(blogResponse.getCategory().getName());
                    textViewDate.setText(getCreatedDate(blogResponse.getCreatedOn()));
                    webView.loadHtml(blogResponse.getDescription());
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Blog> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void shareBlog() {
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://mygrandwedding.com/article.php" + "?id=" + id + "&type=" + 3))
                .setDynamicLinkDomain("dr779.app.goo.gl")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.mygrandwedding.mgw").build())
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            MyActivity.shareInspirations(context, shortLink.toString());
                        } else {
                            Toast.makeText(context, "Unable to share! Try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
