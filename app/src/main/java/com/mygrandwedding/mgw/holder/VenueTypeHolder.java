package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.MyBudgetCallback;

/**
 * Created by gladwinbobby on 25/12/17
 */

public class VenueTypeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView textViewVenueType;
    public ImageView imageViewEdit;
    private MyBudgetCallback callback;

    public VenueTypeHolder(View itemView, MyBudgetCallback callback) {
        super(itemView);
        textViewVenueType = itemView.findViewById(R.id.txt_venue_type);
        imageViewEdit = itemView.findViewById(R.id.img_edit);
        this.callback = callback;
        imageViewEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewEdit) {
            callback.onMyBudgetClick(getLayoutPosition());
        }
    }
}
