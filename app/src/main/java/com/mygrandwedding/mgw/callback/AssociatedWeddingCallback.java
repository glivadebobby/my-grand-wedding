package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 02/10/17
 */

public interface AssociatedWeddingCallback {
    void onRemoveClick(int position);
}
