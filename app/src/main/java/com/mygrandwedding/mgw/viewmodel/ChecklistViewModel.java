package com.mygrandwedding.mgw.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.mygrandwedding.mgw.model.Checklist;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 18/10/17
 */

public class ChecklistViewModel extends ViewModel {

    private LiveData<List<Checklist>> data;

    public LiveData<List<Checklist>> getData(Context context) {
        if (data == null) {
            data = getAppDatabase(context).weddingDao().getChecklist();
        }
        return data;
    }
}
