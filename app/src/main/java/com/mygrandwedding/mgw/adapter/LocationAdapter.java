package com.mygrandwedding.mgw.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.LocationCallback;
import com.mygrandwedding.mgw.holder.LocationHolder;
import com.mygrandwedding.mgw.model.Location;

import java.util.List;

/**
 * Created by gladwinbobby on 30/09/17
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationHolder> {

    private List<Location> locationList;
    private LocationCallback callback;

    public LocationAdapter(List<Location> locationList, LocationCallback callback) {
        this.locationList = locationList;
        this.callback = callback;
    }

    @Override
    public LocationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LocationHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_location, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(LocationHolder holder, int position) {
        Location location = locationList.get(position);
        holder.textViewLocation.setText(location.getLocation());
    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }
}
