package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 28/09/17
 */

public class CreateQuote {

    @SerializedName("id")
    private Integer id;
    @SerializedName("vendor")
    private Integer vendor;
    @SerializedName("full_name")
    private String fullName;
    @SerializedName("email")
    private String email;
    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("location")
    private String location;
    @SerializedName("requirement")
    private String requirement;
    @SerializedName("message")
    private String message;
    @SerializedName("wedding_date")
    private String weddingDate;

    public CreateQuote(Integer vendor, String fullName, String email, String mobileNumber,
                       String location, String requirement, String message, String weddingDate) {
        this.vendor = vendor;
        this.fullName = fullName;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.location = location;
        this.requirement = requirement;
        this.message = message;
        this.weddingDate = weddingDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVendor() {
        return vendor;
    }

    public void setVendor(Integer vendor) {
        this.vendor = vendor;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getWeddingDate() {
        return weddingDate;
    }

    public void setWeddingDate(String weddingDate) {
        this.weddingDate = weddingDate;
    }
}
