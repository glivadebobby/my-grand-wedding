package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 16/10/17
 */

public class UpdateUsername {

    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("otp")
    private String otp;

    public UpdateUsername(String mobileNumber, String otp) {
        this.mobileNumber = mobileNumber;
        this.otp = otp;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
