package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.ReviewCallback;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by gladwinbobby on 08/09/17
 */

public class ReviewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public CircleImageView imageViewProfile;
    public TextView textViewName, textViewDate, textViewRating, textViewReview;
    public ImageView imageViewEdit;
    private ReviewCallback callback;

    public ReviewHolder(View itemView, ReviewCallback callback) {
        super(itemView);
        imageViewProfile = itemView.findViewById(R.id.img_profile);
        textViewName = itemView.findViewById(R.id.txt_name);
        textViewDate = itemView.findViewById(R.id.txt_date);
        textViewRating = itemView.findViewById(R.id.txt_rating);
        textViewReview = itemView.findViewById(R.id.txt_review);
        imageViewEdit = itemView.findViewById(R.id.img_edit);
        this.callback = callback;
        imageViewEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewEdit) {
            callback.onEditClick(getLayoutPosition());
        }
    }
}
