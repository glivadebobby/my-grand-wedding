package com.mygrandwedding.mgw;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.app.MyActivity;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Wedding;

import jp.wasabeef.glide.transformations.BlurTransformation;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;
import static com.mygrandwedding.mgw.util.DateParser.getFormattedDate;

public class MyWeddingSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private ImageView imageViewHeader;
    private Toolbar toolbar;
    private TextView textViewTitle, textViewLocation, textViewDate, textViewInviteBride, textViewInviteCollaborator;
    private Wedding wedding;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wedding_settings);
        initObjects();
        initToolbar();
        initCallbacks();
        setHeaderImage();
        loadMyWedding();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.wedding_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_update:
                launch(context, MyWeddingActivity.class);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == textViewInviteBride) {
            inviteBride();
        } else if (view == textViewInviteCollaborator) {
            inviteCollaborator();
        }
    }

    private void initObjects() {
        imageViewHeader = findViewById(R.id.img_header);
        toolbar = findViewById(R.id.toolbar);
        textViewTitle = findViewById(R.id.txt_category);
        textViewLocation = findViewById(R.id.txt_location);
        textViewDate = findViewById(R.id.txt_date);
        textViewInviteBride = findViewById(R.id.txt_invite_bride);
        textViewInviteCollaborator = findViewById(R.id.txt_invite_collaborator);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        textViewInviteBride.setOnClickListener(this);
        textViewInviteCollaborator.setOnClickListener(this);
    }

    private void setHeaderImage() {
        GlideApp.with(context)
                .load(R.drawable.header)
                .transform(new BlurTransformation(45))
                .into(imageViewHeader);
    }

    private void loadMyWedding() {
        LiveData<Wedding> data = getAppDatabase(context).weddingDao().getMyWedding(preference.getId());
        data.observe(this, new Observer<Wedding>() {
            @Override
            public void onChanged(@Nullable Wedding wedding) {
                MyWeddingSettingsActivity.this.wedding = wedding;
                if (wedding != null) {
                    textViewInviteBride.setText(wedding.isBride(preference.getId()) ? getString(R.string.txt_invite_groom) : getString(R.string.txt_invite_bride));
                    textViewTitle.setText(wedding.getTitle());
                    textViewLocation.setText(wedding.getLocation());
                    textViewDate.setText(getFormattedDate(wedding.getDate()));
                }
            }
        });
    }

    private void inviteBride() {
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://mygrandwedding.com/invite.php" + "?code=" + wedding.getInvitationCode() + "&bride=" + wedding.isBride(preference.getId())))
                .setDynamicLinkDomain("dr779.app.goo.gl")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.mygrandwedding.mgw").build())
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            MyActivity.inviteBride(context, wedding.isBride(preference.getId()), wedding.getInvitationCode(), shortLink.toString());
                        } else {
                            Toast.makeText(context, "Unable to invite! Try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void inviteCollaborator() {
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://mygrandwedding.com/invite.php" + "?code=" + wedding.getCollaboratorCode()))
                .setDynamicLinkDomain("dr779.app.goo.gl")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.mygrandwedding.mgw").build())
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            MyActivity.inviteCollaborator(context, wedding.getCollaboratorCode(), shortLink.toString());
                        } else {
                            Toast.makeText(context, "Unable to invite! Try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
