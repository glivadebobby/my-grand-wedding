package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageFragment extends Fragment {

    private static final String ARGUMENT_IMAGE = "image";
    private View rootView;
    private Context context;
    private PhotoView photoView;

    public ImageFragment() {
        // Required empty public constructor
    }

    public static ImageFragment newInstance(String image) {
        Bundle bundle = new Bundle();
        bundle.putString(ARGUMENT_IMAGE, image);
        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_image, container, false);
        initObjects();
        processBundle();
        return rootView;
    }

    private void initObjects() {
        photoView = rootView.findViewById(R.id.img);

        context = getActivity();
    }

    private void processBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            GlideApp.with(context)
                    .load(bundle.getString(ARGUMENT_IMAGE))
                    .into(photoView);
        }
    }
}
