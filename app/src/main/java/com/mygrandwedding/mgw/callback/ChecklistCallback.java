package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 18/10/17
 */

public interface ChecklistCallback {
    void onChecklistClick(int position);
}
