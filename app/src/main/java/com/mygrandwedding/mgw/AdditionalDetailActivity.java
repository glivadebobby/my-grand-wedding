package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.VerifyMobile;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_EMAIL;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_PHONE;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class AdditionalDetailActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutEmail, layoutPhone;
    private TextInputEditText editTextEmail, editTextPhone;
    private Button buttonContinue;
    private TextView textViewBackLogin;
    private FrameLayout layoutLoading;
    private MyPreference preference;
    private String email, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_detail);
        initObjects();
        initToolbar();
        initCallbacks();
        setEmailVisibility();
        setBackLoginText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextEmail) isValidEmail();
            if (view == editTextPhone) isValidPhone();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonContinue) {
            processAdditionalDetail();
        } else if (view == textViewBackLogin) {
            preference.clearUser();
            launchClearStack(context, SplashActivity.class);
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutEmail = findViewById(R.id.email);
        layoutPhone = findViewById(R.id.phone);
        editTextEmail = findViewById(R.id.input_email);
        editTextPhone = findViewById(R.id.input_phone);
        buttonContinue = findViewById(R.id.btn_continue);
        textViewBackLogin = findViewById(R.id.txt_back_login);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        editTextEmail.setOnFocusChangeListener(this);
        editTextPhone.setOnFocusChangeListener(this);
        buttonContinue.setOnClickListener(this);
        textViewBackLogin.setOnClickListener(this);
    }

    private void setEmailVisibility() {
        layoutEmail.setVisibility(preference.getEmail() == null ? View.VISIBLE : View.GONE);
    }

    private void setBackLoginText() {
        SpannableString spannableString = new SpannableString(getString(R.string.txt_back_login));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_txt)), 18, 29, 0);
        spannableString.setSpan(new RelativeSizeSpan(1.05f), 18, 29, 0);
        textViewBackLogin.setText(spannableString);
    }

    private void processAdditionalDetail() {
        email = editTextEmail.getText().toString().trim();
        phone = editTextPhone.getText().toString().trim();
        if (isValidEmail(email) && isValidPhone(phone)) {
            setLoading(true);
            updateMobileEmail();
        }
    }

    private void isValidEmail() {
        isValidEmail(editTextEmail.getText().toString().trim());
    }

    private boolean isValidEmail(String email) {
        if (layoutEmail.getVisibility() == View.GONE) return true;
        if (email.isEmpty()) {
            layoutEmail.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutEmail.getHint()));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            layoutEmail.setError(String.format(Locale.getDefault(), getString(R.string.error_invalid), layoutEmail.getHint()));
            return false;
        }
        layoutEmail.setErrorEnabled(false);
        return true;
    }

    private void isValidPhone() {
        isValidPhone(editTextPhone.getText().toString().trim());
    }

    private boolean isValidPhone(String phone) {
        if (phone.isEmpty()) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPhone.getHint()));
            return false;
        } else if (phone.length() < 10) {
            layoutPhone.setError(String.format(Locale.getDefault(), getString(R.string.error_length), layoutPhone.getHint(), 10));
            return false;
        }
        layoutPhone.setErrorEnabled(false);
        return true;
    }

    private void updateMobileEmail() {
        Call<Data> call = getApiService().verifyMobile(preference.getId(),
                new VerifyMobile(email.isEmpty() ? null : email, phone.isEmpty() ? null : phone));
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    if (preference.getEmail() != null) preference.setEmail(email);
                    Toast.makeText(context, "OTP code sent to " + phone, Toast.LENGTH_SHORT).show();
                    Bundle bundle = new Bundle();
                    bundle.putString(EXTRA_PHONE, phone.isEmpty() ? null : phone);
                    bundle.putString(EXTRA_EMAIL, email.isEmpty() ? null : email);
                    launchWithBundle(context, VerifyMobileOtpActivity.class, bundle);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
