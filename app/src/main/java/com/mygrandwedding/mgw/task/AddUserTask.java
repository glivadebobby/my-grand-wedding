package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.User;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class AddUserTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private User user;

    public AddUserTask(Context context, User user) {
        this.context = context;
        this.user = user;
    }

    @Override
    protected Void doInBackground(Void... params) {
        getAppDatabase(context).deleteDao().deleteUser();
        getAppDatabase(context).userDao().insertUser(user);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
