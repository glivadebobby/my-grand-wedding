package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 07/09/17
 */

public interface PhotoCallback {
    void onPhotoClick(int position);
}
