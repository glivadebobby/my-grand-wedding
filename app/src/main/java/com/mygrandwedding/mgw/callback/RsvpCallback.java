package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 03/12/17
 */

public interface RsvpCallback {
    void onRsvpClick(int position);
}
