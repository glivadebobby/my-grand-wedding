package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;
import com.mygrandwedding.mgw.util.MyData;

import java.util.Locale;

import static com.mygrandwedding.mgw.adapter.MyBudgetAdapter.TYPE_MY_BUDGET;
import static com.mygrandwedding.mgw.util.MyData.HOTEL_RESORT;
import static com.mygrandwedding.mgw.util.MyData.MANDAPAM;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_VENUES;
import static com.mygrandwedding.mgw.util.MyData.getCategoryIcon;
import static com.mygrandwedding.mgw.util.MyData.getCategoryTitle;

/**
 * Created by gladwinbobby on 19/12/17
 */

public class MyBudget {

    private int type;
    @SerializedName("id")
    private Integer id;
    @SerializedName("wedding")
    private Integer wedding;
    @SerializedName("category")
    private Integer category;
    @SerializedName("sub_category")
    private Integer subCategory;
    @SerializedName("count")
    private Integer count;
    @SerializedName("total_cost")
    private Float totalCost;
    @SerializedName("per_item_cost")
    private Float perItemCost;
    @SerializedName("created_on")
    private String createdOn;

    public MyBudget(Integer category) {
        this.category = category;
    }

    public MyBudget(int type, Integer category) {
        this.type = type;
        this.category = category;
    }

    public MyBudget(Integer wedding, Integer category, Integer subCategory) {
        this.wedding = wedding;
        this.category = category;
        this.subCategory = subCategory;
    }

    public MyBudget(Integer wedding, Integer category, Float totalCost) {
        this.wedding = wedding;
        this.category = category;
        this.totalCost = totalCost;
    }

    public MyBudget(Integer wedding, Integer category, Integer subCategory, Float totalCost) {
        this.wedding = wedding;
        this.category = category;
        this.subCategory = subCategory;
        this.totalCost = totalCost;
    }

    public MyBudget(Integer wedding, Integer category, Integer subCategory, Integer count, Float perItemCost) {
        this.wedding = wedding;
        this.category = category;
        this.subCategory = subCategory;
        this.count = count;
        this.perItemCost = perItemCost;
    }

    public MyBudget(Integer wedding, Integer category, Integer count, Float perItemCost, Float totalCost) {
        this.wedding = wedding;
        this.category = category;
        this.count = count;
        this.perItemCost = perItemCost;
        this.totalCost = totalCost;
    }

    public int getType() {
        return type == 0 ? TYPE_MY_BUDGET : type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWedding() {
        return wedding;
    }

    public void setWedding(Integer wedding) {
        this.wedding = wedding;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(Integer subCategory) {
        this.subCategory = subCategory;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Float totalCost) {
        this.totalCost = totalCost;
    }

    public String getFoodCost() {
        if (perItemCost == null || count == null) return null;
        else return String.format(Locale.getDefault(), "%1$.2f", perItemCost * count);
    }

    public Float getPerItemCost() {
        return perItemCost;
    }

    public void setPerItemCost(Float perItemCost) {
        this.perItemCost = perItemCost;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getIcon() {
        return getCategoryIcon(category);
    }

    public String getTitle() {
        if (category == WEDDING_VENUES) {
            if (subCategory == null) return "-NA-";
            else if (subCategory == MANDAPAM) return "Mandapam";
            else if (subCategory == HOTEL_RESORT) return "Hotel/Resort";
        }
        return getCategoryTitle(category);
    }

    public String getNoOf() {
        return MyData.getNoOf(category);
    }

    public String getCostPer() {
        return MyData.getCostPer(category);
    }
}
