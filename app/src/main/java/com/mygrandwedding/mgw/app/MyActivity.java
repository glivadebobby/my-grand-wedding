package com.mygrandwedding.mgw.app;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.mygrandwedding.mgw.R;

/**
 * Created by gladwinbobby on 03/09/17
 */

public class MyActivity {
    public static void launch(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }

    public static void launchWithResult(Activity activity, Class<?> cls, int requestCode) {
        Intent intent = new Intent(activity, cls);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void launchWithBundle(Context context, Class<?> cls, Bundle bundle) {
        Intent intent = new Intent(context, cls);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void launchWithBundleResult(Activity activity, Class<?> cls, Bundle bundle, int requestCode) {
        Intent intent = new Intent(activity, cls);
        intent.putExtras(bundle);
        activity.startActivityForResult(intent, requestCode);
    }

    public static void launchClearStack(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void launchDialPad(Context context, String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        context.startActivity(intent);
    }

    public static void shareApp(Context context) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Plan the most important day of your life without any flaws. Get the quotations you require according to your budget. Download " + context.getString(R.string.app_name) + " and make this day an auspicious one." + "\n" + "https://play.google.com/store/apps/details?id=" + context.getPackageName());
        context.startActivity(Intent.createChooser(shareIntent, "Share " + context.getString(R.string.app_name) + " App"));
    }

    public static void shareVendor(Context context, String url) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey!! Checkout this vendor " + url);
        context.startActivity(Intent.createChooser(shareIntent, "Share Vendor"));
    }

    public static void shareInspirations(Context context, String url) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey!! Checkout this wedding inspiration " + url);
        context.startActivity(Intent.createChooser(shareIntent, "Share Inspiration"));
    }

    public static void inviteReview(Context context, String url) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey!! Checkout this vendor " + url);
        context.startActivity(Intent.createChooser(shareIntent, "Invite to Review"));
    }

    public static void inviteBride(Context context, boolean bride, String code, String url) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey!! Let\'s plan our wedding together in one platform with " + context.getString(R.string.app_name) + ". Join me using this code \'" + code + "\'. Download the app here and make it easy." + "\n" + url);
        context.startActivity(Intent.createChooser(shareIntent, "Invite " + (bride ? "Groom" : "Bride")));
    }

    public static void inviteCollaborator(Context context, String code, String url) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Hey!! I am getting married. Please, help me plan my wedding in this platform along with " + context.getString(R.string.app_name) + ". Join my Wedding using the code, '" + code + "'. Download the app here." + "\n" + url);
        context.startActivity(Intent.createChooser(shareIntent, "Invite Collaborator"));
    }

    public static void rateApp(Context context) {
        String appPackageName = context.getPackageName();
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
