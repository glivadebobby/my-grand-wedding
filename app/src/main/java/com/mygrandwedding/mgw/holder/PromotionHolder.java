package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.PromotionCallback;

/**
 * Created by gladwinbobby on 10/09/17
 */

public class PromotionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewPromotion;
    public TextView textViewTitle, textViewCategory;
    private PromotionCallback callback;

    public PromotionHolder(View itemView, PromotionCallback callback) {
        super(itemView);
        imageViewPromotion = itemView.findViewById(R.id.img_promotion);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        textViewCategory = itemView.findViewById(R.id.txt_category);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onPromotionClick(getLayoutPosition());
        }
    }
}
