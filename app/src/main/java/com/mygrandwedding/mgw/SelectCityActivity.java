package com.mygrandwedding.mgw;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.mygrandwedding.mgw.adapter.LocationAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.LocationCallback;
import com.mygrandwedding.mgw.model.Location;
import com.mygrandwedding.mgw.task.AddLocationListTask;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

public class SelectCityActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, LocationCallback {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewLocation;
    private List<Location> locationList;
    private LocationAdapter locationAdapter;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_city);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
        loadLocations();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getLocations();
    }

    @Override
    public void onLocationClick(int position) {
        Location location = locationList.get(position);
        preference.setCityId(location.getId());
        setResult(RESULT_OK);
        finish();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewLocation = findViewById(R.id.location);

        context = this;
        locationList = new ArrayList<>();
        locationAdapter = new LocationAdapter(locationList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewLocation.setLayoutManager(new LinearLayoutManager(context));
        viewLocation.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        viewLocation.setAdapter(locationAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
    }

    private void loadLocations() {
        LiveData<List<Location>> data = getAppDatabase(context).generalDao().getLocations();
        data.observe(this, new Observer<List<Location>>() {
            @Override
            public void onChanged(@Nullable List<Location> locations) {
                locationList.clear();
                if (locations != null && !locations.isEmpty()) {
                    locationList.add(new Location(0, "All Cities"));
                    locationList.addAll(locations);
                } else {
                    refreshLayout.setRefreshing(true);
                    getLocations();
                }
                locationAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getLocations() {
        Call<List<Location>> call = getApiService().getLocations(preference.getUserToken());
        call.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(@NonNull Call<List<Location>> call,
                                   @NonNull Response<List<Location>> response) {
                refreshLayout.setRefreshing(false);
                List<Location> locationListResponse = response.body();
                if (response.isSuccessful() && locationListResponse != null) {
                    new AddLocationListTask(context, locationListResponse).execute();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Location>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
