package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 27/09/17
 */

public interface ImageCallback {
    void onAddClick();

    void onPictureClick(int position);

    void onRemoveClick(int position);
}
