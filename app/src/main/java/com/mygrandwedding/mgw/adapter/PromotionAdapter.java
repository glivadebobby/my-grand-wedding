package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.PromotionCallback;
import com.mygrandwedding.mgw.holder.PromotionHolder;
import com.mygrandwedding.mgw.model.Promotion;

import java.util.List;

/**
 * Created by gladwinbobby on 10/09/17
 */

public class PromotionAdapter extends RecyclerView.Adapter<PromotionHolder> {

    private Context context;
    private List<Promotion> promotionList;
    private PromotionCallback callback;

    public PromotionAdapter(Context context, List<Promotion> promotionList, PromotionCallback callback) {
        this.context = context;
        this.promotionList = promotionList;
        this.callback = callback;
    }

    @Override
    public PromotionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PromotionHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_promotion, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(PromotionHolder holder, int position) {
        Promotion promotion = promotionList.get(position);
        GlideApp.with(context)
                .load(promotion.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewPromotion);
        holder.textViewTitle.setText(promotion.getTitle());
        holder.textViewCategory.setText(promotion.getCategory().getName());
    }

    @Override
    public int getItemCount() {
        return promotionList.size();
    }
}
