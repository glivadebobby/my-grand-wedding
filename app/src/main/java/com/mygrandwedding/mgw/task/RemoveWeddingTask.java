package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.NotifyRefresh;
import com.mygrandwedding.mgw.model.Wedding;

import org.greenrobot.eventbus.EventBus;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class RemoveWeddingTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private Wedding wedding;

    public RemoveWeddingTask(Context context, Wedding wedding) {
        this.context = context;
        this.wedding = wedding;
    }

    @Override
    protected Void doInBackground(Void... params) {
        getAppDatabase(context).weddingDao().deleteWedding(wedding);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        EventBus.getDefault().post(new NotifyRefresh());
    }
}
