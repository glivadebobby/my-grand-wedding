package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.RsvpAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.RsvpCallback;
import com.mygrandwedding.mgw.model.Rsvp;
import com.mygrandwedding.mgw.model.RsvpData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;

public class RsvpActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Runnable, RsvpCallback {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private TextView textViewTotalAttenders;
    private RecyclerView viewRsvp;
    private List<Rsvp> rsvpList;
    private GridLayoutManager layoutManager;
    private RsvpAdapter rsvpAdapter;
    private MyPreference preference;
    private int id;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rsvp);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    @Override
    public void onRsvpClick(int position) {
        Rsvp rsvp = rsvpList.get(position);
        rsvp.setExpand(!rsvp.isExpand());
        rsvpAdapter.notifyItemChanged(position);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        textViewTotalAttenders = findViewById(R.id.txt_total_attenders);
        viewRsvp = findViewById(R.id.rsvp);

        context = this;
        preference = new MyPreference(context);
        rsvpList = new ArrayList<>();
        layoutManager = new GridLayoutManager(context, 2);
        rsvpAdapter = new RsvpAdapter(context, rsvpList, this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
        }
    }

    private void initRecyclerView() {
        viewRsvp.setLayoutManager(layoutManager);
        viewRsvp.setAdapter(rsvpAdapter);
        viewRsvp.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getRsvp();
                }
            }
        });
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (rsvpList.get(position).isExpand()) return 2;
                return 1;
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void resetList() {
        count = 0;
        setUrl();
        getRsvp();
    }

    private void setUrl() {
        url = MGW_URL + "rsvp/list/" + id + "/";
    }

    private void clearList() {
        if (count == 0) {
            rsvpList.clear();
            rsvpAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        if (rsvpList.isEmpty()) {
            Toast.makeText(context, "No RSVPs yet", Toast.LENGTH_SHORT).show();
        }
    }

    private void getRsvp() {
        setLoading(true);
        Call<RsvpData> call = getApiService().getRsvp(preference.getUserToken(), url);
        call.enqueue(new Callback<RsvpData>() {
            @Override
            public void onResponse(@NonNull Call<RsvpData> call, @NonNull Response<RsvpData> response) {
                refreshLayout.setRefreshing(false);
                setLoading(false);
                RsvpData rsvpDataResponse = response.body();
                if (response.isSuccessful() && rsvpDataResponse != null) {
                    clearList();
                    count = rsvpDataResponse.getCount();
                    textViewTotalAttenders.setVisibility(View.VISIBLE);
                    textViewTotalAttenders.setText(String.format(Locale.getDefault(),
                            getString(R.string.format_total_attenders), rsvpDataResponse.getTotalAttenders()));
                    url = rsvpDataResponse.getNextUrl();
                    rsvpList.addAll(rsvpDataResponse.getRsvpList());
                    rsvpAdapter.notifyDataSetChanged();
                    isEmpty();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<RsvpData> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                setLoading(false);
                processFailure(context, t);
            }
        });
    }
}
