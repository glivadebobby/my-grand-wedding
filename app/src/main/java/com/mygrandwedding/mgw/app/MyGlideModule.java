package com.mygrandwedding.mgw.app;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by gladwinbobby on 24/09/17
 */

@GlideModule
public class MyGlideModule extends AppGlideModule {
}
