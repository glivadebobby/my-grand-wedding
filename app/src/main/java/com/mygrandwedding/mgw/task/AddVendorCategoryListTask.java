package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.VendorCategory;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class AddVendorCategoryListTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private List<VendorCategory> vendorCategoryList;

    public AddVendorCategoryListTask(Context context, List<VendorCategory> vendorCategoryList) {
        this.context = context;
        this.vendorCategoryList = vendorCategoryList;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getAppDatabase(context).deleteDao().deleteVendorCategory();
        getAppDatabase(context).deleteDao().deleteVendorSubcategory();
        for (VendorCategory vendorCategory : vendorCategoryList) {
            if (vendorCategory.getSubcategoryList() != null) {
                vendorCategory.setSubcategoryCount(vendorCategory.getSubcategoryList().size());
                new AddVendorSubcategoryListTask(context, vendorCategory.getSubcategoryList()).execute();
            }
        }
        getAppDatabase(context).generalDao().insertVendorCategoryList(vendorCategoryList);
        return null;
    }
}
