package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.holder.WpPhotoHolder;

import java.util.List;

/**
 * Created by gladwinbobby on 05/11/17
 */

public class WpPhotoAdapter extends RecyclerView.Adapter<WpPhotoHolder> {

    private Context context;
    private List<Integer> photoList;

    public WpPhotoAdapter(Context context, List<Integer> photoList) {
        this.context = context;
        this.photoList = photoList;
    }

    @Override
    public WpPhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WpPhotoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wp_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(WpPhotoHolder holder, int position) {
        Glide.with(context)
                .load(photoList.get(position))
                .into(holder.imageViewPhoto);
    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }
}
