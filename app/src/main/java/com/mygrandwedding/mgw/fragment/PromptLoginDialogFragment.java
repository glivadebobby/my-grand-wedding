package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.mygrandwedding.mgw.LoginActivity;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.SignUpActivity;

import static com.mygrandwedding.mgw.app.MyActivity.launch;

/**
 * Created by gladwinbobby on 24/10/17
 */

public class PromptLoginDialogFragment extends DialogFragment implements View.OnClickListener {

    private Context context;
    private View rootView;
    private Button buttonRegister, buttonLogin;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_prompt_login, container, false);
        initObjects();
        initCallbacks();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonRegister) {
            launch(context, SignUpActivity.class);
            dismiss();
        } else if (view == buttonLogin) {
            launch(context, LoginActivity.class);
            dismiss();
        }
    }

    private void initObjects() {
        buttonRegister = rootView.findViewById(R.id.btn_register);
        buttonLogin = rootView.findViewById(R.id.btn_login);

        context = getActivity();
    }

    private void initCallbacks() {
        buttonRegister.setOnClickListener(this);
        buttonLogin.setOnClickListener(this);
    }
}
