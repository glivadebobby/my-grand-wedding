package com.mygrandwedding.mgw.fragment;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.mygrandwedding.mgw.GetQuoteActivity;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.VendorActivity;
import com.mygrandwedding.mgw.VendorSubcategoryActivity;
import com.mygrandwedding.mgw.WeddingPlannerActivity;
import com.mygrandwedding.mgw.adapter.VendorCategoryAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.CategoryCallback;
import com.mygrandwedding.mgw.model.NotifyRefresh;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.task.AddVendorCategoryListTask;
import com.mygrandwedding.mgw.viewmodel.VendorCategoryViewModel;
import com.yayandroid.parallaxrecyclerview.ParallaxRecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.CHENNAI_CITY_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

/**
 * A simple {@link Fragment} subclass.
 */
public class VendorCategoryFragment extends Fragment implements CategoryCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable, View.OnClickListener {

    private Context context;
    private View rootView;
    private SwipeRefreshLayout refreshLayout;
    private ParallaxRecyclerView viewCategories;
    private FloatingActionButton buttonSpecialRate;
    private List<VendorCategory> vendorCategoryList;
    private VendorCategoryAdapter categoryAdapter;
    private MyPreference preference;

    public VendorCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_vendor_category, container, false);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        loadVendorCategories();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onCategoryClick(int position) {
        preference.resetPriceRating();
        VendorCategory vendorCategory = vendorCategoryList.get(position);
        if (vendorCategory.getId() == 0) {
            launch(context, WeddingPlannerActivity.class);
        } else {
            Bundle bundle = new Bundle();
            bundle.putParcelable(EXTRA_VENDOR, vendorCategory);
            if (vendorCategory.getSubcategoryCount() > 0)
                launchWithBundle(context, VendorSubcategoryActivity.class, bundle);
            else launchWithBundle(context, VendorActivity.class, bundle);
        }
    }

    @Override
    public void onRefresh() {
        getCategories();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getCategories();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSpecialRate) {
            launch(context, GetQuoteActivity.class);
        }
    }

    @Subscribe
    public void refresh(NotifyRefresh notifyRefresh) {
        showCase();
        run();
    }

    private void initObjects() {
        refreshLayout = rootView.findViewById(R.id.refresh);
        viewCategories = rootView.findViewById(R.id.categories);
        buttonSpecialRate = rootView.findViewById(R.id.fab_special_rate);

        context = getActivity();
        vendorCategoryList = new ArrayList<>();
        categoryAdapter = new VendorCategoryAdapter(context, vendorCategoryList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        buttonSpecialRate.setOnClickListener(this);
    }

    private void initRecyclerView() {
        viewCategories.setLayoutManager(new LinearLayoutManager(context));
        viewCategories.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        viewCategories.setAdapter(categoryAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void loadVendorCategories() {
        VendorCategoryViewModel viewModel = ViewModelProviders.of(this).get(VendorCategoryViewModel.class);
        viewModel.getData(context).observe(this, new Observer<List<VendorCategory>>() {
            @Override
            public void onChanged(@Nullable List<VendorCategory> vendorCategories) {
                vendorCategoryList.clear();
                if (vendorCategories != null && !vendorCategories.isEmpty()) {
                    vendorCategoryList.addAll(vendorCategories);
                }
                if (preference.getCityId() == CHENNAI_CITY_ID) {
                    vendorCategoryList.add(0, new VendorCategory(0, 0, R.drawable.bg_wedding_planners, "Wedding Planners"));
                }
                categoryAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getCategories() {
        Call<List<VendorCategory>> call = getApiService()
                .getVendorCategory(preference.getCityId() > 0 ? preference.getCityId() : null);
        call.enqueue(new Callback<List<VendorCategory>>() {
            @Override
            public void onResponse(@NonNull Call<List<VendorCategory>> call,
                                   @NonNull Response<List<VendorCategory>> response) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    List<VendorCategory> categoryListResponse = response.body();
                    if (response.isSuccessful() && categoryListResponse != null) {
                        new AddVendorCategoryListTask(context, categoryListResponse).execute();
                    } else processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<VendorCategory>> call, @NonNull Throwable t) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    processFailure(context, t);
                }
            }
        });
    }

    private void showCase() {
        if (getActivity() == null || !preference.getIntro1() || (preference.getIntro2() && preference.getIntro3()))
            return;
        new TapTargetSequence(getActivity())
                .targets(TapTarget.forBounds(getRect(), "Scroll down and select to view vendors")
                                .icon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_vertical_scroll))
                                .tintTarget(false)
                                .cancelable(false)
                                .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                        TapTarget.forView(buttonSpecialRate, "Send your requirement and get special rates")
                                .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf"))
                                .tintTarget(false)
                                .cancelable(false))
                .listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        preference.setIntro2(true);
                        preference.setIntro3(true);
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }

    private Rect getRect() {
        int distance = 150;
        int width = getResources().getDisplayMetrics().widthPixels / 2;
        int height = getResources().getDisplayMetrics().heightPixels / 2;
        int left = width - distance;
        int top = height - distance;
        int right = width + distance;
        int bottom = height + distance;
        return new Rect(left, top, right, bottom);
    }
}
