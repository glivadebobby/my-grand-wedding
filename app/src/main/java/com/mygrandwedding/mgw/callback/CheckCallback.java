package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 18/10/17
 */

public interface CheckCallback {
    void onCheckClick(int position, int checkPosition);

    void onCheckedTextClick(int position, int checkPosition);
}
