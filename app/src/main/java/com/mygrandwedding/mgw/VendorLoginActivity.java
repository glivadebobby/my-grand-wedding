package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Login;
import com.mygrandwedding.mgw.model.User;

import java.util.Locale;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;

public class VendorLoginActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextView textViewForgotPassword, textViewSignUp;
    private TextInputLayout layoutUsername, layoutPassword;
    private TextInputEditText editTextUsername, editTextPassword;
    private Button buttonContinue;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_login);
        initObjects();
        initToolbar();
        initCallbacks();
        setPasswordFont();
        setSignUpText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextUsername) isValidUsername();
            if (view == editTextPassword) isValidPassword();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == textViewForgotPassword) launch(context, SendOtpActivity.class);
        else if (view == textViewSignUp) launch(context, VendorSignUpActivity.class);
        else if (view == buttonContinue) processLogin();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        textViewForgotPassword = findViewById(R.id.txt_forgot_password);
        textViewSignUp = findViewById(R.id.txt_sign_up);
        layoutUsername = findViewById(R.id.username);
        layoutPassword = findViewById(R.id.password);
        editTextUsername = findViewById(R.id.input_username);
        editTextPassword = findViewById(R.id.input_password);
        buttonContinue = findViewById(R.id.btn_continue);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        textViewForgotPassword.setOnClickListener(this);
        textViewSignUp.setOnClickListener(this);
        editTextUsername.setOnFocusChangeListener(this);
        editTextPassword.setOnFocusChangeListener(this);
        buttonContinue.setOnClickListener(this);
    }

    private void setPasswordFont() {
        layoutPassword.setTypeface(editTextUsername.getTypeface());
        editTextPassword.setTypeface(editTextUsername.getTypeface());
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void setSignUpText() {
        SpannableString spannableString = new SpannableString(getString(R.string.txt_new_vendor));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_txt)), 17, 24, 0);
        spannableString.setSpan(new RelativeSizeSpan(1.05f), 17, 24, 0);
        textViewSignUp.setText(spannableString);
    }

    private void processLogin() {
        String username = editTextUsername.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        if (isValidUsername(username) && isValidPassword(password)) {
            setLoading(true);
            login(new Login(preference.getRole(), username, password, UUID.randomUUID().toString()));
        }
    }

    private void isValidUsername() {
        isValidUsername(editTextUsername.getText().toString().trim());
    }

    private boolean isValidUsername(String username) {
        if (username.isEmpty()) {
            layoutUsername.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutUsername.getHint()));
            return false;
        }
        layoutUsername.setErrorEnabled(false);
        return true;
    }

    private void isValidPassword() {
        isValidPassword(editTextPassword.getText().toString().trim());
    }

    private boolean isValidPassword(String password) {
        if (password.isEmpty()) {
            layoutPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPassword.getHint()));
            return false;
        } else if (password.length() < 6) {
            layoutPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        layoutPassword.setErrorEnabled(false);
        return true;
    }

    private void login(Login login) {
        Call<User> call = getApiService().login(login);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                setLoading(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    preference.setId(userResponse.getId());
                    preference.setName(userResponse.getFirstName());
                    preference.setPhone(userResponse.getUsername());
                    preference.setEmail(userResponse.getEmail());
                    preference.setToken(userResponse.getToken());
                    launchClearStack(context, VendorMainActivity.class);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
