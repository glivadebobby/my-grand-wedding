package com.mygrandwedding.mgw.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.holder.FaqHolder;
import com.mygrandwedding.mgw.model.Faq;

import java.util.List;

/**
 * Created by gladwinbobby on 11/10/17
 */

public class FaqAdapter extends RecyclerView.Adapter<FaqHolder> {

    private List<Faq> faqList;

    public FaqAdapter(List<Faq> faqList) {
        this.faqList = faqList;
    }

    @Override
    public FaqHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new FaqHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_faq, parent, false));
    }

    @Override
    public void onBindViewHolder(FaqHolder holder, int position) {
        Faq faq = faqList.get(position);
        holder.textViewQuestion.setText(faq.getQuestion());
        holder.textViewAnswer.setText(faq.getAnswer());
    }

    @Override
    public int getItemCount() {
        return faqList.size();
    }
}
