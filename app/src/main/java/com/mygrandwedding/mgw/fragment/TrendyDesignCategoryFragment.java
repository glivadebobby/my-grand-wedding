package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.TrendyDesignActivity;
import com.mygrandwedding.mgw.adapter.TrendyDesignAdapter;
import com.mygrandwedding.mgw.callback.TrendyDesignCallback;
import com.mygrandwedding.mgw.model.TrendyDesign;
import com.mygrandwedding.mgw.model.TrendyDesignData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrendyDesignCategoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, Runnable, TrendyDesignCallback {

    private Context context;
    private View rootView;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewTrendyDesign;
    private List<TrendyDesign> trendyDesignList;
    private LinearLayoutManager layoutManager;
    private TrendyDesignAdapter trendyDesignAdapter;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    public TrendyDesignCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_trendy_design_category, container, false);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        return rootView;
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    @Override
    public void onTrendyDesignClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, trendyDesignList.get(position).getId());
        bundle.putString(EXTRA_TITLE, trendyDesignList.get(position).getTitle());
        launchWithBundle(context, TrendyDesignActivity.class, bundle);
    }

    private void initObjects() {
        refreshLayout = rootView.findViewById(R.id.refresh);
        viewTrendyDesign = rootView.findViewById(R.id.trendy_design);

        context = getActivity();
        trendyDesignList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        trendyDesignAdapter = new TrendyDesignAdapter(context, trendyDesignList, this);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecyclerView() {
        viewTrendyDesign.setLayoutManager(new GridLayoutManager(context, 2));
        viewTrendyDesign.setAdapter(trendyDesignAdapter);
        viewTrendyDesign.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getDesigns();
                }
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void resetList() {
        count = 0;
        setUrl();
        getDesigns();
    }

    private void setUrl() {
        url = MGW_URL + "trendy-designs/list/";
    }

    private void clearList() {
        if (count == 0) {
            trendyDesignList.clear();
            trendyDesignAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        if (trendyDesignList.isEmpty()) {
            Toast.makeText(context, "No trendy designs found", Toast.LENGTH_SHORT).show();
        }
    }

    private void getDesigns() {
        setLoading(true);
        Call<TrendyDesignData> call = getApiService().getDesigns(url);
        call.enqueue(new Callback<TrendyDesignData>() {
            @Override
            public void onResponse(@NonNull Call<TrendyDesignData> call,
                                   @NonNull Response<TrendyDesignData> response) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    TrendyDesignData trendyDesignDataResponse = response.body();
                    if (response.isSuccessful() && trendyDesignDataResponse != null) {
                        setLoading(false);
                        clearList();
                        count = trendyDesignDataResponse.getCount();
                        url = trendyDesignDataResponse.getNextUrl();
                        trendyDesignList.addAll(trendyDesignDataResponse.getTrendyDesignList());
                        trendyDesignAdapter.notifyDataSetChanged();
                        isEmpty();
                    } else processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<TrendyDesignData> call, @NonNull Throwable t) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    processFailure(context, t);
                }
            }
        });
    }
}