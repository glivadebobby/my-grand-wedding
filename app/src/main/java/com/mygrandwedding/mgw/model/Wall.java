package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import static com.mygrandwedding.mgw.util.DateParser.getRelativeTime;

/**
 * Created by gladwinbobby on 23/10/17
 */

public class Wall {

    @SerializedName("id")
    private int id;
    @SerializedName("wedding")
    private int wedding;
    @SerializedName("type")
    private int type;
    @SerializedName("role")
    private String role;
    @SerializedName("created_on")
    private String createdOn;
    @SerializedName("user")
    private User user;
    @SerializedName("vendor")
    private Vendor vendor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWedding() {
        return wedding;
    }

    public void setWedding(int wedding) {
        this.wedding = wedding;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getCreatedOn() {
        return getRelativeTime(createdOn);
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }
}
