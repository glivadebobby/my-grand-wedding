package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.ImageCallback;

/**
 * Created by gladwinbobby on 27/09/17
 */

public class ImageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewVendor, imageViewRemove;
    private ImageCallback callback;

    public ImageHolder(View itemView, ImageCallback callback) {
        super(itemView);
        imageViewVendor = itemView.findViewById(R.id.img_vendor);
        imageViewRemove = itemView.findViewById(R.id.img_remove);
        this.callback = callback;
        imageViewVendor.setOnClickListener(this);
        imageViewRemove.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewVendor) {
            callback.onPictureClick(getLayoutPosition() - 1);
        } else if (view == imageViewRemove) {
            callback.onRemoveClick(getLayoutPosition() - 1);
        }
    }
}
