package com.mygrandwedding.mgw;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.AssociatedWeddingAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.AssociatedWeddingCallback;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.Wedding;
import com.mygrandwedding.mgw.task.AddWeddingListTask;
import com.mygrandwedding.mgw.task.RemoveWeddingTask;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

public class AssociatedWeddingActivity extends AppCompatActivity
        implements SwipeRefreshLayout.OnRefreshListener, Runnable, AssociatedWeddingCallback {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewAssociatedWedding;
    private FrameLayout layoutLoading;
    private List<Wedding> weddingList;
    private AssociatedWeddingAdapter weddingAdapter;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_associated_wedding);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
        loadAssociatedWedding();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getAssociatedWedding();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getAssociatedWedding();
    }

    @Override
    public void onRemoveClick(int position) {
        Wedding wedding = weddingList.get(position);
        setLoading(true);
        removeWedding(wedding);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewAssociatedWedding = findViewById(R.id.associated_wedding);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        weddingList = new ArrayList<>();
        weddingAdapter = new AssociatedWeddingAdapter(context, weddingList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewAssociatedWedding.setLayoutManager(new LinearLayoutManager(context));
        viewAssociatedWedding.setAdapter(weddingAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
    }

    private void loadAssociatedWedding() {
        LiveData<List<Wedding>> data = getAppDatabase(context).weddingDao().getAssociatedWeddings();
        data.observe(this, new Observer<List<Wedding>>() {
            @Override
            public void onChanged(@Nullable List<Wedding> weddings) {
                weddingList.clear();
                if (weddings != null && !weddings.isEmpty()) {
                    weddingList.addAll(weddings);
                } else {
                    refreshLayout.setRefreshing(true);
                    getAssociatedWedding();
                }
                weddingAdapter.notifyDataSetChanged();
            }
        });
    }

    private void getAssociatedWedding() {
        Call<List<Wedding>> call = getApiService().getAssociatedWedding(preference.getUserToken());
        call.enqueue(new Callback<List<Wedding>>() {
            @Override
            public void onResponse(@NonNull Call<List<Wedding>> call,
                                   @NonNull Response<List<Wedding>> response) {
                refreshLayout.setRefreshing(false);
                List<Wedding> weddingListResponse = response.body();
                if (response.isSuccessful() && weddingListResponse != null) {
                    new AddWeddingListTask(context, weddingListResponse).execute();
                    if (weddingListResponse.isEmpty()) {
                        Toast.makeText(context, "No weddings found", Toast.LENGTH_SHORT).show();
                    }
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<List<Wedding>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void removeWedding(final Wedding wedding) {
        if (preference.getUserToken() == null) return;
        Call<Data> call = getApiService().removeWedding(preference.getUserToken(), wedding.getId());
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    Toast.makeText(context, dataResponse.getData(), Toast.LENGTH_SHORT).show();
                    new RemoveWeddingTask(context, wedding).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
