package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.WallAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.VendorCallback;
import com.mygrandwedding.mgw.model.Wall;
import com.mygrandwedding.mgw.model.WallData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_IMAGE;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class WallActivity extends AppCompatActivity implements VendorCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewWall;
    private List<Wall> wallList;
    private LinearLayoutManager layoutManager;
    private WallAdapter wallAdapter;
    private MyPreference preference;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wall);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            finish();
            launch(context, MainActivity.class);
        } else
            super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onVendorClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, wallList.get(position).getVendor().getId());
        bundle.putString(EXTRA_IMAGE, wallList.get(position).getVendor().getImage());
        launchWithBundle(context, VendorDetailActivity.class, bundle);
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewWall = findViewById(R.id.wall);

        context = this;
        wallList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        wallAdapter = new WallAdapter(context, wallList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewWall.setLayoutManager(layoutManager);
        viewWall.setAdapter(wallAdapter);
        viewWall.setNestedScrollingEnabled(false);
        viewWall.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getWall();
                }
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void resetList() {
        count = 0;
        setUrl();
        getWall();
    }

    private void setUrl() {
        url = MGW_URL + "wall/list/" + preference.getWeddingId() + "/";
    }

    private void clearList() {
        if (count == 0) {
            wallList.clear();
            wallAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        if (wallList.isEmpty()) {
            Toast.makeText(context, "No activities found", Toast.LENGTH_SHORT).show();
        }
    }

    private void getWall() {
        setLoading(true);
        Call<WallData> call = getApiService().getWall(preference.getUserToken(), url);
        call.enqueue(new Callback<WallData>() {
            @Override
            public void onResponse(@NonNull Call<WallData> call,
                                   @NonNull Response<WallData> response) {
                refreshLayout.setRefreshing(false);
                WallData wallDataResponse = response.body();
                if (response.isSuccessful() && wallDataResponse != null) {
                    setLoading(false);
                    clearList();
                    count = wallDataResponse.getCount();
                    url = wallDataResponse.getNextUrl();
                    wallList.addAll(wallDataResponse.getWallList());
                    wallAdapter.notifyDataSetChanged();
                    isEmpty();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<WallData> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
