package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 22/10/17
 */

public class FcmToken {

    @SerializedName("client")
    private String client;
    @SerializedName("fcm_token")
    private String token;

    public FcmToken(String client, String token) {
        this.client = client;
        this.token = token;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
