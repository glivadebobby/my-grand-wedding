package com.mygrandwedding.mgw.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.mygrandwedding.mgw.model.User;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by gladwinbobby on 03/10/17
 */

@Dao
public interface UserDao {
    @Insert(onConflict = REPLACE)
    void insertUser(User user);

    @Query("SELECT * FROM User WHERE id =:userId")
    User getUserData(int userId);

    @Query("SELECT * FROM User WHERE id =:userId")
    LiveData<User> getUser(int userId);

    @Update
    void updateUser(User user);
}
