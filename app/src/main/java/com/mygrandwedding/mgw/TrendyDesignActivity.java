package com.mygrandwedding.mgw;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.mygrandwedding.mgw.adapter.ImageAdapter;
import com.mygrandwedding.mgw.app.MyActivity;
import com.mygrandwedding.mgw.callback.PhotoCallback;
import com.mygrandwedding.mgw.model.Image;
import com.mygrandwedding.mgw.model.TrendyDesign;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_PHOTOS;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_POSITION;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class TrendyDesignActivity extends AppCompatActivity implements PhotoCallback, Runnable,
        SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewPhotos;
    private ArrayList<Image> imageList;
    private ImageAdapter imageAdapter;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trendy_design);
        initObjects();
        initToolbar();
        processBundle();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_share:
                shareTrendyDesign();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getDesigns();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getDesigns();
    }

    @Override
    public void onPhotoClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, id);
        bundle.putInt(EXTRA_POSITION, position);
        bundle.putString(EXTRA_TITLE, getIntent().getStringExtra(EXTRA_TITLE));
        bundle.putParcelableArrayList(EXTRA_PHOTOS, imageList);
        launchWithBundle(context, PhotoDetailActivity.class, bundle);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewPhotos = findViewById(R.id.photos);

        context = this;
        imageList = new ArrayList<>();
        imageAdapter = new ImageAdapter(context, imageList, this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(bundle.getString(EXTRA_TITLE));
        }
    }

    private void initRecyclerView() {
        viewPhotos.setLayoutManager(new GridLayoutManager(context, 2));
        viewPhotos.setAdapter(imageAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.post(this);
    }

    private void getDesigns() {
        Call<TrendyDesign> call = getApiService().getTrendyDesignDetail(id);
        call.enqueue(new Callback<TrendyDesign>() {
            @Override
            public void onResponse(@NonNull Call<TrendyDesign> call, @NonNull Response<TrendyDesign> response) {
                refreshLayout.setRefreshing(false);
                TrendyDesign trendyDesignResponse = response.body();
                if (response.isSuccessful() && trendyDesignResponse != null) {
                    imageList.clear();
                    imageList.addAll(trendyDesignResponse.getImageList());
                    imageAdapter.notifyDataSetChanged();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<TrendyDesign> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void shareTrendyDesign() {
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://mygrandwedding.com/article.php" + "?id=" + id + "&type=" + 2))
                .setDynamicLinkDomain("dr779.app.goo.gl")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.mygrandwedding.mgw").build())
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            MyActivity.shareInspirations(context, shortLink.toString());
                        } else {
                            Toast.makeText(context, "Unable to share! Try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
