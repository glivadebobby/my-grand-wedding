package com.mygrandwedding.mgw;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.mygrandwedding.mgw.adapter.WeddingMenuAdapter;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.WeddingMenuCallback;
import com.mygrandwedding.mgw.fragment.InspirationFragment;
import com.mygrandwedding.mgw.fragment.MyWeddingFragment;
import com.mygrandwedding.mgw.fragment.PromptLoginDialogFragment;
import com.mygrandwedding.mgw.fragment.SpecialRateDialogFragment;
import com.mygrandwedding.mgw.fragment.VendorCategoryFragment;
import com.mygrandwedding.mgw.model.FcmToken;
import com.mygrandwedding.mgw.model.Location;
import com.mygrandwedding.mgw.model.NotifyRefresh;
import com.mygrandwedding.mgw.model.User;
import com.mygrandwedding.mgw.model.Wedding;
import com.mygrandwedding.mgw.task.AddUserTask;
import com.mygrandwedding.mgw.task.AddWeddingListTask;
import com.mygrandwedding.mgw.task.LogoutTask;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_CODE;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.HELP_DESK;
import static com.mygrandwedding.mgw.app.Constant.SPECIAL_RATE_SESSION;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchDialPad;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithResult;
import static com.mygrandwedding.mgw.app.MyActivity.rateApp;
import static com.mygrandwedding.mgw.app.MyActivity.shareApp;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;
import static com.mygrandwedding.mgw.util.MyParser.parseNull;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemReselectedListener, NavigationView.OnNavigationItemSelectedListener, WeddingMenuCallback, View.OnClickListener {

    private static final int REQUEST_CITY = 4;
    private static final String NAV_VENDOR = "vendor";
    private static final String NAV_MY_WEDDING = "my-wedding";
    private static final String NAV_INSPIRATION = "inspiration";
    private Context context;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private TextView textViewLocation;
    private BottomNavigationView bottomNavigationView;
    private FrameLayout layoutLoading;
    private NavigationView navigationView;
    private ActionBarDrawerToggle drawerToggle;
    private Wedding wedding;
    private List<Wedding> weddingList;
    private WeddingMenuAdapter adapter;
    private MyPreference preference;
    private long backPressTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
        initToolbar();
        initDrawer();
        onNavigationItemSelected(bottomNavigationView.getMenu().getItem(0));
        loadUserProfile();
        loadAssociatedWedding();
        loadMyWedding();
        loadLocations();
        processMenuItems();
        displayVendorDetail();
        displayJoinWedding();
        displayArticle();
        updateFcm();
        showCase();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if ((backPressTime + 5000 - System.currentTimeMillis()) < 0) {
            Toast.makeText(context, "Press back again to exit", Toast.LENGTH_SHORT).show();
            backPressTime = System.currentTimeMillis();
        } else
            super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(Gravity.START);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CITY && resultCode == RESULT_OK) {
            loadLocations();
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onClick(View view) {
        if (view == textViewLocation) {
            launchWithResult(this, SelectCityActivity.class, REQUEST_CITY);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawerLayout.closeDrawers();
        switch (item.getItemId()) {
            case R.id.nav_login:
                Bundle bundle = new Bundle();
                bundle.putInt(EXTRA_ID, 3);
                launchWithBundle(this, IntroActivity.class, bundle);
                return false;
            case R.id.nav_join_wedding:
                if (preference.getToken() == null)
                    new PromptLoginDialogFragment().show(getSupportFragmentManager(), "prompt-login");
                else
                    launch(context, JoinWeddingActivity.class);
                return false;
            case R.id.nav_create_wedding:
                if (preference.getToken() == null)
                    new PromptLoginDialogFragment().show(getSupportFragmentManager(), "prompt-login");
                else
                    launch(context, MyWeddingActivity.class);
                return false;
            case R.id.nav_my_wedding_settings:
                launch(context, MyWeddingSettingsActivity.class);
                return false;
            case R.id.nav_my_associated_wedding:
                launch(context, AssociatedWeddingActivity.class);
                return false;
            case R.id.nav_my_shortlists:
                launch(context, ShortlistedActivity.class);
                return false;
            case R.id.nav_my_loves:
                launch(context, LovedActivity.class);
                return false;
            case R.id.nav_special_rate:
                launch(context, GetQuoteActivity.class);
                return false;
            case R.id.nav_promotions:
                launch(context, PromotionActivity.class);
                return false;
            case R.id.nav_profile:
                launch(context, ProfileActivity.class);
                return false;
            case R.id.nav_contact_us:
                launch(context, ContactUsActivity.class);
                return false;
            case R.id.nav_rate_us:
                rateApp(context);
                return false;
            case R.id.nav_share_app:
                shareApp(context);
                return false;
            case R.id.nav_faq:
                launch(context, FaqActivity.class);
                return false;
            case R.id.nav_about_us:
                launch(context, AboutActivity.class);
                return false;
            case R.id.nav_call_us:
                launchDialPad(context, HELP_DESK);
                return false;
            case R.id.nav_privacy_policy:
                launch(context, PrivacyPolicyActivity.class);
                return false;
            case R.id.nav_terms:
                launch(context, TermsConditionsActivity.class);
                return false;
            case R.id.nav_vendors:
                textViewLocation.setVisibility(View.VISIBLE);
                setFragment(new VendorCategoryFragment(), NAV_VENDOR);
                return true;
            case R.id.nav_my_wedding:
                textViewLocation.setVisibility(View.INVISIBLE);
                setFragment(new MyWeddingFragment(), NAV_MY_WEDDING);
                return true;
            case R.id.nav_inspirations:
                textViewLocation.setVisibility(View.INVISIBLE);
                setFragment(new InspirationFragment(), NAV_INSPIRATION);
                return true;
            case R.id.nav_logout:
                logoutAlert();
                return false;
            default:
                return false;
        }
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {

    }

    @Override
    public void onWeddingClick(int position) {
        Wedding wedding = weddingList.get(position);
        for (Wedding wed : weddingList) {
            wed.setSelected(false);
        }
        wedding.setSelected(true);
        adapter.notifyDataSetChanged();
        preference.setWeddingId(wedding.getId());
        int bride = wedding.getBride() == null ? 0 : wedding.getBride();
        int groom = wedding.getGroom() == null ? 0 : wedding.getGroom();
        preference.setCollaborator((bride != preference.getId()) && (groom != preference.getId()));
        EventBus.getDefault().post(new NotifyRefresh());
    }

    private void initObjects() {
        drawerLayout = findViewById(R.id.drawer);
        toolbar = findViewById(R.id.toolbar);
        textViewLocation = findViewById(R.id.txt_location);
        bottomNavigationView = findViewById(R.id.nav_main);
        navigationView = findViewById(R.id.nav);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        weddingList = new ArrayList<>();
        adapter = new WeddingMenuAdapter(context, weddingList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        textViewLocation.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setOnNavigationItemReselectedListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initDrawer() {
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void loadUserProfile() {
        if (preference.getToken() == null) return;
        LiveData<User> data = getAppDatabase(context).userDao().getUser(preference.getId());
        data.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                View headerView = navigationView.getHeaderView(0);
                RelativeLayout layoutProfile = headerView.findViewById(R.id.profile);
                CircleImageView imageViewProfile = headerView.findViewById(R.id.img_profile);
                TextView textViewName = headerView.findViewById(R.id.txt_name);
                layoutProfile.setVisibility(View.VISIBLE);
                layoutProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        drawerLayout.closeDrawers();
                        launch(context, ProfileActivity.class);
                    }
                });
                if (user != null) {
                    GlideApp.with(context)
                            .load(user.getUserProfile().getProfilePic())
                            .placeholder(R.drawable.placeholder)
                            .into(imageViewProfile);
                    textViewName.setText(parseNull(user.getFirstName()));
                } else getUser();
            }
        });
    }

    private void loadAssociatedWedding() {
        if (preference.getToken() == null) return;
        LiveData<List<Wedding>> data = getAppDatabase(context).weddingDao().getAssociatedWeddings();
        data.observe(this, new Observer<List<Wedding>>() {
            @Override
            public void onChanged(@Nullable List<Wedding> weddings) {
                View headerView = navigationView.getHeaderView(0);
                TextView textViewWeddings = headerView.findViewById(R.id.txt_weddings);
                RecyclerView viewWeddings = headerView.findViewById(R.id.weddings);
                viewWeddings.setLayoutManager(new LinearLayoutManager(context));
                viewWeddings.setAdapter(adapter);
                weddingList.clear();
                if (weddings != null && !weddings.isEmpty()) {
                    weddingList.addAll(weddings);
                    adapter.notifyDataSetChanged();
                    if (preference.getWeddingId() != -1) {
                        for (Wedding wedding : weddingList) {
                            if (wedding.getId() == preference.getWeddingId()) {
                                wedding.setSelected(true);
                                break;
                            }
                        }
                        adapter.notifyDataSetChanged();
                    } else {
                        boolean selected = false;
                        if (wedding != null) {
                            for (Wedding wedding : weddingList) {
                                if (wedding.getId() == MainActivity.this.wedding.getId()) {
                                    selected = true;
                                    wedding.setSelected(true);
                                    preference.setWeddingId(wedding.getId());
                                    break;
                                }
                            }
                            adapter.notifyDataSetChanged();
                        }
                        if (!selected) onWeddingClick(0);
                    }
                } else {
                    preference.setWeddingId(-1);
                    preference.setCollaborator(false);
                    getAssociatedWedding();
                }
                textViewWeddings.setVisibility(weddingList.isEmpty() ? View.GONE : View.VISIBLE);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void loadMyWedding() {
        if (preference.getToken() == null) return;
        LiveData<Wedding> data = getAppDatabase(context).weddingDao().getMyWedding(preference.getId());
        data.observe(this, new Observer<Wedding>() {
            @Override
            public void onChanged(@Nullable Wedding wedding) {
                MainActivity.this.wedding = wedding;
                Menu menu = navigationView.getMenu();
                menu.findItem(R.id.nav_create_wedding).setVisible(wedding == null);
                menu.findItem(R.id.nav_my_wedding_settings).setVisible(wedding != null);
                menu.findItem(R.id.nav_my_associated_wedding).setVisible(wedding != null);
            }
        });
    }

    private void loadLocations() {
        LiveData<List<Location>> data = getAppDatabase(context).generalDao().getLocations();
        data.observe(this, new Observer<List<Location>>() {
            @Override
            public void onChanged(@Nullable List<Location> locations) {
                if (locations != null && !locations.isEmpty()) {
                    if (preference.getCityId() < 1) {
                        textViewLocation.setText(getString(R.string.txt_all_cities));
                    } else {
                        for (Location location : locations) {
                            if (location.getId() == preference.getCityId()) {
                                textViewLocation.setText(location.getLocation());
                            }
                        }
                    }
                } else textViewLocation.setText(getString(R.string.txt_all_cities));
                EventBus.getDefault().post(new NotifyRefresh());
            }
        });
    }

    private void processMenuItems() {
        Menu menu = navigationView.getMenu();
        menu.findItem(R.id.nav_login).setVisible(preference.getToken() == null);
        menu.findItem(R.id.nav_my_shortlists).setVisible(preference.getToken() != null);
        menu.findItem(R.id.nav_my_loves).setVisible(preference.getToken() != null);
        menu.findItem(R.id.nav_promotions).setVisible(preference.getToken() != null);
        menu.findItem(R.id.nav_profile).setVisible(preference.getToken() != null);
        menu.findItem(R.id.nav_logout).setVisible(preference.getToken() != null);
    }

    private void displaySpecialRateDialog() {
        if (preference.getSessionCount() % SPECIAL_RATE_SESSION == 0)
            new SpecialRateDialogFragment().show(getSupportFragmentManager(), "special-rate");
        preference.incrementSession();
    }

    private void displayVendorDetail() {
        if (preference.getType() == -1 && preference.getVendorId() != -1) {
            Bundle bundle = new Bundle();
            bundle.putInt(EXTRA_ID, preference.getVendorId());
            launchWithBundle(context, VendorDetailActivity.class, bundle);
            preference.setVendorId(-1);
        }
    }

    private void displayJoinWedding() {
        if (preference.getToken() != null && preference.getCode() != null) {
            Bundle bundle = new Bundle();
            bundle.putString(EXTRA_CODE, preference.getCode());
            launchWithBundle(context, JoinWeddingActivity.class, bundle);
            preference.setCode(null);
        }
    }

    private void displayArticle() {
        if (preference.getType() != -1 && preference.getVendorId() != -1) {
            Bundle bundle = new Bundle();
            bundle.putInt(EXTRA_ID, preference.getVendorId());
            switch (preference.getType()) {
                case 1:
                    launchWithBundle(context, IdeaDetailActivity.class, bundle);
                    break;
                case 2:
                    launchWithBundle(context, TrendyDesignActivity.class, bundle);
                    break;
                case 3:
                    launchWithBundle(context, BlogDetailActivity.class, bundle);
                    break;
            }
            preference.setVendorId(-1);
            preference.setType(-1);
        }
    }

    private void setFragment(Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                android.R.anim.fade_out, android.R.anim.fade_in);
        fragmentTransaction.replace(R.id.container, fragment, tag);
        fragmentTransaction.commit();
    }

    private void logoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setLoading(true);
                logout();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getUser() {
        Call<User> call = getApiService().getUser(preference.getUserToken());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    new AddUserTask(context, userResponse).execute();
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
            }
        });
    }

    private void getAssociatedWedding() {
        Call<List<Wedding>> call = getApiService().getAssociatedWedding(preference.getUserToken());
        call.enqueue(new Callback<List<Wedding>>() {
            @Override
            public void onResponse(@NonNull Call<List<Wedding>> call,
                                   @NonNull Response<List<Wedding>> response) {
                List<Wedding> weddingListResponse = response.body();
                if (response.isSuccessful() && weddingListResponse != null) {
                    new AddWeddingListTask(context, weddingListResponse).execute();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Wedding>> call, @NonNull Throwable t) {
            }
        });
    }

    private void updateFcm() {
        if (preference.isTokenUploaded() || preference.getFcmToken() == null || preference.getToken() == null)
            return;
        Call<Void> call = getApiService().updateFcm(preference.getUserToken(),
                new FcmToken(UUID.randomUUID().toString(), preference.getFcmToken()));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful())
                    preference.setTokenUploaded(true);
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
            }
        });
    }

    private void logout() {
        Call<Void> call = getApiService().logout(preference.getUserToken());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, getString(R.string.alert_logout), Toast.LENGTH_SHORT).show();
                    new LogoutTask(context).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    private void showCase() {
        if (preference.getIntro1()) {
            displaySpecialRateDialog();
            return;
        }
        TapTargetView.showFor(this,
                TapTarget.forView(textViewLocation, "Select the city of your choice", "Tap on the down arrow")
                        .tintTarget(false)
                        .cancelable(false)
                        .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        preference.setIntro1(true);
                        EventBus.getDefault().post(new NotifyRefresh());
                    }
                });
    }
}
