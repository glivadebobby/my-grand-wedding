package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;

/**
 * Created by gladwinbobby on 28/09/17
 */

public class QuoteHolder extends RecyclerView.ViewHolder {

    public TextView textViewCustomerName, textViewLocation, textViewEmail, textViewMobileNumber,
            textViewRequirement, textViewMessage;

    public QuoteHolder(View itemView) {
        super(itemView);
        textViewCustomerName = itemView.findViewById(R.id.txt_customer_name);
        textViewLocation = itemView.findViewById(R.id.txt_location);
        textViewEmail = itemView.findViewById(R.id.txt_email);
        textViewMobileNumber = itemView.findViewById(R.id.txt_mobile_number);
        textViewRequirement = itemView.findViewById(R.id.txt_requirement);
        textViewMessage = itemView.findViewById(R.id.txt_message);
    }
}
