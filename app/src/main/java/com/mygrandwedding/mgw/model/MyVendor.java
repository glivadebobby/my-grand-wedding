package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import static com.mygrandwedding.mgw.util.DateParser.getRelativeTime;

/**
 * Created by gladwinbobby on 26/11/17
 */

public class MyVendor {

    @SerializedName("id")
    private int id;
    @SerializedName("created_on")
    private String date;
    @SerializedName("vendor")
    private Vendor vendor;
    @SerializedName("wedding")
    private Wedding wedding;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return getRelativeTime(date);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Wedding getWedding() {
        return wedding;
    }

    public void setWedding(Wedding wedding) {
        this.wedding = wedding;
    }
}
