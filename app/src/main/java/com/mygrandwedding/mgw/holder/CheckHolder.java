package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckedTextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.CheckCallback;

/**
 * Created by gladwinbobby on 18/10/17
 */

public class CheckHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public CheckedTextView checkedTextView;
    private CheckCallback callback;
    private int position;

    public CheckHolder(View itemView, CheckCallback callback, int position) {
        super(itemView);
        checkedTextView = itemView.findViewById(R.id.txt_check);
        this.callback = callback;
        this.position = position;
        itemView.setOnClickListener(this);
        checkedTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onCheckClick(position, getLayoutPosition());
        } else if (view == checkedTextView) {
            callback.onCheckedTextClick(position, getLayoutPosition());
        }
    }
}
