package com.mygrandwedding.mgw.util;

import android.annotation.SuppressLint;

import com.mygrandwedding.mgw.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by gladwinbobby on 31/10/17
 */

public class MyData {

    public static final int WEDDING_PLANNERS = 0;
    public static final int MANDAPAM = 1;
    public static final int HOTEL_RESORT = 2;
    public static final int FOOD_COST = 3;
    public static final int VENUE_COST = 4;
    public static final int WEDDING_VENUES = 62;
    public static final int WEDDING_DECORATORS = 63;
    public static final int WEDDING_CATERERS = 64;
    public static final int INVITATION_CARDS = 65;
    public static final int WEDDING_PHOTO_VIDEO = 66;
    public static final int BRIDAL_WEAR = 67;
    public static final int GROOM_WEAR = 68;
    public static final int FASHION_JEWELRY = 69;
    public static final int REAL_JEWELRY = 70;
    public static final int MEHANDI_ARTIST = 71;
    public static final int RETURN_GIFTS = 72;
    public static final int MAKEUP_ARTISTS = 73;
    public static final int CHOREOGRAPHERS = 74;
    public static final int DJ_MUSIC = 75;
    public static final int PANDIT_PUROHIT = 76;
    public static final int WEDDING_CAKES = 77;
    public static final int ENTERTAINMENTS = 78;
    public static final int TRANSPORTATIONS = 79;
    public static final int OTHERS = 81;
    public static final int HONEYMOON_PACKAGES = 83;
    public static final int GUEST_ROOMS = 84;

    public static Integer getCategoryBg(int id) {
        return getCategoryImages().get(id);
    }

    public static Integer getCategoryIcon(int id) {
        return getCategoryIcons().get(id);
    }

    public static String getCategoryTitle(int id) {
        return getCategoryTitles().get(id);
    }

    public static String getNoOf(int id) {
        return getNoOf().get(id);
    }

    public static String getCostPer(int id) {
        return getCostPer().get(id);
    }

    private static Map<Integer, Integer> getCategoryImages() {
        @SuppressLint("UseSparseArrays") Map<Integer, Integer> map = new HashMap<>();
        map.put(HONEYMOON_PACKAGES, R.drawable.bg_honeymoon_packages);
        map.put(OTHERS, R.drawable.bg_others);
        map.put(TRANSPORTATIONS, R.drawable.bg_transportations);
        map.put(ENTERTAINMENTS, R.drawable.bg_entertainments);
        map.put(WEDDING_CAKES, R.drawable.bg_wedding_cakes);
        map.put(PANDIT_PUROHIT, R.drawable.bg_pandit_purohit);
        map.put(DJ_MUSIC, R.drawable.bg_dj_music);
        map.put(CHOREOGRAPHERS, R.drawable.bg_choreographers);
        map.put(MAKEUP_ARTISTS, R.drawable.bg_makeup_artists);
        map.put(RETURN_GIFTS, R.drawable.bg_return_gifts);
        map.put(MEHANDI_ARTIST, R.drawable.bg_mehandi_artists);
        map.put(REAL_JEWELRY, R.drawable.bg_real_jewelry);
        map.put(FASHION_JEWELRY, R.drawable.bg_fashion_jewelry);
        map.put(GROOM_WEAR, R.drawable.bg_groom_wear);
        map.put(BRIDAL_WEAR, R.drawable.bg_bridal_wear);
        map.put(WEDDING_PHOTO_VIDEO, R.drawable.bg_wedding_photo_video);
        map.put(INVITATION_CARDS, R.drawable.bg_invitation_cards);
        map.put(WEDDING_CATERERS, R.drawable.bg_wedding_caterers);
        map.put(WEDDING_DECORATORS, R.drawable.bg_wedding_decorators);
        map.put(WEDDING_VENUES, R.drawable.bg_wedding_venues);
        return map;
    }

    private static Map<Integer, Integer> getCategoryIcons() {
        @SuppressLint("UseSparseArrays") Map<Integer, Integer> map = new HashMap<>();
        map.put(GUEST_ROOMS, R.drawable.ic_guest_room);
        map.put(HONEYMOON_PACKAGES, R.drawable.ic_honeymoon_packages);
        map.put(OTHERS, R.drawable.ic_others);
        map.put(TRANSPORTATIONS, R.drawable.ic_transportations);
        map.put(ENTERTAINMENTS, R.drawable.ic_entertainments);
        map.put(WEDDING_CAKES, R.drawable.ic_wedding_cakes);
        map.put(PANDIT_PUROHIT, R.drawable.ic_pandit_purohit);
        map.put(DJ_MUSIC, R.drawable.ic_dj_music);
        map.put(CHOREOGRAPHERS, R.drawable.ic_choreographers);
        map.put(MAKEUP_ARTISTS, R.drawable.ic_makeup_artists);
        map.put(RETURN_GIFTS, R.drawable.ic_return_gifts);
        map.put(MEHANDI_ARTIST, R.drawable.ic_mehandi_artists);
        map.put(REAL_JEWELRY, R.drawable.ic_real_jewelry);
        map.put(FASHION_JEWELRY, R.drawable.ic_fashion_jewelry);
        map.put(GROOM_WEAR, R.drawable.ic_groom_wear);
        map.put(BRIDAL_WEAR, R.drawable.ic_bridal_wear);
        map.put(WEDDING_PHOTO_VIDEO, R.drawable.ic_wedding_photo_video);
        map.put(INVITATION_CARDS, R.drawable.ic_invitation_cards);
        map.put(WEDDING_CATERERS, R.drawable.ic_wedding_caterers);
        map.put(WEDDING_DECORATORS, R.drawable.ic_wedding_decorators);
        map.put(WEDDING_VENUES, R.drawable.ic_wedding_venues);
        map.put(FOOD_COST, R.drawable.ic_food);
        map.put(VENUE_COST, R.drawable.ic_wedding_venues);
        map.put(WEDDING_PLANNERS, R.drawable.ic_wedding_planners);
        return map;
    }

    private static Map<Integer, String> getCategoryTitles() {
        @SuppressLint("UseSparseArrays") Map<Integer, String> map = new HashMap<>();
        map.put(GUEST_ROOMS, "Guest Rooms");
        map.put(HONEYMOON_PACKAGES, "Honeymoon Packages");
        map.put(OTHERS, "Others");
        map.put(TRANSPORTATIONS, "Transportations");
        map.put(ENTERTAINMENTS, "Entertainments");
        map.put(WEDDING_CAKES, "Wedding Cakes");
        map.put(PANDIT_PUROHIT, "Pandit/Purohit");
        map.put(DJ_MUSIC, "DJ & Music");
        map.put(CHOREOGRAPHERS, "Choreographers");
        map.put(MAKEUP_ARTISTS, "Makeup Artists");
        map.put(RETURN_GIFTS, "Return Gifts");
        map.put(MEHANDI_ARTIST, "Mehandi Artists");
        map.put(REAL_JEWELRY, "Real Jewelry");
        map.put(FASHION_JEWELRY, "Fashion Jewelry");
        map.put(GROOM_WEAR, "Groom Wear");
        map.put(BRIDAL_WEAR, "Bridal Wear");
        map.put(WEDDING_PHOTO_VIDEO, "Wedding Photo/Video");
        map.put(INVITATION_CARDS, "Invitation Cards");
        map.put(WEDDING_CATERERS, "Wedding Caterers");
        map.put(WEDDING_DECORATORS, "Wedding Decorators");
        map.put(FOOD_COST, "Food Cost");
        map.put(VENUE_COST, "Venue Cost");
        map.put(WEDDING_VENUES, "Wedding Venues");
        return map;
    }

    private static Map<Integer, String> getNoOf() {
        @SuppressLint("UseSparseArrays") Map<Integer, String> map = new HashMap<>();
        map.put(GUEST_ROOMS, "Rooms");
        map.put(INVITATION_CARDS, "Cards");
        map.put(FOOD_COST, "People");
        return map;
    }

    private static Map<Integer, String> getCostPer() {
        @SuppressLint("UseSparseArrays") Map<Integer, String> map = new HashMap<>();
        map.put(GUEST_ROOMS, "Room");
        map.put(INVITATION_CARDS, "Card");
        map.put(FOOD_COST, "Plate");
        return map;
    }
}
