package com.mygrandwedding.mgw.api;

import com.mygrandwedding.mgw.model.Activate;
import com.mygrandwedding.mgw.model.Blog;
import com.mygrandwedding.mgw.model.BlogData;
import com.mygrandwedding.mgw.model.BudgetData;
import com.mygrandwedding.mgw.model.ChangePassword;
import com.mygrandwedding.mgw.model.Checklist;
import com.mygrandwedding.mgw.model.Code;
import com.mygrandwedding.mgw.model.ContactUs;
import com.mygrandwedding.mgw.model.CreateQuote;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.FcmToken;
import com.mygrandwedding.mgw.model.ForgotPassword;
import com.mygrandwedding.mgw.model.Idea;
import com.mygrandwedding.mgw.model.IdeaCategory;
import com.mygrandwedding.mgw.model.IdeaData;
import com.mygrandwedding.mgw.model.Location;
import com.mygrandwedding.mgw.model.Login;
import com.mygrandwedding.mgw.model.Love;
import com.mygrandwedding.mgw.model.MobileNumber;
import com.mygrandwedding.mgw.model.MyBudget;
import com.mygrandwedding.mgw.model.MyVendor;
import com.mygrandwedding.mgw.model.MyWedding;
import com.mygrandwedding.mgw.model.Otp;
import com.mygrandwedding.mgw.model.Password;
import com.mygrandwedding.mgw.model.Promotion;
import com.mygrandwedding.mgw.model.Quote;
import com.mygrandwedding.mgw.model.Register;
import com.mygrandwedding.mgw.model.RegisterDropdown;
import com.mygrandwedding.mgw.model.ResetPassword;
import com.mygrandwedding.mgw.model.Review;
import com.mygrandwedding.mgw.model.ReviewData;
import com.mygrandwedding.mgw.model.Rsvp;
import com.mygrandwedding.mgw.model.RsvpData;
import com.mygrandwedding.mgw.model.Shortlist;
import com.mygrandwedding.mgw.model.SocialLogin;
import com.mygrandwedding.mgw.model.TrendyDesign;
import com.mygrandwedding.mgw.model.TrendyDesignData;
import com.mygrandwedding.mgw.model.UpdateChecklist;
import com.mygrandwedding.mgw.model.UpdateProfile;
import com.mygrandwedding.mgw.model.UpdateUsername;
import com.mygrandwedding.mgw.model.User;
import com.mygrandwedding.mgw.model.UserProfile;
import com.mygrandwedding.mgw.model.Vendor;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.model.VendorData;
import com.mygrandwedding.mgw.model.VendorDetail;
import com.mygrandwedding.mgw.model.VerifyMobile;
import com.mygrandwedding.mgw.model.WallData;
import com.mygrandwedding.mgw.model.Wedding;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by gladwinbobby on 12/09/17
 */

public interface ApiService {
    @POST("oauth/registration/")
    Call<User> register(@Body Register register);

    @POST("oauth/activate/account/")
    Call<User> activate(@Body Activate activate);

    @POST("oauth/forgot-password/")
    Call<Void> sendOtp(@Body ForgotPassword forgotPassword);

    @POST("oauth/login/")
    Call<User> login(@Body Login login);

    @POST("social-auth/facebook/")
    Call<User> fbLogin(@Body SocialLogin socialLogin);

    @POST("oauth/update/device-token/")
    Call<Void> updateFcm(@Header("Authorization") String token, @Body FcmToken fcmToken);

    @POST("oauth/logout/")
    Call<Void> logout(@Header("Authorization") String token);

    @POST("oauth/reset-password/")
    Call<User> resetPassword(@Body ResetPassword resetPassword);

    @GET("super-admin/registration/drop-down/")
    Call<RegisterDropdown> getRegisterDropdown();

    @POST("oauth/set-password/")
    Call<Data> setPassword(@Header("Authorization") String token, @Body Password password);

    @POST("oauth/change-password/")
    Call<Data> changePassword(@Header("Authorization") String token, @Body ChangePassword changePassword);

    @POST("oauth/initiate/change-mobile-number/")
    Call<Data> changeMobile(@Header("Authorization") String token, @Body MobileNumber mobileNumber);

    @PUT("oauth/update/username/")
    Call<Data> updateUsername(@Header("Authorization") String token, @Body UpdateUsername updateUsername);

    @Multipart
    @POST("vendor/app/create-vendor/")
    Call<User> createVendor(@PartMap Map<String, RequestBody> map, @Part MultipartBody.Part[] images);

    @GET("category/list/create/category/")
    Call<List<VendorCategory>> getVendorCategory(@Query("city") Integer city);

    @GET("promotions/list/")
    Call<List<Promotion>> getPromotions(@Header("Authorization") String token);

    @GET("promotions/retrieve/{id}/")
    Call<Promotion> getPromotionDetail(@Header("Authorization") String token, @Path("id") int id);

    @GET
    Call<VendorData> getVendors(@Url String url);

    @GET
    Call<VendorData> getSuggestedVendors(@Header("Authorization") String token, @Url String url);

    @GET("vendor/retrieve-details/{id}/")
    Call<VendorDetail> getVendorDetail(@Header("Authorization") String token, @Path("id") int id, @Query("wedding") Integer wedding);

    @POST("vendor/app/like/")
    Call<Love> love(@Header("Authorization") String token, @Body Love love);

    @POST("vendor/app/shortlist/")
    Call<Shortlist> shortlist(@Header("Authorization") String token, @Body Shortlist shortlist);

    @GET
    Call<ReviewData> getReviews(@Url String url);

    @POST("vendor/app/create/feedback/")
    Call<Review> review(@Header("Authorization") String token, @Body Review review);

    @GET("vendor/app/list/shortlisted-vendors/")
    Call<List<MyVendor>> getShortlisted(@Header("Authorization") String token);

    @GET("vendor/app/list/shortlisted-vendors/{id}/")
    Call<List<Vendor>> getMyShortlist(@Header("Authorization") String token, @Path("id") int id);

    @GET("vendor/app/list/liked-vendors/")
    Call<List<MyVendor>> getLoved(@Header("Authorization") String token);

    @GET("vendor/app/list/liked-vendors/{id}/")
    Call<List<Vendor>> getMyLove(@Header("Authorization") String token, @Path("id") int id);

    @GET("ideas/list/create/category/")
    Call<List<IdeaCategory>> getIdeaCategory();

    @GET
    Call<IdeaData> getIdeas(@Url String url);

    @GET
    Call<TrendyDesignData> getDesigns(@Url String url);

    @GET
    Call<BlogData> getBlog(@Url String url);

    @GET("ideas/retrieve/update/destroy/idea/{id}/")
    Call<Idea> getIdeaDetail(@Path("id") int id);

    @GET("blog/retrieve/update/destroy/blog/{id}/")
    Call<Blog> getBlogDetail(@Path("id") int id);

    @GET("trendy-designs/retrieve/detail/{id}/")
    Call<TrendyDesign> getTrendyDesignDetail(@Path("id") int id);

    @GET("location/list/create/location/")
    Call<List<Location>> getLocations(@Header("Authorization") String token);

    @GET("quotes/list/create/")
    Call<List<Quote>> getQuotes(@Header("Authorization") String token);

    @POST("quotes/app/create/")
    Call<CreateQuote> createQuote(@Header("Authorization") String token, @Body CreateQuote createQuote);

    @POST("vendor/contact-us/")
    Call<ContactUs> contactUs(@Header("Authorization") String token, @Body ContactUs contactUs);

    @GET("auth/retrieve/user/")
    Call<User> getUser(@Header("Authorization") String token);

    @PUT("auth/update/user/")
    Call<Data> updateProfile(@Header("Authorization") String token, @Body UpdateProfile updateProfile);

    @Multipart
    @PUT("auth/update/userprofile/")
    Call<UserProfile> updatePic(@Header("Authorization") String token, @Part MultipartBody.Part pic);

    @POST("oauth/verify/mobile-number/{id}/")
    Call<Data> verifyMobile(@Path("id") int id, @Body VerifyMobile verifyMobile);

    @POST("oauth/validate/otp/{id}/")
    Call<Data> validateOtp(@Path("id") int id, @Body Otp otp);

    @POST("wedding/create/")
    Call<Wedding> createWedding(@Header("Authorization") String token, @Body MyWedding myWedding);

    @PUT("wedding/update/{id}/")
    Call<Wedding> updateWedding(@Header("Authorization") String token, @Path("id") int id, @Body MyWedding myWedding);

    @POST("wedding/join/")
    Call<Wedding> joinWedding(@Header("Authorization") String token, @Body Code code);

    @GET("wedding/list/associated-weddings/")
    Call<List<Wedding>> getAssociatedWedding(@Header("Authorization") String token);

    @DELETE("wedding/delete/user/{id}/")
    Call<Data> removeWedding(@Header("Authorization") String token, @Path("id") int id);

    @GET("checklist/list/checklist/{id}/")
    Call<List<Checklist>> getChecklist(@Header("Authorization") String token, @Path("id") int id);

    @POST("checklist/create/checklist/{id}/")
    Call<Data> updateChecklist(@Header("Authorization") String token, @Path("id") int id, @Body UpdateChecklist updateChecklist);

    @GET
    Call<WallData> getWall(@Header("Authorization") String token, @Url String url);

    @POST("rsvp/create/")
    Call<Void> createRsvp(@Header("Authorization") String token, @Body Rsvp rsvp);

    @GET("rsvp/retrieve/{id}/")
    Call<Rsvp> getRsvp(@Header("Authorization") String token, @Path("id") int id);

    @GET
    Call<RsvpData> getRsvp(@Header("Authorization") String token, @Url String url);

    @GET("budget/list/{id}/")
    Call<BudgetData> myBudget(@Header("Authorization") String token, @Path("id") int id);

    @POST("budget/create/")
    Call<MyBudget> createBudget(@Header("Authorization") String token, @Body MyBudget myBudget);

    @GET("vendor/call/{id}/")
    Call<Data> callVendor(@Header("Authorization") String token, @Path("id") int id);
}
