package com.mygrandwedding.mgw.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 24/09/17
 */

@Entity
public class Location {

    @PrimaryKey
    @SerializedName("id")
    private int id;
    @Ignore
    private boolean selected;
    @SerializedName("name")
    private String location;

    public Location(int id, String location) {
        this.id = id;
        this.location = location;
    }

    @Ignore
    public Location(int id, boolean selected, String location) {
        this.id = id;
        this.selected = selected;
        this.location = location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return location;
    }
}
