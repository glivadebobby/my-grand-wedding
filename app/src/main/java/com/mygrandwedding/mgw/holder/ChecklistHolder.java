package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.ChecklistCallback;

/**
 * Created by gladwinbobby on 18/10/17
 */

public class ChecklistHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView textViewDuration;
    public RecyclerView viewCheck;
    private ChecklistCallback callback;

    public ChecklistHolder(View itemView, ChecklistCallback callback) {
        super(itemView);
        textViewDuration = itemView.findViewById(R.id.txt_duration);
        viewCheck = itemView.findViewById(R.id.check);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onChecklistClick(getLayoutPosition());
        }
    }
}
