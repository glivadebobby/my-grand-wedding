package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.widget.Toast;

import com.bumptech.glide.request.target.Target;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by gladwinbobby on 13/10/17
 */

public class ShareImageTask extends AsyncTask<String, Void, File> {

    private Context context;
    private String name;

    public ShareImageTask(Context context, String name) {
        this.context = context;
        this.name = name;
    }

    @Override
    protected void onPreExecute() {
        Toast.makeText(context, "Processing..", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected File doInBackground(String... strings) {
        try {
            return GlideApp.with(context)
                    .downloadOnly()
                    .load(strings[0])
                    .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(File file) {
        if (file == null) return;
        File image = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), name);
        try {
            image = copy(file, image);
            Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".fileprovider", image);
            share(uri);
        } catch (IOException e) {
            Toast.makeText(context, "Image sharing failed", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void share(Uri uri) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
        intent.putExtra(Intent.EXTRA_TEXT, "Look what I found! Find more like this with " + context.getString(R.string.app_name) + ". Download the app here " + "\n" + "https://play.google.com/store/apps/details?id=" + context.getPackageName());
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        context.startActivity(Intent.createChooser(intent, "Share Image"));
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    private File copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
        return dst;
    }
}
