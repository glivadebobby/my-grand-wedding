package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.PhotoCallback;
import com.mygrandwedding.mgw.holder.PhotoHolder;
import com.mygrandwedding.mgw.model.Image;

import java.util.List;
import java.util.Locale;

/**
 * Created by gladwinbobby on 07/09/17
 */

public class ImageAdapter extends RecyclerView.Adapter<PhotoHolder> {

    private Context context;
    private List<Image> imageList;
    private PhotoCallback callback;
    private int count = -1;

    public ImageAdapter(Context context, List<Image> imageList, PhotoCallback callback) {
        this.context = context;
        this.imageList = imageList;
        this.callback = callback;
    }

    @Override
    public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PhotoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(PhotoHolder holder, int position) {
        Image image = imageList.get(position);
        GlideApp.with(context)
                .load(image.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewPhoto);
        holder.textViewCount.setText(String.format(Locale.getDefault(), context.getString(R.string.format_photos), count));
        if (count != -1 && holder.getLayoutPosition() + 1 == imageList.size()) {
            holder.viewTint.setVisibility(View.VISIBLE);
            holder.textViewCount.setVisibility(View.VISIBLE);
        } else {
            holder.viewTint.setVisibility(View.GONE);
            holder.textViewCount.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public void setCount(int count) {
        this.count = count;
    }
}
