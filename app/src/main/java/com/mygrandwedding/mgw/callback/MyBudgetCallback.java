package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 25/12/17
 */

public interface MyBudgetCallback {
    void onMyBudgetClick(int position);
}
