package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.mygrandwedding.mgw.adapter.FaqAdapter;
import com.mygrandwedding.mgw.model.Faq;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FaqActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        Runnable {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewFaq;
    private List<Faq> faqList;
    private FaqAdapter faqAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getFaq();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getFaq();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewFaq = findViewById(R.id.faq);

        context = this;
        faqList = new ArrayList<>();
        faqAdapter = new FaqAdapter(faqList);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewFaq.setLayoutManager(new LinearLayoutManager(context));
        viewFaq.setAdapter(faqAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void getFaq() {
        refreshLayout.setRefreshing(false);
        faqList.clear();
        faqList.add(new Faq("How do I start planning my wedding?", "Easy. Go to your \"Settings\" and click on \"Wedding Settings\" to start filling in the details regarding your big day. You can also add the bride/groom to the app so that you're both on the same page!"));
        faqList.add(new Faq("Who is a collaborator? What can a collaborator do?", "A collaborator can be anybody whose inputs you need to plan your big day; your mom, sister or best friend! Since these guys are the people who'd know you and your tastes best, they can help you shortlist vendors by sharing their suggestions on your wedding wall."));
        faqList.add(new Faq("Can a non-collaborator see what's happening on my wedding wall?", "Of course not!"));
        faqList.add(new Faq("How many collaborators can I add?", "You can add up to 10 collaborators for a wedding. Any more than that and you'll be debating vendors more than finalizing them."));
        faqList.add(new Faq("Can a collaborator participate in multiple weddings at the same time?", "Yes! If you have the stamina to plan more than one at a time, we won't be the reason to stop you."));
        faqList.add(new Faq("What do I do if I\'ve forgotten my password?", "Click on the \"Forgot Your Password\" and we'll send you a link to reset it on your registered email id."));
        faqList.add(new Faq("How do I get in touch with a vendor?", "Go to the vendor profile and click on \"Get Free Quote\". Feel free to ask as many questions as you like :-)"));
        faqList.add(new Faq("What do I do if a vendor doesn't reply?", "A vendor usually takes 1-5 days to reply to a query. In case you don't hear from them, you can always reach out to us at info@mygrandwedding.com. We'll try and source the answers for you."));
        faqList.add(new Faq("Can I book vendors from the app?", "As of now, we don't have the feature to book vendors through our app. But you can always get in touch with them through sending enquiries, we will share you the vendor contact details to your mobile and e-mail id. You can directly talk to the vendors."));
        faqAdapter.notifyDataSetChanged();
    }
}
