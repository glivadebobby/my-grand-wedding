package com.mygrandwedding.mgw;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.mygrandwedding.mgw.adapter.MyPagerAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.fragment.VendorFragment;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.model.VendorSubcategory;
import com.mygrandwedding.mgw.viewmodel.VendorSubcategoryViewModel;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundleResult;

public class VendorSubcategoryActivity extends AppCompatActivity implements View.OnClickListener,
        TabLayout.OnTabSelectedListener {

    private static final int REQUEST_FILTER = 4;
    private Context context;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager pagerSubcategory;
    private FloatingActionButton buttonSpecialRate;
    private FloatingActionButton buttonFilter;
    private List<Fragment> fragmentList;
    private MyPagerAdapter pagerAdapter;
    private MyPreference preference;
    private VendorCategory category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_subcategory);
        initObjects();
        initCallbacks();
        initToolbar();
        processBundle();
        initViewPager();
        loadVendorCategories();
        setTabsFont();
        showCase();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_FILTER && resultCode == RESULT_OK) {
            loadVendorCategories();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSpecialRate) {
            launch(context, GetQuoteActivity.class);
        } else if (view == buttonFilter) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(EXTRA_VENDOR, category);
            launchWithBundleResult(this, VendorFilterActivity.class, bundle, REQUEST_FILTER);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        pagerSubcategory.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tab);
        pagerSubcategory = findViewById(R.id.vendor_subcategory);
        buttonSpecialRate = findViewById(R.id.fab_special_rate);
        buttonFilter = findViewById(R.id.fab_filter);

        context = this;
        fragmentList = new ArrayList<>();
        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), fragmentList);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        buttonSpecialRate.setOnClickListener(this);
        buttonFilter.setOnClickListener(this);
        tabLayout.addOnTabSelectedListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            category = bundle.getParcelable(EXTRA_VENDOR);
            if (category != null) {
                if (getSupportActionBar() != null)
                    getSupportActionBar().setTitle(category.getCategory());
                toolbar.setSubtitle(getResources().getQuantityString(R.plurals.result,
                        category.getCount(), category.getCount()));
            }
        }
    }

    private void initViewPager() {
        pagerSubcategory.setAdapter(pagerAdapter);
        pagerSubcategory.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void loadVendorCategories() {
        VendorSubcategoryViewModel viewModel = ViewModelProviders.of(this).get(VendorSubcategoryViewModel.class);
        viewModel.getData(context, category.getId()).observe(this, new Observer<List<VendorSubcategory>>() {
            @Override
            public void onChanged(@Nullable List<VendorSubcategory> vendorSubcategories) {
                if (vendorSubcategories != null) {
                    tabLayout.removeAllTabs();
                    fragmentList.clear();
                    tabLayout.addTab(tabLayout.newTab().setText("All"));
                    fragmentList.add(VendorFragment.newInstance(category.getId(), -1, category));
                    for (VendorSubcategory vendorSubcategory : vendorSubcategories) {
                        tabLayout.addTab(tabLayout.newTab().setText(vendorSubcategory.getName()));
                        fragmentList.add(VendorFragment.newInstance(vendorSubcategory.getCategory(), vendorSubcategory.getId(), category));
                    }
                    pagerAdapter.notifyDataSetChanged();
                    pagerSubcategory.setOffscreenPageLimit(fragmentList.size());
                }
            }
        });
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(context.getAssets(),
                            "fonts/SourceSansPro-SemiBold.ttf"), Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(getResources().getDimension(R.dimen.primary_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    private void showCase() {
        if (preference.getIntro4()) return;
        TapTargetView.showFor(this,
                TapTarget.forView(buttonFilter, "Filter the vendors based on city, price and rating")
                        .tintTarget(false)
                        .cancelable(false)
                        .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        preference.setIntro4(true);
                    }
                });
    }
}
