package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.MyBudgetCallback;

/**
 * Created by gladwinbobby on 25/12/17
 */

public class MyBudgetHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewCategory, imageViewEdit;
    public TextView textViewCategory, textViewPrice;
    private MyBudgetCallback callback;

    public MyBudgetHolder(View itemView, MyBudgetCallback callback) {
        super(itemView);
        imageViewCategory = itemView.findViewById(R.id.img_category);
        imageViewEdit = itemView.findViewById(R.id.img_edit);
        textViewCategory = itemView.findViewById(R.id.txt_category);
        textViewPrice = itemView.findViewById(R.id.txt_price);
        this.callback = callback;
        imageViewEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewEdit) {
            callback.onMyBudgetClick(getLayoutPosition());
        }
    }
}
