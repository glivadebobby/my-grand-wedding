package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.BlogAdapter;
import com.mygrandwedding.mgw.callback.BlogCallback;
import com.mygrandwedding.mgw.model.Blog;
import com.mygrandwedding.mgw.model.BlogData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class BlogActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Runnable, BlogCallback {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewBlog;
    private List<Blog> blogList;
    private LinearLayoutManager layoutManager;
    private BlogAdapter blogAdapter;
    private int id;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    @Override
    public void onBlogClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, blogList.get(position).getId());
        launchWithBundle(context, BlogDetailActivity.class, bundle);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewBlog = findViewById(R.id.blog);

        context = this;
        blogList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        blogAdapter = new BlogAdapter(context, blogList, this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(bundle.getString(EXTRA_TITLE));
            }
        }
    }

    private void initRecyclerView() {
        viewBlog.setLayoutManager(new LinearLayoutManager(context));
        viewBlog.setAdapter(blogAdapter);
        viewBlog.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getBlog();
                }
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void resetList() {
        count = 0;
        setUrl();
        getBlog();
    }

    private void setUrl() {
        url = MGW_URL + "blog/list/blogs/?category=" + id;
    }

    private void clearList() {
        if (count == 0) {
            blogList.clear();
            blogAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        if (blogList.isEmpty()) {
            Toast.makeText(context, "No blogs found", Toast.LENGTH_SHORT).show();
        }
    }

    private void getBlog() {
        setLoading(true);
        Call<BlogData> call = getApiService().getBlog(url);
        call.enqueue(new Callback<BlogData>() {
            @Override
            public void onResponse(@NonNull Call<BlogData> call, @NonNull Response<BlogData> response) {
                refreshLayout.setRefreshing(false);
                BlogData blogDataResponse = response.body();
                if (response.isSuccessful() && blogDataResponse != null) {
                    setLoading(false);
                    clearList();
                    count = blogDataResponse.getCount();
                    url = blogDataResponse.getNextUrl();
                    blogList.addAll(blogDataResponse.getBlogList());
                    blogAdapter.notifyDataSetChanged();
                    isEmpty();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<BlogData> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
