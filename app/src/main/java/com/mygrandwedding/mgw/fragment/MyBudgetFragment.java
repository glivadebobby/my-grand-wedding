package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.adapter.MyBudgetAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.MyBudgetCallback;
import com.mygrandwedding.mgw.model.BudgetData;
import com.mygrandwedding.mgw.model.MyBudget;
import com.mygrandwedding.mgw.model.NotifyRefresh;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.adapter.MyBudgetAdapter.TYPE_MY_BUDGET;
import static com.mygrandwedding.mgw.adapter.MyBudgetAdapter.TYPE_MY_BUDGET_COST;
import static com.mygrandwedding.mgw.adapter.MyBudgetAdapter.TYPE_VENUE_TYPE;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.util.MyData.BRIDAL_WEAR;
import static com.mygrandwedding.mgw.util.MyData.DJ_MUSIC;
import static com.mygrandwedding.mgw.util.MyData.FASHION_JEWELRY;
import static com.mygrandwedding.mgw.util.MyData.FOOD_COST;
import static com.mygrandwedding.mgw.util.MyData.GROOM_WEAR;
import static com.mygrandwedding.mgw.util.MyData.GUEST_ROOMS;
import static com.mygrandwedding.mgw.util.MyData.HOTEL_RESORT;
import static com.mygrandwedding.mgw.util.MyData.INVITATION_CARDS;
import static com.mygrandwedding.mgw.util.MyData.MAKEUP_ARTISTS;
import static com.mygrandwedding.mgw.util.MyData.MANDAPAM;
import static com.mygrandwedding.mgw.util.MyData.OTHERS;
import static com.mygrandwedding.mgw.util.MyData.PANDIT_PUROHIT;
import static com.mygrandwedding.mgw.util.MyData.REAL_JEWELRY;
import static com.mygrandwedding.mgw.util.MyData.TRANSPORTATIONS;
import static com.mygrandwedding.mgw.util.MyData.VENUE_COST;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_DECORATORS;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_PHOTO_VIDEO;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_VENUES;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyBudgetFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, Runnable, MyBudgetCallback {

    private Context context;
    private View rootView;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout layoutMyBudget;
    private RecyclerView viewBudget;
    private TextView textViewTotalBudget;
    private List<MyBudget> myBudgetList;
    private MyBudgetAdapter budgetAdapter;
    private MyPreference preference;

    public MyBudgetFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_budget, container, false);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onRefresh() {
        getMyBudget();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getMyBudget();
    }

    @Override
    public void onMyBudgetClick(int position) {
        MyBudget myBudget = myBudgetList.get(position);
        if (myBudget.getType() == TYPE_VENUE_TYPE) {
            VenueTypeDialogFragment.newInstance(myBudget.getSubCategory() == null ? -1 : myBudget.getSubCategory())
                    .show(getChildFragmentManager(), "wedding-venue");
        } else if (myBudget.getType() == TYPE_MY_BUDGET_COST) {
            int subCategory = -1;
            String count = null;
            String cost = null;
            if (myBudget.getSubCategory() != null) subCategory = myBudget.getSubCategory();
            if (myBudget.getCount() != null && myBudget.getCount() > 0)
                count = String.valueOf(myBudget.getCount());
            if (myBudget.getPerItemCost() != null && myBudget.getPerItemCost() > 0)
                cost = String.format(Locale.getDefault(), "%1$.2f", myBudget.getPerItemCost());
            MyBudgetCostDialogFragment.newInstance(myBudget.getCategory(), subCategory, count, cost,
                    myBudget.getTitle(), myBudget.getNoOf())
                    .show(getChildFragmentManager(), "my-budget-cost");
        } else if (myBudget.getType() == TYPE_MY_BUDGET) {
            String cost = null;
            if (myBudget.getTotalCost() != null && myBudget.getTotalCost() > 0)
                cost = String.format(Locale.getDefault(), "%1$.2f", myBudget.getTotalCost());
            MyBudgetDialogFragment.newInstance(myBudget.getCategory(), cost, myBudget.getTitle())
                    .show(getChildFragmentManager(), "my-budget");
        }
    }

    @Subscribe
    public void refresh(NotifyRefresh notifyRefresh) {
        run();
    }

    private void initObjects() {
        refreshLayout = rootView.findViewById(R.id.refresh);
        layoutMyBudget = rootView.findViewById(R.id.my_budget);
        viewBudget = rootView.findViewById(R.id.budget);
        textViewTotalBudget = rootView.findViewById(R.id.txt_total_budget);

        context = getActivity();
        myBudgetList = new ArrayList<>();
        budgetAdapter = new MyBudgetAdapter(context, myBudgetList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecyclerView() {
        viewBudget.setLayoutManager(new LinearLayoutManager(context));
        viewBudget.setAdapter(budgetAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void getMyBudget() {
        Call<BudgetData> call = getApiService().myBudget(preference.getUserToken(), preference.getWeddingId());
        call.enqueue(new Callback<BudgetData>() {
            @Override
            public void onResponse(@NonNull Call<BudgetData> call,
                                   @NonNull Response<BudgetData> response) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    BudgetData budgetData = response.body();
                    if (response.isSuccessful() && budgetData != null) {
                        layoutMyBudget.setVisibility(View.VISIBLE);
                        myBudgetList.clear();
                        handleMyBudget(budgetData);
                        updateTotal();
                    } else processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BudgetData> call, @NonNull Throwable t) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    processFailure(context, t);
                }
            }
        });
    }

    private void handleMyBudget(BudgetData budgetData) {
        if (budgetData.getWeddingVenue() == null)
            myBudgetList.add(new MyBudget(TYPE_VENUE_TYPE, WEDDING_VENUES));
        else {
            MyBudget myBudget = budgetData.getWeddingVenue();
            myBudget.setType(TYPE_VENUE_TYPE);
            myBudgetList.add(myBudget);

            if (myBudget.getSubCategory() == MANDAPAM) {
                if (myBudget.getPerItemCost() == null || myBudget.getPerItemCost() < 1) {
                    MyBudget budgetMandapam = new MyBudget(TYPE_MY_BUDGET_COST, FOOD_COST);
                    budgetMandapam.setSubCategory(MANDAPAM);
                    myBudgetList.add(budgetMandapam);
                } else {
                    MyBudget budgetMandapam = new MyBudget(TYPE_MY_BUDGET_COST, FOOD_COST);
                    budgetMandapam.setType(TYPE_MY_BUDGET_COST);
                    budgetMandapam.setSubCategory(MANDAPAM);
                    budgetMandapam.setCount(myBudget.getCount());
                    budgetMandapam.setPerItemCost(myBudget.getPerItemCost());
                    myBudgetList.add(budgetMandapam);
                }

                if (myBudget.getTotalCost() == null || myBudget.getTotalCost() < 1) {
                    MyBudget budgetMandapam = new MyBudget(TYPE_MY_BUDGET, VENUE_COST);
                    budgetMandapam.setSubCategory(MANDAPAM);
                    myBudgetList.add(budgetMandapam);
                } else {
                    MyBudget budgetMandapam = new MyBudget(TYPE_MY_BUDGET, VENUE_COST);
                    budgetMandapam.setType(TYPE_MY_BUDGET);
                    budgetMandapam.setSubCategory(MANDAPAM);
                    budgetMandapam.setTotalCost(myBudget.getTotalCost());
                    myBudgetList.add(budgetMandapam);
                }
            } else if (myBudget.getSubCategory() == HOTEL_RESORT) {
                if (myBudget.getPerItemCost() == null || myBudget.getPerItemCost() < 1) {
                    MyBudget budgetHotel = new MyBudget(TYPE_MY_BUDGET_COST, FOOD_COST);
                    budgetHotel.setSubCategory(HOTEL_RESORT);
                    myBudgetList.add(budgetHotel);
                } else {
                    MyBudget budgetHotel = new MyBudget(TYPE_MY_BUDGET_COST, FOOD_COST);
                    budgetHotel.setType(TYPE_MY_BUDGET_COST);
                    budgetHotel.setSubCategory(HOTEL_RESORT);
                    budgetHotel.setCount(myBudget.getCount());
                    budgetHotel.setPerItemCost(myBudget.getPerItemCost());
                    myBudgetList.add(budgetHotel);
                }
            }
        }

        if (budgetData.getGuestRoom() == null)
            myBudgetList.add(new MyBudget(TYPE_MY_BUDGET_COST, GUEST_ROOMS));
        else {
            MyBudget myBudget = budgetData.getGuestRoom();
            myBudget.setType(TYPE_MY_BUDGET_COST);
            myBudgetList.add(myBudget);
        }

        if (budgetData.getInvitationCards() == null)
            myBudgetList.add(new MyBudget(TYPE_MY_BUDGET_COST, INVITATION_CARDS));
        else {
            MyBudget myBudget = budgetData.getInvitationCards();
            myBudget.setType(TYPE_MY_BUDGET_COST);
            myBudgetList.add(myBudget);
        }

        if (budgetData.getWeddingDecorators() == null)
            myBudgetList.add(new MyBudget(WEDDING_DECORATORS));
        else myBudgetList.add(budgetData.getWeddingDecorators());

        if (budgetData.getPhotoVideo() == null) myBudgetList.add(new MyBudget(WEDDING_PHOTO_VIDEO));
        else myBudgetList.add(budgetData.getPhotoVideo());

        if (budgetData.getPanditPurohit() == null) myBudgetList.add(new MyBudget(PANDIT_PUROHIT));
        else myBudgetList.add(budgetData.getPanditPurohit());

        if (budgetData.getMakeup() == null) myBudgetList.add(new MyBudget(MAKEUP_ARTISTS));
        else myBudgetList.add(budgetData.getMakeup());

        if (budgetData.getRealJewelry() == null) myBudgetList.add(new MyBudget(REAL_JEWELRY));
        else myBudgetList.add(budgetData.getRealJewelry());

        if (budgetData.getFashionJewelry() == null) myBudgetList.add(new MyBudget(FASHION_JEWELRY));
        else myBudgetList.add(budgetData.getFashionJewelry());

        if (budgetData.getBridalWear() == null) myBudgetList.add(new MyBudget(BRIDAL_WEAR));
        else myBudgetList.add(budgetData.getBridalWear());

        if (budgetData.getGroomWear() == null) myBudgetList.add(new MyBudget(GROOM_WEAR));
        else myBudgetList.add(budgetData.getGroomWear());

        if (budgetData.getDjMusic() == null) myBudgetList.add(new MyBudget(DJ_MUSIC));
        else myBudgetList.add(budgetData.getDjMusic());

        if (budgetData.getTransportation() == null) myBudgetList.add(new MyBudget(TRANSPORTATIONS));
        else myBudgetList.add(budgetData.getTransportation());

        if (budgetData.getOthers() == null) myBudgetList.add(new MyBudget(OTHERS));
        else myBudgetList.add(budgetData.getOthers());

        budgetAdapter.notifyDataSetChanged();
    }

    private void updateTotal() {
        float total = 0;
        for (MyBudget myBudget : myBudgetList) {
            if (myBudget.getTotalCost() != null) total += myBudget.getTotalCost();
        }
        textViewTotalBudget.setText(String.format(Locale.getDefault(), getString(R.string.format_overall_budget), total));
    }
}
