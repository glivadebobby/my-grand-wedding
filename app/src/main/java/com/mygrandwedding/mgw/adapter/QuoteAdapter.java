package com.mygrandwedding.mgw.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.holder.QuoteHolder;
import com.mygrandwedding.mgw.model.Quote;

import java.util.List;

/**
 * Created by gladwinbobby on 28/09/17
 */

public class QuoteAdapter extends RecyclerView.Adapter<QuoteHolder> {

    private List<Quote> quoteList;

    public QuoteAdapter(List<Quote> quoteList) {
        this.quoteList = quoteList;
    }

    @Override
    public QuoteHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new QuoteHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_quote, parent, false));
    }

    @Override
    public void onBindViewHolder(QuoteHolder holder, int position) {
        Quote quote = quoteList.get(position);
        holder.textViewCustomerName.setText(quote.getFullName());
        holder.textViewLocation.setText(quote.getLocation().getLocation());
        holder.textViewEmail.setText(quote.getEmail());
        holder.textViewMobileNumber.setText(quote.getMobileNumber());
        holder.textViewRequirement.setText(quote.getRequirement());
        holder.textViewMessage.setText(quote.getMessage());
    }

    @Override
    public int getItemCount() {
        return quoteList.size();
    }
}
