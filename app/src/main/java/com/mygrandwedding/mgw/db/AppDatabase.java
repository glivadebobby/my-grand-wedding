package com.mygrandwedding.mgw.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.mygrandwedding.mgw.db.dao.DeleteDao;
import com.mygrandwedding.mgw.db.dao.GeneralDao;
import com.mygrandwedding.mgw.db.dao.UserDao;
import com.mygrandwedding.mgw.db.dao.WeddingDao;
import com.mygrandwedding.mgw.model.BlogCategory;
import com.mygrandwedding.mgw.model.Checklist;
import com.mygrandwedding.mgw.model.IdeaCategory;
import com.mygrandwedding.mgw.model.Location;
import com.mygrandwedding.mgw.model.User;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.model.VendorSubcategory;
import com.mygrandwedding.mgw.model.Wedding;

/**
 * Created by gladwinbobby on 02/10/17
 */

@Database(entities = {User.class, Wedding.class, Location.class, VendorCategory.class,
        VendorSubcategory.class, BlogCategory.class, IdeaCategory.class, Checklist.class},
        version = 6)
public abstract class AppDatabase extends RoomDatabase {

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE VendorCategory ADD COLUMN sort INTEGER NOT NULL DEFAULT 0");
        }
    };

    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE VendorCategory ADD COLUMN maxPrice INTEGER NOT NULL DEFAULT 0");
        }
    };

    private static final String DATABASE_NAME = "mgw";
    private static AppDatabase sInstance;

    public static AppDatabase getAppDatabase(Context context) {
        if (sInstance == null) {
            sInstance = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                    .addMigrations(MIGRATION_1_2)
                    .addMigrations(MIGRATION_2_3)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return sInstance;
    }

    public abstract UserDao userDao();

    public abstract WeddingDao weddingDao();

    public abstract GeneralDao generalDao();

    public abstract DeleteDao deleteDao();
}
