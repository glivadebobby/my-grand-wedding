package com.mygrandwedding.mgw.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.mygrandwedding.mgw.model.VendorSubcategory;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 20/10/17
 */

public class VendorSubcategoryViewModel extends ViewModel {

    private LiveData<List<VendorSubcategory>> data;

    public LiveData<List<VendorSubcategory>> getData(Context context, int id) {
        if (data == null) {
            data = getAppDatabase(context).generalDao().getVendorSubcategories(id);
        }
        return data;
    }
}
