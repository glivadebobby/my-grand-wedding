package com.mygrandwedding.mgw.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.mygrandwedding.mgw.MainActivity;
import com.mygrandwedding.mgw.PromotionDetailActivity;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.RsvpActivity;
import com.mygrandwedding.mgw.VendorMainActivity;
import com.mygrandwedding.mgw.VendorProfileActivity;
import com.mygrandwedding.mgw.WallActivity;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.NotificationMessage;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.MAIN_ACTIVITY;
import static com.mygrandwedding.mgw.app.Constant.PROMOTION_ACTIVITY;
import static com.mygrandwedding.mgw.app.Constant.ROLE_USER;
import static com.mygrandwedding.mgw.app.Constant.ROLE_VENDOR;
import static com.mygrandwedding.mgw.app.Constant.RSVP_ACTIVITY;
import static com.mygrandwedding.mgw.app.Constant.VENDOR_MAIN_ACTIVITY;
import static com.mygrandwedding.mgw.app.Constant.VENDOR_PROFILE_ACTIVITY;
import static com.mygrandwedding.mgw.app.Constant.WALL_ACTIVITY;
import static com.mygrandwedding.mgw.util.MyParser.parseHtml;

/**
 * Created by gladwinbobby on 22/10/17
 */

public class FcmNotification extends FirebaseMessagingService {

    private static final String NOTIFY_ID = "mgw";
    private static final String NOTIFY_CHANNEL = "My Grand Wedding";
    private MyPreference myPreference;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        myPreference = new MyPreference(this);
        Gson gson = new Gson();
        String message = gson.toJson(remoteMessage.getData());
        NotificationMessage notificationMessage = gson.fromJson(message, NotificationMessage.class);
        if (myPreference.getToken() == null) return;
        displayNotification(notificationMessage);
    }

    private void displayNotification(NotificationMessage notificationMessage) {
        Intent intent;
        if (notificationMessage.getId() == MAIN_ACTIVITY) {
            if (myPreference.getRole() == ROLE_USER)
                intent = new Intent(this, MainActivity.class);
            else return;
        } else if (notificationMessage.getId() == WALL_ACTIVITY) {
            if (myPreference.getRole() == ROLE_USER)
                intent = new Intent(this, WallActivity.class);
            else return;
        } else if (notificationMessage.getId() == PROMOTION_ACTIVITY) {
            if (myPreference.getRole() == ROLE_USER) {
                intent = new Intent(this, PromotionDetailActivity.class);
                intent.putExtra(EXTRA_ID, notificationMessage.getPk());
            } else return;
        } else if (notificationMessage.getId() == VENDOR_MAIN_ACTIVITY) {
            if (myPreference.getRole() == ROLE_VENDOR)
                intent = new Intent(this, VendorMainActivity.class);
            else return;
        } else if (notificationMessage.getId() == VENDOR_PROFILE_ACTIVITY) {
            if (myPreference.getRole() == ROLE_VENDOR)
                intent = new Intent(this, VendorProfileActivity.class);
            else return;
        } else if (notificationMessage.getId() == RSVP_ACTIVITY) {
            if (myPreference.getRole() == ROLE_USER && !myPreference.isCollaborator())
                intent = new Intent(this, RsvpActivity.class);
            else return;
        } else {
            intent = new Intent(this, MainActivity.class);
        }
        PendingIntent pendingIntentContent = PendingIntent.getActivity(this,
                notificationMessage.getId(), intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap bitmapLargeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFY_ID);
        builder.setColor(ContextCompat.getColor(this, R.color.primary))
                .setTicker(notificationMessage.getTicker())
                .setContentTitle(notificationMessage.getTitle())
                .setContentText(parseHtml(notificationMessage.getDesc()))
                .setContentIntent(pendingIntentContent)
                .setSmallIcon(R.drawable.ic_notify)
                .setLargeIcon(bitmapLargeIcon)
                .setSound(defaultSoundUri)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationMessage.getImage() != null) {
            try {
                Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(notificationMessage.getImage()).getContent());
                builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(parseHtml(notificationMessage.getDesc())));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(NOTIFY_ID, NOTIFY_CHANNEL,
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify(notificationMessage.getId(), builder.build());
        }
    }
}
