package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.ReviewCallback;
import com.mygrandwedding.mgw.holder.ReviewHolder;
import com.mygrandwedding.mgw.model.Review;

import java.util.List;

/**
 * Created by gladwinbobby on 08/09/17
 */

public class ReviewAdapter extends RecyclerView.Adapter<ReviewHolder> {

    private Context context;
    private List<Review> reviewList;
    private ReviewCallback callback;
    private int userId;

    public ReviewAdapter(Context context, List<Review> reviewList, ReviewCallback callback, int userId) {
        this.context = context;
        this.reviewList = reviewList;
        this.callback = callback;
        this.userId = userId;
    }

    @Override
    public ReviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReviewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(ReviewHolder holder, int position) {
        Review review = reviewList.get(position);
        GlideApp.with(context)
                .load(review.getUser().getUserProfile().getDp())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewProfile);
        holder.textViewName.setText(review.getUser().getFirstName());
        holder.textViewDate.setText(review.getCreatedOn());
        holder.textViewRating.setText(String.valueOf(review.getRating()));
        holder.textViewReview.setText(review.getReview() == null ? context.getString(R.string.error_na) : String.valueOf(review.getReview()));
        holder.itemView.setBackgroundResource(holder.getLayoutPosition() % 2 == 0 ?
                android.R.color.transparent : R.color.grey_60);
        holder.imageViewEdit.setVisibility(review.getUser().getId() == userId ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }
}
