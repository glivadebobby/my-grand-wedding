package com.mygrandwedding.mgw;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.mygrandwedding.mgw.adapter.ImageAdapter;
import com.mygrandwedding.mgw.adapter.ReviewAdapter;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.app.MyActivity;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.PhotoCallback;
import com.mygrandwedding.mgw.callback.ReviewCallback;
import com.mygrandwedding.mgw.fragment.PromptLoginDialogFragment;
import com.mygrandwedding.mgw.model.BusinessDetail;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.Image;
import com.mygrandwedding.mgw.model.Love;
import com.mygrandwedding.mgw.model.Review;
import com.mygrandwedding.mgw.model.Shortlist;
import com.mygrandwedding.mgw.model.VendorDetail;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_IMAGE;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_PHOTOS;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_RATING;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_REVIEW;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;
import static com.mygrandwedding.mgw.util.MyParser.parseHtml;

public class VendorDetailActivity extends AppCompatActivity implements PhotoCallback,
        ReviewCallback, View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, Runnable {

    private static final int REQUEST_REVIEW = 4;
    private Context context;
    private Toolbar toolbar;
    private ImageView imageViewVendor;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout layoutVendorDetail;
    private TextView textViewVendor, textViewLocation, textViewCost, textViewRating, textViewReview,
            textViewShortlist, textViewDetailedInfo, textViewPhotos, textViewReviews, textViewShowAll;
    private FrameLayout shortlist, getQuote, writeReview, callNow;
    private RecyclerView viewPhotos, viewReviews;
    private List<Image> imageList;
    private List<Review> reviewList;
    private ImageAdapter imageAdapter;
    private ReviewAdapter reviewAdapter;
    private MyPreference preference;
    private VendorDetail vendorDetail;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_detail);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.vendor_detail, menu);
        MenuItem itemLove = menu.findItem(R.id.action_love);
        if (vendorDetail != null && vendorDetail.getBusinessDetail() != null) {
            itemLove.setVisible(true);
            itemLove.setIcon(vendorDetail.isLoved() ? R.drawable.ic_loved : R.drawable.ic_love);
        } else itemLove.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_love:
                love();
                return true;
            case R.id.action_share:
                shareVendor();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_REVIEW) {
            if (resultCode == Activity.RESULT_OK) {
                run();
            }
        }
    }

    @Override
    public void onPhotoClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, id);
        bundle.putString(EXTRA_TITLE, vendorDetail.getBusinessDetail().getBusinessName());
        bundle.putParcelableArrayList(EXTRA_PHOTOS, vendorDetail.getBusinessDetail().getImageList());
        launchWithBundle(context, PhotoActivity.class, bundle);
    }

    @Override
    public void onEditClick(int position) {
        Review review = reviewList.get(position);
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, vendorDetail.getBusinessDetail().getId());
        bundle.putFloat(EXTRA_RATING, review.getRating());
        bundle.putString(EXTRA_VENDOR, vendorDetail.getBusinessDetail().getBusinessName());
        bundle.putString(EXTRA_REVIEW, review.getReview());
        Intent intent = new Intent(this, ReviewActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_REVIEW);
    }

    @Override
    public void onClick(View view) {
        if (view == shortlist) {
            shortlist();
        } else if (view == getQuote) {
            Bundle bundle = new Bundle();
            bundle.putInt(EXTRA_ID, id);
            bundle.putString(EXTRA_VENDOR, vendorDetail.getBusinessDetail().getBusinessName());
            launchWithBundle(context, GetQuoteActivity.class, bundle);
        } else if (view == textViewDetailedInfo) {
            textViewDetailedInfo.setMaxLines(Integer.MAX_VALUE);
            textViewDetailedInfo.setEllipsize(null);
        } else if (view == textViewShowAll) {
            Bundle bundle = new Bundle();
            bundle.putInt(EXTRA_ID, vendorDetail.getBusinessDetail().getId());
            bundle.putString(EXTRA_VENDOR, vendorDetail.getBusinessDetail().getBusinessName());
            launchWithBundle(context, ReviewDetailActivity.class, bundle);
        } else if (view == writeReview) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getSupportFragmentManager(), "prompt-login");
            else {
                Bundle bundle = new Bundle();
                bundle.putInt(EXTRA_ID, vendorDetail.getBusinessDetail().getId());
                bundle.putString(EXTRA_VENDOR, vendorDetail.getBusinessDetail().getBusinessName());
                Intent intent = new Intent(this, ReviewActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_REVIEW);
            }
        } else if (view == callNow) {
            callVendor();
        }
    }

    @Override
    public void onRefresh() {
        getVendorDetail();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getVendorDetail();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        imageViewVendor = findViewById(R.id.img_vendor);
        refreshLayout = findViewById(R.id.refresh);
        layoutVendorDetail = findViewById(R.id.vendor_detail);
        textViewVendor = findViewById(R.id.txt_vendor);
        textViewLocation = findViewById(R.id.txt_location);
        textViewCost = findViewById(R.id.txt_cost);
        textViewRating = findViewById(R.id.txt_rating);
        textViewReview = findViewById(R.id.txt_review);
        textViewShortlist = findViewById(R.id.txt_shortlist);
        textViewDetailedInfo = findViewById(R.id.txt_detailed_info);
        textViewPhotos = findViewById(R.id.txt_photos);
        textViewReviews = findViewById(R.id.txt_reviews);
        textViewShowAll = findViewById(R.id.txt_show_all);
        shortlist = findViewById(R.id.shortlist);
        getQuote = findViewById(R.id.get_quote);
        writeReview = findViewById(R.id.write_review);
        callNow = findViewById(R.id.call_now);
        viewPhotos = findViewById(R.id.photos);
        viewReviews = findViewById(R.id.reviews);

        context = this;
        preference = new MyPreference(context);
        imageList = new ArrayList<>();
        reviewList = new ArrayList<>();
        imageAdapter = new ImageAdapter(context, imageList, this);
        reviewAdapter = new ReviewAdapter(context, reviewList, this, preference.getId());
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        shortlist.setOnClickListener(this);
        getQuote.setOnClickListener(this);
        textViewDetailedInfo.setOnClickListener(this);
        textViewShowAll.setOnClickListener(this);
        writeReview.setOnClickListener(this);
        callNow.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            GlideApp.with(context)
                    .load(bundle.getString(EXTRA_IMAGE))
                    .placeholder(R.drawable.placeholder)
                    .into(imageViewVendor);
        }
    }

    private void initRecyclerView() {
        viewPhotos.setLayoutManager(new GridLayoutManager(context, 2));
        viewPhotos.setAdapter(imageAdapter);
        viewPhotos.setNestedScrollingEnabled(false);

        viewReviews.setLayoutManager(new LinearLayoutManager(context));
        viewReviews.setAdapter(reviewAdapter);
        viewReviews.setNestedScrollingEnabled(false);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void getVendorDetail() {
        Call<VendorDetail> call = getApiService().getVendorDetail(preference.getUserToken(), id,
                preference.getWeddingId() == -1 ? null : preference.getWeddingId());
        call.enqueue(new Callback<VendorDetail>() {
            @Override
            public void onResponse(@NonNull Call<VendorDetail> call,
                                   @NonNull Response<VendorDetail> response) {
                refreshLayout.setRefreshing(false);
                VendorDetail vendorDetailResponse = response.body();
                if (response.isSuccessful() && vendorDetailResponse != null) {
                    layoutVendorDetail.setVisibility(View.VISIBLE);
                    vendorDetail = vendorDetailResponse;
                    BusinessDetail businessDetail = vendorDetail.getBusinessDetail();
                    if (businessDetail != null) {
                        textViewVendor.setText(businessDetail.getBusinessName());
                        textViewLocation.setText(businessDetail.getLocation().getLocation());
                        if (businessDetail.getMinPrice() == 0)
                            textViewCost.setText(getString(R.string.txt_price_on_request));
                        else textViewCost.setText(String.format(Locale.getDefault(),
                                getString(R.string.format_cost_onwards), businessDetail.getMinPrice()));
                        textViewRating.setText(vendorDetail.getRating() == null ?
                                getString(R.string.error_na) :
                                String.valueOf(vendorDetail.getRating()));
                        textViewReview.setText(getResources().getQuantityString(R.plurals.review,
                                vendorDetail.getReviewCount(), vendorDetail.getReviewCount()));
                        textViewPhotos.setText(String.format(Locale.getDefault(),
                                getString(R.string.format_photos_count), businessDetail.getImageList().size()));
                        textViewReviews.setText(String.format(Locale.getDefault(),
                                getString(R.string.format_reviews_count), vendorDetail.getReviewCount()));
                        if (businessDetail.getDescription() != null) {
                            textViewDetailedInfo.setText(parseHtml(businessDetail.getDescription()));
                        } else {
                            textViewDetailedInfo.setText(getString(R.string.error_na));
                        }
                    }
                    setImageAndPhotos();
                    setShortlisted();
                    setReviews();
                    invalidateCallOption();
                    showCase();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<VendorDetail> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void setImageAndPhotos() {
        List<Image> list = vendorDetail.getBusinessDetail().getImageList();
        if (list != null && !list.isEmpty()) {
            int size = list.size();
            imageList.clear();
            for (int i = 0; i < list.size(); i++) {
                if (i == 4) break;
                Image image = list.get(i);
                imageList.add(image);
            }
            imageAdapter.notifyDataSetChanged();
            if (size > 4) {
                imageAdapter.setCount(size - 4);
            }
            GlideApp.with(context)
                    .load(list.get(0).getImage())
                    .placeholder(R.drawable.placeholder)
                    .into(imageViewVendor);
        }
    }

    private void setShortlisted() {
        textViewShortlist.setText(vendorDetail.isShortlisted() ? getString(R.string.txt_shortlisted) : getString(R.string.txt_shortlist));
    }

    private void setReviews() {
        reviewList.clear();
        if (vendorDetail.getReview() != null) {
            reviewList.add(vendorDetail.getReview());
            writeReview.setVisibility(View.GONE);
        } else writeReview.setVisibility(View.VISIBLE);
        reviewList.addAll(vendorDetail.getReviewList());
        reviewAdapter.notifyDataSetChanged();
        textViewShowAll.setVisibility(reviewList.size() != vendorDetail.getReviewCount() ? View.VISIBLE : View.INVISIBLE);
    }

    private void invalidateCallOption() {
        if (preference.getToken() == null) {
            callNow.setVisibility(View.GONE);
        } else callNow.setVisibility(View.VISIBLE);
    }

    private void callVendor() {
        Call<Data> call = getApiService().callVendor("Token " + preference.getToken(), id);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                if (response.isSuccessful()) {
                    MyActivity.launchDialPad(VendorDetailActivity.this, vendorDetail.getUsername());
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                processFailure(context, t);
            }
        });
    }

    private void love() {
        if (preference.getToken() == null) {
            new PromptLoginDialogFragment().show(getSupportFragmentManager(), "prompt-login");
            return;
        }
        Call<Love> call = getApiService().love(preference.getUserToken(),
                new Love(vendorDetail.getBusinessDetail().getId(),
                        preference.getWeddingId() == -1 ? null : preference.getWeddingId()));
        call.enqueue(new Callback<Love>() {
            @Override
            public void onResponse(@NonNull Call<Love> call, @NonNull Response<Love> response) {
                Love loveResponse = response.body();
                if (response.isSuccessful() && loveResponse != null) {
                    if (vendorDetail != null)
                        vendorDetail.setLoved(loveResponse.getId() != null);
                    invalidateOptionsMenu();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Love> call, @NonNull Throwable t) {
                processFailure(context, t);
            }
        });
    }

    private void shortlist() {
        if (preference.getToken() == null) {
            new PromptLoginDialogFragment().show(getSupportFragmentManager(), "prompt-login");
            return;
        }
        Call<Shortlist> call = getApiService().shortlist(preference.getUserToken(),
                new Shortlist(vendorDetail.getBusinessDetail().getId(),
                        preference.getWeddingId() == -1 ? null : preference.getWeddingId()));
        call.enqueue(new Callback<Shortlist>() {
            @Override
            public void onResponse(@NonNull Call<Shortlist> call, @NonNull Response<Shortlist> response) {
                Shortlist shortlistResponse = response.body();
                if (response.isSuccessful() && shortlistResponse != null) {
                    if (vendorDetail != null)
                        vendorDetail.setShortlisted(shortlistResponse.getId() != null);
                    setShortlisted();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Shortlist> call, @NonNull Throwable t) {
                processFailure(context, t);
            }
        });
    }

    private void shareVendor() {
        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://mygrandwedding.com/vendor-detail.php" + "?id=" + id))
                .setDynamicLinkDomain("dr779.app.goo.gl")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setIosParameters(new DynamicLink.IosParameters.Builder("com.mygrandwedding.mgw").build())
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            MyActivity.shareVendor(context, shortLink.toString());
                        } else {
                            Toast.makeText(context, "Unable to share! Try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void showCase() {
        if (preference.getIntro6() && preference.getIntro7()) return;
        new TapTargetSequence(this)
                .targets(TapTarget.forView(shortlist, "Shortlist the vendor")
                                .tintTarget(false)
                                .cancelable(false)
                                .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                        TapTarget.forView(getQuote, "Request for vendor quote")
                                .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf"))
                                .tintTarget(false)
                                .cancelable(false))
                .listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        preference.setIntro6(true);
                        preference.setIntro7(true);
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }
}
