package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.mygrandwedding.mgw.R;

/**
 * Created by gladwinbobby on 05/11/17
 */

public class WpPhotoHolder extends RecyclerView.ViewHolder {

    public ImageView imageViewPhoto;

    public WpPhotoHolder(View itemView) {
        super(itemView);
        imageViewPhoto = itemView.findViewById(R.id.img_photo);
    }
}
