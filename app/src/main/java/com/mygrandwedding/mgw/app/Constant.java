package com.mygrandwedding.mgw.app;

/**
 * Created by gladwinbobby on 06/09/17
 */

public class Constant {

    /**
     * Bundle extras
     */
    public static final String EXTRA_ID = "id";
    public static final String EXTRA_PDUS = "pdus";
    public static final String EXTRA_FORMAT = "format";
    public static final String EXTRA_VENDOR = "vendor";
    public static final String EXTRA_RATING = "rating";
    public static final String EXTRA_REVIEW = "review";
    public static final String EXTRA_PHOTOS = "photos";
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_POSITION = "position";
    public static final String EXTRA_IMAGE = "image";
    public static final String EXTRA_EMAIL = "email";
    public static final String EXTRA_PHONE = "phone";
    public static final String EXTRA_CODE = "code";
    public static final String EXTRA_TYPE = "type";

    /**
     * OTP
     */
    public static final String OTP_SENDER = "MGRNDW";

    /**
     * Roles
     */
    public static final int ROLE_VENDOR = 2;
    public static final int ROLE_USER = 4;

    /**
     * Support Contact
     */
    public static final String HELP_DESK = "+91 934 500 4444";
    public static final String MGW_CONTACT = "9962107168";

    /**
     * Map Config
     */
    public static final int MAP_ZOOM = 16;
    public static final double MAP_LAT = 12.9435688;
    public static final double MAP_LNG = 80.2379152;

    /**
     * Special Rate
     */
    public static final int SPECIAL_RATE_SESSION = 7;

    /**
     *
     */
    public static final int CHENNAI_CITY_ID = 15;// TODO: 22/10/17 Confirm with the chennai city id

    /**
     * Notification navigation
     */
    public static final int MAIN_ACTIVITY = 1;
    public static final int WALL_ACTIVITY = 2;
    public static final int PROMOTION_ACTIVITY = 3;
    public static final int VENDOR_MAIN_ACTIVITY = 4;
    public static final int VENDOR_PROFILE_ACTIVITY = 5;
    public static final int RSVP_ACTIVITY = 6;
}
