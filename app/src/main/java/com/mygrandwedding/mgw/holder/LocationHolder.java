package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.LocationCallback;

/**
 * Created by gladwinbobby on 30/09/17
 */

public class LocationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView textViewLocation;
    private LocationCallback callback;

    public LocationHolder(View itemView, LocationCallback callback) {
        super(itemView);
        textViewLocation = itemView.findViewById(R.id.txt_location);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onLocationClick(getLayoutPosition());
        }
    }
}
