package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.LovedCallback;
import com.mygrandwedding.mgw.holder.LovedHolder;
import com.mygrandwedding.mgw.model.MyVendor;

import java.util.List;
import java.util.Locale;

/**
 * Created by gladwinbobby on 18/09/17
 */

public class LovedAdapter extends RecyclerView.Adapter<LovedHolder> {

    private Context context;
    private List<MyVendor> vendorList;
    private LovedCallback callback;

    public LovedAdapter(Context context, List<MyVendor> vendorList, LovedCallback callback) {
        this.context = context;
        this.vendorList = vendorList;
        this.callback = callback;
    }

    @Override
    public LovedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LovedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loved, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(LovedHolder holder, int position) {
        MyVendor vendor = vendorList.get(position);
        GlideApp.with(context)
                .load(vendor.getVendor().getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewVendor);
        if (vendor.getWedding() != null)
            holder.textViewWedding.setText(String.format(Locale.getDefault(), context.getString(R.string.format_vendor_wedding), vendor.getDate(), vendor.getWedding().getTitle()));
        else
            holder.textViewWedding.setText(String.format(Locale.getDefault(), context.getString(R.string.format_vendor_wedding), vendor.getDate(), context.getString(R.string.txt_profile)));
        holder.textViewVendor.setText(vendor.getVendor().getBusinessName());
        holder.textViewLocation.setText(vendor.getVendor().getLocation().getLocation());
        if (vendor.getVendor().getMinPrice() == 0)
            holder.textViewCost.setText(context.getString(R.string.txt_price_on_request));
        else
            holder.textViewCost.setText(String.format(Locale.getDefault(), context.getString(R.string.format_cost_onwards), vendor.getVendor().getMinPrice()));
        holder.textViewRating.setText(vendor.getVendor().getRating() == null ? context.getString(R.string.error_na) : String.valueOf(vendor.getVendor().getRating()));
        holder.textViewReview.setText(context.getResources().getQuantityString(R.plurals.review, vendor.getVendor().getReview(), vendor.getVendor().getReview()));
        if (vendor.getVendor().getVendorType() == 1) {
            holder.diagonalView.setVisibility(View.VISIBLE);
            holder.imageViewType.setVisibility(View.VISIBLE);
            holder.imageViewType.setImageResource(R.drawable.ic_featured);
        } else if (vendor.getVendor().getVendorType() == 2) {
            holder.diagonalView.setVisibility(View.INVISIBLE);
            holder.imageViewType.setVisibility(View.INVISIBLE);
        } else if (vendor.getVendor().getVendorType() == 3) {
            holder.diagonalView.setVisibility(View.VISIBLE);
            holder.imageViewType.setVisibility(View.VISIBLE);
            holder.imageViewType.setImageResource(R.drawable.ic_recommended);
        }
    }

    @Override
    public int getItemCount() {
        return vendorList.size();
    }
}
