package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.ForgotPassword;
import com.mygrandwedding.mgw.model.Otp;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_EMAIL;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_PHONE;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;

public class VerifyMobileOtpActivity extends AppCompatActivity implements View.OnClickListener,
        View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutOtp;
    private TextInputEditText editTextOtp;
    private Button buttonSubmit;
    private TextView textViewSendOtp;
    private FrameLayout layoutLoading;
    private MyPreference preference;
    private String email, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_mobile_otp);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        setPasswordFont();
        setSendOtpText();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextOtp) isValidOtp();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            processOtp();
        } else if (view == textViewSendOtp) {
            setLoading(true);
            sendOtp();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutOtp = findViewById(R.id.otp);
        editTextOtp = findViewById(R.id.input_otp);
        buttonSubmit = findViewById(R.id.btn_submit);
        textViewSendOtp = findViewById(R.id.txt_send_otp);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        editTextOtp.setOnFocusChangeListener(this);
        buttonSubmit.setOnClickListener(this);
        textViewSendOtp.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            email = bundle.getString(EXTRA_EMAIL);
            phone = bundle.getString(EXTRA_PHONE);
        }
    }

    private void setPasswordFont() {
        layoutOtp.setTypeface(textViewSendOtp.getTypeface());
        editTextOtp.setTypeface(textViewSendOtp.getTypeface());
        editTextOtp.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void setSendOtpText() {
        SpannableString spannableString = new SpannableString(getString(R.string.txt_send_otp));
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.primary_txt)), 20, 30, 0);
        spannableString.setSpan(new RelativeSizeSpan(1.05f), 20, 30, 0);
        textViewSendOtp.setText(spannableString);
    }

    private void processOtp() {
        String otp = editTextOtp.getText().toString().trim();
        if (isValidOtp(otp)) {
            setLoading(true);
            validateOtp(new Otp(otp));
        }
    }

    private void isValidOtp() {
        isValidOtp(editTextOtp.getText().toString().trim());
    }

    private boolean isValidOtp(String otp) {
        if (otp.isEmpty()) {
            layoutOtp.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutOtp.getHint()));
            return false;
        } else if (otp.length() < 6) {
            layoutOtp.setError(String.format(Locale.getDefault(), getString(R.string.error_length), layoutOtp.getHint(), 6));
            return false;
        }
        layoutOtp.setErrorEnabled(false);
        return true;
    }

    private void validateOtp(Otp otp) {
        Call<Data> call = getApiService().validateOtp(preference.getId(), otp);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    if (preference.getEmail() != null) preference.setEmail(email);
                    preference.setPhone(phone);
                    preference.setVerified(true);
                    launchClearStack(context, MainActivity.class);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void sendOtp() {
        Call<Void> call = getApiService().sendOtp(new ForgotPassword(preference.getRole(), phone));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "OTP code sent again to " + phone, Toast.LENGTH_SHORT).show();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
