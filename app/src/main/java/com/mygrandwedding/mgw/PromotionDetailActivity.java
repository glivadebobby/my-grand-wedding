package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Promotion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.util.DateParser.getCreatedDate;
import static com.mygrandwedding.mgw.util.MyParser.parseHtml;

public class PromotionDetailActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private Toolbar toolbar;
    private ImageView imageViewPromotion;
    private SwipeRefreshLayout refreshLayout;
    private TextView textViewTitle, textViewCategory, textViewDate, textViewDescription;
    private MyPreference preference;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_detail);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getPromotion();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getPromotion();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        imageViewPromotion = findViewById(R.id.img_promotion);
        refreshLayout = findViewById(R.id.refresh);
        textViewTitle = findViewById(R.id.txt_title);
        textViewCategory = findViewById(R.id.txt_category);
        textViewDate = findViewById(R.id.txt_date);
        textViewDescription = findViewById(R.id.txt_description);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
        }
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void getPromotion() {
        Call<Promotion> call = getApiService().getPromotionDetail(preference.getUserToken(), id);
        call.enqueue(new Callback<Promotion>() {
            @Override
            public void onResponse(@NonNull Call<Promotion> call, @NonNull Response<Promotion> response) {
                refreshLayout.setRefreshing(false);
                Promotion promotionResponse = response.body();
                if (response.isSuccessful() && promotionResponse != null) {
                    GlideApp.with(context)
                            .load(promotionResponse.getImage())
                            .placeholder(R.drawable.placeholder)
                            .into(imageViewPromotion);
                    textViewTitle.setText(promotionResponse.getTitle());
                    textViewCategory.setText(promotionResponse.getCategory().getName());
                    textViewDate.setText(getCreatedDate(promotionResponse.getCreatedOn()));
                    textViewDescription.setText(parseHtml(promotionResponse.getDescription()));
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Promotion> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
