package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.IdeaAdapter;
import com.mygrandwedding.mgw.callback.IdeaCallback;
import com.mygrandwedding.mgw.model.Idea;
import com.mygrandwedding.mgw.model.IdeaData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class IdeaActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Runnable, IdeaCallback {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewIdeas;
    private List<Idea> ideaList;
    private LinearLayoutManager layoutManager;
    private IdeaAdapter ideaAdapter;
    private int id;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idea);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    @Override
    public void onIdeaClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, ideaList.get(position).getId());
        launchWithBundle(context, IdeaDetailActivity.class, bundle);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewIdeas = findViewById(R.id.ideas);

        context = this;
        ideaList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        ideaAdapter = new IdeaAdapter(context, ideaList, this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(bundle.getString(EXTRA_TITLE));
            }
        }
    }

    private void initRecyclerView() {
        viewIdeas.setLayoutManager(new LinearLayoutManager(context));
        viewIdeas.setAdapter(ideaAdapter);
        viewIdeas.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getIdeas();
                }
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void resetList() {
        count = 0;
        setUrl();
        getIdeas();
    }

    private void setUrl() {
        url = MGW_URL + "ideas/list/ideas/?category=" + id;
    }

    private void clearList() {
        if (count == 0) {
            ideaList.clear();
            ideaAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        if (ideaList.isEmpty()) {
            Toast.makeText(context, "No ideas found", Toast.LENGTH_SHORT).show();
        }
    }

    private void getIdeas() {
        setLoading(true);
        Call<IdeaData> call = getApiService().getIdeas(url);
        call.enqueue(new Callback<IdeaData>() {
            @Override
            public void onResponse(@NonNull Call<IdeaData> call,
                                   @NonNull Response<IdeaData> response) {
                refreshLayout.setRefreshing(false);
                IdeaData ideaDataResponse = response.body();
                if (response.isSuccessful() && ideaDataResponse != null) {
                    setLoading(false);
                    clearList();
                    count = ideaDataResponse.getCount();
                    url = ideaDataResponse.getNextUrl();
                    ideaList.addAll(ideaDataResponse.getIdeaList());
                    ideaAdapter.notifyDataSetChanged();
                    isEmpty();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<IdeaData> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
