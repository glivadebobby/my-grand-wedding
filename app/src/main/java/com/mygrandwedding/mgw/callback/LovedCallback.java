package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 04/10/17
 */

public interface LovedCallback {
    void onVendorClick(int position);

    void onRemoveClick(int position);
}
