package com.mygrandwedding.mgw.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

/**
 * Created by gladwinbobby on 03/09/17
 */

public class MyPreference {

    private static final String PREF_USER = "user";
    private static final String PREF_INTRO = "intro";
    private static final String PREF_EXTRAS = "extras";
    private static final String ID = "id";
    private static final String CITY_ID = "city_id";
    private static final String WEDDING_ID = "wedding_id";
    private static final String COLLABORATOR = "collaborator";
    private static final String MIN_PRICE = "min_price";
    private static final String MAX_PRICE = "max_price";
    private static final String RATING = "rating";
    private static final String ROLE = "role";
    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String EMAIL = "email";
    private static final String TOKEN = "token";
    private static final String VERIFIED = "verified";
    private static final String FIRST_TIME = "first_time";
    private static final String FCM_TOKEN = "fcm_token";
    private static final String TOKEN_UPLOADED = "token_uploaded";
    private static final String SKIP = "skip";
    private static final String SESSION_COUNT = "session_count";
    private static final String VENDOR_ID = "id";
    private static final String CODE = "code";
    private static final String TYPE = "type";
    private static final String INTRO1 = "intro1";
    private static final String INTRO2 = "intro2";
    private static final String INTRO3 = "intro3";
    private static final String INTRO4 = "intro4";
    private static final String INTRO5 = "intro5";
    private static final String INTRO6 = "intro6";
    private static final String INTRO7 = "intro7";
    private static final String INTRO8 = "intro8";
    private static final String INTRO9 = "intro9";
    private static final String INTRO10 = "intro10";
    private static final String INTRO11 = "intro11";
    private static final String INTRO12 = "intro12";
    private static final String INTRO13 = "intro13";
    private static final String INTRO14 = "intro14";
    private SharedPreferences preferencesUser, preferencesIntro, preferencesExtras;

    public MyPreference(Context context) {
        preferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        preferencesIntro = context.getSharedPreferences(PREF_INTRO, Context.MODE_PRIVATE);
        preferencesExtras = context.getSharedPreferences(PREF_EXTRAS, Context.MODE_PRIVATE);
    }

    public int getId() {
        return preferencesUser.getInt(encode(ID), -1);
    }

    public void setId(int id) {
        preferencesUser.edit().putInt(encode(ID), id).apply();
    }

    public int getCityId() {
        return preferencesUser.getInt(encode(CITY_ID), 0);
    }

    public void setCityId(int cityId) {
        preferencesUser.edit().putInt(encode(CITY_ID), cityId).apply();
    }

    public int getWeddingId() {
        return preferencesUser.getInt(encode(WEDDING_ID), -1);
    }

    public void setWeddingId(int weddingId) {
        preferencesUser.edit().putInt(encode(WEDDING_ID), weddingId).apply();
    }

    public boolean isCollaborator() {
        return preferencesUser.getBoolean(encode(COLLABORATOR), false);
    }

    public void setCollaborator(boolean collaborator) {
        preferencesUser.edit().putBoolean(encode(COLLABORATOR), collaborator).apply();
    }

    public int getMinPrice() {
        return preferencesUser.getInt(encode(MIN_PRICE), 0);
    }

    public void setMinPrice(int minPrice) {
        preferencesUser.edit().putInt(encode(MIN_PRICE), minPrice).apply();
    }

    public int getMaxPrice() {
        return preferencesUser.getInt(encode(MAX_PRICE), 100);
    }

    public void setMaxPrice(int maxPrice) {
        preferencesUser.edit().putInt(encode(MAX_PRICE), maxPrice).apply();
    }

    public float getRating() {
        return preferencesUser.getFloat(encode(RATING), 0);
    }

    public void setRating(float rating) {
        preferencesUser.edit().putFloat(encode(RATING), rating).apply();
    }

    public void resetPriceRating() {
        setMinPrice(0);
        setMaxPrice(100);
        setRating(0);
    }

    public void resetFilter() {
        setCityId(0);
        setMinPrice(0);
        setMaxPrice(100);
        setRating(0);
    }

    public int getRole() {
        return preferencesUser.getInt(encode(ROLE), -1);
    }

    public void setRole(int role) {
        preferencesUser.edit().putInt(encode(ROLE), role).apply();
    }

    public String getName() {
        return preferencesUser.getString(encode(NAME), null);
    }

    public void setName(String name) {
        preferencesUser.edit().putString(encode(NAME), name).apply();
    }

    public String getPhone() {
        return preferencesUser.getString(encode(PHONE), null);
    }

    public void setPhone(String phone) {
        preferencesUser.edit().putString(encode(PHONE), phone).apply();
    }

    public String getEmail() {
        return preferencesUser.getString(encode(EMAIL), null);
    }

    public void setEmail(String email) {
        preferencesUser.edit().putString(encode(EMAIL), email).apply();
    }

    public String getToken() {
        return preferencesUser.getString(encode(TOKEN), null);
    }

    public void setToken(String token) {
        preferencesUser.edit().putString(encode(TOKEN), token).apply();
    }

    public String getUserToken() {
        return getToken() != null ? "Token " + getToken() : getToken();
    }

    public boolean isVerified() {
        return preferencesUser.getBoolean(encode(VERIFIED), true);
    }

    public void setVerified(boolean verified) {
        preferencesUser.edit().putBoolean(encode(VERIFIED), verified).apply();
    }

    public boolean isTokenUploaded() {
        return preferencesExtras.getBoolean(encode(TOKEN_UPLOADED), false);
    }

    public void setTokenUploaded(boolean tokenUploaded) {
        preferencesExtras.edit().putBoolean(encode(TOKEN_UPLOADED), tokenUploaded).apply();
    }

    public boolean isFirstTime() {
        return preferencesExtras.getBoolean(encode(FIRST_TIME), true);
    }

    public void setFirstTime() {
        preferencesExtras.edit().putBoolean(encode(FIRST_TIME), false).apply();
    }

    public String getFcmToken() {
        return preferencesExtras.getString(encode(FCM_TOKEN), null);
    }

    public void setFcmToken(String fcmToken) {
        preferencesExtras.edit().putString(encode(FCM_TOKEN), fcmToken).apply();
    }

    public boolean isSkip() {
        return preferencesExtras.getBoolean(encode(SKIP), false);
    }

    public void setSkip(boolean skip) {
        setFirstTime();
        preferencesExtras.edit().putBoolean(encode(SKIP), skip).apply();
    }

    public int getSessionCount() {
        return preferencesExtras.getInt(encode(SESSION_COUNT), 0);
    }

    public void incrementSession() {
        preferencesExtras.edit().putInt(encode(SESSION_COUNT), getSessionCount() + 1).apply();
    }

    public int getVendorId() {
        return preferencesExtras.getInt(encode(VENDOR_ID), -1);
    }

    public void setVendorId(int id) {
        preferencesExtras.edit().putInt(encode(VENDOR_ID), id).apply();
    }

    public String getCode() {
        return preferencesExtras.getString(encode(CODE), null);
    }

    public void setCode(String code) {
        preferencesExtras.edit().putString(encode(CODE), code).apply();
    }

    public int getType() {
        return preferencesExtras.getInt(encode(TYPE), -1);
    }

    public void setType(int type) {
        preferencesExtras.edit().putInt(encode(TYPE), type).apply();
    }

    public boolean getIntro1() {
        return preferencesIntro.getBoolean(encode(INTRO1), false);
    }

    public void setIntro1(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO1), done).apply();
    }

    public boolean getIntro2() {
        return preferencesIntro.getBoolean(encode(INTRO2), false);
    }

    public void setIntro2(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO2), done).apply();
    }

    public boolean getIntro3() {
        return preferencesIntro.getBoolean(encode(INTRO3), false);
    }

    public void setIntro3(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO3), done).apply();
    }

    public boolean getIntro4() {
        return preferencesIntro.getBoolean(encode(INTRO4), false);
    }

    public void setIntro4(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO4), done).apply();
    }

    public boolean getIntro5() {
        return preferencesIntro.getBoolean(encode(INTRO5), false);
    }

    public void setIntro5(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO5), done).apply();
    }

    public boolean getIntro6() {
        return preferencesIntro.getBoolean(encode(INTRO6), false);
    }

    public void setIntro6(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO6), done).apply();
    }

    public boolean getIntro7() {
        return preferencesIntro.getBoolean(encode(INTRO7), false);
    }

    public void setIntro7(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO7), done).apply();
    }

    public boolean getIntro8() {
        return preferencesIntro.getBoolean(encode(INTRO8), false);
    }

    public void setIntro8(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO8), done).apply();
    }

    public boolean getIntro9() {
        return preferencesIntro.getBoolean(encode(INTRO9), false);
    }

    public void setIntro9(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO9), done).apply();
    }

    public boolean getIntro10() {
        return preferencesIntro.getBoolean(encode(INTRO10), false);
    }

    public void setIntro10(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO10), done).apply();
    }

    public boolean getIntro11() {
        return preferencesIntro.getBoolean(encode(INTRO11), false);
    }

    public void setIntro11(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO11), done).apply();
    }

    public boolean getIntro12() {
        return preferencesIntro.getBoolean(encode(INTRO12), false);
    }

    public void setIntro12(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO12), done).apply();
    }

    public boolean getIntro13() {
        return preferencesIntro.getBoolean(encode(INTRO13), false);
    }

    public void setIntro13(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO13), done).apply();
    }

    public boolean getIntro14() {
        return preferencesIntro.getBoolean(encode(INTRO14), false);
    }

    public void setIntro14(boolean done) {
        preferencesIntro.edit().putBoolean(encode(INTRO14), done).apply();
    }

    public void clearUser() {
        setTokenUploaded(false);
        setSkip(false);
        preferencesUser.edit().clear().apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
