package com.mygrandwedding.mgw.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gladwinbobby on 05/09/17
 */

@Entity
public class VendorCategory implements Parcelable {

    public static final Creator<VendorCategory> CREATOR = new Creator<VendorCategory>() {
        @Override
        public VendorCategory createFromParcel(Parcel in) {
            return new VendorCategory(in);
        }

        @Override
        public VendorCategory[] newArray(int size) {
            return new VendorCategory[size];
        }
    };

    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("count")
    private int count;
    @Ignore
    private int imageId;
    @SerializedName("sort")
    private int sort;
    @SerializedName("max_price")
    private int maxPrice;
    private int subcategoryCount;
    @SerializedName("name")
    private String category;
    @SerializedName("image")
    private String image;
    @Ignore
    @SerializedName("subcategory_set")
    private List<VendorSubcategory> subcategoryList;

    @Ignore
    public VendorCategory(String category) {
        this.category = category;
    }

    @Ignore
    public VendorCategory(int id, String category) {
        this.id = id;
        this.category = category;
    }

    @Ignore
    public VendorCategory(int id, int count, int imageId, String category) {
        this.id = id;
        this.count = count;
        this.imageId = imageId;
        this.category = category;
    }

    public VendorCategory(int id, int count, int sort, int maxPrice, int subcategoryCount, String category, String image) {
        this.id = id;
        this.count = count;
        this.sort = sort;
        this.maxPrice = maxPrice;
        this.subcategoryCount = subcategoryCount;
        this.category = category;
        this.image = image;
    }

    @Ignore
    protected VendorCategory(Parcel in) {
        id = in.readInt();
        count = in.readInt();
        imageId = in.readInt();
        sort = in.readInt();
        maxPrice = in.readInt();
        subcategoryCount = in.readInt();
        category = in.readString();
        image = in.readString();
        subcategoryList = in.createTypedArrayList(VendorSubcategory.CREATOR);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getFilterPrice() {
        if (maxPrice <= 100) return 1;
        else if (maxPrice <= 1000) return 10;
        else if (maxPrice <= 10000) return 100;
        else if (maxPrice <= 100000) return 1000;
        else if (maxPrice <= 1000000) return 10000;
        else return 100000;
    }

    public int getSubcategoryCount() {
        return subcategoryCount;
    }

    public void setSubcategoryCount(int subcategoryCount) {
        this.subcategoryCount = subcategoryCount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<VendorSubcategory> getSubcategoryList() {
        return subcategoryList;
    }

    public void setSubcategoryList(List<VendorSubcategory> subcategoryList) {
        this.subcategoryList = subcategoryList;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(count);
        dest.writeInt(imageId);
        dest.writeInt(sort);
        dest.writeInt(maxPrice);
        dest.writeInt(subcategoryCount);
        dest.writeString(category);
        dest.writeString(image);
        dest.writeTypedList(subcategoryList);
    }

    @Override
    public String toString() {
        return category;
    }
}
