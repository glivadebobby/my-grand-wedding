package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.Wedding;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class AddWeddingListTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private List<Wedding> weddingList;

    public AddWeddingListTask(Context context, List<Wedding> weddingList) {
        this.context = context;
        this.weddingList = weddingList;
    }

    @Override
    protected Void doInBackground(Void... params) {
        getAppDatabase(context).deleteDao().deleteWedding();
        getAppDatabase(context).weddingDao().insertWeddingList(weddingList);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }
}
