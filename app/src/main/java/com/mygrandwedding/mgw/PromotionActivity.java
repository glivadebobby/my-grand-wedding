package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.PromotionAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.PromotionCallback;
import com.mygrandwedding.mgw.model.Promotion;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class PromotionActivity extends AppCompatActivity implements PromotionCallback, Runnable, SwipeRefreshLayout.OnRefreshListener {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewPromotions;
    private List<Promotion> promotionList;
    private PromotionAdapter promotionAdapter;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);
        initObjects();
        initToolbar();
        initCallbacks();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            finish();
            launch(context, MainActivity.class);
        } else
            super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getPromotions();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getPromotions();
    }

    @Override
    public void onPromotionClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, promotionList.get(position).getId());
        launchWithBundle(context, PromotionDetailActivity.class, bundle);
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewPromotions = findViewById(R.id.promotions);

        context = this;
        promotionList = new ArrayList<>();
        promotionAdapter = new PromotionAdapter(context, promotionList, this);
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecyclerView() {
        viewPromotions.setLayoutManager(new LinearLayoutManager(context));
        viewPromotions.setAdapter(promotionAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void isEmpty() {
        if (promotionList.isEmpty()) {
            Toast.makeText(context, "No promotions available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getPromotions() {
        Call<List<Promotion>> call = getApiService().getPromotions(preference.getUserToken());
        call.enqueue(new Callback<List<Promotion>>() {
            @Override
            public void onResponse(@NonNull Call<List<Promotion>> call, @NonNull Response<List<Promotion>> response) {
                refreshLayout.setRefreshing(false);
                List<Promotion> promotionListResponse = response.body();
                if (response.isSuccessful() && promotionListResponse != null) {
                    promotionList.clear();
                    promotionList.addAll(promotionListResponse);
                    promotionAdapter.notifyDataSetChanged();
                    isEmpty();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<List<Promotion>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
