package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;

/**
 * Created by gladwinbobby on 07/09/17
 */

public class DetailedInfoHolder extends RecyclerView.ViewHolder {

    public TextView textViewTitle, textViewDesc;

    public DetailedInfoHolder(View itemView) {
        super(itemView);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        textViewDesc = itemView.findViewById(R.id.txt_desc);
    }
}
