package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mygrandwedding.mgw.BlogDetailActivity;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.adapter.BlogAdapter;
import com.mygrandwedding.mgw.callback.BlogCallback;
import com.mygrandwedding.mgw.model.Blog;
import com.mygrandwedding.mgw.model.BlogData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlogCategoryFragment extends Fragment implements BlogCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private View rootView;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewBlog;
    private List<Blog> blogList;
    private LinearLayoutManager layoutManager;
    private BlogAdapter blogAdapter;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    public BlogCategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_blog_category, container, false);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        return rootView;
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    @Override
    public void onBlogClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, blogList.get(position).getId());
        launchWithBundle(context, BlogDetailActivity.class, bundle);
    }

    private void initObjects() {
        refreshLayout = rootView.findViewById(R.id.refresh);
        viewBlog = rootView.findViewById(R.id.blog);

        context = getActivity();
        blogList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        blogAdapter = new BlogAdapter(context, blogList, this);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecyclerView() {
        viewBlog.setLayoutManager(new LinearLayoutManager(context));
        viewBlog.setAdapter(blogAdapter);
        viewBlog.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getBlog();
                }
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void resetList() {
        count = 0;
        setUrl();
        getBlog();
    }

    private void setUrl() {
        url = MGW_URL + "blog/list/blogs/";
    }

    private void clearList() {
        if (count == 0) {
            blogList.clear();
            blogAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        if (blogList.isEmpty()) {
            Toast.makeText(context, "No blogs found", Toast.LENGTH_SHORT).show();
        }
    }

    private void getBlog() {
        setLoading(true);
        Call<BlogData> call = getApiService().getBlog(url);
        call.enqueue(new Callback<BlogData>() {
            @Override
            public void onResponse(@NonNull Call<BlogData> call, @NonNull Response<BlogData> response) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    BlogData blogDataResponse = response.body();
                    if (response.isSuccessful() && blogDataResponse != null) {
                        setLoading(false);
                        clearList();
                        count = blogDataResponse.getCount();
                        url = blogDataResponse.getNextUrl();
                        blogList.addAll(blogDataResponse.getBlogList());
                        blogAdapter.notifyDataSetChanged();
                        isEmpty();
                    } else processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<BlogData> call, @NonNull Throwable t) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    processFailure(context, t);
                }
            }
        });
    }
}
