package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.VendorSubcategory;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class AddVendorSubcategoryListTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private List<VendorSubcategory> vendorSubcategoryList;

    public AddVendorSubcategoryListTask(Context context, List<VendorSubcategory> vendorSubcategoryList) {
        this.context = context;
        this.vendorSubcategoryList = vendorSubcategoryList;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getAppDatabase(context).generalDao().insertVendorSubcategoryList(vendorSubcategoryList);
        return null;
    }
}
