package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.AssociatedWeddingCallback;
import com.mygrandwedding.mgw.holder.AssociatedWeddingHolder;
import com.mygrandwedding.mgw.model.Wedding;

import java.util.List;

/**
 * Created by gladwinbobby on 02/10/17
 */

public class AssociatedWeddingAdapter extends RecyclerView.Adapter<AssociatedWeddingHolder> {

    private Context context;
    private List<Wedding> weddingList;
    private AssociatedWeddingCallback callback;

    public AssociatedWeddingAdapter(Context context, List<Wedding> weddingList, AssociatedWeddingCallback callback) {
        this.context = context;
        this.weddingList = weddingList;
        this.callback = callback;
    }

    @Override
    public AssociatedWeddingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AssociatedWeddingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_associated_wedding, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(AssociatedWeddingHolder holder, int position) {
        Wedding wedding = weddingList.get(position);
        holder.textViewTitle.setText(wedding.getTitle());
        holder.textViewDescription.setText(wedding.getDescription());
        holder.textViewRole.setText(wedding.getRole());
    }

    @Override
    public int getItemCount() {
        return weddingList.size();
    }
}
