package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 30/09/17
 */

public interface CityCallback {
    void onCityClick(int position);
}
