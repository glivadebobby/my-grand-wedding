package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.BlogCategory;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class AddBlogCategoryListTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private List<BlogCategory> blogCategoryList;

    public AddBlogCategoryListTask(Context context, List<BlogCategory> blogCategoryList) {
        this.context = context;
        this.blogCategoryList = blogCategoryList;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getAppDatabase(context).deleteDao().deleteBlogCategory();
        getAppDatabase(context).generalDao().insertBlogCategoryList(blogCategoryList);
        return null;
    }
}
