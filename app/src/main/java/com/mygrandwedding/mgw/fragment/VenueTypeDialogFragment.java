package com.mygrandwedding.mgw.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.MyBudget;
import com.mygrandwedding.mgw.model.NotifyRefresh;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.util.MyData.HOTEL_RESORT;
import static com.mygrandwedding.mgw.util.MyData.MANDAPAM;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_VENUES;

/**
 * Created by gladwinbobby on 25/12/17
 */

public class VenueTypeDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String ARG_ID = "id";
    private Context context;
    private View rootView;
    private RadioGroup groupWeddingVenue;
    private Button buttonCancel, buttonSubmit;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    public static VenueTypeDialogFragment newInstance(int id) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_ID, id);
        VenueTypeDialogFragment fragment = new VenueTypeDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_venue_type, container, false);
        initObjects();
        initCallbacks();
        processBundle();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonCancel) {
            dismiss();
        } else if (view == buttonSubmit) {
            processMyBudget();
        }
    }

    private void initObjects() {
        groupWeddingVenue = rootView.findViewById(R.id.grp_wedding_venue);
        buttonCancel = rootView.findViewById(R.id.btn_cancel);
        buttonSubmit = rootView.findViewById(R.id.btn_submit);
        layoutLoading = rootView.findViewById(R.id.loading);

        context = getActivity();
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        buttonCancel.setOnClickListener(this);
        buttonSubmit.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.getInt(ARG_ID) == MANDAPAM)
                ((RadioButton) rootView.findViewById(R.id.radio_mandapam)).setChecked(true);
        }
    }

    private void processMyBudget() {
        createBudget(new MyBudget(preference.getWeddingId(), WEDDING_VENUES,
                groupWeddingVenue.getCheckedRadioButtonId() == R.id.radio_mandapam ? MANDAPAM : HOTEL_RESORT));
    }

    private void createBudget(MyBudget myBudget) {
        setLoading(true);
        Call<MyBudget> call = getApiService().createBudget(preference.getUserToken(), myBudget);
        call.enqueue(new Callback<MyBudget>() {
            @Override
            public void onResponse(@NonNull Call<MyBudget> call, @NonNull Response<MyBudget> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Budget updated", Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new NotifyRefresh());
                    dismiss();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<MyBudget> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
