package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.PhotoCallback;

/**
 * Created by gladwinbobby on 07/09/17
 */

public class PhotoHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewPhoto;
    public View viewTint;
    public TextView textViewCount;
    private PhotoCallback callback;

    public PhotoHolder(View itemView, PhotoCallback callback) {
        super(itemView);
        imageViewPhoto = itemView.findViewById(R.id.img_photo);
        viewTint = itemView.findViewById(R.id.tint);
        textViewCount = itemView.findViewById(R.id.txt_count);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onPhotoClick(getLayoutPosition());
        }
    }
}
