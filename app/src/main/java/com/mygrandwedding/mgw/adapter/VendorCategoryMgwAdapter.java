package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.CategoryCallback;
import com.mygrandwedding.mgw.holder.VendorCategoryMgwHolder;
import com.mygrandwedding.mgw.model.VendorCategory;

import java.util.List;

import static com.mygrandwedding.mgw.util.MyData.getCategoryBg;

/**
 * Created by gladwinbobby on 05/09/17
 */

public class VendorCategoryMgwAdapter extends RecyclerView.Adapter<VendorCategoryMgwHolder> {

    private Context context;
    private List<VendorCategory> vendorCategoryList;
    private CategoryCallback callback;

    public VendorCategoryMgwAdapter(Context context, List<VendorCategory> vendorCategoryList, CategoryCallback callback) {
        this.context = context;
        this.vendorCategoryList = vendorCategoryList;
        this.callback = callback;
    }

    @Override
    public VendorCategoryMgwHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VendorCategoryMgwHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vendor_category_mgw, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(VendorCategoryMgwHolder holder, int position) {
        VendorCategory vendorCategory = vendorCategoryList.get(position);
        holder.textViewCategory.setText(vendorCategory.getCategory());
        if (getCategoryBg(vendorCategory.getId()) != null) {
            GlideApp.with(context)
                    .load(getCategoryBg(vendorCategory.getId()))
                    .into(holder.imageViewCategory);
        } else {
            GlideApp.with(context)
                    .load(vendorCategory.getImage() == null ? vendorCategory.getImageId() : vendorCategory.getImage())
                    .into(holder.imageViewCategory);
        }
    }

    @Override
    public int getItemCount() {
        return vendorCategoryList.size();
    }
}
