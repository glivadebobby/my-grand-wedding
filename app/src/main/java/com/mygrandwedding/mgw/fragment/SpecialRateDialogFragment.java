package com.mygrandwedding.mgw.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.mygrandwedding.mgw.GetQuoteActivity;
import com.mygrandwedding.mgw.R;

import static com.mygrandwedding.mgw.app.MyActivity.launch;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class SpecialRateDialogFragment extends DialogFragment implements View.OnClickListener {

    private Context context;
    private View rootView;
    private ImageView imageViewSpecialRate, imageViewRemove;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_special_rate, container, false);
        initObjects();
        initCallbacks();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewSpecialRate) {
            launch(context, GetQuoteActivity.class);
            dismiss();
        } else if (view == imageViewRemove) {
            dismiss();
        }
    }

    private void initObjects() {
        imageViewSpecialRate = rootView.findViewById(R.id.img_special_rate);
        imageViewRemove = rootView.findViewById(R.id.img_remove);

        context = getActivity();
    }

    private void initCallbacks() {
        imageViewSpecialRate.setOnClickListener(this);
        imageViewRemove.setOnClickListener(this);
    }
}
