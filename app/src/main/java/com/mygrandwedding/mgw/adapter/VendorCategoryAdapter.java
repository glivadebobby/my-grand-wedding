package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.CategoryCallback;
import com.mygrandwedding.mgw.holder.VendorCategoryHolder;
import com.mygrandwedding.mgw.model.VendorCategory;

import java.util.List;

import static com.mygrandwedding.mgw.util.MyData.getCategoryBg;
import static com.mygrandwedding.mgw.util.MyData.getCategoryIcon;

/**
 * Created by gladwinbobby on 05/09/17
 */

public class VendorCategoryAdapter extends RecyclerView.Adapter<VendorCategoryHolder> {

    private Context context;
    private List<VendorCategory> vendorCategoryList;
    private CategoryCallback callback;

    public VendorCategoryAdapter(Context context, List<VendorCategory> vendorCategoryList, CategoryCallback callback) {
        this.context = context;
        this.vendorCategoryList = vendorCategoryList;
        this.callback = callback;
    }

    @Override
    public VendorCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VendorCategoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vendor_category, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(VendorCategoryHolder holder, int position) {
        VendorCategory vendorCategory = vendorCategoryList.get(position);
        holder.imageViewIcon.setImageResource(getCategoryIcon(vendorCategory.getId()) != null ? getCategoryIcon(vendorCategory.getId()) : 0);
        holder.textViewCategory.setText(vendorCategory.getCategory());
        holder.textViewResult.setVisibility(vendorCategory.getCount() == 0 ? View.GONE : View.VISIBLE);
        holder.textViewResult.setText(context.getResources().getQuantityString(R.plurals.result,
                vendorCategory.getCount(), vendorCategory.getCount()));
        holder.getBackgroundImage().reuse();
        if (getCategoryBg(vendorCategory.getId()) != null) {
            GlideApp.with(context)
                    .load(getCategoryBg(vendorCategory.getId()))
                    .into(holder.getBackgroundImage());
        } else {
            GlideApp.with(context)
                    .load(vendorCategory.getImage() == null ? vendorCategory.getImageId() : vendorCategory.getImage())
                    .into(holder.getBackgroundImage());
        }
    }

    @Override
    public int getItemCount() {
        return vendorCategoryList.size();
    }
}
