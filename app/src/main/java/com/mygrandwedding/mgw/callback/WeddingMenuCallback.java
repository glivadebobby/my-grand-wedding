package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 03/10/17
 */

public interface WeddingMenuCallback {
    void onWeddingClick(int position);
}
