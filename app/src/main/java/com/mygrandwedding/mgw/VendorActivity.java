package com.mygrandwedding.mgw;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.mygrandwedding.mgw.adapter.VendorAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.VendorCallback;
import com.mygrandwedding.mgw.model.Vendor;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.model.VendorData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_IMAGE;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundleResult;

public class VendorActivity extends AppCompatActivity implements VendorCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable, View.OnClickListener {

    private static final int REQUEST_FILTER = 4;
    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewVendors;
    private FloatingActionButton buttonSpecialRate;
    private FloatingActionButton buttonFilter;
    private LinearLayout layoutEmpty;
    private TextView textViewEmpty;
    private List<Vendor> vendorList;
    private LinearLayoutManager layoutManager;
    private VendorAdapter vendorAdapter;
    private MyPreference preference;
    private VendorCategory category;
    private int id;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor);
        initObjects();
        initCallbacks();
        initToolbar();
        processBundle();
        initRecyclerView();
        initRefresh();
        initEmpty();
        showCase();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_FILTER && resultCode == RESULT_OK) {
            run();
        }
    }

    @Override
    public void onVendorClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, vendorList.get(position).getUser());
        bundle.putString(EXTRA_IMAGE, vendorList.get(position).getImage());
        launchWithBundle(context, VendorDetailActivity.class, bundle);
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSpecialRate) {
            launch(context, GetQuoteActivity.class);
        } else if (view == buttonFilter) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(EXTRA_VENDOR, category);
            launchWithBundleResult(this, VendorFilterActivity.class, bundle, REQUEST_FILTER);
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewVendors = findViewById(R.id.vendors);
        buttonSpecialRate = findViewById(R.id.fab_special_rate);
        buttonFilter = findViewById(R.id.fab_filter);
        layoutEmpty = findViewById(R.id.empty);
        textViewEmpty = findViewById(R.id.txt_empty);

        context = this;
        vendorList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        vendorAdapter = new VendorAdapter(context, vendorList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        buttonSpecialRate.setOnClickListener(this);
        buttonFilter.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            category = bundle.getParcelable(EXTRA_VENDOR);
            if (category != null) {
                id = category.getId();
                if (getSupportActionBar() != null)
                    getSupportActionBar().setTitle(category.getCategory());
                toolbar.setSubtitle(getResources().getQuantityString(R.plurals.result,
                        category.getCount(), category.getCount()));
            }
        }
    }

    private void initRecyclerView() {
        viewVendors.setLayoutManager(layoutManager);
        viewVendors.setAdapter(vendorAdapter);
        viewVendors.setNestedScrollingEnabled(false);
        viewVendors.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getVendors();
                }
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void initEmpty() {
        textViewEmpty.setText(getString(R.string.error_empty_vendor));
    }

    private void setVendorCount() {
        toolbar.setSubtitle(getResources().getQuantityString(R.plurals.result, count, count));
    }

    private void resetList() {
        count = 0;
        setUrl();
        getVendors();
    }

    private void setUrl() {
        url = MGW_URL + "vendor/app/list/" + id + "/";
        boolean isQuestion = true;
        if (preference.getCityId() > 0) {
            url += "?location=" + preference.getCityId();
            isQuestion = false;
        }
        if (preference.getMinPrice() != 0 && category != null) {
            url += isQuestion ? "?" : "&";
            url += "min_price=" + preference.getMinPrice() * category.getFilterPrice();
            isQuestion = false;
        }
        if (preference.getMaxPrice() != 100 && category != null) {
            url += isQuestion ? "?" : "&";
            url += "max_price=" + preference.getMaxPrice() * category.getFilterPrice();
            isQuestion = false;
        }
        if (preference.getRating() > 0) {
            url += isQuestion ? "?" : "&";
            url += "rating=" + (int) preference.getRating();
        }
    }

    private void clearList() {
        if (count == 0) {
            vendorList.clear();
            vendorAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        layoutEmpty.setVisibility(vendorList.isEmpty() ? View.VISIBLE : View.GONE);
    }

    private void getVendors() {
        setLoading(true);
        Call<VendorData> call = getApiService().getVendors(url);
        call.enqueue(new Callback<VendorData>() {
            @Override
            public void onResponse(@NonNull Call<VendorData> call,
                                   @NonNull Response<VendorData> response) {
                refreshLayout.setRefreshing(false);
                VendorData vendorDataResponse = response.body();
                if (response.isSuccessful() && vendorDataResponse != null) {
                    setLoading(false);
                    clearList();
                    count = vendorDataResponse.getCount();
                    url = vendorDataResponse.getNextUrl();
                    vendorList.addAll(vendorDataResponse.getVendorList());
                    vendorAdapter.notifyDataSetChanged();
                    setVendorCount();
                    isEmpty();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<VendorData> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void showCase() {
        if (preference.getIntro4()) return;
        TapTargetView.showFor(this,
                TapTarget.forView(buttonFilter, "Filter the vendors based on city, price and rating")
                        .tintTarget(false)
                        .cancelable(false)
                        .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        preference.setIntro4(true);
                    }
                });
    }
}
