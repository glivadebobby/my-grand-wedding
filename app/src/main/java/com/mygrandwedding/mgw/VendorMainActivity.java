package com.mygrandwedding.mgw;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.QuoteAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.FcmToken;
import com.mygrandwedding.mgw.model.Quote;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;

public class VendorMainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewQuotes;
    private FrameLayout layoutLoading;
    private List<Quote> quoteList;
    private LinearLayoutManager layoutManager;
    private QuoteAdapter quoteAdapter;
    private MyPreference preference;
    private long backPressTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_main);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
        updateFcm();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if ((backPressTime + 5000 - System.currentTimeMillis()) < 0) {
            Toast.makeText(context, "Press back again to exit", Toast.LENGTH_SHORT).show();
            backPressTime = System.currentTimeMillis();
        } else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.vendor_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_my_profile:
                launch(context, VendorProfileActivity.class);
                return true;
            case R.id.action_change_password:
                launch(context, ChangePasswordActivity.class);
                return true;
            case R.id.action_logout:
                logoutAlert();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getQuotes();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getQuotes();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewQuotes = findViewById(R.id.quotes);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        quoteList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        quoteAdapter = new QuoteAdapter(quoteList);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initRecyclerView() {
        viewQuotes.setLayoutManager(layoutManager);
        viewQuotes.setAdapter(quoteAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void getQuotes() {
        Call<List<Quote>> call = getApiService().getQuotes(preference.getUserToken());
        call.enqueue(new Callback<List<Quote>>() {
            @Override
            public void onResponse(@NonNull Call<List<Quote>> call,
                                   @NonNull Response<List<Quote>> response) {
                refreshLayout.setRefreshing(false);
                List<Quote> quoteListResponse = response.body();
                if (response.isSuccessful() && quoteListResponse != null) {
                    quoteList.clear();
                    quoteList.addAll(quoteListResponse);
                    quoteAdapter.notifyDataSetChanged();
                    isEmpty();
                } else {
                    processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Quote>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void isEmpty() {
        if (quoteList.isEmpty()) {
            Toast.makeText(context, "No quote requests available", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateFcm() {
        if (preference.isTokenUploaded() || preference.getFcmToken() == null || preference.getToken() == null)
            return;
        Call<Void> call = getApiService().updateFcm(preference.getUserToken(),
                new FcmToken(UUID.randomUUID().toString(), preference.getFcmToken()));
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful())
                    preference.setTokenUploaded(true);
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {

            }
        });
    }

    private void logoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setProgress(true);
                logout();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void logout() {
        Call<Void> call = getApiService().logout(preference.getUserToken());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                setProgress(false);
                if (response.isSuccessful()) {
                    preference.clearUser();
                    Toast.makeText(context, getString(R.string.alert_logout), Toast.LENGTH_SHORT).show();
                    launchClearStack(context, SplashActivity.class);
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setProgress(false);
                processFailure(context, t);
            }
        });
    }

    private void setProgress(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
