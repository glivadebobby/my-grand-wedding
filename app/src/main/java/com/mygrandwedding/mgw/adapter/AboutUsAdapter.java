package com.mygrandwedding.mgw.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.holder.AboutUsHolder;
import com.mygrandwedding.mgw.model.AboutUs;

import java.util.List;

/**
 * Created by gladwinbobby on 11/10/17
 */

public class AboutUsAdapter extends RecyclerView.Adapter<AboutUsHolder> {

    private List<AboutUs> aboutUsList;

    public AboutUsAdapter(List<AboutUs> aboutUsList) {
        this.aboutUsList = aboutUsList;
    }

    @Override
    public AboutUsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AboutUsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_about_us, parent, false));
    }

    @Override
    public void onBindViewHolder(AboutUsHolder holder, int position) {
        AboutUs faq = aboutUsList.get(position);
        holder.textViewTitle.setText(faq.getTitle());
        holder.textViewDescription.setText(faq.getDescription());
    }

    @Override
    public int getItemCount() {
        return aboutUsList.size();
    }
}
