package com.mygrandwedding.mgw.util;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyTypefaceSpan;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Created by gladwinbobby on 11/10/17
 */

public class MyParser {
    public static String parseNull(Integer data) {
        return data == null ? "-NA-" : String.valueOf(data);
    }

    public static String parseNull(Float data) {
        return data == null ? "-NA-" : String.format(Locale.getDefault(), "\u20B9 %1$.2f", data);
    }

    public static String parseNull(String data) {
        return data == null || data.isEmpty() ? "-NA-" : data;
    }

    public static Spanned parseHtml(String data) {
        if (data == null || data.isEmpty()) return new SpannableString("");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(data, 0);
        } else {
            //noinspection deprecation
            return Html.fromHtml(data);
        }
    }

    public static SpannableString getSpannedText(Context context, String data) {
        SpannableString spannableString = new SpannableString(data);
        CalligraphyTypefaceSpan typefaceSpan = new CalligraphyTypefaceSpan(TypefaceUtils.load(context.getAssets(),
                "fonts/SourceSansPro-Bold.ttf"));
        spannableString.setSpan(typefaceSpan, 0, data.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }
}
