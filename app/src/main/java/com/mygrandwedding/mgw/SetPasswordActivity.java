package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.Password;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;

public class SetPasswordActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutPassword, layoutRepeatPassword;
    private TextInputEditText editTextPassword, editTextRepeatPassword;
    private Button buttonSubmit;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        initObjects();
        initToolbar();
        initCallbacks();
        setPasswordFont();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (view == editTextPassword) isValidCurrentPassword();
        if (view == editTextRepeatPassword) isValidRepeatPassword();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            processChangePassword();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutPassword = findViewById(R.id.password);
        layoutRepeatPassword = findViewById(R.id.repeat_password);
        editTextPassword = findViewById(R.id.input_password);
        editTextRepeatPassword = findViewById(R.id.input_repeat_password);
        buttonSubmit = findViewById(R.id.btn_submit);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        editTextPassword.setOnFocusChangeListener(this);
        editTextRepeatPassword.setOnFocusChangeListener(this);
        buttonSubmit.setOnClickListener(this);
    }

    private void setPasswordFont() {
        layoutPassword.setTypeface(TypefaceUtils.load(getAssets(), "fonts/SourceSansPro-Regular.ttf"));
        layoutRepeatPassword.setTypeface(TypefaceUtils.load(getAssets(), "fonts/SourceSansPro-Regular.ttf"));
        editTextPassword.setTypeface(TypefaceUtils.load(getAssets(), "fonts/SourceSansPro-Regular.ttf"));
        editTextRepeatPassword.setTypeface(TypefaceUtils.load(getAssets(), "fonts/SourceSansPro-Regular.ttf"));
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
        editTextRepeatPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    private void processChangePassword() {
        String password = editTextPassword.getText().toString().trim();
        String repeatPassword = editTextRepeatPassword.getText().toString().trim();
        if (isValidCurrentPassword(password) && isValidRepeatPassword(password, repeatPassword)) {
            setLoading(true);
            setPassword(new Password(password));
        }
    }

    private void isValidCurrentPassword() {
        isValidCurrentPassword(editTextPassword.getText().toString().trim());
    }

    private boolean isValidCurrentPassword(String changePassword) {
        if (changePassword.isEmpty()) {
            layoutPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutPassword.getHint()));
            return false;
        } else if (changePassword.length() < 6) {
            layoutPassword.setError(getString(R.string.error_password_length));
            return false;
        }
        layoutPassword.setErrorEnabled(false);
        return true;
    }

    private void isValidRepeatPassword() {
        isValidRepeatPassword(editTextPassword.getText().toString().trim(), editTextRepeatPassword.getText().toString().trim());
    }

    private boolean isValidRepeatPassword(String password, String repeatPassword) {
        if (repeatPassword.isEmpty()) {
            layoutRepeatPassword.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutRepeatPassword.getHint()));
            return false;
        } else if (repeatPassword.length() < 6) {
            layoutRepeatPassword.setError(getString(R.string.error_password_length));
            return false;
        } else if (!password.equals(repeatPassword)) {
            layoutRepeatPassword.setError(getString(R.string.error_password_mismatch));
            return false;
        }
        layoutRepeatPassword.setErrorEnabled(false);
        return true;
    }

    private void setPassword(Password password) {
        Call<Data> call = getApiService().setPassword(preference.getUserToken(), password);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    Toast.makeText(context, dataResponse.getData(), Toast.LENGTH_SHORT).show();
                    setResult(RESULT_OK);
                    finish();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
