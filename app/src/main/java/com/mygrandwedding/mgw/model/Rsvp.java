package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 03/12/17
 */

public class Rsvp {

    @SerializedName("id")
    private Integer id;
    @SerializedName("wedding")
    private int wedding;
    @SerializedName("count")
    private int count;
    @SerializedName("is_coming")
    private boolean isComing;
    private boolean isExpand;
    @SerializedName("message")
    private String message;
    @SerializedName("created_on")
    private String date;
    @SerializedName("user")
    private User user;

    public Rsvp(int wedding, int count, boolean isComing, String message) {
        this.wedding = wedding;
        this.count = count;
        this.isComing = isComing;
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getWedding() {
        return wedding;
    }

    public void setWedding(int wedding) {
        this.wedding = wedding;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isComing() {
        return isComing;
    }

    public void setComing(boolean coming) {
        isComing = coming;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFormattedMessage() {
        if (message == null || message.isEmpty()) return "~ no special message ~";
        return "\'" + message + "\'";
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
