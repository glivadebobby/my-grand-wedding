package com.mygrandwedding.mgw.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.CategoryCallback;
import com.yayandroid.parallaxrecyclerview.ParallaxViewHolder;

/**
 * Created by gladwinbobby on 05/09/17
 */

public class VendorCategoryHolder extends ParallaxViewHolder implements View.OnClickListener {

    public ImageView imageViewIcon;
    public TextView textViewCategory, textViewResult;
    private CategoryCallback callback;

    public VendorCategoryHolder(View itemView, CategoryCallback callback) {
        super(itemView);
        imageViewIcon = itemView.findViewById(R.id.img_icon);
        textViewCategory = itemView.findViewById(R.id.txt_category);
        textViewResult = itemView.findViewById(R.id.txt_result);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public int getParallaxImageId() {
        return R.id.img_category;
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) callback.onCategoryClick(getLayoutPosition());
    }
}
