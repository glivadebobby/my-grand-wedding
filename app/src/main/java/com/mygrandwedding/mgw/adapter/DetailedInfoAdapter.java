package com.mygrandwedding.mgw.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.holder.DetailedInfoHolder;
import com.mygrandwedding.mgw.model.DetailedInfo;

import java.util.List;

/**
 * Created by gladwinbobby on 07/09/17
 */

public class DetailedInfoAdapter extends RecyclerView.Adapter<DetailedInfoHolder> {

    private List<DetailedInfo> detailedInfoList;

    public DetailedInfoAdapter(List<DetailedInfo> detailedInfoList) {
        this.detailedInfoList = detailedInfoList;
    }

    @Override
    public DetailedInfoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DetailedInfoHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detailed_info, parent, false));
    }

    @Override
    public void onBindViewHolder(DetailedInfoHolder holder, int position) {
        DetailedInfo detailedInfo = detailedInfoList.get(position);
        holder.textViewTitle.setText(detailedInfo.getTitle());
        holder.textViewDesc.setText(detailedInfo.getDesc());
        holder.itemView.setBackgroundResource(holder.getLayoutPosition() % 2 == 0 ? android.R.color.transparent : R.color.grey_60);
    }

    @Override
    public int getItemCount() {
        return detailedInfoList.size();
    }
}
