package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.mygrandwedding.mgw.adapter.MyPagerAdapter;
import com.mygrandwedding.mgw.fragment.HomeFragment;
import com.mygrandwedding.mgw.fragment.IntroFragment;
import com.mygrandwedding.mgw.helper.ParallaxPagerTransformer;
import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;

public class IntroActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private ViewPager viewPager;
    private ImageView imageViewLogo;
    private PageIndicatorView indicatorView;
    private List<Fragment> fragmentList;
    private MyPagerAdapter pagerAdapter;
    private float mHomeLogoPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        initObjects();
        initCallbacks();
        initViewPager();
        setFragments();
        setLogoPosition();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (position == 2) {
            imageViewLogo.setTranslationY(positionOffset * -mHomeLogoPosition / 2);
            indicatorView.setTranslationY(positionOffset * indicatorView.getHeight() * 5);
        }
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View view) {

    }

    private void initObjects() {
        viewPager = findViewById(R.id.intro);
        imageViewLogo = findViewById(R.id.img_logo);
        indicatorView = findViewById(R.id.indicator);

        fragmentList = new ArrayList<>();
        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), fragmentList);
    }

    private void initCallbacks() {
        viewPager.addOnPageChangeListener(this);
    }

    private void initViewPager() {
        viewPager.setAdapter(pagerAdapter);
        viewPager.setPageTransformer(false, new ParallaxPagerTransformer(R.id.img_intro));
        indicatorView.setViewPager(viewPager);
    }

    private void setFragments() {
        fragmentList.add(IntroFragment.newInstance(R.drawable.intro1, "Welcome to My Grand Wedding.\nDecided to Tie the Knot??"));
        fragmentList.add(IntroFragment.newInstance(R.drawable.intro2, "Don\'t know where to begin?\nSelect from more than 1000 options."));
        fragmentList.add(IntroFragment.newInstance(R.drawable.intro3, "Get Trendy Ideas\nto make your wedding awesome."));
        fragmentList.add(new HomeFragment());
        pagerAdapter.notifyDataSetChanged();
        indicatorView.setCount(fragmentList.size());
    }

    private void setLogoPosition() {
        imageViewLogo.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageViewLogo.getViewTreeObserver().removeOnPreDrawListener(this);
                int imageHeightPixels = imageViewLogo.getHeight();
                mHomeLogoPosition = imageHeightPixels * 4f;
                return false;
            }
        });
    }

    private void processBundle() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Bundle bundle = getIntent().getExtras();
                if (bundle != null) {
                    viewPager.setCurrentItem(bundle.getInt(EXTRA_ID));
                }
            }
        }, 200);
    }
}
