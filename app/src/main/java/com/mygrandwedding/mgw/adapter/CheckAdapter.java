package com.mygrandwedding.mgw.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.CheckCallback;
import com.mygrandwedding.mgw.holder.CheckHolder;
import com.mygrandwedding.mgw.model.Checklist;

import java.util.List;

/**
 * Created by gladwinbobby on 18/10/17
 */

public class CheckAdapter extends RecyclerView.Adapter<CheckHolder> {

    private List<Checklist> checklistList;
    private CheckCallback callback;
    private int position;

    public CheckAdapter(List<Checklist> checklistList, CheckCallback callback, int position) {
        this.checklistList = checklistList;
        this.callback = callback;
        this.position = position;
    }

    @Override
    public CheckHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CheckHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_check, parent, false), callback, position);
    }

    @Override
    public void onBindViewHolder(CheckHolder holder, int position) {
        Checklist checklist = checklistList.get(position);
        holder.checkedTextView.setText(checklist.getName());
        holder.checkedTextView.setChecked(checklist.isChecked());
    }

    @Override
    public int getItemCount() {
        return checklistList.size();
    }
}
