package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.IdeaCallback;
import com.mygrandwedding.mgw.holder.IdeaHolder;
import com.mygrandwedding.mgw.model.Idea;

import java.util.List;

/**
 * Created by gladwinbobby on 18/09/17
 */

public class IdeaAdapter extends RecyclerView.Adapter<IdeaHolder> {

    private Context context;
    private List<Idea> ideaList;
    private IdeaCallback callback;

    public IdeaAdapter(Context context, List<Idea> ideaList, IdeaCallback callback) {
        this.context = context;
        this.ideaList = ideaList;
        this.callback = callback;
    }

    @Override
    public IdeaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new IdeaHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_idea, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(IdeaHolder holder, int position) {
        Idea idea = ideaList.get(position);
        GlideApp.with(context)
                .load(idea.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewIdea);
        holder.textViewTitle.setText(idea.getTitle());
    }

    @Override
    public int getItemCount() {
        return ideaList.size();
    }
}
