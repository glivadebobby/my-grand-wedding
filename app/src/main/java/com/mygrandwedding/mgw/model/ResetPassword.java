package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 13/09/17
 */

public class ResetPassword {

    @SerializedName("role")
    private int role;
    @SerializedName("mobile_number")
    private String mobileNumber;
    @SerializedName("otp")
    private String otp;
    @SerializedName("password")
    private String password;
    @SerializedName("client")
    private String client;

    public ResetPassword(int role, String mobileNumber, String otp, String password, String client) {
        this.role = role;
        this.mobileNumber = mobileNumber;
        this.otp = otp;
        this.password = password;
        this.client = client;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
}
