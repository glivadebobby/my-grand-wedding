package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.IdeaCallback;

/**
 * Created by gladwinbobby on 18/09/17
 */

public class IdeaHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewIdea;
    public TextView textViewTitle;
    private IdeaCallback callback;

    public IdeaHolder(View itemView, IdeaCallback callback) {
        super(itemView);
        imageViewIdea = itemView.findViewById(R.id.img_idea);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onIdeaClick(getLayoutPosition());
        }
    }
}
