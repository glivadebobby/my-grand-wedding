package com.mygrandwedding.mgw.model;

/**
 * Created by gladwinbobby on 11/10/17
 */

public class AboutUs {

    private int id;
    private String title;
    private String description;

    public AboutUs(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
