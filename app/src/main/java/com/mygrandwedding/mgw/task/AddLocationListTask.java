package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.Location;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class AddLocationListTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private List<Location> locationList;

    public AddLocationListTask(Context context, List<Location> locationList) {
        this.context = context;
        this.locationList = locationList;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getAppDatabase(context).deleteDao().deleteLocation();
        getAppDatabase(context).generalDao().insertLocationList(locationList);
        return null;
    }
}
