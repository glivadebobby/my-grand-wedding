package com.mygrandwedding.mgw;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.User;
import com.mygrandwedding.mgw.model.UserProfile;
import com.mygrandwedding.mgw.model.Wedding;
import com.mygrandwedding.mgw.task.AddUserTask;
import com.mygrandwedding.mgw.task.LogoutTask;
import com.mygrandwedding.mgw.task.UpdateUserTask;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithResult;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;
import static com.mygrandwedding.mgw.util.DateParser.getDob;
import static com.mygrandwedding.mgw.util.MyParser.parseNull;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_SET_PASSWORD = 1;
    private static final int REQUEST_COVER_PIC = 2;
    private static final int REQUEST_PROFILE_PIC = 3;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 4;
    private Context context;
    private Toolbar toolbar;
    private ImageView imageViewCover;
    private LinearLayout layoutProfileDetail;
    private TextView textViewUpdateCover, textViewName, textViewMobileNumber, textViewEmail,
            textViewGender, textViewDob, textViewEditProfile, textViewMyWedding,
            textViewAssociatedWedding, textViewChangePhone, textViewChangePassword, textViewLogout;
    private CircleImageView imageViewProfile;
    private FloatingActionButton buttonProfile;
    private FrameLayout layoutLoading;
    private User user;
    private Wedding wedding;
    private MyPreference preference;
    private int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initObjects();
        initToolbar();
        initCallbacks();
        loadUserProfile();
        loadMyWedding();
        getUser();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED)
                    pickImages();
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SET_PASSWORD && resultCode == RESULT_OK) {
            getUser();
        } else {
            EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
                @Override
                public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                    if (!imageFiles.isEmpty()) {
                        File file = imageFiles.get(0);
                        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"), file);
                        MultipartBody.Part part = MultipartBody.Part.createFormData(type == REQUEST_COVER_PIC
                                        ? "cover_photo" : "profile_pic", file.getName(),
                                requestBody);
                        setLoading(true);
                        updatePic(part);
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        if (view == textViewUpdateCover) {
            type = REQUEST_COVER_PIC;
            processPickImages();
        } else if (view == buttonProfile) {
            type = REQUEST_PROFILE_PIC;
            processPickImages();
        } else if (view == textViewEditProfile) {
            launch(context, EditProfileActivity.class);
        } else if (view == textViewMyWedding) {
            if (wedding == null) launch(context, MyWeddingActivity.class);
            else launch(context, MyWeddingSettingsActivity.class);
        } else if (view == textViewAssociatedWedding) {
            launch(context, AssociatedWeddingActivity.class);
        } else if (view == textViewChangePhone) {
            launch(context, ChangePhoneActivity.class);
        } else if (view == textViewChangePassword) {
            if (user.isHasPassword()) launch(context, ChangePasswordActivity.class);
            else launchWithResult(this, SetPasswordActivity.class, REQUEST_SET_PASSWORD);
        } else if (view == textViewLogout) {
            logoutAlert();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        imageViewCover = findViewById(R.id.img_cover);
        layoutProfileDetail = findViewById(R.id.profile_detail);
        textViewUpdateCover = findViewById(R.id.txt_update_cover);
        textViewName = findViewById(R.id.txt_name);
        textViewMobileNumber = findViewById(R.id.txt_mobile_number);
        textViewEmail = findViewById(R.id.txt_email);
        textViewGender = findViewById(R.id.txt_gender);
        textViewDob = findViewById(R.id.txt_dob);
        textViewEditProfile = findViewById(R.id.txt_edit_profile);
        textViewMyWedding = findViewById(R.id.txt_my_wedding);
        textViewAssociatedWedding = findViewById(R.id.txt_associated_wedding);
        textViewChangePhone = findViewById(R.id.txt_change_phone);
        textViewChangePassword = findViewById(R.id.txt_change_password);
        textViewLogout = findViewById(R.id.txt_logout);
        imageViewProfile = findViewById(R.id.img_profile);
        buttonProfile = findViewById(R.id.fab_profile);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        textViewUpdateCover.setOnClickListener(this);
        buttonProfile.setOnClickListener(this);
        textViewEditProfile.setOnClickListener(this);
        textViewMyWedding.setOnClickListener(this);
        textViewAssociatedWedding.setOnClickListener(this);
        textViewChangePhone.setOnClickListener(this);
        textViewChangePassword.setOnClickListener(this);
        textViewLogout.setOnClickListener(this);
    }

    private void loadUserProfile() {
        LiveData<User> data = getAppDatabase(context).userDao().getUser(preference.getId());
        data.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                ProfileActivity.this.user = user;
                if (user != null) {
                    layoutProfileDetail.setVisibility(View.VISIBLE);
                    GlideApp.with(context)
                            .load(user.getUserProfile().getCoverPic())
                            .placeholder(R.drawable.placeholder)
                            .into(imageViewCover);
                    GlideApp.with(context)
                            .load(user.getUserProfile().getProfilePic())
                            .placeholder(R.drawable.placeholder)
                            .into(imageViewProfile);
                    textViewName.setText(parseNull(user.getFirstName()));
                    textViewMobileNumber.setText(parseNull(user.getUsername()));
                    textViewEmail.setText(parseNull(user.getEmail()));
                    textViewGender.setText(parseNull(user.getUserProfile().getGender()));
                    textViewDob.setText(parseNull(getDob(user.getUserProfile().getDob())));
                    textViewChangePassword.setText(user.isHasPassword() ? getString(R.string.txt_change_password) : getString(R.string.txt_set_password));
                }
            }
        });
    }

    private void loadMyWedding() {
        LiveData<Wedding> data = getAppDatabase(context).weddingDao().getMyWedding(preference.getId());
        data.observe(this, new Observer<Wedding>() {
            @Override
            public void onChanged(@Nullable Wedding wedding) {
                ProfileActivity.this.wedding = wedding;
                textViewMyWedding.setText(wedding == null ? getString(R.string.nav_create_wedding) : getString(R.string.txt_my_wedding_settings));
            }
        });
    }

    private void getUser() {
        Call<User> call = getApiService().getUser(preference.getUserToken());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    new AddUserTask(context, userResponse).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                processFailure(context, t);
            }
        });
    }

    private void processPickImages() {
        if (hasStoragePermission()) {
            pickImages();
        } else {
            requestStoragePermission();
        }
    }

    private void pickImages() {
        EasyImage.configuration(context)
                .setImagesFolderName(getString(R.string.app_name))
                .setAllowMultiplePickInGallery(true);
        EasyImage.openChooserWithGallery(this, "Select " + (type == REQUEST_COVER_PIC ? "Cover" : "Profile") + " Pic", type);
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(context, WRITE_EXTERNAL_STORAGE) == PERMISSION_GRANTED;
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXTERNAL_STORAGE);
    }

    private void updatePic(MultipartBody.Part part) {
        Call<UserProfile> call = getApiService().updatePic(preference.getUserToken(), part);
        call.enqueue(new Callback<UserProfile>() {
            @Override
            public void onResponse(@NonNull Call<UserProfile> call, @NonNull Response<UserProfile> response) {
                setLoading(false);
                UserProfile userProfileResponse = response.body();
                if (response.isSuccessful() && userProfileResponse != null) {
                    Toast.makeText(context, (type == REQUEST_COVER_PIC ? "Cover" : "Profile") + " pic updated", Toast.LENGTH_SHORT).show();
                    user.getUserProfile().setProfilePic(userProfileResponse.getProfilePic());
                    user.getUserProfile().setCoverPic(userProfileResponse.getCoverPic());
                    new UpdateUserTask(context, user).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<UserProfile> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void logoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to logout?");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setLoading(true);
                logout();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void logout() {
        Call<Void> call = getApiService().logout(preference.getUserToken());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, getString(R.string.alert_logout), Toast.LENGTH_SHORT).show();
                    new LogoutTask(context).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
