package com.mygrandwedding.mgw.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.CityCallback;
import com.mygrandwedding.mgw.holder.CityHolder;
import com.mygrandwedding.mgw.model.Location;

import java.util.List;

/**
 * Created by gladwinbobby on 30/09/17
 */

public class CityAdapter extends RecyclerView.Adapter<CityHolder> {

    private List<Location> locationList;
    private CityCallback callback;
    private int colorBlack;

    public CityAdapter(List<Location> locationList, CityCallback callback) {
        this.locationList = locationList;
        this.callback = callback;
        colorBlack = Color.parseColor("#373737");
    }

    @Override
    public CityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CityHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(CityHolder holder, int position) {
        Location location = locationList.get(position);
        holder.textViewCity.setText(location.getLocation());
        holder.textViewCity.setBackgroundResource(location.isSelected() ? R.drawable.bg_curved_primary : R.drawable.bg_curved_stroke_grey);
        holder.textViewCity.setTextColor(location.isSelected() ? Color.WHITE : colorBlack);
    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }
}
