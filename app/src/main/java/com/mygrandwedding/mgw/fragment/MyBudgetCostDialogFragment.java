package com.mygrandwedding.mgw.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.MyBudget;
import com.mygrandwedding.mgw.model.NotifyRefresh;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.util.MyData.WEDDING_VENUES;

/**
 * Created by gladwinbobby on 25/12/17
 */

public class MyBudgetCostDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String ARG_ID = "id";
    private static final String ARG_SUB_ID = "sub_id";
    private static final String ARG_COUNT = "count";
    private static final String ARG_COST = "cost";
    private static final String ARG_HEADER = "header";
    private static final String ARG_COUNT_HINT = "count_hint";
    private Context context;
    private View rootView;
    private TextView textViewHeader;
    private TextInputLayout layoutCount, layoutCost;
    private TextInputEditText editTextCount, editTextCost;
    private Button buttonCancel, buttonSubmit;
    private FrameLayout layoutLoading;
    private MyPreference preference;
    private int id, subId;

    public static MyBudgetCostDialogFragment newInstance(int id, int subId, String count, String cost, String header, String countHint) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_ID, id);
        bundle.putInt(ARG_SUB_ID, subId);
        bundle.putString(ARG_COUNT, count);
        bundle.putString(ARG_COST, cost);
        bundle.putString(ARG_HEADER, header);
        bundle.putString(ARG_COUNT_HINT, countHint);
        MyBudgetCostDialogFragment fragment = new MyBudgetCostDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_my_budget_cost, container, false);
        initObjects();
        initCallbacks();
        processBundle();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonCancel) {
            dismiss();
        } else if (view == buttonSubmit) {
            processMyBudget();
        }
    }

    private void initObjects() {
        textViewHeader = rootView.findViewById(R.id.txt_header);
        layoutCount = rootView.findViewById(R.id.count);
        layoutCost = rootView.findViewById(R.id.cost);
        editTextCount = rootView.findViewById(R.id.input_count);
        editTextCost = rootView.findViewById(R.id.input_cost);
        buttonCancel = rootView.findViewById(R.id.btn_cancel);
        buttonSubmit = rootView.findViewById(R.id.btn_submit);
        layoutLoading = rootView.findViewById(R.id.loading);

        context = getActivity();
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        buttonCancel.setOnClickListener(this);
        buttonSubmit.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            id = bundle.getInt(ARG_ID);
            subId = bundle.getInt(ARG_SUB_ID);
            editTextCount.setText(bundle.getString(ARG_COUNT));
            editTextCost.setText(bundle.getString(ARG_COST));
            textViewHeader.setText(String.format(Locale.getDefault(), getString(R.string.format_budget_for), bundle.getString(ARG_HEADER)));
            editTextCount.setHint(String.format(Locale.getDefault(), getString(R.string.format_no_of), bundle.getString(ARG_COUNT_HINT)));
        }
    }

    private void processMyBudget() {
        String count = editTextCount.getText().toString().trim();
        String cost = editTextCost.getText().toString().trim();
        int noOfCount = 0;
        float price = 0f;
        try {
            noOfCount = Integer.parseInt(count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            price = Float.parseFloat(cost);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (count.isEmpty()) {
            layoutCount.requestFocus();
            layoutCount.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutCount.getHint()));
        } else if (noOfCount == 0) {
            layoutCount.requestFocus();
            layoutCount.setError(String.format(Locale.getDefault(), getString(R.string.error_invalid), layoutCount.getHint()));
        } else if (cost.isEmpty()) {
            layoutCost.requestFocus();
            layoutCost.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutCost.getHint()));
        } else if (price == 0f) {
            layoutCost.requestFocus();
            layoutCost.setError(String.format(Locale.getDefault(), getString(R.string.error_invalid), layoutCost.getHint()));
        } else {
            if (subId != -1)
                createBudget(new MyBudget(preference.getWeddingId(), WEDDING_VENUES, subId, noOfCount, price));
            else
                createBudget(new MyBudget(preference.getWeddingId(), id, noOfCount, price, noOfCount * price));
        }
    }

    private void createBudget(MyBudget myBudget) {
        setLoading(true);
        Call<MyBudget> call = getApiService().createBudget(preference.getUserToken(), myBudget);
        call.enqueue(new Callback<MyBudget>() {
            @Override
            public void onResponse(@NonNull Call<MyBudget> call, @NonNull Response<MyBudget> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Budget updated", Toast.LENGTH_SHORT).show();
                    EventBus.getDefault().post(new NotifyRefresh());
                    dismiss();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<MyBudget> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
    }
}
