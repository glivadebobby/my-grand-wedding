package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.ImageCallback;
import com.mygrandwedding.mgw.holder.AddImageHolder;
import com.mygrandwedding.mgw.holder.ImageHolder;

import java.util.List;

/**
 * Created by gladwinbobby on 27/09/17
 */

public class AddImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ADD_IMAGE = 1;
    private static final int IMAGE = 2;
    private Context mContext;
    private List<String> mImageList;
    private ImageCallback mCallback;

    public AddImageAdapter(Context mContext, List<String> mImageList, ImageCallback mCallback) {
        this.mContext = mContext;
        this.mImageList = mImageList;
        this.mCallback = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case ADD_IMAGE:
                return new AddImageHolder(inflater.inflate(R.layout.item_add_image, parent, false), mCallback);
            case IMAGE:
                return new ImageHolder(inflater.inflate(R.layout.item_image, parent, false), mCallback);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == IMAGE) {
            GlideApp.with(mContext)
                    .load(mImageList.get(position - 1))
                    .into(((ImageHolder) holder).imageViewVendor);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? ADD_IMAGE : IMAGE;
    }

    @Override
    public int getItemCount() {
        return mImageList.size() + 1;
    }
}
