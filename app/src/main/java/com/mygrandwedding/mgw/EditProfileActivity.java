package com.mygrandwedding.mgw;

import android.app.DatePickerDialog;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Data;
import com.mygrandwedding.mgw.model.UpdateProfile;
import com.mygrandwedding.mgw.model.User;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;
import static com.mygrandwedding.mgw.util.DateParser.getDate;
import static com.mygrandwedding.mgw.util.DateParser.getDayOfMonth;
import static com.mygrandwedding.mgw.util.DateParser.getDob;
import static com.mygrandwedding.mgw.util.DateParser.getFormattedDate;
import static com.mygrandwedding.mgw.util.DateParser.getMonth;
import static com.mygrandwedding.mgw.util.DateParser.getYear;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutName, layoutGender, layoutDob;
    private TextInputEditText editTextName, editTextGender, editTextDob;
    private Button buttonSave;
    private FrameLayout layoutLoading;
    private MyPreference preference;
    private User user;
    private String dateOfBirth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        initObjects();
        initToolbar();
        initCallbacks();
        loadUserProfile();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextName) isValidName();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == editTextGender) {
            promptGenderSelection();
        } else if (view == editTextDob) {
            promptDatePickerDialog();
        } else if (view == buttonSave) {
            processEditProfile();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutName = findViewById(R.id.name);
        layoutGender = findViewById(R.id.gender);
        layoutDob = findViewById(R.id.dob);
        editTextName = findViewById(R.id.input_name);
        editTextGender = findViewById(R.id.input_gender);
        editTextDob = findViewById(R.id.input_dob);
        buttonSave = findViewById(R.id.btn_save);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        editTextName.setOnFocusChangeListener(this);
        editTextGender.setOnClickListener(this);
        editTextDob.setOnClickListener(this);
        buttonSave.setOnClickListener(this);
    }

    private void loadUserProfile() {
        LiveData<User> data = getAppDatabase(context).userDao().getUser(preference.getId());
        data.observe(this, new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                if (user != null) {
                    EditProfileActivity.this.user = user;
                    editTextName.setText(user.getFirstName());
                    editTextName.setSelection(editTextName.getText().toString().length());
                    editTextGender.setText(user.getUserProfile().getGender());
                    dateOfBirth = user.getUserProfile().getDob();
                    editTextDob.setText(getDob(user.getUserProfile().getDob()));
                }
            }
        });
    }

    private void promptGenderSelection() {
        final String[] options = {"Male", "Female"};
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select your Gender");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editTextGender.setText(options[which]);
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void promptDatePickerDialog() {
        int year = getYear(dateOfBirth);
        int month = getMonth(dateOfBirth);
        int dayOfMonth = getDayOfMonth(dateOfBirth);
        openDatePickerDialog(year, month, dayOfMonth);
    }

    private void openDatePickerDialog(int year, int month, int dayOfMonth) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                dateOfBirth = getDate(year, month, dayOfMonth);
                editTextDob.setText(getFormattedDate(year, month, dayOfMonth));
            }
        }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void processEditProfile() {
        String name = editTextName.getText().toString().trim();
        String gender = editTextGender.getText().toString().trim();
        if (isValidName(name) && isValidGender() && isValidDob()) {
            setLoading(true);
            updateProfile(new UpdateProfile(name, gender, dateOfBirth));
        }
    }

    private void isValidName() {
        isValidName(editTextName.getText().toString().trim());
    }

    private boolean isValidName(String name) {
        if (name.isEmpty()) {
            layoutName.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutName.getHint()));
            return false;
        }
        layoutName.setErrorEnabled(false);
        return true;
    }

    private boolean isValidGender() {
        return isValidGender(editTextGender.getText().toString().trim());
    }

    private boolean isValidGender(String gender) {
        if (gender.isEmpty()) {
            Toast.makeText(context, String.format(Locale.getDefault(), getString(R.string.error_empty),
                    layoutGender.getHint()), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean isValidDob() {
        return isValidDob(editTextDob.getText().toString().trim());
    }

    private boolean isValidDob(String dob) {
        if (dob.isEmpty()) {
            Toast.makeText(context, String.format(Locale.getDefault(), getString(R.string.error_empty),
                    layoutDob.getHint()), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void updateProfile(final UpdateProfile updateProfile) {
        Call<Data> call = getApiService().updateProfile(preference.getUserToken(), updateProfile);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                setLoading(false);
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    Toast.makeText(context, dataResponse.getData(), Toast.LENGTH_SHORT).show();
                    user.setFirstName(updateProfile.getFirstName());
                    user.getUserProfile().setGender(updateProfile.getGender());
                    user.getUserProfile().setDob(updateProfile.getDob());
                    new UpdateUserTask(user).execute();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonSave.setVisibility(loading ? View.GONE : View.VISIBLE);
    }

    private class UpdateUserTask extends AsyncTask<Void, Void, Void> {

        private User user;

        UpdateUserTask(User user) {
            this.user = user;
        }

        @Override
        protected Void doInBackground(Void... params) {
            getAppDatabase(context).userDao().updateUser(user);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            finish();
        }
    }
}
