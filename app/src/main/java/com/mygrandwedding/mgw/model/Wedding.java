package com.mygrandwedding.mgw.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static com.mygrandwedding.mgw.util.DateParser.getFormattedDate;

/**
 * Created by gladwinbobby on 02/10/17
 */

@Entity
public class Wedding {

    @PrimaryKey
    @SerializedName("id")
    private int id;
    @Ignore
    private boolean selected;
    @SerializedName("groom")
    private Integer groom;
    @SerializedName("bride")
    private Integer bride;
    @SerializedName("title")
    private String title;
    @SerializedName("location")
    private String location;
    @SerializedName("date")
    private String date;
    @SerializedName("invitation_code")
    private String invitationCode;
    @SerializedName("collaborator_code")
    private String collaboratorCode;
    @SerializedName("role")
    private String role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Integer getGroom() {
        return groom;
    }

    public void setGroom(Integer groom) {
        this.groom = groom;
    }

    public Integer getBride() {
        return bride;
    }

    public void setBride(Integer bride) {
        this.bride = bride;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getCollaboratorCode() {
        return collaboratorCode;
    }

    public void setCollaboratorCode(String collaboratorCode) {
        this.collaboratorCode = collaboratorCode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return getFormattedDate(date) + " in " + location;
    }

    public boolean isBride(int userId) {
        return bride != null && userId == bride;
    }
}
