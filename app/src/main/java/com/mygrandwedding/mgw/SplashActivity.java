package com.mygrandwedding.mgw;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.mygrandwedding.mgw.app.MyPreference;

import static com.mygrandwedding.mgw.app.Constant.EXTRA_CODE;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TYPE;
import static com.mygrandwedding.mgw.app.Constant.ROLE_USER;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final MyPreference preference = new MyPreference(this);
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        if (pendingDynamicLinkData != null) {
                            Uri deepLink = pendingDynamicLinkData.getLink();
                            try {
                                if (deepLink.getQueryParameter(EXTRA_ID) != null) {
                                    int vendorId = Integer.parseInt(deepLink.getQueryParameter(EXTRA_ID));
                                    preference.setVendorId(vendorId);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                if (deepLink.getQueryParameter(EXTRA_CODE) != null) {
                                    String code = deepLink.getQueryParameter(EXTRA_CODE);
                                    preference.setCode(code);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                if (deepLink.getQueryParameter(EXTRA_TYPE) != null) {
                                    int type = Integer.parseInt(deepLink.getQueryParameter(EXTRA_TYPE));
                                    preference.setType(type);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
        if (preference.isFirstTime()) {
            launch(this, IntroActivity.class);
        } else if (preference.getToken() == null && !preference.isSkip()) {
            Bundle bundle = new Bundle();
            bundle.putInt(EXTRA_ID, 3);
            launchWithBundle(this, IntroActivity.class, bundle);
        } else if (!preference.isVerified()) {
            launch(this, AdditionalDetailActivity.class);
        } else {
            launch(this, preference.getRole() == ROLE_USER ? MainActivity.class : VendorMainActivity.class);
        }
    }
}
