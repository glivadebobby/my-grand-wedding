package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.WeddingMenuCallback;

/**
 * Created by gladwinbobby on 03/10/17
 */

public class WeddingMenuHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public View selected;
    public TextView textViewTitle, textViewRole;
    private WeddingMenuCallback callback;

    public WeddingMenuHolder(View itemView, WeddingMenuCallback callback) {
        super(itemView);
        selected = itemView.findViewById(R.id.selected);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        textViewRole = itemView.findViewById(R.id.txt_role);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onWeddingClick(getLayoutPosition());
        }
    }
}
