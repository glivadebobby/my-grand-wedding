package com.mygrandwedding.mgw.app;

import android.support.multidex.MultiDexApplication;

import com.mygrandwedding.mgw.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by gladwinbobby on 03/09/17
 */

public class AppController extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/SourceSansPro-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }
}
