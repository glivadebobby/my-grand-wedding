package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 18/09/17
 */

public interface IdeaCallback {
    void onIdeaClick(int position);
}
