package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.URLUtil;

import com.mygrandwedding.mgw.adapter.MyPagerAdapter;
import com.mygrandwedding.mgw.fragment.ImageFragment;
import com.mygrandwedding.mgw.model.Image;
import com.mygrandwedding.mgw.task.ShareImageTask;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_PHOTOS;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_POSITION;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;

public class PhotoDetailActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private ViewPager pagerPhotos;
    private ArrayList<Image> images;
    private List<Fragment> fragmentList;
    private MyPagerAdapter pagerAdapter;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_detail);
        initObjects();
        initToolbar();
        initViewPager();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_share:
                if (images != null) {
                    String image = images.get(pagerPhotos.getCurrentItem()).getImage();
                    new ShareImageTask(context, URLUtil.guessFileName(image, null, "image/*"))
                            .execute(image);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        pagerPhotos = findViewById(R.id.photos);

        context = this;
        fragmentList = new ArrayList<>();
        images = new ArrayList<>();
        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), fragmentList);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            int position = bundle.getInt(EXTRA_POSITION);
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(bundle.getString(EXTRA_TITLE));
            images = bundle.getParcelableArrayList(EXTRA_PHOTOS);
            if (images != null) {
                for (Image image : images) {
                    fragmentList.add(ImageFragment.newInstance(image.getImage()));
                }
            }
            pagerAdapter.notifyDataSetChanged();
            if (fragmentList.size() - 1 >= position) pagerPhotos.setCurrentItem(position);
        }
    }

    private void initViewPager() {
        pagerPhotos.setAdapter(pagerAdapter);
    }
}
