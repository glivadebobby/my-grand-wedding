package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.BlogCallback;

/**
 * Created by gladwinbobby on 18/09/17
 */

public class BlogHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewBlog;
    public TextView textViewTitle;
    private BlogCallback callback;

    public BlogHolder(View itemView, BlogCallback callback) {
        super(itemView);
        imageViewBlog = itemView.findViewById(R.id.img_blog);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onBlogClick(getLayoutPosition());
        }
    }
}
