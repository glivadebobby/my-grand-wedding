package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.LoginActivity;
import com.mygrandwedding.mgw.MainActivity;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.SignUpActivity;
import com.mygrandwedding.mgw.VendorLoginActivity;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.app.MyPreference;

import static com.mygrandwedding.mgw.app.Constant.ROLE_USER;
import static com.mygrandwedding.mgw.app.Constant.ROLE_VENDOR;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private View rootView;
    private ImageView imageView;
    private Button buttonSignUp, buttonLogin;
    private TextView textViewVendorLogin, textViewSkipNow;
    private MyPreference preference;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        initObjects();
        initCallbacks();
        loadImage();
        return rootView;
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSignUp) {
            preference.setFirstTime();
            preference.setRole(ROLE_USER);
            launch(context, SignUpActivity.class);
        } else if (view == buttonLogin) {
            preference.setFirstTime();
            preference.setRole(ROLE_USER);
            launch(context, LoginActivity.class);
        } else if (view == textViewVendorLogin) {
            preference.setFirstTime();
            preference.setRole(ROLE_VENDOR);
            launch(context, VendorLoginActivity.class);
        } else if (view == textViewSkipNow) {
            preference.setSkip(true);
            preference.setRole(ROLE_USER);
            launchClearStack(context, MainActivity.class);
        }
    }

    private void initObjects() {
        imageView = rootView.findViewById(R.id.img_intro);
        buttonSignUp = rootView.findViewById(R.id.btn_sign_up);
        buttonLogin = rootView.findViewById(R.id.btn_login);
        textViewVendorLogin = rootView.findViewById(R.id.txt_vendor_login);
        textViewSkipNow = rootView.findViewById(R.id.txt_skip_now);

        context = getActivity();
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        buttonSignUp.setOnClickListener(this);
        buttonLogin.setOnClickListener(this);
        textViewVendorLogin.setOnClickListener(this);
        textViewSkipNow.setOnClickListener(this);
    }

    private void loadImage() {
        GlideApp.with(context)
                .load(R.drawable.intro4)
                .into(imageView);
    }
}
