package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.CityCallback;

/**
 * Created by gladwinbobby on 30/09/17
 */

public class CityHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView textViewCity;
    private CityCallback callback;

    public CityHolder(View itemView, CityCallback callback) {
        super(itemView);
        textViewCity = itemView.findViewById(R.id.txt_city);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onCityClick(getLayoutPosition());
        }
    }
}
