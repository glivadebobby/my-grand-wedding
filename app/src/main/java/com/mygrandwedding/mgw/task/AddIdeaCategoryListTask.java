package com.mygrandwedding.mgw.task;

import android.content.Context;
import android.os.AsyncTask;

import com.mygrandwedding.mgw.model.IdeaCategory;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 14/10/17
 */

public class AddIdeaCategoryListTask extends AsyncTask<Void, Void, Void> {

    private Context context;
    private List<IdeaCategory> ideaCategoryList;

    public AddIdeaCategoryListTask(Context context, List<IdeaCategory> ideaCategoryList) {
        this.context = context;
        this.ideaCategoryList = ideaCategoryList;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        getAppDatabase(context).deleteDao().deleteIdeaCategory();
        getAppDatabase(context).generalDao().insertIdeaCategoryList(ideaCategoryList);
        return null;
    }
}
