package com.mygrandwedding.mgw.helper;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

/**
 * Created by gladwinbobby on 26/09/17
 */

public class PrefixTextInputEditText extends TextInputEditText {

    private String mPrefix = "+91";
    private Rect mPrefixRect = new Rect();

    public PrefixTextInputEditText(Context context) {
        super(context);
    }

    public PrefixTextInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PrefixTextInputEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        getPaint().getTextBounds(mPrefix, 0, mPrefix.length(), mPrefixRect);
        mPrefixRect.right += getPaint().measureText(" ");
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawText(mPrefix, super.getCompoundPaddingLeft(), getBaseline(), getPaint());
    }

    @Override
    public int getCompoundPaddingLeft() {
        return super.getCompoundPaddingLeft() + mPrefixRect.width();
    }
}
