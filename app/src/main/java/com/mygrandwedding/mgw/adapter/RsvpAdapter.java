package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.RsvpCallback;
import com.mygrandwedding.mgw.holder.RsvpHolder;
import com.mygrandwedding.mgw.model.Rsvp;

import java.util.List;
import java.util.Locale;

/**
 * Created by gladwinbobby on 03/12/17
 */

public class RsvpAdapter extends RecyclerView.Adapter<RsvpHolder> {

    private Context context;
    private List<Rsvp> rsvpList;
    private RsvpCallback callback;

    public RsvpAdapter(Context context, List<Rsvp> rsvpList, RsvpCallback callback) {
        this.context = context;
        this.rsvpList = rsvpList;
        this.callback = callback;
    }

    @Override
    public RsvpHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RsvpHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rsvp, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(RsvpHolder holder, int position) {
        Rsvp rsvp = rsvpList.get(position);
        GlideApp.with(context)
                .load(rsvp.getUser().getUserProfile().getDp())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewProfile);
        holder.textViewName.setText(rsvp.getUser().getFirstName());
        holder.textViewMessage.setVisibility(rsvp.isExpand() ? View.VISIBLE : View.GONE);
        holder.textViewMessage.setText(rsvp.getFormattedMessage());
        holder.textViewCount.setVisibility(rsvp.getCount() == 0 ? View.GONE : View.VISIBLE);
        holder.textViewCount.setText(String.format(Locale.getDefault(), context.getString(R.string.format_count), rsvp.getCount()));
    }

    @Override
    public int getItemCount() {
        return rsvpList.size();
    }
}
