package com.mygrandwedding.mgw;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mygrandwedding.mgw.adapter.MyPagerAdapter;
import com.mygrandwedding.mgw.fragment.MyBudgetFragment;
import com.mygrandwedding.mgw.fragment.SuggestedVendorFragment;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BudgetActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private Context context;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager pagerBudget;
    private List<Fragment> fragmentList;
    private MyPagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);
        initObjects();
        initToolbar();
        initCallbacks();
        initViewPager();
        initTabs();
        setTabsFont();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        pagerBudget.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tab);
        pagerBudget = findViewById(R.id.budget);

        context = this;
        fragmentList = new ArrayList<>();
        pagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), fragmentList);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        tabLayout.addOnTabSelectedListener(this);
    }

    private void initViewPager() {
        pagerBudget.setAdapter(pagerAdapter);
        pagerBudget.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void initTabs() {
        tabLayout.addTab(tabLayout.newTab().setText("My Budget"));
        fragmentList.add(new MyBudgetFragment());
        tabLayout.addTab(tabLayout.newTab().setText("Suggested Vendors"));
        fragmentList.add(new SuggestedVendorFragment());
        pagerAdapter.notifyDataSetChanged();
        pagerBudget.setOffscreenPageLimit(fragmentList.size());
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(context.getAssets(),
                            "fonts/SourceSansPro-SemiBold.ttf"), Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(getResources().getDimension(R.dimen.primary_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }
}
