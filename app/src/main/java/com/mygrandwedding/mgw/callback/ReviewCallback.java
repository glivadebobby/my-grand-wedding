package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 08/09/17
 */

public interface ReviewCallback {
    void onEditClick(int position);
}
