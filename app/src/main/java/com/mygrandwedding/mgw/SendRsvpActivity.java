package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Rsvp;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;

public class SendRsvpActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener, SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RelativeLayout layoutRsvp;
    private TextView textViewWedding, textViewAttend, textViewDecline, textViewAttenders, textViewMessage;
    private TextInputLayout layoutAttenders, layoutMessage;
    private TextInputEditText editTextAttenders, editTextMessage;
    private Button buttonSave;
    private FrameLayout layoutLoading;
    private MyPreference preference;
    private int id;
    private boolean attending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_rsvp);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        getRsvp();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getRsvp();
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextAttenders) isValidAttenders();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == textViewAttend) {
            attending = true;
            setAttending();
        } else if (view == textViewDecline) {
            attending = false;
            setAttending();
        } else if (view == buttonSave) {
            processRsvp();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        layoutRsvp = findViewById(R.id.rsvp);
        textViewWedding = findViewById(R.id.txt_wedding);
        textViewAttend = findViewById(R.id.txt_attend);
        textViewDecline = findViewById(R.id.txt_decline);
        textViewAttenders = findViewById(R.id.txt_attenders);
        textViewMessage = findViewById(R.id.txt_message);
        layoutAttenders = findViewById(R.id.attenders);
        layoutMessage = findViewById(R.id.message);
        editTextAttenders = findViewById(R.id.input_attenders);
        editTextMessage = findViewById(R.id.input_message);
        buttonSave = findViewById(R.id.btn_save);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
        textViewAttend.setOnClickListener(this);
        textViewDecline.setOnClickListener(this);
        editTextAttenders.setOnFocusChangeListener(this);
        editTextMessage.setOnFocusChangeListener(this);
        buttonSave.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            textViewWedding.setText(bundle.getString(EXTRA_TITLE));
        }
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void setAttending() {
        if (attending) {
            textViewAttend.setTextColor(ContextCompat.getColor(context, R.color.primary));
            textViewDecline.setTextColor(ContextCompat.getColor(context, R.color.secondary_txt));
            textViewAttend.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_attending, 0, 0);
            textViewDecline.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_decline, 0, 0);
            textViewAttenders.setVisibility(View.VISIBLE);
            layoutAttenders.setVisibility(View.VISIBLE);
            editTextMessage.setHint(getString(R.string.hint_rsvp_attend));
        } else {
            textViewAttend.setTextColor(ContextCompat.getColor(context, R.color.secondary_txt));
            textViewDecline.setTextColor(ContextCompat.getColor(context, R.color.primary));
            textViewAttend.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_attend, 0, 0);
            textViewDecline.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_declining, 0, 0);
            textViewAttenders.setVisibility(View.GONE);
            layoutAttenders.setVisibility(View.GONE);
            editTextAttenders.getText().clear();
            editTextMessage.setHint(getString(R.string.hint_rsvp_decline));
        }
        textViewMessage.setVisibility(View.VISIBLE);
        layoutMessage.setVisibility(View.VISIBLE);
        buttonSave.setVisibility(View.VISIBLE);
    }

    private void processRsvp() {
        String attenders = editTextAttenders.getText().toString().trim();
        String message = editTextMessage.getText().toString().trim();
        if (isValidAttenders(attenders)) {
            setLoading(true);
            int count = 0;
            if (!attenders.isEmpty())
                try {
                    count = Integer.parseInt(attenders);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            createRsvp(new Rsvp(id, count, attending, message));
        }
    }

    private void isValidAttenders() {
        isValidAttenders(editTextAttenders.getText().toString().trim());
    }

    private boolean isValidAttenders(String attenders) {
        if (attenders.isEmpty() && layoutAttenders.getVisibility() == View.VISIBLE) {
            layoutAttenders.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), getString(R.string.txt_wedding_attenders)));
            return false;
        }
        layoutAttenders.setErrorEnabled(false);
        return true;
    }

    private void getRsvp() {
        Call<Rsvp> call = getApiService().getRsvp(preference.getUserToken(), id);
        call.enqueue(new Callback<Rsvp>() {
            @Override
            public void onResponse(@NonNull Call<Rsvp> call, @NonNull Response<Rsvp> response) {
                refreshLayout.setRefreshing(false);
                layoutRsvp.setVisibility(View.VISIBLE);
                Rsvp rsvpResponse = response.body();
                if (response.isSuccessful() && rsvpResponse != null) {
                    attending = rsvpResponse.isComing();
                    setAttending();
                    editTextAttenders.setText(String.valueOf(rsvpResponse.getCount()));
                    editTextMessage.setText(rsvpResponse.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Rsvp> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void createRsvp(Rsvp rsvp) {
        Call<Void> call = getApiService().createRsvp(preference.getUserToken(), rsvp);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "RSVP sent to the bride/groom", Toast.LENGTH_SHORT).show();
                    finish();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonSave.setVisibility(loading ? View.GONE : View.VISIBLE);
    }
}
