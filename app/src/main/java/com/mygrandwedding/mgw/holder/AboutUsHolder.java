package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;

/**
 * Created by gladwinbobby on 11/10/17
 */

public class AboutUsHolder extends RecyclerView.ViewHolder {

    public TextView textViewTitle, textViewDescription;

    public AboutUsHolder(View itemView) {
        super(itemView);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        textViewDescription = itemView.findViewById(R.id.txt_description);
    }
}
