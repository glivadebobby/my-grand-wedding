package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

import static com.mygrandwedding.mgw.util.DateParser.getCreatedDate;

/**
 * Created by gladwinbobby on 08/09/17
 */

public class Review {

    @SerializedName("id")
    private int id;
    @SerializedName("vendor")
    private int vendor;
    @SerializedName("rating")
    private Float rating;
    @SerializedName("review")
    private String review;
    @SerializedName("created_on")
    private String createdOn;
    @SerializedName("user")
    private User user;

    public Review(int vendor, Float rating, String review) {
        this.vendor = vendor;
        this.rating = rating;
        this.review = review;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVendor() {
        return vendor;
    }

    public void setVendor(int vendor) {
        this.vendor = vendor;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getCreatedOn() {
        return getCreatedDate(createdOn);
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
