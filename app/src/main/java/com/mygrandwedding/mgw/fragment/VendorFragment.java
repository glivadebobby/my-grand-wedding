package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.VendorDetailActivity;
import com.mygrandwedding.mgw.adapter.VendorAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.VendorCallback;
import com.mygrandwedding.mgw.model.Vendor;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.model.VendorData;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.MGW_URL;
import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_IMAGE;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

/**
 * A simple {@link Fragment} subclass.
 */
public class VendorFragment extends Fragment implements VendorCallback, SwipeRefreshLayout.OnRefreshListener, Runnable {

    private static final String ARG_CATEGORY_ID = "category_id";
    private static final String ARG_SUBCATEGORY_ID = "subcategory_id";
    private static final String ARG_VENDOR = "vendor";
    private Context context;
    private View rootView;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewVendors;
    private LinearLayout layoutEmpty;
    private TextView textViewEmpty;
    private List<Vendor> vendorList;
    private LinearLayoutManager layoutManager;
    private VendorAdapter vendorAdapter;
    private MyPreference preference;
    private VendorCategory category;
    private int categoryId, subcategoryId;
    private int count = 0;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private String url;

    public VendorFragment() {
        // Required empty public constructor
    }

    public static VendorFragment newInstance(int categoryId, int subcategoryId, VendorCategory category) {
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_CATEGORY_ID, categoryId);
        bundle.putInt(ARG_SUBCATEGORY_ID, subcategoryId);
        bundle.putParcelable(ARG_VENDOR, category);
        VendorFragment fragment = new VendorFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_vendor, container, false);
        initObjects();
        initCallbacks();
        initEmpty();
        processBundle();
        initRecyclerView();
        initRefresh();
        return rootView;
    }

    @Override
    public void onVendorClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, vendorList.get(position).getUser());
        bundle.putString(EXTRA_IMAGE, vendorList.get(position).getImage());
        launchWithBundle(context, VendorDetailActivity.class, bundle);
    }

    @Override
    public void onRefresh() {
        resetList();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        resetList();
    }

    private void initObjects() {
        refreshLayout = rootView.findViewById(R.id.refresh);
        viewVendors = rootView.findViewById(R.id.vendors);
        layoutEmpty = rootView.findViewById(R.id.empty);
        textViewEmpty = rootView.findViewById(R.id.txt_empty);

        context = getActivity();
        vendorList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        vendorAdapter = new VendorAdapter(context, vendorList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initEmpty() {
        textViewEmpty.setText(getString(R.string.error_empty_vendor));
    }

    private void processBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryId = bundle.getInt(ARG_CATEGORY_ID);
            subcategoryId = bundle.getInt(ARG_SUBCATEGORY_ID);
            category = bundle.getParcelable(EXTRA_VENDOR);
        }
    }

    private void initRecyclerView() {
        viewVendors.setLayoutManager(layoutManager);
        viewVendors.setAdapter(vendorAdapter);
        viewVendors.setNestedScrollingEnabled(false);
        viewVendors.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                if (!isLoading && (url != null) && (totalItemCount <= (lastVisibleItem + visibleThreshold))) {
                    getVendors();
                }
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void resetList() {
        count = 0;
        setUrl();
        getVendors();
    }

    private void setUrl() {
        url = MGW_URL + "vendor/app/list/" + categoryId + "/";
        boolean isQuestion = true;
        if (subcategoryId > 0) {
            url += "?subcategory=" + subcategoryId;
            isQuestion = false;
        }
        if (preference.getCityId() > 0) {
            url += isQuestion ? "?" : "&";
            url += "location=" + preference.getCityId();
        }
        if (preference.getMinPrice() != 0 && category != null) {
            url += isQuestion ? "?" : "&";
            url += "min_price=" + preference.getMinPrice() * category.getFilterPrice();
        }
        if (preference.getMaxPrice() != 100 && category != null) {
            url += isQuestion ? "?" : "&";
            url += "max_price=" + preference.getMaxPrice() * category.getFilterPrice();
        }
        if (preference.getRating() > 0) {
            url += isQuestion ? "?" : "&";
            url += "rating=" + (int) preference.getRating();
        }
    }

    private void clearList() {
        if (count == 0) {
            vendorList.clear();
            vendorAdapter.notifyDataSetChanged();
        }
    }

    private void setLoading(boolean loading) {
        isLoading = loading;
    }

    private void isEmpty() {
        layoutEmpty.setVisibility(vendorList.isEmpty() ? View.VISIBLE : View.GONE);
    }

    private void getVendors() {
        setLoading(true);
        Call<VendorData> call = getApiService().getVendors(url);
        call.enqueue(new Callback<VendorData>() {
            @Override
            public void onResponse(@NonNull Call<VendorData> call,
                                   @NonNull Response<VendorData> response) {
                refreshLayout.setRefreshing(false);
                VendorData vendorDataResponse = response.body();
                if (response.isSuccessful() && vendorDataResponse != null) {
                    setLoading(false);
                    clearList();
                    count = vendorDataResponse.getCount();
                    url = vendorDataResponse.getNextUrl();
                    vendorList.addAll(vendorDataResponse.getVendorList());
                    vendorAdapter.notifyDataSetChanged();
                    isEmpty();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<VendorData> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }
}
