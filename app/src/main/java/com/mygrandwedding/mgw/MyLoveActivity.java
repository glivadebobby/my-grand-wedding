package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.mygrandwedding.mgw.adapter.MyLoveAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.LovedCallback;
import com.mygrandwedding.mgw.model.Love;
import com.mygrandwedding.mgw.model.Vendor;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_IMAGE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

public class MyLoveActivity extends AppCompatActivity implements LovedCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewVendors;
    private List<Vendor> vendorList;
    private LinearLayoutManager layoutManager;
    private MyLoveAdapter vendorAdapter;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_love);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onVendorClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, vendorList.get(position).getUser());
        bundle.putString(EXTRA_IMAGE, vendorList.get(position).getImage());
        launchWithBundle(context, VendorDetailActivity.class, bundle);
    }

    @Override
    public void onRemoveClick(int position) {
        removeLike(vendorList.get(position).getId());
    }

    @Override
    public void onRefresh() {
        getLoves();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getLoves();
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        refreshLayout = findViewById(R.id.refresh);
        viewVendors = findViewById(R.id.vendors);

        context = this;
        vendorList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        vendorAdapter = new MyLoveAdapter(context, vendorList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        viewVendors.setLayoutManager(layoutManager);
        viewVendors.setAdapter(vendorAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void getLoves() {
        Call<List<Vendor>> call = getApiService().getMyLove(preference.getUserToken(), preference.getWeddingId());
        call.enqueue(new Callback<List<Vendor>>() {
            @Override
            public void onResponse(@NonNull Call<List<Vendor>> call,
                                   @NonNull Response<List<Vendor>> response) {
                refreshLayout.setRefreshing(false);
                List<Vendor> vendorListResponse = response.body();
                if (response.isSuccessful() && vendorListResponse != null) {
                    vendorList.clear();
                    vendorList.addAll(vendorListResponse);
                    vendorAdapter.notifyDataSetChanged();
                    isEmpty();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<List<Vendor>> call, @NonNull Throwable t) {
                refreshLayout.setRefreshing(false);
                processFailure(context, t);
            }
        });
    }

    private void isEmpty() {
        if (vendorList.isEmpty()) {
            Toast.makeText(context, "No vendors has been loved", Toast.LENGTH_SHORT).show();
        }
    }

    private void removeLike(int id) {
        Call<Love> call = getApiService().love(preference.getUserToken(), new Love(id, preference.getWeddingId()));
        call.enqueue(new Callback<Love>() {
            @Override
            public void onResponse(@NonNull Call<Love> call, @NonNull Response<Love> response) {
                Love loveResponse = response.body();
                if (response.isSuccessful() && loveResponse != null) {
                    run();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Love> call, @NonNull Throwable t) {
                processFailure(context, t);
            }
        });
    }
}
