package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.CategoryCallback;
import com.mygrandwedding.mgw.holder.BlogCategoryHolder;
import com.mygrandwedding.mgw.model.BlogCategory;

import java.util.List;

/**
 * Created by gladwinbobby on 05/09/17
 */

public class BlogCategoryAdapter extends RecyclerView.Adapter<BlogCategoryHolder> {

    private Context context;
    private List<BlogCategory> blogCategoryList;
    private CategoryCallback callback;

    public BlogCategoryAdapter(Context context, List<BlogCategory> blogCategoryList, CategoryCallback callback) {
        this.context = context;
        this.blogCategoryList = blogCategoryList;
        this.callback = callback;
    }

    @Override
    public BlogCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BlogCategoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_blog_category, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(BlogCategoryHolder holder, int position) {
        BlogCategory vendorCategory = blogCategoryList.get(position);
        GlideApp.with(context)
                .load(vendorCategory.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewCategory);
        holder.textViewCategory.setText(vendorCategory.getCategory());
    }

    @Override
    public int getItemCount() {
        return blogCategoryList.size();
    }
}
