package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mygrandwedding.mgw.callback.ImageCallback;

/**
 * Created by gladwinbobby on 27/09/17
 */

public class AddImageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageCallback callback;

    public AddImageHolder(View itemView, ImageCallback callback) {
        super(itemView);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            callback.onAddClick();
        }
    }
}
