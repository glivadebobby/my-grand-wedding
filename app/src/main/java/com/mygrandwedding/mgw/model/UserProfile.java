package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 12/09/17
 */

public class UserProfile {

    @SerializedName("id")
    private int id;
    @SerializedName("role")
    private int role;
    @SerializedName("gender")
    private String gender;
    @SerializedName("dob")
    private String dob;
    @SerializedName("cover_photo")
    private String coverPic;
    @SerializedName("profile_pic")
    private String profilePic;
    @SerializedName("google_profile_pic")
    private String googleProfilePic;
    @SerializedName("facebook_profile_pic")
    private String facebookProfilePic;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCoverPic() {
        return coverPic;
    }

    public void setCoverPic(String coverPic) {
        this.coverPic = coverPic;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGoogleProfilePic() {
        return googleProfilePic;
    }

    public void setGoogleProfilePic(String googleProfilePic) {
        this.googleProfilePic = googleProfilePic;
    }

    public String getFacebookProfilePic() {
        return facebookProfilePic;
    }

    public void setFacebookProfilePic(String facebookProfilePic) {
        this.facebookProfilePic = facebookProfilePic;
    }

    public String getDp() {
        if (profilePic != null) return profilePic;
        else if (googleProfilePic != null) return googleProfilePic;
        else if (facebookProfilePic != null) return facebookProfilePic;
        else return null;
    }
}
