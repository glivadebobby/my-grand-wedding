package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;

/**
 * Created by gladwinbobby on 23/10/17
 */

public class UserWeddingHolder extends RecyclerView.ViewHolder {

    public LinearLayout layoutUserWedding;
    public ImageView imageViewProfile;
    public TextView textViewTitle, textViewDate;

    public UserWeddingHolder(View itemView) {
        super(itemView);
        layoutUserWedding = itemView.findViewById(R.id.user_wedding);
        imageViewProfile = itemView.findViewById(R.id.img_profile);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        textViewDate = itemView.findViewById(R.id.txt_date);
    }
}
