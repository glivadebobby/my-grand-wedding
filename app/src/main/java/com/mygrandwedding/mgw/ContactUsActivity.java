package com.mygrandwedding.mgw;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.ContactUs;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.MAP_LAT;
import static com.mygrandwedding.mgw.app.Constant.MAP_LNG;
import static com.mygrandwedding.mgw.app.Constant.MAP_ZOOM;

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener,
        View.OnFocusChangeListener, OnMapReadyCallback {

    private Context context;
    private Toolbar toolbar;
    private TextInputLayout layoutSubject, layoutMessage;
    private TextInputEditText editTextSubject, editTextMessage;
    private SupportMapFragment mMapFragment;
    private Button buttonSubmit;
    private FrameLayout layoutLoading;
    private MyPreference preference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        initObjects();
        initToolbar();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFocusChange(View view, boolean b) {
        if (!b) {
            if (view == editTextSubject) isValidSubject();
            if (view == editTextMessage) isValidMessage();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        googleMap.animateCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(MAP_LAT, MAP_LNG), MAP_ZOOM));
        googleMap.addMarker(new MarkerOptions()
                .title(getString(R.string.app_name))
                .position(new LatLng(MAP_LAT, MAP_LNG)))
                .showInfoWindow();
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmit) {
            processContactUs();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        layoutSubject = findViewById(R.id.subject);
        layoutMessage = findViewById(R.id.message);
        editTextSubject = findViewById(R.id.input_subject);
        editTextMessage = findViewById(R.id.input_message);
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        buttonSubmit = findViewById(R.id.btn_submit);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initCallbacks() {
        editTextSubject.setOnFocusChangeListener(this);
        editTextMessage.setOnFocusChangeListener(this);
        mMapFragment.getMapAsync(this);
        buttonSubmit.setOnClickListener(this);
    }

    private void processContactUs() {
        String subject = editTextSubject.getText().toString().trim();
        String message = editTextMessage.getText().toString().trim();
        if (isValidSubject(subject) && isValidMessage(message)) {
            setLoading(true);
            contactUs(new ContactUs(subject, message));
        }
    }

    private void isValidSubject() {
        isValidSubject(editTextSubject.getText().toString().trim());
    }

    private boolean isValidSubject(String requirement) {
        if (requirement.isEmpty()) {
            layoutSubject.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutSubject.getHint()));
            return false;
        }
        layoutSubject.setErrorEnabled(false);
        return true;
    }

    private void isValidMessage() {
        isValidMessage(editTextMessage.getText().toString().trim());
    }

    private boolean isValidMessage(String description) {
        if (description.isEmpty()) {
            layoutMessage.setError(String.format(Locale.getDefault(), getString(R.string.error_empty), layoutMessage.getHint()));
            return false;
        }
        layoutMessage.setErrorEnabled(false);
        return true;
    }

    private void contactUs(ContactUs contactUs) {
        Call<ContactUs> call = getApiService().contactUs(preference.getUserToken(), contactUs);
        call.enqueue(new Callback<ContactUs>() {
            @Override
            public void onResponse(@NonNull Call<ContactUs> call, @NonNull Response<ContactUs> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "We have received your query. Thanks for contacting us", Toast.LENGTH_SHORT).show();
                    finish();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<ContactUs> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonSubmit.setVisibility(loading ? View.GONE : View.VISIBLE);
    }
}
