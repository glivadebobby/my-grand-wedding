package com.mygrandwedding.mgw.api;

import android.content.Context;
import android.widget.Toast;

import com.mygrandwedding.mgw.R;

import static com.mygrandwedding.mgw.helper.ConnectivityStatus.isConnected;

/**
 * Created by gladwinbobby on 30/09/17
 */

public class FailureHandler {
    public static void processFailure(Context context, Throwable throwable) {
        if (isConnected(context))
            Toast.makeText(context, context.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(context, context.getString(R.string.error_internet), Toast.LENGTH_SHORT).show();
    }
}
