package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intrusoft.squint.DiagonalView;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.VendorCallback;

/**
 * Created by gladwinbobby on 23/10/17
 */

public class WallLovedHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewProfile, imageViewVendor;
    public TextView textViewTitle, textViewRole, textViewDate, textViewVendor, textViewLocation,
            textViewCost, textViewRating, textViewReview;
    public DiagonalView diagonalView;
    public ImageView imageViewType;
    private VendorCallback callback;

    public WallLovedHolder(View itemView, VendorCallback callback) {
        super(itemView);
        imageViewProfile = itemView.findViewById(R.id.img_profile);
        imageViewVendor = itemView.findViewById(R.id.img_vendor);
        textViewTitle = itemView.findViewById(R.id.txt_title);
        textViewRole = itemView.findViewById(R.id.txt_role);
        textViewDate = itemView.findViewById(R.id.txt_date);
        textViewVendor = itemView.findViewById(R.id.txt_vendor);
        textViewLocation = itemView.findViewById(R.id.txt_location);
        textViewCost = itemView.findViewById(R.id.txt_cost);
        textViewRating = itemView.findViewById(R.id.txt_rating);
        textViewReview = itemView.findViewById(R.id.txt_review);
        diagonalView = itemView.findViewById(R.id.diagonal);
        imageViewType = itemView.findViewById(R.id.img_type);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) callback.onVendorClick(getLayoutPosition());
    }
}
