package com.mygrandwedding.mgw.fragment;


import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.mygrandwedding.mgw.AssociatedWeddingActivity;
import com.mygrandwedding.mgw.BudgetActivity;
import com.mygrandwedding.mgw.ChecklistActivity;
import com.mygrandwedding.mgw.GetQuoteActivity;
import com.mygrandwedding.mgw.JoinWeddingActivity;
import com.mygrandwedding.mgw.MyLoveActivity;
import com.mygrandwedding.mgw.MyShortlistActivity;
import com.mygrandwedding.mgw.MyWeddingActivity;
import com.mygrandwedding.mgw.ProfileActivity;
import com.mygrandwedding.mgw.PromotionActivity;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.RsvpActivity;
import com.mygrandwedding.mgw.SendRsvpActivity;
import com.mygrandwedding.mgw.WallActivity;
import com.mygrandwedding.mgw.app.MyActivity;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.NotifyRefresh;
import com.mygrandwedding.mgw.model.Wedding;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import cn.iwgang.countdownview.CountdownView;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_TITLE;
import static com.mygrandwedding.mgw.app.MyActivity.launch;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;
import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;
import static com.mygrandwedding.mgw.util.DateParser.getWeddingDuration;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyWeddingFragment extends Fragment implements View.OnClickListener {

    private Context context;
    private View rootView;
    private NestedScrollView scrollView;
    private TextView textViewWedding, textViewWeddingData, textViewEditWedding, textViewInviteBride;
    private CountdownView countdownView;
    private FrameLayout layoutLoves, layoutShortlists, layoutWall, layoutChecklist, layoutRsvp,
            layoutBudget, layoutMyProfile, layoutJoinWedding, layoutMyWedding, layoutAssociatedWedding,
            layoutInviteBride, layoutInviteCollaborator, layoutSpecialRate, layoutPromotions;
    private LinearLayout layoutRsvpBudget, layoutInvitation;
    private CardView cardViewBudget;
    private KonfettiView konfettiView;
    private MyPreference preference;
    private Wedding wedding;

    public MyWeddingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_wedding, container, false);
        initObjects();
        initCallbacks();
        loadSelectedWedding();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        if (view == textViewEditWedding) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else
                launch(context, MyWeddingActivity.class);
        } else if (view == layoutLoves) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else if (preference.getWeddingId() == -1)
                Toast.makeText(context, getString(R.string.txt_prompt_wedding), Toast.LENGTH_SHORT).show();
            else
                launch(context, MyLoveActivity.class);
        } else if (view == layoutShortlists) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else if (preference.getWeddingId() == -1)
                Toast.makeText(context, getString(R.string.txt_prompt_wedding), Toast.LENGTH_SHORT).show();
            else
                launch(context, MyShortlistActivity.class);
        } else if (view == layoutWall) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else if (preference.getWeddingId() == -1)
                Toast.makeText(context, getString(R.string.txt_prompt_wedding), Toast.LENGTH_SHORT).show();
            else
                launch(context, WallActivity.class);
        } else if (view == layoutChecklist) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else if (preference.getWeddingId() == -1)
                Toast.makeText(context, getString(R.string.txt_prompt_wedding), Toast.LENGTH_SHORT).show();
            else
                launch(context, ChecklistActivity.class);
        } else if (view == layoutRsvp) {
            Bundle bundle = new Bundle();
            bundle.putInt(EXTRA_ID, wedding.getId());
            bundle.putString(EXTRA_TITLE, wedding.getTitle());
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else if (preference.isCollaborator())
                launchWithBundle(context, SendRsvpActivity.class, bundle);
            else launchWithBundle(context, RsvpActivity.class, bundle);
        } else if (view == layoutBudget) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else
                launch(context, BudgetActivity.class);
        } else if (view == layoutMyProfile) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else
                launch(context, ProfileActivity.class);
        } else if (view == layoutJoinWedding) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else
                launch(context, JoinWeddingActivity.class);
        } else if (view == layoutMyWedding) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else launch(context, MyWeddingActivity.class);
        } else if (view == layoutAssociatedWedding) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else launch(context, AssociatedWeddingActivity.class);
        } else if (view == layoutInviteBride) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else if (wedding != null)
                inviteBride();
            else layoutInvitation.setVisibility(View.GONE);
        } else if (view == layoutInviteCollaborator) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else if (wedding != null)
                inviteCollaborator();
            else layoutInvitation.setVisibility(View.GONE);
        } else if (view == layoutSpecialRate) {
            launch(context, GetQuoteActivity.class);
        } else if (view == layoutPromotions) {
            if (preference.getToken() == null)
                new PromptLoginDialogFragment().show(getChildFragmentManager(), "prompt-login");
            else
                launch(context, PromotionActivity.class);
        }
    }

    @Subscribe
    public void refresh(NotifyRefresh notifyRefresh) {
        loadSelectedWedding();
    }

    private void initObjects() {
        scrollView = rootView.findViewById(R.id.wedding);
        textViewWedding = rootView.findViewById(R.id.txt_wedding);
        textViewWeddingData = rootView.findViewById(R.id.txt_wedding_data);
        textViewEditWedding = rootView.findViewById(R.id.txt_edit_wedding);
        textViewInviteBride = rootView.findViewById(R.id.txt_invite_bride);
        countdownView = rootView.findViewById(R.id.countdown);
        layoutLoves = rootView.findViewById(R.id.loves);
        layoutShortlists = rootView.findViewById(R.id.shortlists);
        layoutWall = rootView.findViewById(R.id.wall);
        layoutChecklist = rootView.findViewById(R.id.checklist);
        layoutRsvp = rootView.findViewById(R.id.rsvp);
        layoutBudget = rootView.findViewById(R.id.budget);
        layoutMyProfile = rootView.findViewById(R.id.my_profile);
        layoutJoinWedding = rootView.findViewById(R.id.join_wedding);
        layoutMyWedding = rootView.findViewById(R.id.my_wedding);
        layoutAssociatedWedding = rootView.findViewById(R.id.associated_wedding);
        layoutInviteBride = rootView.findViewById(R.id.invite_bride);
        layoutInviteCollaborator = rootView.findViewById(R.id.invite_collaborator);
        layoutSpecialRate = rootView.findViewById(R.id.special_rate);
        layoutPromotions = rootView.findViewById(R.id.promotions);
        layoutRsvpBudget = rootView.findViewById(R.id.rsvp_budget);
        layoutInvitation = rootView.findViewById(R.id.invitation);
        cardViewBudget = rootView.findViewById(R.id.budget_card);
        konfettiView = rootView.findViewById(R.id.konfetti);

        context = getActivity();
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        textViewEditWedding.setOnClickListener(this);
        layoutLoves.setOnClickListener(this);
        layoutShortlists.setOnClickListener(this);
        layoutWall.setOnClickListener(this);
        layoutChecklist.setOnClickListener(this);
        layoutRsvp.setOnClickListener(this);
        layoutBudget.setOnClickListener(this);
        layoutMyProfile.setOnClickListener(this);
        layoutJoinWedding.setOnClickListener(this);
        layoutMyWedding.setOnClickListener(this);
        layoutAssociatedWedding.setOnClickListener(this);
        layoutInviteBride.setOnClickListener(this);
        layoutInviteCollaborator.setOnClickListener(this);
        layoutSpecialRate.setOnClickListener(this);
        layoutPromotions.setOnClickListener(this);
    }

    private void loadSelectedWedding() {
        LiveData<Wedding> data = getAppDatabase(context).weddingDao().getSelectedWedding(preference.getWeddingId());
        data.observe(this, new Observer<Wedding>() {
            @Override
            public void onChanged(@Nullable Wedding wedding) {
                MyWeddingFragment.this.wedding = wedding;
                processWedding();
                showCaseTitle();
            }
        });
    }

    private void processWedding() {
        if (wedding != null) {
            textViewEditWedding.setVisibility(preference.isCollaborator() ? View.INVISIBLE : View.VISIBLE);
            textViewWedding.setText(wedding.getTitle());
            long weddingDuration = getWeddingDuration(wedding.getDate());
            if (weddingDuration < 0) {
                countdownView.setVisibility(View.GONE);
                textViewWeddingData.setVisibility(View.VISIBLE);
                textViewWeddingData.setText(getString(R.string.txt_wedding_past));
            } else if (weddingDuration == 0) {
                countdownView.setVisibility(View.GONE);
                textViewWeddingData.setVisibility(View.VISIBLE);
                textViewWeddingData.setText(getString(R.string.txt_wedding_day));
                displayKonfetti();
            } else {
                countdownView.setVisibility(View.VISIBLE);
                textViewWeddingData.setVisibility(View.GONE);
                countdownView.start(weddingDuration);
            }
            layoutRsvpBudget.setVisibility(View.VISIBLE);
            textViewInviteBride.setText(wedding.isBride(preference.getId())
                    ? getString(R.string.txt_invite_groom)
                    : getString(R.string.txt_invite_bride));
            textViewInviteBride.setCompoundDrawablesWithIntrinsicBounds(0,
                    wedding.isBride(preference.getId()) ? R.drawable.ic_invite_groom
                            : R.drawable.ic_invite_bride, 0, 0);
        } else {
            countdownView.setVisibility(View.GONE);
            textViewWeddingData.setVisibility(View.VISIBLE);
            textViewEditWedding.setVisibility(View.INVISIBLE);
            layoutRsvpBudget.setVisibility(View.GONE);
            textViewWedding.setText(null);
            countdownView.stop();
            textViewWeddingData.setText(getString(R.string.txt_prompt_wedding));
        }
        layoutInvitation.setVisibility(preference.getWeddingId() == -1 || preference.isCollaborator() ? View.GONE : View.VISIBLE);
        cardViewBudget.setVisibility(preference.isCollaborator() ? View.GONE : View.VISIBLE);
    }

    private void displayKonfetti() {
        konfettiView.build()
                .addColors(Color.YELLOW, Color.GREEN, Color.MAGENTA)
                .setDirection(0.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(2000L)
                .addShapes(Shape.RECT, Shape.CIRCLE)
                .addSizes(new Size(12, 5f))
                .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
                .stream(300, 5000L);
    }

    private void inviteBride() {
        if (getActivity() != null)
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse("https://mygrandwedding.com/invite.php" + "?code=" + wedding.getInvitationCode() + "&bride=" + wedding.isBride(preference.getId())))
                    .setDynamicLinkDomain("dr779.app.goo.gl")
                    .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                    .setIosParameters(new DynamicLink.IosParameters.Builder("com.mygrandwedding.mgw").build())
                    .buildShortDynamicLink()
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<ShortDynamicLink>() {
                        @Override
                        public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                            if (isAdded()) {
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    MyActivity.inviteBride(context, wedding.isBride(preference.getId()), wedding.getInvitationCode(), shortLink.toString());
                                } else {
                                    Toast.makeText(context, "Unable to invite! Try again later!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
    }

    private void inviteCollaborator() {
        if (getActivity() != null)
            FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse("https://mygrandwedding.com/invite.php" + "?code=" + wedding.getCollaboratorCode()))
                    .setDynamicLinkDomain("dr779.app.goo.gl")
                    .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                    .setIosParameters(new DynamicLink.IosParameters.Builder("com.mygrandwedding.mgw").build())
                    .buildShortDynamicLink()
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<ShortDynamicLink>() {
                        @Override
                        public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                            if (isAdded()) {
                                if (task.isSuccessful()) {
                                    Uri shortLink = task.getResult().getShortLink();
                                    MyActivity.inviteCollaborator(context, wedding.getCollaboratorCode(), shortLink.toString());
                                } else {
                                    Toast.makeText(context, "Unable to invite! Try again later!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
    }

    private void showCaseTitle() {
        if (getActivity() == null || preference.getIntro5()) return;
        TapTargetView.showFor(getActivity(),
                TapTarget.forView(textViewWedding, "Give a wedding title")
                        .cancelable(false)
                        .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        preference.setIntro5(true);
                        showCaseChecklist();
                    }
                });
    }

    private void showCaseChecklist() {
        if (getActivity() == null || preference.getIntro8() || layoutChecklist.getVisibility() != View.VISIBLE)
            return;
        TapTargetView.showFor(getActivity(),
                TapTarget.forView(layoutChecklist, "Checklist will not let you miss anything important")
                        .tintTarget(false)
                        .cancelable(false)
                        .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        preference.setIntro8(true);
                        if (cardViewBudget.getVisibility() == View.VISIBLE) showCaseBudget();
                    }
                });
    }

    private void showCaseBudget() {
        if (preference.getIntro9()) return;
        int scrollTo = ((View) cardViewBudget.getParent().getParent().getParent().getParent()).getTop() + cardViewBudget.getBottom();
        scrollView.smoothScrollTo(0, scrollTo);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null)
                    TapTargetView.showFor(getActivity(),
                            TapTarget.forView(layoutBudget, "Plan your budget using wedding calculator")
                                    .tintTarget(false)
                                    .cancelable(false)
                                    .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                            new TapTargetView.Listener() {
                                @Override
                                public void onTargetClick(TapTargetView view) {
                                    super.onTargetClick(view);
                                    preference.setIntro9(true);
                                    if (layoutInvitation.getVisibility() == View.VISIBLE)
                                        showCaseInvite();
                                }
                            });
            }
        }, 500);
    }

    private void showCaseInvite() {
        if (preference.getIntro10() && preference.getIntro11()) return;
        int scrollTo = ((View) layoutInvitation.getParent().getParent().getParent().getParent()).getTop() + layoutInvitation.getBottom();
        scrollView.smoothScrollTo(0, scrollTo);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    new TapTargetSequence(getActivity())
                            .targets(TapTarget.forView(layoutInviteBride, "Invite Bride/Groom")
                                            .tintTarget(false)
                                            .cancelable(false)
                                            .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                                    TapTarget.forView(layoutInviteCollaborator, "Invite guests to let them know important information")
                                            .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf"))
                                            .tintTarget(false)
                                            .cancelable(false))
                            .listener(new TapTargetSequence.Listener() {
                                @Override
                                public void onSequenceFinish() {
                                    preference.setIntro10(true);
                                    preference.setIntro11(true);
                                }

                                @Override
                                public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                                }

                                @Override
                                public void onSequenceCanceled(TapTarget lastTarget) {

                                }
                            }).start();
                }
            }
        }, 500);
    }
}
