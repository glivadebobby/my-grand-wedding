package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 01/10/17
 */

public class MyWedding {

    @SerializedName("id")
    private Integer id;
    @SerializedName("bride")
    private Integer bride;
    @SerializedName("groom")
    private Integer groom;
    @SerializedName("created_by")
    private Integer createdBy;
    @SerializedName("is_bride")
    private boolean isBride;
    @SerializedName("title")
    private String title;
    @SerializedName("location")
    private String location;
    @SerializedName("date")
    private String date;

    public MyWedding(String title, String location, String date) {
        this.title = title;
        this.location = location;
        this.date = date;
    }

    public MyWedding(Integer createdBy, boolean isBride, String title, String location, String date) {
        this.createdBy = createdBy;
        this.isBride = isBride;
        this.title = title;
        this.location = location;
        this.date = date;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBride() {
        return bride;
    }

    public void setBride(Integer bride) {
        this.bride = bride;
    }

    public Integer getGroom() {
        return groom;
    }

    public void setGroom(Integer groom) {
        this.groom = groom;
    }

    public boolean isBride() {
        return isBride;
    }

    public void setBride(boolean bride) {
        isBride = bride;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
