package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.MyBudgetCallback;

/**
 * Created by gladwinbobby on 25/12/17
 */

public class MyBudgetCostHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewCategory, imageViewEdit;
    public TextView textViewCategory, textViewNoOf, textViewCount, textViewCostPer, textViewCost, textViewPrice;
    private MyBudgetCallback callback;

    public MyBudgetCostHolder(View itemView, MyBudgetCallback callback) {
        super(itemView);
        imageViewCategory = itemView.findViewById(R.id.img_category);
        imageViewEdit = itemView.findViewById(R.id.img_edit);
        textViewCategory = itemView.findViewById(R.id.txt_category);
        textViewNoOf = itemView.findViewById(R.id.txt_no_of);
        textViewCount = itemView.findViewById(R.id.txt_count);
        textViewCostPer = itemView.findViewById(R.id.txt_cost_per);
        textViewCost = itemView.findViewById(R.id.txt_cost);
        textViewPrice = itemView.findViewById(R.id.txt_price);
        this.callback = callback;
        imageViewEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imageViewEdit) {
            callback.onMyBudgetClick(getLayoutPosition());
        }
    }
}
