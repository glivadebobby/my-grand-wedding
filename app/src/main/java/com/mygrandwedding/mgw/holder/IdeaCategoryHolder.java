package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.CategoryCallback;

/**
 * Created by gladwinbobby on 05/09/17
 */

public class IdeaCategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewCategory;
    public TextView textViewCategory;
    private CategoryCallback callback;

    public IdeaCategoryHolder(View itemView, CategoryCallback callback) {
        super(itemView);
        imageViewCategory = itemView.findViewById(R.id.img_category);
        textViewCategory = itemView.findViewById(R.id.txt_category);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) callback.onCategoryClick(getLayoutPosition());
    }
}
