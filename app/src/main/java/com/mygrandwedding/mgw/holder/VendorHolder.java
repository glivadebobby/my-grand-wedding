package com.mygrandwedding.mgw.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intrusoft.squint.DiagonalView;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.callback.VendorCallback;

/**
 * Created by gladwinbobby on 06/09/17
 */

public class VendorHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView imageViewVendor;
    public TextView textViewVendor, textViewLocation, textViewCost, textViewRating, textViewReview;
    public DiagonalView diagonalView;
    public ImageView imageViewType;
    private VendorCallback callback;

    public VendorHolder(View itemView, VendorCallback callback) {
        super(itemView);
        imageViewVendor = itemView.findViewById(R.id.img_vendor);
        textViewVendor = itemView.findViewById(R.id.txt_vendor);
        textViewLocation = itemView.findViewById(R.id.txt_location);
        textViewCost = itemView.findViewById(R.id.txt_cost);
        textViewRating = itemView.findViewById(R.id.txt_rating);
        textViewReview = itemView.findViewById(R.id.txt_review);
        diagonalView = itemView.findViewById(R.id.diagonal);
        imageViewType = itemView.findViewById(R.id.img_type);
        this.callback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) callback.onVendorClick(getLayoutPosition());
    }
}
