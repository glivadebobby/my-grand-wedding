package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 19/12/17
 */

public class BudgetData {

    @SerializedName("wedding_venue")
    private MyBudget weddingVenue;
    @SerializedName("guest_room")
    private MyBudget guestRoom;
    @SerializedName("invitation_cards")
    private MyBudget invitationCards;
    @SerializedName("wedding_decorators")
    private MyBudget weddingDecorators;
    @SerializedName("photo_video")
    private MyBudget photoVideo;
    @SerializedName("pandit_purohit")
    private MyBudget panditPurohit;
    @SerializedName("makeup")
    private MyBudget makeup;
    @SerializedName("real_jewelry")
    private MyBudget realJewelry;
    @SerializedName("fashion_jewelry")
    private MyBudget fashionJewelry;
    @SerializedName("bridal_wear")
    private MyBudget bridalWear;
    @SerializedName("groom_wear")
    private MyBudget groomWear;
    @SerializedName("dj_music")
    private MyBudget djMusic;
    @SerializedName("transportation")
    private MyBudget transportation;
    @SerializedName("others")
    private MyBudget others;

    public MyBudget getWeddingVenue() {
        return weddingVenue;
    }

    public void setWeddingVenue(MyBudget weddingVenue) {
        this.weddingVenue = weddingVenue;
    }

    public MyBudget getGuestRoom() {
        return guestRoom;
    }

    public void setGuestRoom(MyBudget guestRoom) {
        this.guestRoom = guestRoom;
    }

    public MyBudget getInvitationCards() {
        return invitationCards;
    }

    public void setInvitationCards(MyBudget invitationCards) {
        this.invitationCards = invitationCards;
    }

    public MyBudget getWeddingDecorators() {
        return weddingDecorators;
    }

    public void setWeddingDecorators(MyBudget weddingDecorators) {
        this.weddingDecorators = weddingDecorators;
    }

    public MyBudget getPhotoVideo() {
        return photoVideo;
    }

    public void setPhotoVideo(MyBudget photoVideo) {
        this.photoVideo = photoVideo;
    }

    public MyBudget getPanditPurohit() {
        return panditPurohit;
    }

    public void setPanditPurohit(MyBudget panditPurohit) {
        this.panditPurohit = panditPurohit;
    }

    public MyBudget getMakeup() {
        return makeup;
    }

    public void setMakeup(MyBudget makeup) {
        this.makeup = makeup;
    }

    public MyBudget getRealJewelry() {
        return realJewelry;
    }

    public void setRealJewelry(MyBudget realJewelry) {
        this.realJewelry = realJewelry;
    }

    public MyBudget getFashionJewelry() {
        return fashionJewelry;
    }

    public void setFashionJewelry(MyBudget fashionJewelry) {
        this.fashionJewelry = fashionJewelry;
    }

    public MyBudget getBridalWear() {
        return bridalWear;
    }

    public void setBridalWear(MyBudget bridalWear) {
        this.bridalWear = bridalWear;
    }

    public MyBudget getGroomWear() {
        return groomWear;
    }

    public void setGroomWear(MyBudget groomWear) {
        this.groomWear = groomWear;
    }

    public MyBudget getDjMusic() {
        return djMusic;
    }

    public void setDjMusic(MyBudget djMusic) {
        this.djMusic = djMusic;
    }

    public MyBudget getTransportation() {
        return transportation;
    }

    public void setTransportation(MyBudget transportation) {
        this.transportation = transportation;
    }

    public MyBudget getOthers() {
        return others;
    }

    public void setOthers(MyBudget others) {
        this.others = others;
    }
}
