package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 11/09/17
 */

public interface TrendyDesignCallback {
    void onTrendyDesignClick(int position);
}
