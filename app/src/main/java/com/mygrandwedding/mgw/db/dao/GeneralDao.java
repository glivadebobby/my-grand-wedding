package com.mygrandwedding.mgw.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.mygrandwedding.mgw.model.BlogCategory;
import com.mygrandwedding.mgw.model.IdeaCategory;
import com.mygrandwedding.mgw.model.Location;
import com.mygrandwedding.mgw.model.VendorCategory;
import com.mygrandwedding.mgw.model.VendorSubcategory;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by gladwinbobby on 13/10/17
 */

@Dao
public interface GeneralDao {
    @Insert(onConflict = REPLACE)
    void insertLocationList(List<Location> locationList);

    @Insert(onConflict = REPLACE)
    void insertVendorCategoryList(List<VendorCategory> vendorCategoryList);

    @Insert(onConflict = REPLACE)
    void insertVendorSubcategoryList(List<VendorSubcategory> vendorSubcategoryList);

    @Insert(onConflict = REPLACE)
    void insertBlogCategoryList(List<BlogCategory> blogCategoryList);

    @Insert(onConflict = REPLACE)
    void insertIdeaCategoryList(List<IdeaCategory> ideaCategoryList);

    @Query("SELECT * FROM Location")
    LiveData<List<Location>> getLocations();

    @Query("SELECT * FROM VendorCategory ORDER BY sort")
    LiveData<List<VendorCategory>> getVendorCategories();

    @Query("SELECT * FROM VendorSubcategory WHERE category =:categoryId ORDER BY id")
    LiveData<List<VendorSubcategory>> getVendorSubcategories(int categoryId);

    @Query("SELECT * FROM VendorCategory WHERE id =:categoryId")
    VendorCategory getVendorCategory(int categoryId);

    @Query("SELECT * FROM BlogCategory")
    LiveData<List<BlogCategory>> getBlogCategories();

    @Query("SELECT * FROM IdeaCategory")
    LiveData<List<IdeaCategory>> getIdeaCategories();
}
