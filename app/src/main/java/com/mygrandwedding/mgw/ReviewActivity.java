package com.mygrandwedding.mgw;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.model.Review;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_RATING;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_REVIEW;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_VENDOR;

public class ReviewActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private Toolbar toolbar;
    private TextView textViewRate;
    private RatingBar ratingBar;
    private TextInputLayout layoutReview;
    private TextInputEditText editTextReview;
    private Button buttonSubmitReview;
    private FrameLayout layoutLoading;
    private MyPreference preference;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        initObjects();
        initToolbar();
        initCallbacks();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSubmitReview) {
            processReview();
        }
    }

    private void initObjects() {
        toolbar = findViewById(R.id.toolbar);
        textViewRate = findViewById(R.id.txt_rate);
        ratingBar = findViewById(R.id.rating);
        layoutReview = findViewById(R.id.review);
        editTextReview = findViewById(R.id.input_review);
        buttonSubmitReview = findViewById(R.id.btn_submit_review);
        layoutLoading = findViewById(R.id.loading);

        context = this;
        preference = new MyPreference(context);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCallbacks() {
        buttonSubmitReview.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getInt(EXTRA_ID);
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(String.format(Locale.getDefault(),
                        getString(R.string.format_review), bundle.getString(EXTRA_VENDOR)));
            textViewRate.setText(String.format(Locale.getDefault(),
                    getString(R.string.format_rate), bundle.getString(EXTRA_VENDOR)));
            ratingBar.setRating(bundle.getFloat(EXTRA_RATING));
            editTextReview.setText(bundle.getString(EXTRA_REVIEW));
            editTextReview.setSelection(editTextReview.getText().toString().trim().length());
        }
    }

    private void processReview() {
        float rating = ratingBar.getRating();
        String review = editTextReview.getText().toString().trim();
        if (review.isEmpty()) {
            layoutReview.setError(getString(R.string.error_empty_message));
        } else {
            setLoading(true);
            review(new Review(id, rating, review));
        }
    }

    private void review(Review review) {
        Call<Review> call = getApiService().review(preference.getUserToken(), review);
        call.enqueue(new Callback<Review>() {
            @Override
            public void onResponse(@NonNull Call<Review> call, @NonNull Response<Review> response) {
                setLoading(false);
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Review submitted", Toast.LENGTH_SHORT).show();
                    setResult(Activity.RESULT_OK, new Intent());
                    finish();
                } else processError(context, response.code(), response.errorBody());
            }

            @Override
            public void onFailure(@NonNull Call<Review> call, @NonNull Throwable t) {
                setLoading(false);
                processFailure(context, t);
            }
        });
    }

    private void setLoading(boolean loading) {
        layoutLoading.setVisibility(loading ? View.VISIBLE : View.GONE);
        buttonSubmitReview.setVisibility(loading ? View.GONE : View.VISIBLE);
    }
}
