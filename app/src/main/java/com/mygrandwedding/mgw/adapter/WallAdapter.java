package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.VendorCallback;
import com.mygrandwedding.mgw.holder.UserWeddingHolder;
import com.mygrandwedding.mgw.holder.WallLovedHolder;
import com.mygrandwedding.mgw.holder.WallShortlistedHolder;
import com.mygrandwedding.mgw.model.User;
import com.mygrandwedding.mgw.model.Vendor;
import com.mygrandwedding.mgw.model.Wall;

import java.util.List;
import java.util.Locale;

/**
 * Created by gladwinbobby on 23/10/17
 */

public class WallAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_LOVED = 1;
    private static final int TYPE_SHORTLISTED = 2;
    private static final int TYPE_CREATED = 3;
    private static final int TYPE_JOINED = 4;
    private static final int TYPE_LEFT = 5;
    private Context context;
    private List<Wall> wallList;
    private VendorCallback callback;

    public WallAdapter(Context context, List<Wall> wallList, VendorCallback callback) {
        this.context = context;
        this.wallList = wallList;
        this.callback = callback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_LOVED) {
            return new WallLovedHolder(inflater.inflate(R.layout.item_wall_loved, parent, false), callback);
        } else if (viewType == TYPE_SHORTLISTED) {
            return new WallShortlistedHolder(inflater.inflate(R.layout.item_wall_shortlisted, parent, false), callback);
        } else if (viewType == TYPE_CREATED || viewType == TYPE_JOINED || viewType == TYPE_LEFT) {
            return new UserWeddingHolder(inflater.inflate(R.layout.item_user_wedding, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_LOVED:
                bindLovedViewHolder((WallLovedHolder) holder, position);
                break;
            case TYPE_SHORTLISTED:
                bindShortlistedViewHolder((WallShortlistedHolder) holder, position);
                break;
            case TYPE_CREATED:
                bindUserWeddingViewHolder((UserWeddingHolder) holder, position);
                break;
            case TYPE_JOINED:
                bindUserWeddingViewHolder((UserWeddingHolder) holder, position);
                break;
            case TYPE_LEFT:
                bindUserWeddingViewHolder((UserWeddingHolder) holder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return wallList.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return wallList.size();
    }

    private void bindLovedViewHolder(WallLovedHolder holder, int position) {
        Wall wall = wallList.get(position);
        Vendor vendor = wall.getVendor();
        User user = wall.getUser();
        GlideApp.with(context)
                .load(user.getUserProfile().getDp())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewProfile);
        holder.textViewTitle.setText(String.format(Locale.getDefault(),
                context.getString(R.string.format_loved_vendor), user.getFirstName()));
        holder.textViewRole.setVisibility(wall.getRole() == null ? View.GONE : View.VISIBLE);
        holder.textViewRole.setText(wall.getRole());
        holder.textViewDate.setText(wall.getCreatedOn());
        GlideApp.with(context)
                .load(vendor.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewVendor);
        holder.textViewVendor.setText(vendor.getBusinessName());
        holder.textViewLocation.setText(vendor.getLocation().getLocation());
        if (vendor.getMinPrice() == 0)
            holder.textViewCost.setText(context.getString(R.string.txt_price_on_request));
        else
            holder.textViewCost.setText(String.format(Locale.getDefault(), context.getString(R.string.format_cost_onwards), vendor.getMinPrice()));
        holder.textViewRating.setText(vendor.getRating() == null ? context.getString(R.string.error_na) : String.valueOf(vendor.getRating()));
        holder.textViewReview.setText(context.getResources().getQuantityString(R.plurals.review, vendor.getReview(), vendor.getReview()));
        if (vendor.getVendorType() == 1) {
            holder.diagonalView.setVisibility(View.VISIBLE);
            holder.imageViewType.setVisibility(View.VISIBLE);
            holder.imageViewType.setImageResource(R.drawable.ic_featured);
        } else if (vendor.getVendorType() == 2) {
            holder.diagonalView.setVisibility(View.INVISIBLE);
            holder.imageViewType.setVisibility(View.INVISIBLE);
        } else if (vendor.getVendorType() == 3) {
            holder.diagonalView.setVisibility(View.VISIBLE);
            holder.imageViewType.setVisibility(View.VISIBLE);
            holder.imageViewType.setImageResource(R.drawable.ic_recommended);
        }
    }

    private void bindShortlistedViewHolder(WallShortlistedHolder holder, int position) {
        Wall wall = wallList.get(position);
        Vendor vendor = wall.getVendor();
        User user = wall.getUser();
        GlideApp.with(context)
                .load(user.getUserProfile().getDp())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewProfile);
        holder.textViewTitle.setText(String.format(Locale.getDefault(),
                context.getString(R.string.format_shortlisted_vendor), user.getFirstName()));
        holder.textViewRole.setVisibility(wall.getRole() == null ? View.GONE : View.VISIBLE);
        holder.textViewRole.setText(wall.getRole());
        holder.textViewDate.setText(wall.getCreatedOn());
        GlideApp.with(context)
                .load(vendor.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewVendor);
        holder.textViewVendor.setText(vendor.getBusinessName());
        holder.textViewLocation.setText(vendor.getLocation().getLocation());
        if (vendor.getMinPrice() == 0)
            holder.textViewCost.setText(context.getString(R.string.txt_price_on_request));
        else
            holder.textViewCost.setText(String.format(Locale.getDefault(), context.getString(R.string.format_cost_onwards), vendor.getMinPrice()));
        holder.textViewRating.setText(vendor.getRating() == null ? context.getString(R.string.error_na) : String.valueOf(vendor.getRating()));
        holder.textViewReview.setText(context.getResources().getQuantityString(R.plurals.review, vendor.getReview(), vendor.getReview()));
        if (vendor.getVendorType() == 1) {
            holder.diagonalView.setVisibility(View.VISIBLE);
            holder.imageViewType.setVisibility(View.VISIBLE);
            holder.imageViewType.setImageResource(R.drawable.ic_featured);
        } else if (vendor.getVendorType() == 2) {
            holder.diagonalView.setVisibility(View.INVISIBLE);
            holder.imageViewType.setVisibility(View.INVISIBLE);
        } else if (vendor.getVendorType() == 3) {
            holder.diagonalView.setVisibility(View.VISIBLE);
            holder.imageViewType.setVisibility(View.VISIBLE);
            holder.imageViewType.setImageResource(R.drawable.ic_recommended);
        }
    }

    private void bindUserWeddingViewHolder(UserWeddingHolder holder, int position) {
        Wall wall = wallList.get(position);
        User user = wall.getUser();
        GlideApp.with(context)
                .load(user.getUserProfile().getDp())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewProfile);
        if (wall.getType() == TYPE_CREATED) {
            holder.layoutUserWedding.setBackgroundResource(R.color.green);
            holder.textViewTitle.setText(String.format(Locale.getDefault(),
                    context.getString(R.string.format_created_wedding), user.getFirstName(), wall.getRole()));
        } else if (wall.getType() == TYPE_JOINED) {
            holder.layoutUserWedding.setBackgroundResource(R.color.yellow);
            holder.textViewTitle.setText(String.format(Locale.getDefault(),
                    context.getString(R.string.format_joined_wedding), user.getFirstName(), wall.getRole()));
        } else if (wall.getType() == TYPE_LEFT) {
            holder.layoutUserWedding.setBackgroundResource(R.color.red);
            holder.textViewTitle.setText(String.format(Locale.getDefault(),
                    context.getString(R.string.format_left_wedding), user.getFirstName(), wall.getRole()));
        }
        holder.textViewDate.setText(wall.getCreatedOn());
    }
}
