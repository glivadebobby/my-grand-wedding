package com.mygrandwedding.mgw.callback;

/**
 * Created by gladwinbobby on 30/09/17
 */

public interface LocationCallback {
    void onLocationClick(int position);
}
