package com.mygrandwedding.mgw.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.app.GlideApp;
import com.mygrandwedding.mgw.callback.BlogCallback;
import com.mygrandwedding.mgw.holder.BlogHolder;
import com.mygrandwedding.mgw.model.Blog;

import java.util.List;

/**
 * Created by gladwinbobby on 18/09/17
 */

public class BlogAdapter extends RecyclerView.Adapter<BlogHolder> {

    private Context context;
    private List<Blog> blogList;
    private BlogCallback callback;

    public BlogAdapter(Context context, List<Blog> blogList, BlogCallback callback) {
        this.context = context;
        this.blogList = blogList;
        this.callback = callback;
    }

    @Override
    public BlogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BlogHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_blog, parent, false), callback);
    }

    @Override
    public void onBindViewHolder(BlogHolder holder, int position) {
        Blog blog = blogList.get(position);
        GlideApp.with(context)
                .load(blog.getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.imageViewBlog);
        holder.textViewTitle.setText(blog.getTitle());
    }

    @Override
    public int getItemCount() {
        return blogList.size();
    }
}
