package com.mygrandwedding.mgw.api;

import android.content.Context;
import android.widget.Toast;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.SplashActivity;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.task.LogoutTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;

import static com.mygrandwedding.mgw.app.MyActivity.launchClearStack;

/**
 * Created by gladwinbobby on 25/09/17
 */

public class ErrorHandler {
    public static void processError(Context context, int statusCode, ResponseBody body) {
        MyPreference preference = new MyPreference(context);
        String message = null;
        try {
            JSONObject jsonObject = new JSONObject(body.string());
            if (jsonObject.has("non_field_errors")) {
                JSONArray jsonArray = jsonObject.getJSONArray("non_field_errors");
                message = jsonArray.getString(0);
            } else if (jsonObject.has("detail")) {
                message = jsonObject.getString("detail");
            } else {
                for (int i = 0; i < jsonObject.length(); i++) {
                    JSONArray keyArray = new JSONArray(
                            jsonObject.getString(jsonObject.names().getString(i)));
                    message = keyArray.getString(0);
                }
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

        switch (statusCode) {
            case 400:
                if (message != null) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
                break;
            case 401:
                if (message != null) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
                new LogoutTask(context).execute();
                break;
            case 403:
                if (message != null) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
                preference.clearUser();
                launchClearStack(context, SplashActivity.class);
                break;
            case 404:
                if (message != null) {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                }
                break;
            case 500:
                Toast.makeText(context, context.getString(R.string.error_500), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(context, context.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
