package com.mygrandwedding.mgw.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 24/09/17
 */

public class Love {

    @SerializedName("id")
    private Integer id;
    @SerializedName("vendor")
    private Integer vendor;
    @SerializedName("wedding")
    private Integer wedding;

    public Love(Integer vendor, Integer wedding) {
        this.vendor = vendor;
        this.wedding = wedding;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVendor() {
        return vendor;
    }

    public void setVendor(Integer vendor) {
        this.vendor = vendor;
    }

    public Integer getWedding() {
        return wedding;
    }

    public void setWedding(Integer wedding) {
        this.wedding = wedding;
    }
}
