package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;
import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.adapter.MyPagerAdapter;
import com.mygrandwedding.mgw.app.MyPreference;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspirationFragment extends Fragment implements TabLayout.OnTabSelectedListener {

    private Context context;
    private View rootView;
    private TabLayout tabLayout;
    private ViewPager pagerInspiration;
    private List<Fragment> fragmentList;
    private MyPagerAdapter pagerAdapter;
    private MyPreference preference;

    public InspirationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_inspiration, container, false);
        initObjects();
        initCallbacks();
        initViewPager();
        initTabs();
        setTabsFont();
        showCase();
        return rootView;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        pagerInspiration.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void initObjects() {
        tabLayout = rootView.findViewById(R.id.tab);
        pagerInspiration = rootView.findViewById(R.id.inspiration);

        context = getActivity();
        fragmentList = new ArrayList<>();
        pagerAdapter = new MyPagerAdapter(getChildFragmentManager(), fragmentList);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        tabLayout.addOnTabSelectedListener(this);
    }

    private void initViewPager() {
        pagerInspiration.setAdapter(pagerAdapter);
        pagerInspiration.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void initTabs() {
        tabLayout.addTab(tabLayout.newTab().setText("Ideas"));
        fragmentList.add(new IdeaCategoryFragment());
        tabLayout.addTab(tabLayout.newTab().setText("Trendy Designs"));
        fragmentList.add(new TrendyDesignCategoryFragment());
        tabLayout.addTab(tabLayout.newTab().setText("Blog"));
        fragmentList.add(new BlogCategoryFragment());
        pagerAdapter.notifyDataSetChanged();
        pagerInspiration.setOffscreenPageLimit(fragmentList.size());
    }

    private void setTabsFont() {
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typeface.createFromAsset(context.getAssets(),
                            "fonts/SourceSansPro-SemiBold.ttf"), Typeface.NORMAL);
                    ((TextView) tabViewChild).setTextSize(getResources().getDimension(R.dimen.primary_txt));
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    private void showCase() {
        if (getActivity() == null || (preference.getIntro12() && preference.getIntro13() && preference.getIntro14()))
            return;
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        new TapTargetSequence(getActivity())
                .targets(TapTarget.forView(vg.getChildAt(0), "Get trendy ideas")
                                .cancelable(false)
                                .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf")),
                        TapTarget.forView(vg.getChildAt(1), "View trendy designs")
                                .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf"))
                                .cancelable(false),
                        TapTarget.forView(vg.getChildAt(2), "Read articles to get an idea")
                                .textTypeface(Typeface.createFromAsset(getResources().getAssets(), "fonts/SourceSansPro-SemiBold.ttf"))
                                .cancelable(false))
                .listener(new TapTargetSequence.Listener() {
                    @Override
                    public void onSequenceFinish() {
                        preference.setIntro12(true);
                        preference.setIntro13(true);
                        preference.setIntro14(true);
                    }

                    @Override
                    public void onSequenceStep(TapTarget lastTarget, boolean targetClicked) {

                    }

                    @Override
                    public void onSequenceCanceled(TapTarget lastTarget) {

                    }
                }).start();
    }
}
