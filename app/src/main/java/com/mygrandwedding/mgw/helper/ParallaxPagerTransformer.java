package com.mygrandwedding.mgw.helper;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by gladwinbobby on 03/09/17
 */

public class ParallaxPagerTransformer implements ViewPager.PageTransformer {

    private int id;

    public ParallaxPagerTransformer(int id) {
        this.id = id;
    }

    @Override
    public void transformPage(View view, float position) {
        View parallaxView = view.findViewById(id);
        if (position > -1 && position < 1) {
            float width = parallaxView.getWidth();
            parallaxView.setTranslationX(-(position * width * 0.6f));
            float sc = (float) view.getWidth() / view.getWidth();
            if (position == 0) {
                view.setScaleX(1);
                view.setScaleY(1);
            } else {
                view.setScaleX(sc);
                view.setScaleY(sc);
            }
        }
    }
}
