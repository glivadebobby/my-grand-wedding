package com.mygrandwedding.mgw.model;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by gladwinbobby on 12/09/17
 */

@Entity
public class User {

    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("has_password")
    private boolean hasPassword;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("username")
    private String username;
    @SerializedName("email")
    private String email;
    @SerializedName("token")
    private String token;
    @Embedded(prefix = "user_profile")
    @SerializedName("userprofile")
    private UserProfile userProfile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isHasPassword() {
        return hasPassword;
    }

    public void setHasPassword(boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }
}
