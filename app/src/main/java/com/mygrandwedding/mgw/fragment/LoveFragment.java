package com.mygrandwedding.mgw.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mygrandwedding.mgw.R;
import com.mygrandwedding.mgw.VendorDetailActivity;
import com.mygrandwedding.mgw.adapter.MyLoveAdapter;
import com.mygrandwedding.mgw.app.MyPreference;
import com.mygrandwedding.mgw.callback.LovedCallback;
import com.mygrandwedding.mgw.model.Love;
import com.mygrandwedding.mgw.model.Vendor;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mygrandwedding.mgw.api.ApiClient.getApiService;
import static com.mygrandwedding.mgw.api.ErrorHandler.processError;
import static com.mygrandwedding.mgw.api.FailureHandler.processFailure;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_ID;
import static com.mygrandwedding.mgw.app.Constant.EXTRA_IMAGE;
import static com.mygrandwedding.mgw.app.MyActivity.launchWithBundle;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoveFragment extends Fragment implements LovedCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context context;
    private View rootView;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView viewVendors;
    private List<Vendor> vendorList;
    private LinearLayoutManager layoutManager;
    private MyLoveAdapter vendorAdapter;
    private MyPreference preference;

    public LoveFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_love, container, false);
        initObjects();
        initCallbacks();
        initRecyclerView();
        initRefresh();
        return rootView;
    }

    @Override
    public void onVendorClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, vendorList.get(position).getUser());
        bundle.putString(EXTRA_IMAGE, vendorList.get(position).getImage());
        launchWithBundle(context, VendorDetailActivity.class, bundle);
    }

    @Override
    public void onRemoveClick(int position) {
        removeLove(vendorList.get(position).getId());
    }

    @Override
    public void onRefresh() {
        getLoves();
    }

    @Override
    public void run() {
        refreshLayout.setRefreshing(true);
        getLoves();
    }

    private void initObjects() {
        refreshLayout = rootView.findViewById(R.id.refresh);
        viewVendors = rootView.findViewById(R.id.vendors);

        context = getActivity();
        vendorList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(context);
        vendorAdapter = new MyLoveAdapter(context, vendorList, this);
        preference = new MyPreference(context);
    }

    private void initCallbacks() {
        refreshLayout.setOnRefreshListener(this);
    }

    private void initRecyclerView() {
        viewVendors.setLayoutManager(layoutManager);
        viewVendors.setAdapter(vendorAdapter);
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        refreshLayout.post(this);
    }

    private void getLoves() {
        Call<List<Vendor>> call = getApiService().getMyLove(preference.getUserToken(), preference.getWeddingId());
        call.enqueue(new Callback<List<Vendor>>() {
            @Override
            public void onResponse(@NonNull Call<List<Vendor>> call,
                                   @NonNull Response<List<Vendor>> response) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    List<Vendor> vendorListResponse = response.body();
                    if (response.isSuccessful() && vendorListResponse != null) {
                        vendorList.clear();
                        vendorList.addAll(vendorListResponse);
                        vendorAdapter.notifyDataSetChanged();
                    } else processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Vendor>> call, @NonNull Throwable t) {
                if (isAdded()) {
                    refreshLayout.setRefreshing(false);
                    processFailure(context, t);
                }
            }
        });
    }

    private void removeLove(int id) {
        Call<Love> call = getApiService().love(preference.getUserToken(), new Love(id, preference.getWeddingId()));
        call.enqueue(new Callback<Love>() {
            @Override
            public void onResponse(@NonNull Call<Love> call, @NonNull Response<Love> response) {
                if (isAdded()) {
                    Love loveResponse = response.body();
                    if (response.isSuccessful() && loveResponse != null) {
                        run();
                    } else processError(context, response.code(), response.errorBody());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Love> call, @NonNull Throwable t) {
                if (isAdded()) {
                    processFailure(context, t);
                }
            }
        });
    }
}
