package com.mygrandwedding.mgw.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.mygrandwedding.mgw.model.VendorCategory;

import java.util.List;

import static com.mygrandwedding.mgw.db.AppDatabase.getAppDatabase;

/**
 * Created by gladwinbobby on 20/10/17
 */

public class VendorCategoryViewModel extends ViewModel {

    private LiveData<List<VendorCategory>> data;

    public LiveData<List<VendorCategory>> getData(Context context) {
        if (data == null) {
            data = getAppDatabase(context).generalDao().getVendorCategories();
        }
        return data;
    }
}
