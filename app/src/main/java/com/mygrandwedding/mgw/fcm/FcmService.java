package com.mygrandwedding.mgw.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mygrandwedding.mgw.app.MyPreference;

/**
 * Created by gladwinbobby on 22/10/17
 */

public class FcmService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        storeToken(token);
    }

    private void storeToken(String token) {
        MyPreference preference = new MyPreference(this);
        preference.setFcmToken(token);
        preference.setTokenUploaded(false);
    }
}
